/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.transform.utils;

import iv3d.base.beanfactory.BeanFactory;
import iv3d.base.exceptions.FactoryException;
import iv3d.base.exceptions.TransformException;
import iv3d.base.transform.utils.policies.ConstructorTransformPolicy;
import iv3d.base.transform.utils.policies.MethodTransformPolicy;
import iv3d.base.transform.utils.policies.NumberTransformPolicy;
import iv3d.base.transform.utils.policies.StaticMethodTransformPolicy;
import iv3d.base.transform.utils.policies.TransformerBeanLookupTransformPolicy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Utility class for doing object conversions. 
 * 
 * @author Fredrique Samuels
 */
public final class TransformUtil {
	
	private final List<String> searchPackages;
	private final List<TransformPolicy> policies;
	private static TransformUtil instance;
	
	/**
	 * Policy that creates a new object via a single argument constructor.
	 */
	public static String CONSTRUCTOR_POLICY  = ConstructorTransformPolicy.class.getCanonicalName();
	
	/**
	 * <p>
	 * Transform policy that create a new object using a zero argument method and returns
	 * the transformer object.
	 * </p>
	 * <code>Integer i = new Float().intValue()</code>
	 *    
	 */
	public static String METHOD_POLICY  = MethodTransformPolicy.class.getCanonicalName();
	
	/**
	 * <p>
	 * Transform policy that create a new object using a single argument static method and returns
	 * the transformer object.
	 * </p>
	 * <code>Integer i = Float.valueof(5)</code>   
	 */
	public static String STATIC_METHOD_POLICY = StaticMethodTransformPolicy.class.getCanonicalName();
	
	/**
	 * A policy that tries to a load an specialized bean object from a class name
	 * returned by the {@link TransformerNameResolver}.
	 */
	public static String TRANSFORMER_BEAN_LOOKUP_POLICY  = TransformerBeanLookupTransformPolicy.class.getCanonicalName();
	public static String NUMBER_POLICY  = NumberTransformPolicy.class.getCanonicalName();
	
	private TransformUtil() {
		policies = new LinkedList<TransformPolicy>();
		searchPackages = new ArrayList<String>();
	}
	
	private static TransformUtil getInstance() {
		if(instance==null) {
			instance = new TransformUtil();
		}
		return instance;
	}
	
	public static Object tranform(String fromClass, String toClass, Object value) throws TransformException {
		if(fromClass.equals(toClass)) { 
			return value;
		}
		
		ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
		
		Class<?> from;
		try {
			from = systemClassLoader.loadClass(fromClass);
		} catch (ClassNotFoundException e) {
			throw new TransformException(e);
		}
		
		Class<?> to;
		try {
			to = systemClassLoader.loadClass(toClass);
		} catch (ClassNotFoundException e) {
			throw new TransformException(e);
		}
		
		return tranform(from, to, value);
	}
	
	public static Object tranform(Class<?> fromClass, Class<?> toClass, Object value) throws TransformException {
		if(value==null) { 
			return null;
		}
		
		if(fromClass.equals(toClass) || fromClass==toClass) { 
			return value;
		}

		List<TransformPolicy> policies = getInstance().getTransformPolicies();
		
		for (TransformPolicy t : policies) {
			if(t.canTransform(fromClass, toClass)) {
				return t.transform(fromClass, toClass, value);
			}
		}

		String message = "Unable to transform object from type %s to %s";
		throw new TransformException(String.format(message, fromClass.getCanonicalName(), toClass.getCanonicalName()));
	}

	private List<TransformPolicy> getTransformPolicies() {
		return policies;
	}
	
	private static void addTransformStrategy(TransformPolicy s) {
		if(s==null) {
			return;
		}
		getInstance().addStrategy(s);
	}
	
	public static synchronized void addTransformStrategy(String clazz) throws FactoryException {
		if(clazz==null) {
			return;
		}
		addTransformStrategy((TransformPolicy) BeanFactory.newBean(clazz, TransformPolicy.class));
	}
	
	public static synchronized void addTransformStrategy(String strategyClazz, String nameResolverClazz) throws FactoryException {
		if(strategyClazz==null) {
			return;
		}
		if(nameResolverClazz==null) {
			return;
		}
		addTransformPolicy((TransformPolicy) BeanFactory.newBean(strategyClazz, TransformPolicy.class),
				(TransformerNameResolver) BeanFactory.newBean(nameResolverClazz, TransformerNameResolver.class));
	}
	
	private static void addTransformPolicy(TransformPolicy policy,
			TransformerNameResolver nameResolver) {
		if(policy instanceof HasNameResolverContext) {
			((HasNameResolverContext)policy).setNameResolver(nameResolver);
		}
		addTransformStrategy(policy);
	}

	private void addStrategy(TransformPolicy s) {
		if(s instanceof HasSearchPackageContext) {
			String[] array = getPackageNames();
			((HasSearchPackageContext)s).setSearchPackages(array);
		}
		policies.add(0, s);
	}
	
	public static synchronized String transformToString(Object obj) {
		return obj.toString();
	}
	
	public static synchronized void addPackage(String packageName) { 
		getInstance().addPackageToSearchPath(packageName);
	}

	private void addPackageToSearchPath(String packageName) {
		searchPackages.add(0, packageName);
		String[] array = getPackageNames();
		for(TransformPolicy p : getInstance().getTransformPolicies()) {
			if( p instanceof HasSearchPackageContext) {
				((HasSearchPackageContext)p).setSearchPackages(array);
			}
		}
	}

	private String[] getPackageNames() {
		String[] array = searchPackages.toArray(new String[]{});
		return array;
	}
	
}
