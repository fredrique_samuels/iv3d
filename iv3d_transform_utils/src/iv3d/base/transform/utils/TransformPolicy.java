/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.transform.utils;

import iv3d.base.exceptions.TransformException;

/**
 * Public interface for any object that can transform one object into a new 
 * instance of another type.
 * 
 * @author Fredrique Samuels
 */
public interface TransformPolicy {
	
	/**
	 * 
	 * @param from The starting data type.
	 * @param to The target data type.
	 * @return <code>true</code> if this policy can transform the <code>from</code> type to the <code>to</code> type.
	 */
	boolean canTransform(Class<?> from, Class<?> to);
	
	/**
	 * Transform one object to a new instance ot another object,
	 * 
	 * @param from The starting data type.
	 * @param to The target data type.
	 * @param value The resulting object.
	 * @return The resulting object.
	 * @throws TransformException If the object cannot be transformed.
	 */
	Object transform(Class<?> from, Class<?> to, Object value) throws TransformException;
	
}
