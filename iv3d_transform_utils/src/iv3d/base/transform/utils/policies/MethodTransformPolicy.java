package iv3d.base.transform.utils.policies;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import iv3d.base.exceptions.TransformException;
import iv3d.base.transform.utils.HasNameResolverContext;
import iv3d.base.transform.utils.TransformPolicy;
import iv3d.base.transform.utils.TransformerNameResolver;
import iv3d.base.transform.utils.resolvers.DefaultMethodNameResolver;

public class MethodTransformPolicy implements TransformPolicy, HasNameResolverContext {

	private TransformerNameResolver resolver;
	
	public MethodTransformPolicy() {
		resolver = new DefaultMethodNameResolver();
	}

	@Override
	public boolean canTransform(Class<?> from, Class<?> to) {
		try {
			getTransformMethod(from, to);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	private Method getTransformMethod(Class<?> from, Class<?> to) throws SecurityException, NoSuchMethodException {
		String methodName = resolver.getBeanName(from, to);
		return from.getMethod(methodName);
	}

	@Override
	public void setNameResolver(TransformerNameResolver resolver) {
		this.resolver = resolver;
	}

	@Override
	public Object transform(Class<?> from, Class<?> to, Object value)
			throws TransformException {
		try {
			Method transformMethod = getTransformMethod(from, to);
			Object result = transformMethod.invoke(value);
			return to.cast(result);
		} catch (SecurityException e) {
			throw new TransformException(e);
		} catch (NoSuchMethodException e) {
			throw new TransformException(e);
		} catch (IllegalArgumentException e) {
			throw new TransformException(e);
		} catch (IllegalAccessException e) {
			throw new TransformException(e);
		} catch (InvocationTargetException e) {
			throw new TransformException(e);
		} catch (Exception e) {
			throw new TransformException(e);
		}
	}
}
