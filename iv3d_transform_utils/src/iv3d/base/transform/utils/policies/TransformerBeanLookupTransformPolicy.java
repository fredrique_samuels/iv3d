package iv3d.base.transform.utils.policies;

import iv3d.base.beanfactory.BeanFactory;
import iv3d.base.exceptions.FactoryException;
import iv3d.base.exceptions.TransformException;
import iv3d.base.transform.utils.HasNameResolverContext;
import iv3d.base.transform.utils.HasSearchPackageContext;
import iv3d.base.transform.utils.TransformPolicy;
import iv3d.base.transform.utils.Transformer;
import iv3d.base.transform.utils.TransformerNameResolver;
import iv3d.base.transform.utils.resolvers.DefaultTransformBeanNameResolver;

public class TransformerBeanLookupTransformPolicy implements TransformPolicy, HasNameResolverContext, HasSearchPackageContext { 
	
	private TransformerNameResolver beanNameResolver;
	private String[] packages;
	
	public TransformerBeanLookupTransformPolicy() {
		setNameResolver(new DefaultTransformBeanNameResolver());
	}

	@Override
	public boolean canTransform(Class<?> from, Class<?> to) {
		String beanName = beanNameResolver.getBeanName(from, to);
		return getTransformerBean(beanName) != null;
	}
	
	public Transformer getTransformerBean(String beanName) {
		for(String p : packages) {
			try {
				return (Transformer) BeanFactory.newBean(p+"."+beanName, Transformer.class);
			} catch (FactoryException e) {
				//ignore
			} 
		}
		return null;
	}

	@Override
	public void setNameResolver(TransformerNameResolver resolver) {
		this.beanNameResolver = resolver;
	}

	@Override
	public Object transform(Class<?> from, Class<?> to, Object value)
			throws TransformException {
		
		String beanName = beanNameResolver.getBeanName(from, to);
		Transformer transformer;
		
		try {
			transformer =  getTransformerBean(beanName);
		} catch (Exception e) {
			throw new TransformException(e);
		}
		
		Object fromValue;
		try {
			fromValue = from.cast(value);
		} catch (Exception e) {
			throw new TransformException(e, "Unable to cast target object to given type.");
		}
		
		Object toValue;
		try {
			toValue = transformer.transform(fromValue);
		} catch (Exception e) {
			throw new TransformException(e);
		}
		
		try {
			return to.cast(toValue);
		} catch (Exception e) {
			throw new TransformException(e);
		}
	}

	@Override
	public void setSearchPackages(String[] packages) {
		this.packages = packages;
	}

}
