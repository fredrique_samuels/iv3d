package iv3d.base.transform.utils.policies;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import iv3d.base.exceptions.TransformException;
import iv3d.base.transform.utils.HasNameResolverContext;
import iv3d.base.transform.utils.TransformPolicy;
import iv3d.base.transform.utils.TransformerNameResolver;
import iv3d.base.transform.utils.resolvers.DefaultStaticMethodNameResolver;

public class StaticMethodTransformPolicy implements TransformPolicy, HasNameResolverContext {

	private TransformerNameResolver resolver;
	
	public StaticMethodTransformPolicy() {
		setNameResolver(new DefaultStaticMethodNameResolver());
	}

	@Override
	public boolean canTransform(Class<?> from, Class<?> to) {
		String methodName = resolver.getBeanName(from, to);
		try {
			to.getMethod(methodName, from);
		} catch (SecurityException e) {
			return false;
		} catch (NoSuchMethodException e) {
			return false;
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public void setNameResolver(TransformerNameResolver resolver) {
		this.resolver = resolver;
	}

	@Override
	public Object transform(Class<?> from, Class<?> to, Object value)
			throws TransformException {
		String methodName = resolver.getBeanName(from, to);
		try {
			Method method = to.getMethod(methodName, from);
			return method.invoke(null, from.cast(value));
		} catch (SecurityException e) {
			throw new TransformException(e);
		} catch (NoSuchMethodException e) {
			throw new TransformException(e);
		} catch (IllegalArgumentException e) {
			throw new TransformException(e);
		} catch (IllegalAccessException e) {
			throw new TransformException(e);
		} catch (InvocationTargetException e) {
			throw new TransformException(e);
		} catch (Exception e) {
			throw new TransformException(e);
		}
	}

}
