package iv3d.base.transform.utils.policies;

import iv3d.base.exceptions.TransformException;
import iv3d.base.transform.utils.TransformPolicy;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ConstructorTransformPolicy implements TransformPolicy {
	
	@Override
	public boolean canTransform(Class<?> from, Class<?> to) {
		try {
			to.getConstructor(from);
			return true;
		} catch (SecurityException e) {
			return false;
		} catch (NoSuchMethodException e) {
			return false;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public Object transform(Class<?> from, Class<?> to, Object value)
			throws TransformException {
		Constructor<?> constructor;
		try {
			constructor = to.getConstructor(from);
		} catch (SecurityException e) {
			throw new TransformException(e);
		} catch (NoSuchMethodException e) {
			throw new TransformException(e);
		} catch (Exception e) {
			throw new TransformException(e);
		}
		
		try {
			return constructor.newInstance(value);
		} catch (IllegalArgumentException e) {
			throw new TransformException(e);
		} catch (InstantiationException e) {
			throw new TransformException(e);
		} catch (IllegalAccessException e) {
			throw new TransformException(e);
		} catch (InvocationTargetException e) {
			throw new TransformException(e);
		} catch (Exception e) {
			throw new TransformException(e);
		}
	}

}
