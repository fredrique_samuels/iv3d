package iv3d.base.transform.utils.policies;

import iv3d.base.exceptions.TransformException;
import iv3d.base.transform.utils.TransformPolicy;

public class NumberTransformPolicy implements TransformPolicy {

	@Override
	public boolean canTransform(Class<?> from, Class<?> to) {
		if(!isNumber(from)) {
			return false;
		}
		
		return isNumber(to);
	}

	private boolean isNumber(Class<?> type) {
		Class<?> superType = type;
		do {
			if(superType==Number.class) {
				return true;
			}
			superType = superType.getSuperclass();
		} while (superType!=null);
		
		return false;
	}

	@Override
	public Object transform(Class<?> from, Class<?> to, Object value)
			throws TransformException {
		
		Number n=null;
		try {
			n = Number.class.cast(value);
		} catch (Exception e) {
			throw new TransformException(e);
		}
		
		if(to==Integer.class) {
			return new Integer(n.intValue());
		} else if(to==Float.class) {
			return new Float(n.floatValue());
		} else if(to==Double.class) {
			return new Double(n.doubleValue());
		} else if(to==Short.class) {
			return new Short(n.shortValue());
		} else if(to==Byte.class) {
			return new Byte(n.byteValue());
		} else if(to==Long.class) {
			return new Long(n.longValue());
		}

		String message = "Unable to transform the value %s to type %s";
		throw new TransformException(String.format(message, value.toString(), to.getName()));
	}

}
