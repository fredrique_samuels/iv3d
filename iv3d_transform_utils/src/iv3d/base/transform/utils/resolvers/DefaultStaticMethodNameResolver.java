package iv3d.base.transform.utils.resolvers;

import iv3d.base.transform.utils.TransformerNameResolver;

public class DefaultStaticMethodNameResolver implements TransformerNameResolver {

	@Override
	public String getBeanName(Class<?> from, Class<?> to) {
		return "valueOf";
	}
}
