package iv3d.base.transform.utils.resolvers;

import iv3d.base.transform.utils.TransformerNameResolver;

public class DefaultMethodNameResolver implements TransformerNameResolver {

	@Override
	public String getBeanName(Class<?> from, Class<?> to) {
		String packageName = to.getPackage().getName();
		return String.format("to%s", packageName.isEmpty() ? to.getName() :
			to.getName().replace(packageName+".", ""));
	}
}
