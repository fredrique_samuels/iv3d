package iv3d.base.transform.utils.resolvers;

import iv3d.base.transform.utils.TransformerNameResolver;

public class DefaultTransformBeanNameResolver implements TransformerNameResolver {

	@Override
	public String getBeanName(Class<?> from, Class<?> to) {
		String fromName = getFormattedClassName(from);
		String toName = getFormattedClassName(to);
		return String.format("%sTo%sTransformer", fromName, toName);
	}

	private String getFormattedClassName(Class<?> from) {
		String canonicalName = from.getCanonicalName();
		String[] split = canonicalName.replace(".", " ").split(" ");
		
		StringBuilder builder = new StringBuilder();
		for(String s: split) {
			builder.append(s.substring(0, 1).toUpperCase());
			builder.append(s.substring(1));
		}
		
		return builder.toString();
	}
}
