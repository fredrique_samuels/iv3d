package iv3d.base.transform.utils;

import iv3d.base.exceptions.FactoryException;
import iv3d.base.exceptions.TransformException;
import junit.framework.TestCase;

public class TestStandardTransforms extends TestCase {

	String demoString = "33";
	Double demoDouble = 33.3;
	Float demoFloat = 33.3f;
	Integer demoInt = 6;
	Long demoLong = 6L;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();

		TransformUtil.addTransformStrategy(TransformUtil.TRANSFORMER_BEAN_LOOKUP_POLICY);
		TransformUtil.addTransformStrategy(TransformUtil.NUMBER_POLICY);
		TransformUtil.addTransformStrategy(TransformUtil.CONSTRUCTOR_POLICY);
		TransformUtil.addTransformStrategy(TransformUtil.STATIC_METHOD_POLICY);
		TransformUtil.addTransformStrategy(TransformUtil.METHOD_POLICY);
		
		TransformUtil.addPackage("iv3d.base.transform.utils.transformers");
	}
	
	public void testToString() throws FactoryException, TransformException {
		TransformUtil.tranform(Double.class, String.class, demoDouble);
		TransformUtil.tranform(Float.class, String.class, demoFloat);
		TransformUtil.tranform(Integer.class, String.class, demoInt);
		TransformUtil.tranform(String.class, String.class, demoString);
		TransformUtil.tranform(Long.class, String.class, demoLong);
	}
	
	public void testToDouble() throws FactoryException, TransformException {
		TransformUtil.tranform(Double.class, Double.class, demoDouble);
		TransformUtil.tranform(Float.class, Double.class, demoFloat);
		TransformUtil.tranform(Integer.class, Double.class, demoInt);
		TransformUtil.tranform(String.class, Double.class, demoString);
		TransformUtil.tranform(Long.class, Double.class, demoLong);
	}
	
	public void testToFloat() throws FactoryException, TransformException {
		TransformUtil.tranform(Double.class, Float.class, demoDouble);
		TransformUtil.tranform(Float.class, Float.class, demoFloat);
		TransformUtil.tranform(Integer.class, Float.class, demoInt);
		TransformUtil.tranform(String.class, Float.class, demoString);
		TransformUtil.tranform(Long.class, Float.class, demoLong);
	}
	
	public void testToInteger() throws FactoryException, TransformException {
		TransformUtil.tranform(Double.class, Integer.class, demoDouble);
		TransformUtil.tranform(Float.class, Integer.class, demoFloat);
		TransformUtil.tranform(Integer.class, Integer.class, demoInt);
		TransformUtil.tranform(String.class, Integer.class, demoString);
		TransformUtil.tranform(Long.class, Integer.class, demoLong);
	}
	
	public void testToLong() throws FactoryException, TransformException {
		TransformUtil.tranform(Double.class, Long.class, demoDouble);
		TransformUtil.tranform(Float.class, Long.class, demoFloat);
		TransformUtil.tranform(Integer.class, Long.class, demoInt);
		TransformUtil.tranform(String.class, Long.class, demoString);
		TransformUtil.tranform(Long.class, Long.class, demoLong);
	}
}
