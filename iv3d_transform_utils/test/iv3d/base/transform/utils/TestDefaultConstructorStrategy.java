package iv3d.base.transform.utils;

import iv3d.base.exceptions.FactoryException;
import iv3d.base.exceptions.TransformException;
import iv3d.base.transform.utils.language.Greeting;
import iv3d.base.transform.utils.language.greetings.EnglishGreeting;
import iv3d.base.transform.utils.language.greetings.GermanGreeting;
import junit.framework.TestCase;

public class TestDefaultConstructorStrategy extends TestCase {
	public void testTransform() throws FactoryException, TransformException {
		TransformUtil.addTransformStrategy(TransformUtil.CONSTRUCTOR_POLICY);
		
		EnglishGreeting phrase1 = new EnglishGreeting();
		TransformUtil.tranform(Greeting.class, GermanGreeting.class, phrase1);
	}
	
	public void testTransformFail() throws FactoryException, TransformException {
		TransformUtil.addTransformStrategy(TransformUtil.CONSTRUCTOR_POLICY);
		
		EnglishGreeting phrase1 = new EnglishGreeting();
		try {
			TransformUtil.tranform(EnglishGreeting.class, GermanGreeting.class, phrase1);
			fail("No exception thrown");
		}catch (Exception e) {
		}
	}
}
