package iv3d.base.transform.utils;

import iv3d.base.exceptions.FactoryException;
import iv3d.base.exceptions.TransformException;
import junit.framework.TestCase;

public class TestMethodTransform extends TestCase {
	public void testTransform() throws FactoryException, TransformException {
		TransformUtil.addTransformStrategy(TransformUtil.METHOD_POLICY);
		
		Double d = new Double(5.0);
		String value = (String) TransformUtil.tranform(Double.class, String.class, d);
		assertEquals("5.0", value);
	}
}
