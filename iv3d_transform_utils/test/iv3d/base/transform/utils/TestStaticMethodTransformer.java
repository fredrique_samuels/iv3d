package iv3d.base.transform.utils;

import iv3d.base.exceptions.FactoryException;
import iv3d.base.exceptions.TransformException;
import iv3d.base.transform.utils.language.greetings.EnglishGreeting;
import iv3d.base.transform.utils.language.greetings.SpanishGreeting;
import junit.framework.TestCase;

public class TestStaticMethodTransformer extends TestCase {
	public void testTransform() throws FactoryException, TransformException {
		TransformUtil.addTransformStrategy(TransformUtil.STATIC_METHOD_POLICY);
		
		EnglishGreeting phrase1 = new EnglishGreeting();
		TransformUtil.tranform(EnglishGreeting.class, SpanishGreeting.class, phrase1);
	}
}
