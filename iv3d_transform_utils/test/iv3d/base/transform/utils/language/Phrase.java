package iv3d.base.transform.utils.language;

public interface Phrase {
	String get();
}
