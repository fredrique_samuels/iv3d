package iv3d.base.transform.utils.language.greetings;

import iv3d.base.transform.utils.language.Greeting;

public class EnglishGreeting implements Greeting {
	
	public EnglishGreeting() {
	}
	
	@Override
	public String get() {
		return "Hello";
	}
}
