package iv3d.base.transform.utils.language.greetings;

import iv3d.base.transform.utils.language.Greeting;

public class SpanishGreeting implements Greeting {

	@Override
	public String get() {
		return "Hola";
	}
	
	public static SpanishGreeting valueOf(EnglishGreeting e) {
		return new SpanishGreeting();
	}
}
