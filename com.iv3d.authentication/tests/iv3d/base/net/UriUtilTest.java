package iv3d.base.net;

import iv3d.base.StringProperty;

import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import org.springframework.http.HttpHeaders;

public class UriUtilTest extends TestCase {
	
	public void testQueryStringToMap() {
		String input = "code=AQB4DwAbXRMVus9CRw9iTdF5A5eSjC4epqZVSqz6y2BQppCgVDJfEo&fail=yes";
		Map<String, String> values = NetUtils.queryStringToMap(input);
		
		assertEquals(2, values.size());
		
		assertTrue(values.containsKey("code"));
		assertEquals("AQB4DwAbXRMVus9CRw9iTdF5A5eSjC4epqZVSqz6y2BQppCgVDJfEo", values.get("code"));
		
		assertTrue(values.containsKey("fail"));
		assertEquals("yes", values.get("fail"));
	}

	public void testQueryStringToMapEmpty() {
		String input = "";
		Map<String, String> values = NetUtils.queryStringToMap(input);
		
		assertEquals(0, values.size());
	}
	
	public void testQueryStringToMapOne() {
		String input = "code=AQB4DwAbXRMVus9CRw9iTdF5A5eSjC4epqZVSqz6y2BQppCgVDJfEo";
		Map<String, String> values = NetUtils.queryStringToMap(input);
		
		assertEquals(1, values.size());
		
		assertTrue(values.containsKey("code"));
		assertEquals("AQB4DwAbXRMVus9CRw9iTdF5A5eSjC4epqZVSqz6y2BQppCgVDJfEo", values.get("code"));
	}
	
	public void testQueryStringToStringProperties() {
		String input = "code=AQB4DwAbXRMVus9CRw9iTdF5A5eSjC4epqZVSqz6y2BQppCgVDJfEo&fail=yes&fail=maybe";
		List<StringProperty> array = NetUtils.queryStringToStringProperties(input);
		StringProperty[] values = array.toArray(new StringProperty[0]);
		
		assertEquals(3, values.length);
		
		StringProperty property1 = values[0];
		assertEquals("code", property1.getFirst());
		assertEquals("AQB4DwAbXRMVus9CRw9iTdF5A5eSjC4epqZVSqz6y2BQppCgVDJfEo", property1.getSecond());
		
		StringProperty property2 = values[1];
		assertEquals("fail", property2.getFirst());
		assertEquals("yes", property2.getSecond());
		
		StringProperty property3 = values[2];
		assertEquals("fail", property3.getFirst());
		assertEquals("maybe", property3.getSecond());
	}
	
	public void testQueryStringToStringPropertyEmpty() {
		String input = "";
		List<StringProperty> array = NetUtils.queryStringToStringProperties(input);
		StringProperty[] values = array.toArray(new StringProperty[0]);
		
		assertEquals(0, values.length);
	}
	
	public void testHeadersToProperties() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("A", "X");
		headers.add("A", "Y");
		headers.add("B", "Z");
		
		List<StringProperty> array = NetUtils.headersToStringProperty(headers);
		StringProperty[] values = array.toArray(new StringProperty[0]);
		
		assertEquals(3, values.length);
		
		StringProperty property1 = values[0];
		assertEquals("A", property1.getFirst());
		assertEquals("X", property1.getSecond());
		
		StringProperty property2 = values[1];
		assertEquals("A", property2.getFirst());
		assertEquals("Y", property2.getSecond());
		
		StringProperty property3 = values[2];
		assertEquals("B", property3.getFirst());
		assertEquals("Z", property3.getSecond());
	}
}
