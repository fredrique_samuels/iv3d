package iv3d.authentication.impl;

import iv3d.authentication.Account;
import iv3d.authentication.AccountDao;
import iv3d.authentication.AccountManager;
import iv3d.authentication.Credentials;
import iv3d.authentication.MediaAccountFactory;
import iv3d.authentication.managers.MergedSocailMediaAccountManagerFactory;
import iv3d.base.net.server.DataServer;
import iv3d.base.net.server.DataServerFactory;
import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Verifications;

public class MergedSocailMediaAccountManagerTest extends TestCase {
	
	@Mocked
	AccountDao dao;
	@Mocked
	DataServerFactory dataServerFactory;
	@Mocked
	MediaAccountFactory mediaAccountFactory;
	@Mocked
	DataServer dataServer;
	
	AccountManager instance;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		new NonStrictExpectations() { 
			{
				dataServerFactory.create(); result = dataServer;
			}
		};
		
		MergedSocailMediaAccountManagerFactory factory = new MergedSocailMediaAccountManagerFactory(dao, dataServerFactory, mediaAccountFactory);
		instance = factory.create();
	}
	
	public void testRegistrationSuccess() {
		new NonStrictExpectations() { 
			{
				dao.getAccountLastId(); result = 1L;
				dao.isEmailRegistered(anyString); result = false;
				dao.isUserIdRegistered(anyString); result = false;
			}
		};
		
		Account account = instance.registerWithCredentials(Credentials.create("user", "pass", "fred@email"));
		assertTrue(account.isValid());
		
		new Verifications() {
			{
				dao.isEmailRegistered((String)any);  times = 1;
				dao.isUserIdRegistered((String)any);  times = 1;
				dao.save((Account)any);  times = 1;
			}
		};
	}
	
	public void testInvalidUserId() {
		new NonStrictExpectations() { 
			{
				dao.getAccountLastId(); result = 1L;
				dao.isEmailRegistered(anyString); result = false;
				dao.isUserIdRegistered(anyString); result = true;
			}
		};
	
		Account account = instance.registerWithCredentials(Credentials.create("user", "pass", "fred@email"));
		assertFalse(account.isValid());
		assertEquals("The user name is already in use or not valid!", account.getStatusMessage());
	}
	
	public void testInvalidEmail() {
		new NonStrictExpectations() { 
			{
				dao.getAccountLastId(); result = 1L;
				dao.isEmailRegistered(anyString); result = true;
				dao.isUserIdRegistered(anyString); result = false;
			}
		};
	
		Account account = instance.registerWithCredentials(Credentials.create("user", "pass", "fred@email"));
		assertFalse(account.isValid());
		assertEquals("The email provided is already in use!", account.getStatusMessage());
	}
}
