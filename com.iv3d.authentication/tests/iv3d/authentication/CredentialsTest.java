package iv3d.authentication;

import junit.framework.TestCase;

public class CredentialsTest extends TestCase {
	public void testCredentailDefault() {
		Credentials credentials = Credentials.createEmpty();
		
		assertNull(credentials.getUserId());
		assertNull(credentials.getValue());
		assertNull(credentials.getEmail());
	}
	
	public void testCredentialsUserAndPassword() {
		Credentials credentials = Credentials.create("fred", "password");
		assertEquals("fred", credentials.getUserId());
		assertEquals("QmFzaWMgZnJlZDpwYXNzd29yZA==", credentials.getValue());
		assertNull(credentials.getEmail());
	}
	
	public void testCredentialsWithEmail() {
		Credentials credentials = Credentials.create("fred", "password", "fred@hmail.com");
		assertEquals("fred", credentials.getUserId());
		assertEquals("QmFzaWMgZnJlZDpwYXNzd29yZA==", credentials.getValue());
		assertEquals("fred@hmail.com", credentials.getEmail());
	}
}
