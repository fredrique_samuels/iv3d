package iv3d.authentication;

import iv3d.base.StringProperty;

import java.util.Map;

import junit.framework.TestCase;

public class StringPropertyTest extends TestCase {
	public void testConstruction() {
		StringProperty property = new StringProperty("first", "second");
		
		assertEquals("first", property.getFirst());
		assertEquals("second", property.getSecond());
	}
	
	public void testToMap() {
		StringProperty property1 = new StringProperty("first", "second");
		StringProperty property2 = new StringProperty("first", "fifth");
		StringProperty property3 = new StringProperty("third", "forth");
		
		Map<String, String> map = StringProperty.toMap(";", property1, property2, property3);
		
		assertNotNull(map);
		assertEquals(2, map.size());
		
		assertTrue(map.containsKey("first"));
		assertEquals("second;fifth", map.get("first"));
		
		assertTrue(map.containsKey("third"));
		assertEquals("forth", map.get("third"));
	}
}
