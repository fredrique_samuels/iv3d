package iv3d.authentication;

import iv3d.authentication.Account;
import iv3d.base.StringProperty;
import junit.framework.TestCase;

public class AccountTest extends TestCase {
	public void testInValidSessions() {
		Account account = Account.createInValid("Some message");
		
		assertFalse(account.isValid());
		assertEquals("Some message", account.getStatusMessage());
		assertNull(account.getAccountId());
		assertNotNull(account.getProperties());
		assertEquals(0, account.getProperties().length);
	}
	
	public void testValidNoProperties() {
		Account account = Account.createValid("account_id");
		
		assertTrue(account.isValid());
		assertEquals("valid", account.getStatusMessage());
		assertEquals("account_id", account.getAccountId());
		assertNotNull(account.getProperties());
		assertEquals(0, account.getProperties().length);
	}
	
	public void testValidWithProperties() {
		Account account = Account.createValid("account_id",
				new StringProperty("name", "John"),
				new StringProperty("gender", "male"));
		
		assertTrue(account.isValid());
		assertEquals("valid", account.getStatusMessage());
		assertEquals("account_id", account.getAccountId());
		assertNotNull(account.getProperties());
		assertEquals(2, account.getProperties().length);

		assertEquals("name", account.getProperties()[0].getFirst());
		assertEquals("John", account.getProperties()[0].getSecond());
		
		assertEquals("gender", account.getProperties()[1].getFirst());
		assertEquals("male", account.getProperties()[1].getSecond());
	}
	
	
	public void testCredentials() {
		Account account = Account.createValid("account_id",
				Credentials.createEmpty(),
				new StringProperty("name", "John"));
		
		assertTrue(account.isValid());
		assertEquals("valid", account.getStatusMessage());
		assertEquals("account_id", account.getAccountId());
		
		assertNotNull(account.getCredentials());
		
		assertNotNull(account.getProperties());
		assertEquals(1, account.getProperties().length);
	}
}
