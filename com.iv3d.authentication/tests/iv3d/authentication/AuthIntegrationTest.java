package iv3d.authentication;

import iv3d.authentication.AuthenticationIntegration;
import junit.framework.TestCase;

public class AuthIntegrationTest extends TestCase {
	
	public void testFacebookConstant() {
		assertEquals("Facebook", AuthenticationIntegration.FACEBOOK.getId());
	}
	
	public void testGoogleConstant() {
		assertEquals("Google", AuthenticationIntegration.GOOGLE.getId());
	}
	
	public void testTwitterConstant() {
		assertEquals("Twitter", AuthenticationIntegration.TWITTER.getId());
	}
	
}
