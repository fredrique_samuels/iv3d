package iv3d.authentication.server;

import iv3d.authentication.server.biz.DemoPathHandler;
import iv3d.base.net.server.DataPacket;
import iv3d.base.net.server.DataServer;
import iv3d.base.net.server.DataTransmitter;
import iv3d.base.net.server.httpserver.HttpDataServerFactory;

import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;

public class HttpDataServerTest extends TestCase {
	public void testFacebookCallback() {
		
		HttpDataServerFactory serverFactory = new HttpDataServerFactory(8081);
		DemoPathHandler pathFactory = new DemoPathHandler("/test");
		
		DataServer server = serverFactory.create();
		DataTransmitter dataTransmitter = server.getDataTransmitter();
		
		assertEquals("http://localhost:8081", server.getDomain());
		assertTrue(server.addPathHandlers(pathFactory));
		
		String result = null;
		boolean triggered = false;
		try {
			System.out.println("Sending request");
			DataPacket packet = dataTransmitter.send(new DataPacket("/test", null));
			
			assertNotNull(packet);
			result = packet.getData();
			
			System.out.println("waiting for trigger");
			triggered = pathFactory.waitForTrigger(5, TimeUnit.SECONDS);
			
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		} finally {			
			server.stop();
		}
		
		assertTrue("Auhentication Server did not receive request!", triggered);
		assertEquals("test result", result);
	}
}
