package iv3d.authentication.server.biz;

import iv3d.base.net.server.DataPacket;
import iv3d.base.net.server.PathHandler;
import iv3d.base.net.server.PathHandlerSource;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class DemoPathHandler implements PathHandler, PathHandlerSource {

	private Semaphore eventTrigger;
	private String path;

	public DemoPathHandler(String path) {
		this.path = path;
		eventTrigger = new Semaphore(1);
		setTrigger();
	}

	private void setTrigger() {
		try {
			eventTrigger.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public PathHandler[] getPathHandlers() {
		return new PathHandler[]{this};
	}

	@Override
	public String getPath() {
		return path;
	}
	
	public boolean waitForTrigger(long timeout, TimeUnit units) {
		boolean result = false;
		try {
			result = eventTrigger.tryAcquire(timeout, units);
			eventTrigger.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public DataPacket process(DataPacket packet) {
		eventTrigger.release();
		return packet.createWithData("test result");
	}

}
