package iv3d.authentication;

import iv3d.base.net.server.httpserver.HttpDataServerSettings;
import junit.framework.TestCase;

public class AuthSettingsTest extends TestCase {
	public void testConstruction() {
		HttpDataServerSettings settings = HttpDataServerSettings.create();
		
		assertEquals(8123, settings.getPort());
		assertEquals(HttpDataServerSettings.LOCALHOST, settings.getHost());
		assertTrue(settings.isLocal());
	}
	
	public void testLocalhostConstant() {
		assertEquals("http://localhost", HttpDataServerSettings.LOCALHOST);
	}
	
	public void testCustomConstruction() {
		HttpDataServerSettings settings = HttpDataServerSettings.createForPort(1234);
		
		assertEquals(1234, settings.getPort());
		assertEquals(HttpDataServerSettings.LOCALHOST, settings.getHost());
		assertTrue(settings.isLocal());
	}
	
	public void testCustomConstructionWithHost() {
		HttpDataServerSettings settings = HttpDataServerSettings.create("http://www.google.com", 1234);
		
		assertEquals(1234, settings.getPort());
		assertEquals("http://www.google.com", settings.getHost());
		assertFalse(settings.isLocal());
	}
	
}
