/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.authentication;

import org.apache.commons.codec.binary.Base64;

public final class Credentials {
		
	private String encryptedValue;
	private String email;
	private String userId;

	private Credentials() {
	}
	
	private Credentials(String id, String password) {
		this.userId = id;
		this.encryptedValue = encrypt(id, password);
	}
	
	public final String getValue() {
		return encryptedValue;
	}

	private static String encrypt(String id, String password) {
		return Base64.encodeBase64String(String.format("Basic %s:%s", id, password).getBytes());
	}

	public static Credentials createEmpty() {
		return new Credentials();
	}
	
	public static Credentials create(String id, String password) {
		return new Credentials(id, password);
	}
	
	public static Credentials create(String id, String password, String email) {
		Credentials credentials = new Credentials(id, password);
		credentials.setEmail(email);
		return credentials;
	}
	
	public final boolean equals(Credentials c) {
		if(c.encryptedValue==null || encryptedValue==null) {
			return false;
		}
		return c.encryptedValue.equals(encryptedValue);
	}

	public final String getEmail() {
		return email;
	}

	private void setEmail(String email) {
		this.email = email;
	}

	public final String getUserId() {
		return userId;
	}
}
