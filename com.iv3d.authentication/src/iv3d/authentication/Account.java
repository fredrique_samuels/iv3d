/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.authentication;
			
import iv3d.base.StringProperty;


public final class Account {
	
	private boolean valid;
	private String accountId;
	private String statusMessage;

	private StringProperty[] properties;
	private Credentials credentials;

	private Account(boolean valid, String statusMessage) {
		this.statusMessage = statusMessage;
		this.valid = valid;
		this.properties = new StringProperty[0];
	}
	
	private Account(String accountId, StringProperty... properties) {
		this(true, "valid");
		this.accountId = accountId;
		this.properties = properties;
	}
	
	private Account(String accountId, Credentials credentials,
			StringProperty[] properties) {
		this(accountId, properties);
		this.credentials = credentials; 
	}

	public final  boolean isValid() {
		return valid;
	}
	
	public final String getAccountId() {
		return accountId;
	}
	
	public final String getStatusMessage() {
		return statusMessage;
	}
	
	public final StringProperty[] getProperties() {
		return properties;
	}
	
	public final Credentials getCredentials() {
		return credentials;
	}

	public static Account createInValid(String message) {
		return new Account(false, message);
	}
	
	public static Account createValid(String accountId, StringProperty... properties) {
		return new Account(accountId, properties);
	}
	
	public static Account createValid(String accountId, Credentials credentials, StringProperty... properties) {
		return new Account(accountId, credentials, properties);
	}
}
