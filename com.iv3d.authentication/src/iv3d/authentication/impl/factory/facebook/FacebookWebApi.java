/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.authentication.impl.factory.facebook;

import iv3d.authentication.Account;
import iv3d.authentication.impl.factory.facebook.biz.FacebookBaseProfile;
import iv3d.authentication.impl.factory.facebook.biz.FacebookOAuthHandler;
import iv3d.base.StringProperty;
import iv3d.base.exceptions.ExceptionUtils;
import iv3d.base.net.UrlOpener;
import iv3d.base.net.server.PathHandler;
import iv3d.base.net.server.PathHandlerSource;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public  final class FacebookWebApi implements PathHandlerSource {
	
	private static final Logger logger = Logger.getLogger(FacebookWebApi.class.getName());
	private FacebookOAuthManager facebookOAuthManager;
	private RestTemplate restTemplate;

	public FacebookWebApi(String domain, UrlOpener urlOpener) {
		facebookOAuthManager = new FacebookOAuthManager(domain, urlOpener);
		restTemplate = new RestTemplate();
	}
	
	public final PathHandler[] getPathHandlers() {
		ArrayList<PathHandler> result = new ArrayList<PathHandler>();
		result.add(new FacebookOAuthHandler(facebookOAuthManager));
		return result.toArray(new PathHandler[0]);
	}

	public synchronized final Account create() {
		String accessToken = facebookOAuthManager.requestUserCredentials();
		FacebookBaseProfile profile = getProfile(accessToken);
		return createSessionFromProfile(profile);
	}
	
	private Account createSessionFromProfile(FacebookBaseProfile profile) {
		return Account.createValid(profile.getId(), 
				new StringProperty(StringProperty.NAME, profile.getFirst_name()),
				new StringProperty(StringProperty.ACCOUNT_ID, profile.getId()),
				new StringProperty(StringProperty.LAST_NAME, profile.getLast_name()),
				new StringProperty(StringProperty.GENDER, profile.getGender()),
				new StringProperty(StringProperty.LOCALE, profile.getLocale()),
				new StringProperty(StringProperty.TIME_ZONE, profile.getTimezone()));
	}

	public final FacebookBaseProfile getProfile(String accessToken) {
		String url = getUserIdUrl(accessToken);
		ResponseEntity<String> result = restTemplate.getForEntity(url, String.class);
		return parseBaseProfile(result.getBody());
	}

	private FacebookBaseProfile parseBaseProfile(String body) {
		try {
            return getProfileFromJson(body);
        } catch (IOException e) {
        	logger.warn(ExceptionUtils.getStackTrace(e));
        }
		return new FacebookBaseProfile();
	}

	private FacebookBaseProfile getProfileFromJson(String body)
			throws IOException, JsonParseException, JsonMappingException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.readValue(body, FacebookBaseProfile.class);
	}

	private String getUserIdUrl(String accessToken) {
		String template = "https://graph.facebook.com/me?access_token=%s";
		return String.format(template, accessToken);
	}
	
}
