/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
s *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.authentication.impl.factory.facebook;

import iv3d.authentication.impl.factory.facebook.biz.FacebookPaths;
import iv3d.base.exceptions.ExceptionUtils;
import iv3d.base.net.NetUtils;
import iv3d.base.net.UrlOpener;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.web.client.RestTemplate;

public final class FacebookOAuthManager {
	
	private static Logger logger = Logger.getLogger(FacebookOAuthManager.class.getName());
	
	private static final String CLIENT_ID = "276035225926701";
	private static final String SECRET_ID = "d51258c50ffec4cc380da8ac18c28594";
		
	private String accessToken;
	private final String redirectUrl;
	private final UrlOpener urlOpener;
	private final Semaphore userSignInLock;
	
	public FacebookOAuthManager(String domain, UrlOpener urlOpener) {
		this.redirectUrl = createRedirectUrl(domain);
		this.urlOpener = urlOpener;
		this.userSignInLock = new Semaphore(1);
	}

	private String createRedirectUrl(String domain) {
		return String.format("%s%s", domain, FacebookPaths.OAUTH_PATH);
	}

	public synchronized final String requestUserCredentials() {
		urlOpener.open(getOAuthUrl());
		return waitForUserSignInOrTimeOut();
	}

	public final String getAccessToken(String code) {
        String url = getAccessTokenUrl(code);
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(url, String.class);
        return NetUtils.queryStringToMap(result).get("access_token");
	}

	public final void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
		releaseUserSignInLock();
	}

	private String waitForUserSignInOrTimeOut() {
		accessToken = null;
		if(!acquireUserSignInLockNoWait()) {
			return null;
		}
		
		if(!acquireUserSignInLock()) {
			return null;
		}
		
		releaseUserSignInLock();
		return accessToken;
	}

	private boolean acquireUserSignInLockNoWait() {
		try {
			return userSignInLock.tryAcquire(1, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			logger.warn(ExceptionUtils.getStackTrace(e));
		}
		
		return false;
	}

	private boolean acquireUserSignInLock() {
		try {
			return userSignInLock.tryAcquire(1, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			logger.warn(ExceptionUtils.getStackTrace(e));
		}
		return false;
	}
	
	private void releaseUserSignInLock() {
		userSignInLock.release();
	}
	
	private String getOAuthUrl() { 
		String template = "https://www.facebook.com/dialog/oauth?client_id=%s&redirect_uri=%s&scope=ads_read";
        return String.format(template, CLIENT_ID, redirectUrl);
	}

	private String getAccessTokenUrl(String code) {
		String template = "https://graph.facebook.com/oauth/access_token?client_id=%s&redirect_uri=%s&client_secret=%s&code=%s";
        return String.format(template, CLIENT_ID, redirectUrl, SECRET_ID, code);
	}
}
