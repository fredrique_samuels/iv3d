/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.authentication.impl.factory.facebook.biz;

import iv3d.authentication.impl.factory.facebook.FacebookOAuthManager;
import iv3d.base.StringProperty;
import iv3d.base.net.server.DataPacket;
import iv3d.base.net.server.PathHandler;

import java.util.Map;

public class FacebookOAuthHandler implements PathHandler {

	private FacebookOAuthManager facebookOAuthManager;

	public FacebookOAuthHandler(FacebookOAuthManager facebookOAuthManager) {
		this.facebookOAuthManager = facebookOAuthManager;
	}

	private void createAccessToken(Map<String, String> params) {
		String code = params.get("code");
		String accessToken = facebookOAuthManager.getAccessToken(code);
		facebookOAuthManager.setAccessToken(accessToken);
	}

	@Override
	public final String getPath() {
		return FacebookPaths.OAUTH_PATH;
	}

	@Override
	public final DataPacket process(DataPacket packet) {
		StringProperty[] properties = packet.getProperties();
		createAccessToken(StringProperty.toMap(";", properties));
		return createResponse(packet);
	}

	private DataPacket createResponse(DataPacket packet) {
		String response = "<!DOCTYPE html><html><head><style>img {    width:100%;}</style></head><body><p>It is better to use the style attribute (instead of the width and height attributes), because it prevents internal or external styles sheets to change the original size of an image:</p><img src=\"html5.gif\" alt=\"HTML5 Icon\"></body></html>";
		return packet.createWithData(response);
	}
	
}
