/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.authentication.managers;

import iv3d.authentication.Account;
import iv3d.authentication.AccountDao;
import iv3d.authentication.AccountManager;
import iv3d.authentication.Credentials;
import iv3d.authentication.MediaAccountFactory;
import iv3d.base.net.server.DataServer;
import iv3d.base.net.server.DataServerFactory;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

public final class MergedSocailMediaAccountManager implements AccountManager {
	private static final Logger logger = Logger
			.getLogger(MergedSocailMediaAccountManager.class.getName());

	private final AccountDao dao;
	private final DataServer dataServer;
//	private final MediaAccountFactory mediaAccountFactory;

	MergedSocailMediaAccountManager(AccountDao dao,
			DataServerFactory serverFactory,
			MediaAccountFactory mediaAccountFactory) {
		this.dao = dao;
//		this.mediaAccountFactory = mediaAccountFactory;
		this.dataServer = serverFactory.create();
	}

	@Override
	public final Account loginWithCredentials(Credentials credentials) {
		return dao.getForCredentials(credentials);
	}

	@Override
	public final void shutdown() {
		logger.info("Shutting down authentication server.");
		dataServer.stop();
	}

	@Override
	public final Account registerWithCredentials(Credentials credentials) {
		if(isEmailRegistered(credentials))
			return Account.createInValid("The email provided is already in use!");
			
		if(isUserIdInUse(credentials))
			return Account.createInValid("The user name is already in use or not valid!");
		
		return createAccount(credentials);
	}

	private boolean isUserIdInUse(Credentials credentials) {
		String userId = credentials.getUserId();
		return userId!=null && !userId.isEmpty() && dao.isUserIdRegistered(userId);
	}

	private boolean isEmailRegistered(Credentials credentials) {
		return dao.isEmailRegistered(credentials.getEmail());
	}

	private Account createAccount(Credentials credentials) {
		long accountLastId = dao.getAccountLastId();
		String accountId = Base64.encodeBase64String((credentials.getUserId() + accountLastId).getBytes()) ;
		Account account = Account.createValid(accountId, credentials);
		dao.save(account);
		return account;
	}

}
