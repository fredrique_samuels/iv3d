/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.authentication.managers;

import iv3d.authentication.MediaAccountFactory;
import iv3d.authentication.Account;
import iv3d.authentication.impl.factory.facebook.FacebookWebApi;
import iv3d.base.net.UrlOpener;

public class SocailNetworkSessionFactory implements MediaAccountFactory {
	
	private final FacebookWebApi facebook;

	public SocailNetworkSessionFactory(String domain, UrlOpener urlOpener) {
		facebook = new FacebookWebApi(domain, urlOpener);
	}

	@Override
	public Account createFacebookAccount() {
		return facebook.create();
	}

	@Override
	public Account createGoogleAccount() {
		return Account.createInValid("Google implemention not ready yet!");
	}

	@Override
	public Account createTwitterAccount() {
		return Account.createInValid("Twitter implemention not ready yet!");
	}
}
