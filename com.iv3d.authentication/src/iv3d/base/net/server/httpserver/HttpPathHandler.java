/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.net.server.httpserver;

import iv3d.base.StringProperty;
import iv3d.base.net.NetUtils;
import iv3d.base.net.server.DataPacket;
import iv3d.base.net.server.PathHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

class HttpPathHandler implements HttpHandler {

	private PathHandler pathHandler;

	public HttpPathHandler(PathHandler pathHandler) {
		this.pathHandler = pathHandler;
	}
	
	@Override
	public void handle(HttpExchange exchange) throws IOException {
		DataPacket packet = toPacket(exchange);
		DataPacket response = pathHandler.process(packet);
		writeResponse(exchange, response);
		sendResponseHeaders(exchange, response);
		writeData(exchange, response);
	}
	
	private void writeData(HttpExchange exchange, DataPacket response) throws IOException {
        OutputStream os = exchange.getResponseBody();
		os.write(response.hasData()?response.getData().getBytes():new byte[0]);
        os.close();
	}

	private void sendResponseHeaders(HttpExchange exchange, DataPacket response) throws IOException {
		exchange.sendResponseHeaders(200, response.getDataSize());
	}

	private void writeResponse(HttpExchange exchange, DataPacket packet) {
		writeHeaders(exchange, packet);
	}

	private void writeHeaders(HttpExchange exchange, DataPacket packet) {
		exchange.getResponseHeaders().add("Content-Type", "text/html; charset=utf-8");
		StringProperty[] properties = packet.getProperties();
		for(StringProperty p:properties)
			exchange.getResponseHeaders().add(p.getFirst(), p.getSecond());
	}

	private DataPacket toPacket(HttpExchange exchange) throws IOException {
		String path = getPath(exchange);
		String data = hasData(exchange)?getData(exchange):null;
		StringProperty[] properties = getProperties(exchange);
		return new DataPacket(path, data, properties);
	}

	private boolean hasData(HttpExchange exchange) {
		return exchange.getRequestBody()!=null;
	}

	private StringProperty[] getProperties(HttpExchange exchange) {
		ArrayList<StringProperty> result = new ArrayList<StringProperty>();
		addHeaderProperties(exchange, result);
		addQueryParamProperties(exchange, result);
		return result.toArray(new StringProperty[0]);
	}

	private void addQueryParamProperties(HttpExchange exchange,
			ArrayList<StringProperty> result) {
		String query = exchange.getRequestURI().getQuery();
		List<StringProperty> properties = NetUtils.queryStringToStringProperties(query);
		result.addAll(properties);
	}

	private void addHeaderProperties(HttpExchange exchange,
			ArrayList<StringProperty> result) {
		Headers requestHeaders = exchange.getRequestHeaders();
		for(Entry<String, List<String>> entry : requestHeaders.entrySet()) {
			result.addAll(toProperties(entry));
		}
	}

	private Collection<? extends StringProperty> toProperties(
			Entry<String, List<String>> entry) {
		ArrayList<StringProperty> arrayList = new ArrayList<StringProperty>();
		List<String> list = entry.getValue();
		for(String value: list) {
			arrayList.add(new StringProperty(entry.getKey(), value));
		}
		return arrayList;
	}

	private String getData(HttpExchange exchange) throws IOException {
		InputStream input = exchange.getRequestBody();
		String result = IOUtils.toString(input, "UTF-8");
		input.close();
		return result;
	}

	private String getPath(HttpExchange exchange) {
		return exchange.getRequestURI().getPath();
	}
}
