/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.net.server.httpserver;

public final class HttpDataServerSettings {
	private final int port;
	private final String host;
	private final String domain;
	public final static String LOCALHOST = "http://localhost";

	private HttpDataServerSettings(int port, String host) {
		this.port = port;
		this.host = host;
		this.domain = String.format("%s:%d", host, port);
	}

	public int getPort() {
		return port;
	}

	public String getHost() {
		return host;
	}

	public String getDomain() {
		return domain;
	}

	public boolean isLocal() {
		return host.equals(LOCALHOST);
	}

	public static HttpDataServerSettings create() {
		return new HttpDataServerSettings(8123, LOCALHOST);
	}

	public static HttpDataServerSettings createForPort(int port) {
		return new HttpDataServerSettings(port, LOCALHOST);
	}

	public static HttpDataServerSettings create(String host, int port) {
		return new HttpDataServerSettings(port, host);
	}
}
