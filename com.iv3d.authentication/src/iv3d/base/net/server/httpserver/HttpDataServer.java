/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.net.server.httpserver;

import iv3d.base.exceptions.ExceptionUtils;
import iv3d.base.net.NetUtils;
import iv3d.base.net.server.DataServer;
import iv3d.base.net.server.DataTransmitter;
import iv3d.base.net.server.PathHandler;
import iv3d.base.net.server.PathHandlerSource;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.logging.Logger;

import com.sun.net.httpserver.HttpServer;

public final class HttpDataServer implements DataServer {
	private static Logger logger = Logger.getLogger(HttpDataServer.class.getName());
	
	private String domain;
	private HttpServer server;
	private Thread serverThread;
	private DataTransmitter dataTransmitter;
	
	HttpDataServer(final int port){
		super();
		this.server = createHttpServer(port);
		this.serverThread = createServerThread();
		this.serverThread.setName("AuthenticationServerImpl at port="+port);
		this.dataTransmitter = new HttpServerTransmitter(port);
		this.domain = NetUtils.buildDomain(HttpDataServerSettings.LOCALHOST, port);
	}

	private Thread createServerThread() {
		return new Thread() {
			@Override
			public void run() {
				server.start();
				logger.info("Started authentication server");
			}
		};
	}

	private HttpServer createHttpServer(final int port) {
		try {
			return HttpServer.create(new InetSocketAddress(port), 0);
		} catch (IOException e) {
			logger.warning(ExceptionUtils.getStackTrace(e));
		}
		return null;
	}


	@Override
	public boolean addPathHandlers(PathHandlerSource factory) {
		return addEndPoints(factory.getPathHandlers());
	}
	
	public boolean addEndPoints(PathHandler... endPoints) {
		if(isRunning()) {
			addAll(endPoints);
			return true;
		}
		logger.warning("Unable to add end point. Server is not running yet.");
		return false;
	}

	private void addAll(PathHandler... endPoints) {
		for(PathHandler endPoint:endPoints) {
			String path = endPoint.getPath();
			logger.info("Creating end point path=\"" + path + "\"");
			server.createContext(path, new HttpPathHandler(endPoint));
		}
	}
	
	private boolean isRunning() {
		return server!=null;
	}
	
	public synchronized void start() {
		if(readyToLaunch()) {
			serverThread.start();
		}
	}

	public final boolean readyToLaunch() {
		return server!=null && !serverThread.isAlive();
	}

	@Override
	public void stop() {
		if(server!=null) {
			server.stop(Thread.NORM_PRIORITY);
		}
	}

	@Override
	public DataTransmitter getDataTransmitter() {
		return dataTransmitter;
	}

	@Override
	public String getDomain() {
		return domain;
	}
}
