/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.net.server.httpserver;

import iv3d.base.StringProperty;
import iv3d.base.net.NetUtils;
import iv3d.base.net.server.DataPacket;
import iv3d.base.net.server.DataTransmitter;
import iv3d.base.net.server.exception.DataServerException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class HttpServerTransmitter implements DataTransmitter {
	
	private final String domain;
	private final RestTemplate restTemplate;

	public HttpServerTransmitter(int port) {
		this.domain = "http://localhost:" + port;
		this.restTemplate = new RestTemplate();
	}

	private URI createUrl(String path) throws DataServerException {
		String format = String.format("%s%s", domain, path);
		try {
			return new URI(format);
		} catch (URISyntaxException e) {
			throw new DataServerException(e);
		}
	}

	private HttpHeaders createHeadersFromProperties(StringProperty[] properties) {
		HttpHeaders httpHeaders = new HttpHeaders();
		for(StringProperty property:properties) {
			httpHeaders.add(property.getFirst(), property.getSecond());
		}
		return httpHeaders;
	}

	@Override
	public  DataPacket send(DataPacket packet) throws DataServerException {
		if(packet.hasData()) {
			return doPost(packet);
		}
		return doGet(packet);
	}

	private DataPacket doPost(DataPacket packet) throws DataServerException {
		try {
			HttpHeaders headers = createHeadersFromProperties(packet.getProperties());
			URI url = createUrl(packet.getPath());			
			HttpEntity<String> requestEntity = new HttpEntity<String>(packet.getData(), headers);
			ResponseEntity<String> exchange = restTemplate.exchange(url, 
					HttpMethod.POST, requestEntity, String.class);
			return toPacket(exchange);
		} catch (Exception e) {
			throw new DataServerException(e);
		}
	}
	
	private DataPacket doGet(DataPacket packet) throws DataServerException {
		try {
			HttpHeaders headers = createHeadersFromProperties(packet.getProperties());
			URI url = createUrl(packet.getPath());			
			HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
			ResponseEntity<String> exchange = restTemplate.exchange(url, 
					HttpMethod.GET, requestEntity, String.class);
			return toPacket(exchange);
		} catch (Exception e) {
			throw new DataServerException(e);
		}
	}

	private DataPacket toPacket(ResponseEntity<String> entity) {
		String data = entity.getBody();
		HttpHeaders headers = entity.getHeaders();
		List<StringProperty> properties = NetUtils.headersToStringProperty(headers);
		return new DataPacket("", data, properties.toArray(new StringProperty[0]));
	}

}
