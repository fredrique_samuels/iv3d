/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.net;

import iv3d.base.StringProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.http.HttpHeaders;

public final class NetUtils {
	private NetUtils(){}

	public static Map<String, String> queryStringToMap(String input) {
		Map<String, String> results = new HashMap<String, String>();
		if(input.isEmpty()) { 
			return results;
		}
		
		String[] split = input.split("&");
		for(String s : split) {
			int index = s.indexOf("=");
			String name = s.substring(0, index);
			String value = s.substring(index+1, s.length());
			results.put(name, value);
		}
		
		return results;
	}
	
	public static List<StringProperty> 
	queryStringToStringProperties(String input) {
		ArrayList<StringProperty> results = new ArrayList<StringProperty>();
		boolean notNullOrEmpty = input!=null && !input.isEmpty();
		if(notNullOrEmpty) { 
			addAllParams(input, results);
		}
		return results;
	}

	private static void addAllParams(String input,
			ArrayList<StringProperty> results) {
		String[] split = input.split("&");
		for(String s : split) {
			int index = s.indexOf("=");
			String name = s.substring(0, index);
			String value = s.substring(index+1, s.length());
			results.add(new StringProperty(name, value));
		}
	}

	public static List<StringProperty> headersToStringProperty(
			HttpHeaders headers) {
		ArrayList<StringProperty> array = new ArrayList<StringProperty>();
		for(Map.Entry<String, List<String>> entry : headers.entrySet()) {
			addEntryToList(entry, array);
		}
		return array;
	}

	private static void addEntryToList(Entry<String, List<String>> entry,
			ArrayList<StringProperty> array) {
		for (String value : entry.getValue()) {
			array.add(new StringProperty(entry.getKey(), value));
		}
	}

	public static String buildDomain(String host, int port) {
		return String.format("%s:%d", host, port);
	}

}
