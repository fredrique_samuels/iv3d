/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.storage.sql;

import iv3d.base.exceptions.ExceptionUtils;

import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.log4j.Logger;

public final class SqlLiteUtils {
	
	private static Logger logger  = Logger.getLogger(SqlLiteUtils.class.getName());
			
	private SqlLiteUtils(){}
	
	public static Connection getDbConnection(String url) {
		Connection c = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(url);
			c.setAutoCommit(false);
		} catch (Exception e) {
			logger.warn(ExceptionUtils.getStackTrace(e));
			return c;
		}
		
		logger.info(String.format("Opened database '%s' successfully", url));
		return c;
	}
	
	public static String createInsertStatement(String table,
			String[] properties, String[] values) {
		return String.format("insert into %s (%s) values (%s);", 
				table,
				joinPropertyStrings(properties),
				joinValueStrings(values));
	}

	private static String joinValueStrings(String[] values) {
		if(values.length==0) {return "";}
		
		StringBuilder builder = new StringBuilder(String.format("'%s'",values[0]));
		for(int i=1;i<values.length;++i) {
			builder.append(String.format(",'%s'",values[i]));
		}
		return builder.toString();
	}

	private static String joinPropertyStrings(String[] properties) {
		if(properties.length==0) {return "";}
		
		StringBuilder builder = new StringBuilder(properties[0]);
		for(int i=1;i<properties.length;++i) {
			builder.append(",");
			builder.append(properties[i]);
		}
		return builder.toString();
	}
}
