/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import iv3d.base.math.Matrix44;
import iv3d.base.math.MatrixStack;
import iv3d.base.math.Vector2;
import iv3d.base.math.Vector3;
import iv3d.base.math.Vector4;

public class GraphicsMatrix {
	public static enum MatrixMode {
		PROJECTION, MODELVIEW
	}

	private MatrixStack projectMatrix;
	private MatrixStack activeMatrix;
	private MatrixStack modelMatrix;

	public GraphicsMatrix() {
		projectMatrix = new MatrixStack();
		modelMatrix = new MatrixStack();
		setMatrixMode(MatrixMode.MODELVIEW);
	}

	public final GraphicsMatrix setMatrixMode(MatrixMode mode) {
		if (mode == MatrixMode.PROJECTION) {
			this.activeMatrix = projectMatrix;
		} else {
			this.activeMatrix = modelMatrix;
		}
		return this;
	}

	public final Matrix44 getModelMatrix() {
		return modelMatrix.get();
	}

	public final Matrix44 getProjectionMatrix() {
		return projectMatrix.get();
	}

	public final Matrix44 getModelViewProjectionMatrix() {
		return Matrix44.multiply(projectMatrix.get(), modelMatrix.get());
	}

	public final GraphicsMatrix multMatrix(Matrix44 m) {
		activeMatrix.multiMatrixf(m);
		return this;
	}

	public final GraphicsMatrix multMatrix(float[] m) {
		activeMatrix.multiMatrixf(m);
		return this;
	}

	public final GraphicsMatrix pushMatrix() {
		activeMatrix.pushMatrix();
		return this;
	}

	public final GraphicsMatrix popMatrix() {
		activeMatrix.popMatrix();
		return this;
	}

	public final GraphicsMatrix translate(Vector2 v) {
		activeMatrix.translate(v);
		return this;
	}

	public final GraphicsMatrix translate(float x, float y) {
		activeMatrix.translate(x, y);
		return this;
	}

	public final GraphicsMatrix translate(Vector3 v) {
		activeMatrix.translate(v);
		return this;
	}

	public final GraphicsMatrix translate(float x, float y, float z) {
		activeMatrix.translate(x, y, z);
		return this;
	}

	public final GraphicsMatrix loadMatrix(Matrix44 m) {
		activeMatrix.loadMatrix(m);
		return this;
	}

	public final GraphicsMatrix loadMatrix(float[] m) {
		activeMatrix.loadMatrix(m);
		return this;
	}
	
	public final GraphicsMatrix loadIdentity() {
		activeMatrix.loadIdentity();
		return this;
	}
	
	public final GraphicsMatrix setLookAt(float eyex, float eyey, float eyez,
			float centerx, float centery, float centerz, float upx, float upy,
			float upz) {
		activeMatrix.setLookAtMatrix(eyex, eyey, eyez, centerx, centery, centerz, upx, upy, upz);
		return this;
	}

	public final GraphicsMatrix setPerspectiveView(float arc, float aspect,
			float clipnear, float clipfar) {
		activeMatrix.setPerspectiveView(arc, aspect, clipnear, clipfar);
		return this;
	}

	public final GraphicsMatrix setOrthoView(float left, float right, float bottom,
			float top, float clipnear, float clipfar) {
		activeMatrix.setOrthoView(left, right, bottom, top, clipnear, clipfar);
		return this;
	}
	
	public final GraphicsMatrix rotateEuler(Vector3 v) {
		activeMatrix.rotateEuler(v);
		return this;
	}

	public final GraphicsMatrix rotateEuler(float x, float y, float z) {
		activeMatrix.rotateEuler(x, y, z);
		return this;
	}
	
	public final GraphicsMatrix rotateAxis(Vector4 v) {
		activeMatrix.rotateAxis(v);
		return this;
	}
	
	public final GraphicsMatrix rotateAxis(float x, float y, float z, float w) {
		activeMatrix.rotateAxis(x, y, z, w);
		return this;
	}
	
	public final GraphicsMatrix scale(Vector3 v) {
		activeMatrix.scale(v);
		return this;
	}

	public final GraphicsMatrix scale(float x, float y, float z) {
		activeMatrix.scale(x, y, z);
		return this;
	}
}
