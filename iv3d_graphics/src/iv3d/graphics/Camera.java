/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import iv3d.base.Bounds;
import iv3d.base.Rectangle;
import iv3d.base.math.Matrix44;
import iv3d.base.math.Vector3;
import iv3d.base.math.Vector4;

import java.nio.FloatBuffer;

public class Camera {

	public static enum CameraMode {
		PERSPECTIVE, ORTHO
	}

	private Vector3 lookAtPosition;
	private Vector3 position;
	private Bounds viewport;
	private float clipNear;
	private float clipFar;
	private CameraMode mode;
	private float angle;
	private OrthoSettings orthoSettings;
	private boolean enabled;
	private float roll;
	private FloatBuffer lookAtMatrixBuffer;
	private FloatBuffer lookAtMatrixBufferI;
	private Matrix44 projectionMatrix;
	private FloatBuffer projectionBuffer;

	public Camera() {
		lookAtPosition = new Vector3(0, 0, 0);
		position = new Vector3();
		viewport = new Bounds(0, 0, 1, 1);
		clipNear = 0.1f;
		clipFar = 1000;
		mode = CameraMode.PERSPECTIVE;
		angle = 60;
		orthoSettings = new OrthoSettings(-100, 100, 100, -100);
		enabled = true;
		roll = 0;
		
		projectionBuffer = GraphicsUtils.asFloatBuffer(new float[16]);
		projectionMatrix = new Matrix44();
		updateProjectionMatrix();
		
		lookAtMatrixBufferI = GraphicsUtils.asFloatBuffer(new float[16]);
		lookAtMatrixBuffer = GraphicsUtils.asFloatBuffer(new float[16]);
		updateLookAtMatrix();
	}

	private void updateLookAtMatrix() {
		Matrix44 lookAtMatrix = getLookAtMatrix();
		lookAtMatrixBuffer.put(lookAtMatrix.toArray());
		lookAtMatrixBuffer.flip();
		lookAtMatrixBufferI.put(lookAtMatrix.getInverse().toArray());
		lookAtMatrixBufferI.flip();
	}

	public final Vector3 getLookAtPosition() {
		return lookAtPosition;
	}

	public final Camera setLookAtPosition(Vector3 lookAtPosition) {
		this.lookAtPosition = lookAtPosition;
		updateLookAtMatrix();
		return this;
	}

	public final Vector3 getPosition() {
		return position;
	}

	public Camera setPosition(Vector3 position) {
		this.position =  new Vector3(position);
		updateLookAtMatrix();
		return this;
	}
	
	public final Bounds getViewport() {
		return viewport;
	}

	public final Camera setViewport(Bounds viewport) {
		this.viewport = viewport;
		updateProjectionMatrix();
		return this;
	}

	public final float getClipNear() {
		return clipNear;
	}

	public final Camera setClipNear(float clipNear) {
		this.clipNear = clipNear;
		updateProjectionMatrix();
		return this;
	}

	public final float getClipFar() {
		return clipFar;
	}

	public final Camera setClipFar(float clipFar) {
		this.clipFar = clipFar;
		updateProjectionMatrix();
		return this;
	}

	public final CameraMode getMode() {
		return mode;
	}

	public final Camera setMode(CameraMode mode) {
		this.mode = mode;
		updateProjectionMatrix();
		return this;
	}

	public final float getAngle() {
		return angle;
	}

	public final Camera setAngle(float angle) {
		this.angle = angle;
		updateProjectionMatrix();
		return this;
	}

	public final OrthoSettings getOrthoSettings() {
		return orthoSettings;
	}

	public final Camera setOrthoSettings(OrthoSettings orthoSettings) {
		this.orthoSettings = orthoSettings;
		updateProjectionMatrix();
		return this;
	}

	public final boolean isEnabled() {
		return enabled;
	}

	public final Camera setEnabled(boolean enabled) {
		this.enabled = enabled;
		return this;
	}

	public Matrix44 getLookAtMatrix() {
		Matrix44 mat =  new Matrix44().makeLookAt(position.getX(), position.getY(),
				position.getZ(), lookAtPosition.getX(), lookAtPosition.getY(),
				lookAtPosition.getZ(), 0, 1, 0);
		return mat;
	}
	
	public Rectangle getViewport(int width,int height) {
		return new Rectangle((int) (width * viewport.getX()),
				(int) (height * viewport.getY()), (int) (width * viewport.getWidth()),
				(int) (height * viewport.getHeight()));
	}
	
	public final void updateProjectionMatrix() {
		GraphicsContext gl = GraphicsContext.getInstance();
		int w = gl.getScreenWidth();
		int h = gl.getScreenHeight();

		float fw = w * viewport.getWidth();
		float fh = h * viewport.getHeight();
		GraphicsUtils.setViewport((int) (w * viewport.getX()),
				(int) (h * viewport.getY()), (int) (w * viewport.getWidth()),
				(int) (h * viewport.getHeight()));
		float ar = fw / fh;
		
		projectionMatrix.identity();
		if (mode == CameraMode.PERSPECTIVE) {
			projectionMatrix.makeProjectionPersp(angle, ar, clipNear, clipFar);
		} else {
			projectionMatrix.makeProjectionOrtho(orthoSettings.getLeft(), orthoSettings.getRight(),
					orthoSettings.getBottom(), orthoSettings.getTop(),
					clipNear, clipFar);
		}
		projectionMatrix.rotate(new Vector4(0, 1, 0, roll));
		projectionBuffer.put(projectionMatrix.toArray());
		projectionBuffer.flip();
	}

	public Camera bind(GraphicsContext gl) {
		gl.setCamera(this);
		return this;
	}
	
	public final Matrix44 getProjectionMatrix(int screenWidth, int screenHeight) {
		Matrix44 mat44 = new Matrix44();
		if (mode == CameraMode.PERSPECTIVE) {
			mat44.makeProjectionPersp(angle, (float)screenWidth/(float)screenHeight, clipNear, clipFar);
		} else {
			mat44.makeProjectionOrtho(orthoSettings.getLeft(), orthoSettings.getRight(),
					orthoSettings.getBottom(), orthoSettings.getTop(),
					clipNear, clipFar);
		}
		mat44.rotate(new Vector3(0, roll, 0));
		return mat44;
	}
	
	public final Camera setRoll(Float roll) {
		this.roll = roll;
		updateProjectionMatrix();
		return this;
	}
	
	public final FloatBuffer getLookAtBuffer() {
		return lookAtMatrixBuffer;
	}
	
	public final FloatBuffer getLookAtBufferI() {
		return lookAtMatrixBufferI;
	}
	
	public final FloatBuffer getProjectionBuffer() {
		return projectionBuffer;
	}
	
	public final Matrix44 getProjectionMatrix() {
		return projectionMatrix.copy();
	}
	
	public static float getScreenHeightFromDistance(float distance, float angle) {  
		float alpha = angle*.5f;
		float beta = 90-alpha;
		double sinBeta = Math.sin(Math.toRadians(beta));
		double sinAlpha = Math.sin(Math.toRadians(alpha));
		return (float) (distance * (sinAlpha/sinBeta)) * 2;
	}
	
	public static float getDistanceForScreenHeight(float height, float angle) {  
		float alpha = angle*.5f;
		float beta = 90-alpha;
		double sinBeta = Math.sin(Math.toRadians(beta));
		double sinAlpha = Math.sin(Math.toRadians(alpha));
		float hh = .5f * height;
		return (float) (hh * (sinBeta/sinAlpha));
	}
	
}
