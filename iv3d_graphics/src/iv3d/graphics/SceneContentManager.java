/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class SceneContentManager{
	List<Light> lights;
	List<Model> models;
	
	public SceneContentManager() {
		lights = new LinkedList<Light>();
		models = new LinkedList<Model>();
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		removeAll();
	}
	
	public void clear() {
		removeAll();
	}
	
	public SceneContentManager addLight(Light light) {
		if (light == null) {
			return this;
		}
		
		if (lights.contains(light)) {
			return this;
		}
		
		lights.add(light);
		return this;
	}
	
	public SceneContentManager removeLight(Light light) {
		if (lights.contains(light)) {
			lights.remove(light);
		}
		return this;
	}
	
	public SceneContentManager removeAllLights(){ 
		lights.clear();
		return this;
	}
	
	public SceneContentManager addModel(Model model) {
		if (model == null) {
			return this;
		}
		
		if (models.contains(model)) {
			return this;
		}
		
		models.add(model);
		return this;
	}
	
	public SceneContentManager removeModel(Model model) {
		if (models.contains(model)) {
			models.remove(model);
		}
		return this;
	}
	
	public SceneContentManager removeAllModels(){ 
		models.clear();
		return this;
	}
	
	public SceneContentManager removeAll() {
		removeAllLights();
		removeAllModels();
		return this;
	}

	public Iterator<Light> getLightIterator() {
		return lights.iterator();
	}
	
	public Iterator<Model> getModelIterator() {
		return models.iterator();
	}

	public Light[] getLights() {
		return lights.toArray(new Light[0]);
	}
}
