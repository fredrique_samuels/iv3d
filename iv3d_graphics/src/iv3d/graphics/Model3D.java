/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import iv3d.base.ImageSource;
import iv3d.base.MatrixTransform;
import iv3d.base.Transform;
import iv3d.base.TransformState;
import iv3d.base.exceptions.GraphicsException;
import iv3d.base.math.Matrix44;
import iv3d.base.math.Quaternion;
import iv3d.base.math.Vector3;
import iv3d.graphics.Object3D.DefaultEntityRenderer;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;


public class Model3D extends Model implements Transform {
	private static final DefaultEntityRenderer defaultRenderer = new DefaultEntityRenderer();
	public final Map<String, Material> materials;
	public final Map<String, Object3D> meshList;
	private final Map<Entity, MatrixGroup> entities;
	private MatrixTransform transform;
	private boolean cullfaceEnabled;

	public Model3D() {
		super();
		cullfaceEnabled = true;
		materials = new HashMap<String, Material>();
		meshList = new HashMap<String, Object3D>();
		entities = new LinkedHashMap<Entity, MatrixGroup>();
		transform = new MatrixTransform();
	}
	
	public Model3D(Model3D model) {
		super();
		transform = new MatrixTransform();
		
		materials = new HashMap<String, Material>();
		for(Entry<String, Material> entry : model.materials.entrySet()) {
			materials.put(entry.getKey(), entry.getValue().copy());
		}
		
		meshList = new HashMap<String, Object3D>();
		for(Entry<String, Object3D> entry : model.meshList.entrySet()) {
			meshList.put(entry.getKey(), entry.getValue().copy(materials));
		}
		
		entities = new LinkedHashMap<Entity, MatrixGroup>();
		
		setScale(model.getScale());
		setPosition(model.getPosition());
		setRotation(model.getRotation());
	}

	public Material createMaterial(String name) {
		Material material = new Material(name);
		materials.put(name, material);
		return material;
	}

	public Material getMaterial(String name) {
		if (materials.containsKey(name)) {
			return materials.get(name);
		}
		return null;
	}

	public Set<String> getMaterialNames() {
		return materials.keySet();
	}
	
	public Model3D enableCullFace() {
		cullfaceEnabled = true;
		return this;
	}
	
	public Model3D disableCullFace() {
		cullfaceEnabled = false;
		return this;
	}
	
	public final Object3D createMesh(String name) {
		if(meshList.containsKey(name)) {
			meshList.get(name).destroy();
		}
		Object3D model = createObject3D();
		meshList.put(name, model);
		return model;
	}
	
	public Object3D createObject3D() {
		return new Object3D();
	}

	public Object3D getMesh(String name) {
		if (meshList.containsKey(name)) {
			return meshList.get(name);
		}
		return null;
	}

	public Set<String> getMeshNames() {
		return meshList.keySet();
	}

	public Model3D setShader(Shader shader) {
		for (Material material : materials.values()) {
			material.setShader(shader);
		}
		return this;
	}

	public Model3D setShader(Shader shader, String materialName) {
		Material material = getMaterial(materialName);
		if (material != null) {
			material.setShader(shader);
		}
		return this;
	}

	public Model3D setTexture(Texture texture) {
		return setTexture(texture, TextureLocation.TEXTURE_0);
	}

	public Model3D setTexture(Texture texture, TextureLocation location) {
		for (Material material : materials.values()) {
			material.setTexture(texture, location);
		}
		return this;
	}

	public Model3D setTexture(String filename, int filterOptions) throws GraphicsException {
		ImageSource image = GraphicsUtils.readImageFromFile(filename, filterOptions);
		return setTexture(image);
	}

	public Model3D setTexture(ImageSource image) {
		return setTexture(new Texture().load(image));
	}

	public Model3D setTexture(String filename, int filterOptions, TextureLocation location)
			throws GraphicsException {
		ImageSource image = GraphicsUtils.readImageFromFile(filename, filterOptions);
		return setTexture(image, location);
	}

	public Model3D setTexture(ImageSource image, TextureLocation location) {
		return setTexture(new Texture().load(image), location);
	}

	public final Model3D destroy() {
		entities.clear();
		return this;
	}

	public Model3D clearObjects() {
		entities.clear();
		return this;
	}

	public Model3D bindEntity(Entity entity) {
		if (entity == null) {
			return this;
		}

		if (entities.containsKey(entity)) {
			return this;
		}

		MatrixGroup matrixGroup = new MatrixGroup(this, entity);
		entity.addTransformChangeListener(matrixGroup);
		entities.put(entity, matrixGroup);
		matrixGroup.updateMatrices();
		return this;
	}

	public Model3D unbindEntity(Entity entity) {
		if (entities.containsKey(entity)) {
			MatrixGroup matrixGroup = entities.get(entity);
			entity.removeTransformChangeListener(matrixGroup);
			entities.remove(entity);
		}
		return this;
	}
	
	public void render(GraphicsContext gl) {
		if(cullfaceEnabled) {
			GraphicsUtils.enableCullFace();
		} else {
			GraphicsUtils.disableCullFace();
		}
		render(gl, defaultRenderer);
	}

	public void render(GraphicsContext gl, EntityRenderer entityRenderer) {
		for (Object3D model : meshList.values()) {
			model.render(gl, entityRenderer, entities.values());
		}
	}

	public void unbindAll() {
		entities.clear();
	}

	private void updateAllMatrixGroups() {
		for(Entry<Entity, MatrixGroup> entry : entities.entrySet()) {
			entry.getValue().updateMatrices();
		}
	}
	
	@Override
	public float[] toArray() {
		return transform.toArray();
	}

	@Override
	public float[] toArrayI() {
		return transform.toArrayI();
	}

	@Override
	public Vector3 getPosition() {
		return transform.getPosition();
	}

	@Override
	public float[] getRotation() {
		return transform.getRotation();
	}

	@Override
	public Vector3 getScale() {
		return transform.getScale();
	}

	@Override
	public Transform setPosition(float x, float y, float z) {
		return setPosition(new Vector3(x, y, z));
	}

	@Override
	public Transform setPosition(Vector3 v) {
		transform.setPosition(v);
		updateAllMatrixGroups();
		return transform;
	}

	@Override
	public Transform setRotation(Matrix44 rot) {
		transform.setRotation(rot);
		updateAllMatrixGroups();
		return transform;
	}

	@Override
	public Transform setRotation(float[] rot) {
		return setRotation(new Matrix44(rot));
	}

	@Override
	public Transform setRotation(float x, float y, float z) {
		transform.setRotation(x, y, z);
		updateAllMatrixGroups();
		return transform;
	}

	@Override
	public Transform setRotation(Quaternion q) {
		transform.setRotation(q);
		updateAllMatrixGroups();
		return transform;
	}

	@Override
	public Transform setScale(float x, float y, float z) {
		return setScale(new Vector3(x, y, z));
	}

	@Override
	public Transform setScale(Vector3 v) {
		transform.setScale(v);
		updateAllMatrixGroups();
		return transform;
	}

	@Override
	public Transform setRotation(Vector3 v) {
		transform.setRotation(v);
		updateAllMatrixGroups();
		return transform;
	}

	@Override
	public Transform setPositionX(Float v) {
		transform.setPositionX(v);
		updateAllMatrixGroups();
		return transform;
	}

	@Override
	public Transform setPositionY(Float v) {
		transform.setPositionY(v);
		updateAllMatrixGroups();
		return transform;
	}

	@Override
	public Transform setPositionZ(Float v) {
		transform.setPositionZ(v);
		updateAllMatrixGroups();
		return transform;
	}

	@Override
	public TransformState getTransformState() {
		return transform.getTransformState();
	}
}
