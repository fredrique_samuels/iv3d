/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import java.nio.FloatBuffer;

import iv3d.base.TransformState;
import iv3d.base.math.Matrix44;
import iv3d.base.math.Vector3;

public class MatrixGroup implements TransformChangeListener {

	private final Model3D model;
	private final RenderTarget target;
	private Matrix44 objectMatrix;
	private FloatBuffer normalBuffer;
	private FloatBuffer objectBuffer;
	private FloatBuffer objectBufferI;
	
	public MatrixGroup(Model3D model, RenderTarget target) {
		super();
		this.model = model;
		this.target = target;
		objectMatrix = new Matrix44();
		objectBuffer = GraphicsUtils.asFloatBuffer(new float[16]);
		objectBufferI = GraphicsUtils.asFloatBuffer(new float[16]);
		normalBuffer = GraphicsUtils.asFloatBuffer(new float[9]);
	}

	@Override
	public void transformChanged(Entity entity) {
		update();
	}

	public void updateMatrices() {
		update();
	}
	
	public RenderTarget getTarget() {
		return target;
	}

	private void update() {
		objectMatrix = calculateObjectMatrix();
		
		objectBuffer.put(objectMatrix.toArray());
		objectBuffer.flip();
		
		Matrix44 inverse = objectMatrix.getInverse();
		objectBufferI.put(inverse.toArray());
		objectBufferI.flip();
		
		normalBuffer.put(inverse.getTranspose().toMatrix33());
		normalBuffer.flip();
	}

	public final Matrix44 getObjectMatrix() {
		return objectMatrix;
	}

	public final FloatBuffer getNormalBuffer() {
		return normalBuffer;
	}

	public final FloatBuffer getObjectBuffer() {
		return objectBuffer;
	}

	public final FloatBuffer getObjectBufferI() {
		return objectBufferI;
	}

	private Matrix44 calculateObjectMatrix() {
		TransformState entityState = target.getTransformState();
		TransformState modelState = model.getTransformState();
		
		Vector3 position = entityState.getPosition().add(modelState.getPosition());
		Vector3 scale = Vector3.scale(entityState.getScale(), modelState.getScale());
		Matrix44 rotation = Matrix44.multiply(modelState.getRotation(), entityState.getRotation());

		objectMatrix.identity();
		objectMatrix.translate(position);
		objectMatrix.multiply(rotation);
		objectMatrix.scale(scale);
		return objectMatrix;
	}

}
