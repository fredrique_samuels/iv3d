/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import iv3d.graphics.shaders.ShaderProperty;

public class TextureLocation extends ShaderProperty {
	
	public final static TextureLocation TEXTURE_0 = new TextureLocation("cg_Texture0", 0);
	public final static TextureLocation TEXTURE_1 = new TextureLocation("cg_Texture1", 1);
	public final static TextureLocation TEXTURE_2 = new TextureLocation("cg_Texture2", 2);
	public final static TextureLocation TEXTURE_3 = new TextureLocation("cg_Texture3", 3);
	public final static TextureLocation TEXTURE_PROJ = new TextureLocation("cg_ProjMap", 4);
	public final static TextureLocation TEXTURE_SPHERE = new TextureLocation("cg_SphereMap", 5);
	public final static TextureLocation TEXTURE_BUMP = new TextureLocation("cg_BumpMap", 6);
	public final static TextureLocation TEXTURE_SHADOW = new TextureLocation("cg_ShadowMap", 7);
	
	public static TextureLocation fromIndex(int index) {
		TextureLocation[] locations = new TextureLocation[]{
				TEXTURE_0,
				TEXTURE_1,
				TEXTURE_2,
				TEXTURE_3,
				TEXTURE_PROJ,
				TEXTURE_SPHERE,
				TEXTURE_BUMP,
				TEXTURE_SHADOW};
		return locations[index];
	}

	private final int index;
	
	private TextureLocation(String name, int index) {
		super(name);
		this.index = index;
	}

	public int getIndex() {
		return index;
	}

	@Override
	public void setValue(Shader shader, Object value) {
		if (value==null) {
			Texture.unbind(index);
			return;
		}
		
		if (!(value instanceof Texture)){
			Texture.unbind(index);
			return;
		}
		((Texture)value).bind(index);
		shader.setUniform1i(getName(), index);
	}
}
