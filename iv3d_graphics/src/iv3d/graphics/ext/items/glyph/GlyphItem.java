/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.ext.items.glyph;

import iv3d.base.ImageSource;
import iv3d.base.exceptions.GraphicsException;
import iv3d.base.ext.groups.Item;
import iv3d.graphics.GraphicsUtils;
import iv3d.graphics.TextureUtils;

public class GlyphItem implements Item {
	private final ImageSource image;
	private final MetaData metaData;

	public GlyphItem(ImageSource image, MetaData metaData) {
		this.image = image;
		this.metaData = metaData;
	}
	
	public GlyphItem(String image, MetaData metaData) throws GraphicsException {
		this(GraphicsUtils.readImageFromFile(image, TextureUtils.TEXTURE_OPTION_FILTER),
				metaData);
	}

	public GlyphItem(String image) throws GraphicsException {
		this(image, null);
	}

	public GlyphItem(ImageSource image) {
		this(image, null);
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
	}

	public final ImageSource getImage() {
		return image;
	}

	public final MetaData getMetaData() {
		return metaData;
	}
}
