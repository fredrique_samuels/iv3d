package iv3d.graphics.ext.items.glyph;

import iv3d.base.events.EventListener;
import iv3d.base.exceptions.GraphicsException;
import iv3d.base.ext.groups.ItemTransform;
import iv3d.base.ext.groups.ItemView;
import iv3d.base.math.Vector3;
import iv3d.graphics.Entity;
import iv3d.graphics.Texture;
import iv3d.graphics.TextureLocation;
import iv3d.graphics.TextureUtils;
import iv3d.graphics.models.ModelRegistry;
import iv3d.graphics.shaders.ShaderProperty;

public class GlyphItemView extends ItemView<GlyphItem>{

	private Entity entity;
	private Entity reflection;
	private float reflectionSpace;
	private GlyphItem value;

	GlyphItemView(boolean enabledReflection, String modelKey, boolean invertFade) {
		super();
		entity =  new Entity();
		entity.enableEvents();
		reflectionSpace = 0.1f;
		try {
			ModelRegistry.getModel(modelKey).bindEntity(entity);
		} catch (GraphicsException e) {
			e.printStackTrace();
		}
		
		if(enabledReflection) {
			reflection = new Entity();
			try {
				if(invertFade) {
					reflection.setTexture("/images/fade_inv.png", TextureUtils.TEXTURE_OPTION_FILTER, TextureLocation.TEXTURE_3);
				} else {
					reflection.setTexture("/images/fade.png", TextureUtils.TEXTURE_OPTION_FILTER, TextureLocation.TEXTURE_3);
				}
				reflection.getTexture(TextureLocation.TEXTURE_3).enableAlphaMask();
				ModelRegistry.getModel(modelKey).bindEntity(reflection);
			} catch (GraphicsException e) {
				e.printStackTrace();
			}
		}
		
		setupEventHooks();
	}
	
	@Override
	public void hide() {
		entity.hide();
		if(reflection!=null) {
			reflection.hide();
		}
	}

	@Override
	public boolean isVisible() {
		return entity.isVisible();
	}

	@Override
	public void applyTransform(ItemTransform transform) {
		Vector3 scale = transform.getScale();
		Vector3 position = transform.getPosition();
		Vector3 rotation = transform.getRotation();

		entity.setPosition(position);
		entity.setRotation(rotation);
		entity.setScale(scale);
		entity.setProperty(ShaderProperty.PROPERTY_FLOAT_ALPHA, transform.getAlpha());
		
		if(reflection!=null) {
			reflection.setPosition(position.getX(), position.getY()-Math.abs(scale.getY()+reflectionSpace), position.getZ());
			reflection.setRotation(rotation.getX(), rotation.getY(), rotation.getZ());
			reflection.setScale(scale.getX(), -scale.getY(), scale.getZ());
			reflection.setProperty(ShaderProperty.PROPERTY_FLOAT_ALPHA, transform.getAlpha());
		}
	}

	@Override
	public void setValue(GlyphItem value) {
		this.value = value;
		if(value==null) {
			entity.setTexture((Texture)null);
			if(reflection!=null) {
				reflection.setTexture((Texture)null);
			}
			return;
		}
		
		entity.setTexture(value.getImage());
		if(reflection!=null) {
			reflection.setTexture(value.getImage());
		}
	}

	@Override
	public void show() {
		entity.show();
		if(reflection!=null) {
			reflection.show();
		}
	}

	@Override
	public void addEventListener(EventListener listener) {
		entity.addEventListener(listener);
	}

	public void setReflectionSpace(float reflectionSpace) {
		this.reflectionSpace = reflectionSpace;
	}

	public final GlyphItem getValue() {
		return value;
	}
}
