package iv3d.graphics.ext.items.glyph;

import iv3d.base.exceptions.GraphicsException;
import iv3d.base.ext.groups.ItemFactory;
import iv3d.graphics.Model3D;
import iv3d.graphics.models.ModelRegistry;
import iv3d.graphics.models.PlaneModel;

public class GlyphItemFactory implements ItemFactory<GlyphItemView> {
		private final String modelKey;
		private final boolean reflectionEnabled;
		private final boolean invertFade;
		
		public GlyphItemFactory(boolean reflectionEnabled, boolean invertFade) {
			super();
			this.modelKey = getModelKey();
			this.reflectionEnabled = reflectionEnabled;
			this.invertFade = invertFade;
		}
		
		protected void finalize() throws Throwable {
			try {
				ModelRegistry.getModel(getModelKey()).clearObjects();
				ModelRegistry.deRegister(getModelKey());
			} catch (GraphicsException e) {
				e.printStackTrace();
			}
		};

		public final String getModelKey() {
			return String.valueOf(hashCode());
		}

		@Override
		public GlyphItemView createMenuItem() {
			GlyphItemView item = new GlyphItemView(reflectionEnabled, modelKey, invertFade);
			item.show();
			return item;
		}

		@Override
		public void init() {
			if(ModelRegistry.isRegistered(modelKey)) {
				return;
			}
			
			try {
				Model3D model = new PlaneModel();
				model.disableCullFace();
				ModelRegistry.registerModel(modelKey, model);
			} catch (GraphicsException e) {
				e.printStackTrace();
			}
		}
	}