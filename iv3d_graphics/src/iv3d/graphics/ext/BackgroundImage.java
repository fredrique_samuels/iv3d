package iv3d.graphics.ext;

import iv3d.base.ImageSource;
import iv3d.base.exceptions.GraphicsException;
import iv3d.graphics.Camera;
import iv3d.graphics.Entity;
import iv3d.graphics.GraphicsContext;
import iv3d.graphics.GraphicsUtils;
import iv3d.graphics.TextureUtils;

public class BackgroundImage extends Entity {
	public BackgroundImage(Camera camera, String imageFile) {
		GraphicsContext context = GraphicsContext.getInstance();
		int screenWidth = context.getScreenWidth();
		double x = screenWidth*.5;
		float angle = camera.getAngle();
		double theta = Math.sin(Math.toRadians(angle*.5));
		float bgDistance = (float) (( x / theta ) * Math.sin(Math.toRadians(angle*.5)));
		
		ImageSource readImageFromFile=null;
		try {
			readImageFromFile = GraphicsUtils.readImageFromFile(imageFile,
						TextureUtils.TEXTURE_OPTION_NONE);
		} catch (GraphicsException e1) {
			e1.printStackTrace();
		}
		
		float w = readImageFromFile.getWidth();
		float h = readImageFromFile.getHeight();
		float ar = w / h;
		
		setPosition(0, 0, -bgDistance);
		this.setTexture(readImageFromFile);
		this.setScale(ar * h, h, 1);
		this.setProperty("alpha", java.lang.Float.valueOf(1.0f));
	}
}
