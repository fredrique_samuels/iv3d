package iv3d.graphics.ext;

import iv3d.base.ext.groups.Item;
import iv3d.base.ext.groups.ItemFactory;
import iv3d.base.ext.groups.ItemGroup;
import iv3d.base.ext.groups.ItemTransform;
import iv3d.base.ext.groups.ItemView;

import java.util.LinkedList;
import java.util.List;

public class AnimatedItemDisplay<ItemType extends Item, ItemViewType extends ItemView<ItemType>> extends ItemGroup<ItemType, ItemViewType> {
	private final ItemTransform[] activeTransforms;
	private final ItemTransform[] fadeOutTransforms;
	private final ItemTransform[] fadeInTransforms;
	private final LinkedList<ItemType> values;

	public AnimatedItemDisplay(ItemTransform[] activeItems, 
			ItemTransform fadeInOffset,
			ItemTransform fadeOutOffset,
			ItemFactory<ItemViewType> factory) {
		super(200L, buildAllTransforms(activeItems, fadeOutOffset), factory);
		values = new LinkedList<ItemType>();
		this.activeTransforms = activeItems;
		this.fadeInTransforms = offsetTransforms(activeItems, fadeInOffset);
		this.fadeOutTransforms = offsetTransforms(activeItems, fadeOutOffset);
	}
	
	private static ItemTransform[] offsetTransforms(ItemTransform[] activeItems,
			ItemTransform fadeOffset) {
		ItemTransform[] result = new ItemTransform[activeItems.length];
		for (int i = 0; i < activeItems.length; i++) {
			result[i] = activeItems[i].offset(fadeOffset);
		}
		return result;
	}

	private static ItemTransform[] buildAllTransforms(
			ItemTransform[] activeItems, ItemTransform fadeOffset) {
		ItemTransform[] result = new ItemTransform[activeItems.length*2];
		ItemTransform[] fadeTransforms = offsetTransforms(activeItems, fadeOffset);
		java.lang.System.arraycopy(activeItems, 0, result, 0, activeItems.length);
		java.lang.System.arraycopy(fadeTransforms, 0, result, activeItems.length, activeItems.length);
		return result;
	}
	
	public final void add(ItemType item){
		values.add(item);
		
		int valueCount = values.size();
		if(valueCount > activeTransforms.length) {
			return;
		}
		
		int itemIndex = valueCount-1;
		
		setItemValue(itemIndex, item);
		animateItem(itemIndex, fadeInTransforms[itemIndex], activeTransforms[itemIndex]);
	}
	
	public final void remove(ItemType item) {
		if(!values.contains(item)) {
			return;
		}
		
		int index = values.indexOf(item);
		ItemTransform[] currentItemTransforms = getCurrentItemTransforms();
		
		// remove item
		values.remove(item);
		
		// fade out index
		int fadeOutItemIndex = getFadeOutItemIndex(index);
		setItemValue(fadeOutItemIndex, item);
		animateItem(fadeOutItemIndex, currentItemTransforms[index], fadeOutTransforms[index]);
		
		// If the item is the last one
		setValues();
		
		//hide last tile
		int lastIndex = getBorderIndex();
		setItemTransform(lastIndex, ItemTransform.NOT_VISIBLE);
		
		// set positions
		for(int i=index;i<getBorderIndex(); ++i) {
			if(i+1==activeTransforms.length) {
				// fade in new last item if any
				setItemValue(i, values.get(i));
				animateItem(i, fadeInTransforms[i], activeTransforms[i]);
			} else {
				ItemTransform transform = currentItemTransforms[i+1];
				animateItem(i, transform, activeTransforms[i]);				
			}
		}
	}

	private int getBorderIndex() {
		return values.size()<activeTransforms.length?values.size():activeTransforms.length;
	}

	private void setValues() {		
		List<ItemType> subList = values.subList(0, getBorderIndex());
		for(int i=0;i<subList.size();++i) {
			setItemValue(i, subList.get(i));
		}
	}

	private int getFadeOutItemIndex(int index) {
		return activeTransforms.length + index;
	}
}
