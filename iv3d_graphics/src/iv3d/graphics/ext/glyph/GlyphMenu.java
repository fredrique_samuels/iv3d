package iv3d.graphics.ext.glyph;

import iv3d.base.LinkedSelectionList;
import iv3d.base.exceptions.GraphicsException;
import iv3d.base.ext.groups.ItemGroup;
import iv3d.base.ext.groups.ItemTransform;
import iv3d.graphics.Model3D;
import iv3d.graphics.ext.items.glyph.GlyphItem;
import iv3d.graphics.ext.items.glyph.GlyphItemFactory;
import iv3d.graphics.ext.items.glyph.GlyphItemView;
import iv3d.graphics.models.ModelRegistry;

public class GlyphMenu extends ItemGroup<GlyphItem, GlyphItemView>{
 
	private final int targetItemIndex;
	private final ItemTransform[] transforms;
	private final LinkedSelectionList<GlyphItem> model;
	
	public GlyphMenu(ItemTransform[] transforms, 
			int selectionItemIndex, 
			final boolean enableReflections,
			final boolean invertFade,
			final long animationSpeed) {
		super(animationSpeed, transforms, new GlyphItemFactory(enableReflections, invertFade));
		model = new LinkedSelectionList<GlyphItem>();
		this.targetItemIndex = selectionItemIndex; 
		this.transforms = transforms;
	}
	
	public final Model3D toModel() {
		GlyphItemFactory factory = (GlyphItemFactory)getView().getFactory();
		try {
			return ModelRegistry.getModel(factory.getModelKey());
		} catch (GraphicsException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public final void add(GlyphItem  item) {
		setTransforms(transforms);
	}

	public final void setSelected(int i) {
		model.setSelectedIndex(i);
		refresh();
	}

	private void refresh() {
		GlyphItem[] values = getValues();
		setItemValues(values);
		resetPositions();
	}

	private GlyphItem[] getValues() {
		GlyphItem[] values = new GlyphItem[transforms.length];
		for (int i=0;i<transforms.length;++i) {
			int valueIndex = i-targetItemIndex+model.selectedIndex();
			if(valueIndex<0 || valueIndex>=model.size()) {
				values[i] = null;
			} else {
				values[i] = model.get(valueIndex);
			}
		}
		return values;
	}

	private void resetPositions() {
		ItemTransform[] settings = getTargetTransforms();
		setTransforms(settings);
	}

	private ItemTransform[] getTargetTransforms() {
		GlyphItem[] values = getValues();
		ItemTransform[] settings = new ItemTransform[transforms.length];
		for (int i=0;i<transforms.length;++i) {
			settings[i] = values[i]==null?new ItemTransform(transforms[i], 0) : transforms[i];
		}
		return settings;
	}
	
	public final void next() { 
		if (!model.hasNext()) {
			return;
		}
		
		model.next();
		
		// set values to new ones
		GlyphItem[] values = getValues();
		setItemValues(values);
		
//		refresh();
		
		ItemTransform[] currentItemTransforms = getCurrentItemTransforms();
		setTransforms(1, currentItemTransforms);
		animateTransforms(getTargetTransforms());
	}
	
	public final void prev() { 
		if (!model.hasPrevious()) {
			return;
		}
		
		model.prev();
		
		// set values to new ones
		GlyphItem[] values = getValues();
		setItemValues(values);
//		refresh();
		
		ItemTransform[] currentItemTransforms = getCurrentItemTransforms();
		setTransforms(-1, currentItemTransforms);
		animateTransforms(getTargetTransforms());
	}

	public final void addItem(GlyphItem item) {
		model.add(item);
		refresh();
	}

	public final boolean hasNext() {
		return model.hasNext();
	}

	public final boolean hasPrevious() {
		return model.hasPrevious();
	}
}
