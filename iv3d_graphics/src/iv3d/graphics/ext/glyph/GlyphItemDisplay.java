package iv3d.graphics.ext.glyph;

import iv3d.base.exceptions.GraphicsException;
import iv3d.base.ext.groups.ItemTransform;
import iv3d.graphics.Model3D;
import iv3d.graphics.ext.AnimatedItemDisplay;
import iv3d.graphics.ext.items.glyph.GlyphItem;
import iv3d.graphics.ext.items.glyph.GlyphItemFactory;
import iv3d.graphics.ext.items.glyph.GlyphItemView;
import iv3d.graphics.models.ModelRegistry;

public class GlyphItemDisplay extends AnimatedItemDisplay<GlyphItem, GlyphItemView> {

	public GlyphItemDisplay(ItemTransform[] activeItems,
			ItemTransform fadeInOffset, ItemTransform fadeOutOffset, 
			boolean reflectionEnabled, boolean invertFade) {
		super(activeItems, fadeInOffset, fadeOutOffset, 
				 new GlyphItemFactory(reflectionEnabled, invertFade));
	}

	public final Model3D toModel() {
		GlyphItemFactory factory = (GlyphItemFactory)getView().getFactory();
		try {
			return ModelRegistry.getModel(factory.getModelKey());
		} catch (GraphicsException e) {
			e.printStackTrace();
		}
		return null;
	}
}
