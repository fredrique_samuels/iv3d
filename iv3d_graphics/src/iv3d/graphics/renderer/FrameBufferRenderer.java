/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.renderer;

import iv3d.base.Bounds;
import iv3d.base.Color;
import iv3d.base.Dimension;
import iv3d.base.Rectangle;
import iv3d.graphics.Camera;
import iv3d.graphics.FrameBuffer;
import iv3d.graphics.GraphicsContext;
import iv3d.graphics.GraphicsUtils;
import iv3d.graphics.Model;
import iv3d.graphics.Renderer;
import iv3d.graphics.Scene;

import java.util.Iterator;



public class FrameBufferRenderer extends Renderer {

	public FrameBufferRenderer(GraphicsContext gl) {
		super(gl);
	}

	@Override
	protected void postRender() {
		
	}

	@Override
	protected void preRender() {
		
	}

	@Override
	protected boolean render(GraphicsContext gl, Scene scene) {
		if(!scene.canRenderToImage()) {
			return true;
		}
		
		FrameBuffer frameBuffer = scene.getFrameBuffer();

		int origWidth = gl.getScreenWidth();
        int origHeight = gl.getScreenHeight();
        Dimension screenSize = gl.getScreenSize();
        Dimension frameSize = frameBuffer.getDimensions();
        
        // Fit
        Dimension scaleToFit = Dimension.scaleToFit(screenSize, frameSize);
        
        // Find new screen location.
        Rectangle newScreen = null;
        if(scaleToFit.getWidth()==frameSize.getWidth()) {        	
        	newScreen = new Rectangle(0, (int) Math.round(0.5*(frameSize.getHeight()-scaleToFit.getHeight())), scaleToFit.getWidth(), scaleToFit.getHeight()); 
        } else {	
        	newScreen = new Rectangle((int) Math.round(0.5*(frameSize.getWidth()-scaleToFit.getWidth())), 0, scaleToFit.getWidth(), scaleToFit.getHeight()); 
        }
        
        frameBuffer.setUsedArea(newScreen);
        frameBuffer.bind();
		GraphicsUtils.setClearColor(scene.getBackgroundColor());
		GraphicsUtils.clearColorAndDepth();
		
		Iterator<Camera> camIter = scene.getCameraIterator();
		while (camIter.hasNext()) {
			gl.setScreenSize(frameSize);

			Camera camera = camIter.next();
			Bounds oldViewport = camera.getViewport();
			
	        // find viewport area.
			int camScreenWidth = (int) (newScreen.getWidth()*oldViewport.getWidth());
			int camScreenHeight = (int) (newScreen.getHeight()*oldViewport.getHeight());
			int camScreenX = newScreen.getX()+(int) (newScreen.getWidth()*oldViewport.getX());
			int camScreenY = newScreen.getY()+(int) (newScreen.getHeight()*oldViewport.getY());
			
			// find new viewport
			float vpX = (float)camScreenX/frameSize.getWidth();
			float vpY = (float)camScreenY/frameSize.getHeight();
			float vpW = (float)camScreenWidth/frameSize.getWidth();
			float vpH = (float)camScreenHeight/frameSize.getHeight();
			
			camera.setViewport(new Bounds(vpX, vpY, vpW, vpH));
			camera.updateProjectionMatrix();
			camera.bind(gl);

			Iterator<Model> modelIter = scene.getContentManager().getModelIterator();
			while (modelIter.hasNext()) {
				Model model = modelIter.next();
				model.render(gl);
			}
			
			gl.setScreenSize(origWidth, origHeight);
			camera.setViewport(oldViewport);
			camera.updateProjectionMatrix();
		}
		
		FrameBuffer.unbind();
		GraphicsUtils.setClearColor(Color.BLACK);
        
		return true;
	}

}
