/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.renderer;

import iv3d.base.Color;
import iv3d.base.Rectangle;
import iv3d.base.events.EventReceiver;
import iv3d.base.events.SelectionInfo;
import iv3d.base.exceptions.GraphicsException;
import iv3d.base.math.MathUtils;
import iv3d.base.math.Matrix44;
import iv3d.base.math.Vector3;
import iv3d.graphics.Camera;
import iv3d.graphics.EntityRenderer;
import iv3d.graphics.GraphicsContext;
import iv3d.graphics.GraphicsUtils;
import iv3d.graphics.IndexData;
import iv3d.graphics.Model;
import iv3d.graphics.RenderTarget;
import iv3d.graphics.Renderer;
import iv3d.graphics.Scene;
import iv3d.graphics.Shader;
import iv3d.graphics.Camera.CameraMode;

import java.util.Iterator;

public class PickRenderer extends Renderer {
	private static final String SHADER_VERT = ""
			+ "uniform mat4 cg_ProjectionMatrix;"
			+ "uniform mat4 cg_ModelViewMatrix;"
			+ "attribute vec4 cg_Vertex;"
			+ "void main() {"
			+ "    gl_Position = cg_ProjectionMatrix*cg_ModelViewMatrix*cg_Vertex;"
			+ "}";
	private static final String WHITE_SHADER_FRAG = "void main() {gl_FragColor = vec4(1,1,1,1);}";
	private static final String BLACK_SHADER_FRAG = "void main() {gl_FragColor = vec4(0,0,0,1);}";
	private static Shader whiteShader;
	private static Shader blackShader;
	private int mouseX;
	private int mouseY;
	private SelectionInfo pick;
	private EntityRenderer renderer;

	public PickRenderer(GraphicsContext gl) {
		super(gl);
		enableDecendingScenes();
		setupShaders();
		mouseX = -1;
		mouseY = -1;
	}

	private void setupShaders() {
		try {
			if (whiteShader == null) {
				whiteShader = new Shader();
				whiteShader.loadFromSource(SHADER_VERT, WHITE_SHADER_FRAG);
			}
			if (blackShader == null) {
				blackShader = new Shader();
				blackShader.loadFromSource(SHADER_VERT, BLACK_SHADER_FRAG);
			}
		} catch (GraphicsException e) {
			e.printStackTrace();
		}
	}

	public void setMousePosition(int x, int y) {
		mouseX = x;
		mouseY = y;
	}

	@Override
	protected boolean render(GraphicsContext gl, Scene scene) {
		if(!scene.canRenderToScreen()) {
			return true;
		}
		
		Iterator<Camera> camIter = scene.getCameraIterator();
		while (camIter.hasNext()) {
			Camera camera = camIter.next();
			camera.bind(gl);

			Iterator<Model> modelIter = scene.getContentManager()
					.getModelIterator();
			while (modelIter.hasNext()) {
				Model model = modelIter.next();
				model.render(gl, renderer);
			}
		}
		if (pick != null) {
			return false;
		}
		return true;
	}

	@Override
	protected void preRender() {
		GraphicsUtils.setClearColor(.5f, .5f, .5f, 1);
		GraphicsUtils.clearColorAndDepth();
		pick = null;
		renderer = pickRenderer;
		enableWhiteShader();
	}

	@Override
	protected void postRender() {
		Shader.unlock();
		Shader.unbind();
		GraphicsUtils.setClearColor(Color.BLACK);
		if (pick == null) {
			setPick(null);
		}
	}

	public SelectionInfo getPickResult() {
		return pick;
	}

	private void checkForWhite(EventReceiver entity) {
		Color color = getPixelColor();
		if (Color.WHITE.equals(color)) {
			setPick(entity);
			enableBlackShader();
		}
	}

	protected void checkForBlack(EventReceiver entity) {
		Color color = getPixelColor();
		if (Color.BLACK.equals(color)) {
			setPick(entity);
			enableWhiteShader();
		}
	}

	private void enableWhiteShader() {
		Shader.unlock();
		whiteShader.bind();
		Shader.lock();
	}

	private void enableBlackShader() {
		Shader.unlock();
		blackShader.bind();
		Shader.lock();
	}

	private Color getPixelColor() {
		Color color = GraphicsUtils.getReadPixelAt(mouseX, mouseY);
		return color;
	}

	private void setPick(EventReceiver entity) {

		if (entity != null) {
			GraphicsContext graphics = getGraphics();
			Camera camera = graphics.getCamera();
			Rectangle viewport = camera.getViewport(graphics.getScreenWidth(),
					graphics.getScreenHeight());
			Matrix44 modelMatrix = camera.getLookAtMatrix();
			Matrix44 projMatrix = camera.getProjectionMatrix();

			int trueMouseY = camera.getMode() == CameraMode.PERSPECTIVE ? 
					viewport.getHeight() - mouseY : mouseY;
			float winz = GraphicsUtils.getDepthAt(mouseX, trueMouseY);
			Vector3 position = MathUtils.unProject(mouseX, trueMouseY, winz,
					modelMatrix, projMatrix, viewport);
			pick = new SelectionInfo(entity, position, modelMatrix, projMatrix,
					Matrix44.multiply(projMatrix, modelMatrix));
		} else {
			pick = new SelectionInfo(null, new Vector3(), new Matrix44(),
					new Matrix44(), new Matrix44());
		}
	}

	EntityRenderer pickRenderer = new EntityRenderer() {

		@Override
		public void render(RenderTarget target, IndexData data) {
			if(target==null) {
				return;
			}
			
			if (!target.isEventsEnabled()) {
				return;
			}
			
			EventReceiver eventReceiver = target.asEventReceiver();
			if(eventReceiver==null) {
				return;
			}
			
			data.render();
			if (Shader.getActiveShader() == blackShader) {
				checkForBlack(eventReceiver);
			} else {
				checkForWhite(eventReceiver);
			}
		}
	};
}
