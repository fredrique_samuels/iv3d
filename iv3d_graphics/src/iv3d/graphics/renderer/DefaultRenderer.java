/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.renderer;


import iv3d.graphics.Camera;
import iv3d.graphics.EntityRenderer;
import iv3d.graphics.FrameBuffer;
import iv3d.graphics.GraphicsContext;
import iv3d.graphics.GraphicsUtils;
import iv3d.graphics.IndexData;
import iv3d.graphics.Light;
import iv3d.graphics.Model;
import iv3d.graphics.RenderTarget;
import iv3d.graphics.Renderer;
import iv3d.graphics.Scene;
import iv3d.graphics.Shader;
import iv3d.graphics.shaders.ShaderRegistry;
import iv3d.graphics.shaders.store.SHADER_RED;

import java.util.Iterator;


public class DefaultRenderer extends Renderer {

	public DefaultRenderer(GraphicsContext gl) {
		super(gl);
	}

	@Override
	protected boolean render(GraphicsContext gl, Scene scene) {
		if (!scene.canRenderToScreen()) {
			return true;
		}
		renderShadowBuffersForLights(gl, scene);
		renderFinal(gl, scene);
		return true;
	}

	private void renderShadowBuffersForLights(GraphicsContext gl, Scene scene) {
		Shader easyShader = ShaderRegistry.getShader(SHADER_RED.ID);
		Shader.unlock();
		easyShader.bind();
		Shader.lock();
		
		for(int i=0;i<gl.getLightCount();++i) {
			Light light = gl.getLight(i);
			if(!light.isShadowEnabled()) {
				continue;
			}
			
			FrameBuffer frameBuffer = light.getFrameBuffer();
			renderShadowScene(gl, scene, light, frameBuffer);
		}
		
		Shader.unlock();
		Shader.unbind();
		
	}

	private void renderShadowScene(GraphicsContext gl, Scene scene, Camera camera,
			FrameBuffer frameBuffer) {

		int origWidth = gl.getScreenWidth();
        int origHeight = gl.getScreenHeight();
        gl.setScreenSize(frameBuffer.getWidth(), frameBuffer.getHeight());
		CastShadowRenderer renderer = new CastShadowRenderer();
		
        frameBuffer.bind();
		GraphicsUtils.clearColorAndDepth();
		
		camera.bind(gl);
		camera.updateProjectionMatrix();

		Iterator<Model> modelIter = scene.getContentManager().getModelIterator();
		while (modelIter.hasNext()) {
			Model model = modelIter.next();
			model.render(gl, renderer);
		}
		
		FrameBuffer.unbind();
        gl.setScreenSize(origWidth, origHeight);
		camera.updateProjectionMatrix();
	}

	private void renderFinal(GraphicsContext gl, Scene scene) {
		Iterator<Camera> camIter = scene.getCameraIterator();
		while (camIter.hasNext()) {
			GraphicsUtils.clearDepth();
			Camera camera = camIter.next();
			camera.bind(gl);

			Iterator<Model> modelIter = scene.getContentManager().getModelIterator();
			while (modelIter.hasNext()) {
				Model model = modelIter.next();
				model.render(gl);
			}
		}
	}

	@Override
	protected void preRender() {
		GraphicsUtils.clearColorAndDepth();
	}

	@Override
	protected void postRender() {
	}
	
	static class CastShadowRenderer implements EntityRenderer {

		@Override
		public void render(RenderTarget entity, IndexData data) {
			if(!entity.castShadowEnabled()) {
				return;
			}
			data.render();
		}
	}

}
