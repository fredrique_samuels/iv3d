/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import iv3d.base.Dimension;
import iv3d.base.Rectangle;
import iv3d.base.animation.AnimationManager;
import iv3d.base.events.Event;
import iv3d.base.events.EventManager;
import iv3d.base.events.SelectionInfo;
import iv3d.graphics.GraphicsUtils;
import iv3d.graphics.renderer.DefaultRenderer;
import iv3d.graphics.renderer.FrameBufferRenderer;
import iv3d.graphics.renderer.PickRenderer;
import iv3d.graphics.shaders.ShaderRegistry;

import java.util.List;


public class GraphicsContext {

	private Camera camera;
	private int width;
	private int height;
	private DefaultRenderer defaultRenderer;
	private PickRenderer pickRenderer;
	private FrameBufferRenderer framebufferRenderer;
	private EventManager eventManager;
	private Light[] lights;
	
	private static GraphicsContext instance;

	public static GraphicsContext getInstance() {
		if(instance==null) {
			instance = new GraphicsContext();
		}
		return instance;
	}
	
	private GraphicsContext() {
		super();
	}
	
	public void init(int width, int height) {
		eventManager = new EventManager();
		defaultRenderer = new DefaultRenderer(this);
		pickRenderer = new PickRenderer(this);
		framebufferRenderer = new FrameBufferRenderer(this);
		
		GraphicsUtils.init();
		AnimationManager.start();
		ShaderRegistry.init();
		setScreenSize(width, height);
		GraphicsUtils.setClearColor(1.0f, 1.0f, .5f, 1.0f);
	}

	public Shader getShader() {
		return Shader.getActiveShader();
	}

	public Light getLight(int i) {
		if (lights == null) {
			return null;
		}
		if (i >= lights.length) {
			return null;
		}
		return lights[i];
	}

	public int getLightCount() {
		return lights == null ? 0 : lights.length;
	}

	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	public Camera getCamera() {
		return camera;
	}

	public void setScreenSize(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	public void setScreenSize(Dimension size) {
		setScreenSize(size.getWidth(), size.getHeight());
	}

	public int getScreenWidth() {
		return width;
	}

	public int getScreenHeight() {
		return height;
	}
	
	public Dimension getScreenSize() {
		return new Dimension(width, height);
	}

	public void render(List<Event> events) {
		SceneManager.update();
		processEvents(events);
		framebufferRenderer.render();
		defaultRenderer.render();
	}

	private void processEvents(List<Event> events) {
		if (events.size() > 0) {
			pickRenderer.setMousePosition(events.get(0).getX(), 
					events.get(0).getY());
		}
		pickRenderer.render();
		SelectionInfo pickResult = pickRenderer.getPickResult();
		for (Event event : events) {
			eventManager.processEvent(event, pickResult);
		}
	}

	public void setLights(Light[] lights) {
		this.lights = lights;
	}

	public Rectangle getViewport() {
		return new Rectangle(0,0,width, height);
	}

}
