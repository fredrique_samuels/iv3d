/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.models;

import iv3d.graphics.Material;
import iv3d.graphics.Model3D;
import iv3d.graphics.Object3D;
import iv3d.graphics.RenderMode;

public class PlaneModel extends Model3D {
	
	public static final String ID = "iv3d.graphics.models.PlaneModel";
	
	public static final String MATERIAL_DEFAULT_ID = "default";

	public PlaneModel() {
		super();

		float[] verts = new float[] { -.5f, -.5f, 0, .5f, -.5f, 0, .5f, .5f, 0,
				-.5f, .5f, 0 };
		float[] normals = new float[] { 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1 };
		float[] texcoords = new float[] { 0, 1, 1, 1, 1, 0, 0, 0 };
		int[] indices = new int[] { 3, 2, 1, 0 };
		
		Material material = createMaterial(MATERIAL_DEFAULT_ID);
		Object3D model = createMesh("default");
		model.setIndexData(material, indices, RenderMode.QUADS);
		model.setData(verts, texcoords, normals);
	}
}
