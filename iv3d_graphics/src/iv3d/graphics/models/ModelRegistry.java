/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.models;

import java.util.HashMap;
import java.util.Map;

import iv3d.base.exceptions.GraphicsException;
import iv3d.graphics.Model3D;

/**
 * A register for Model objects. Models are indexed by a unique String.
 */
public class ModelRegistry {
	
	private static Map<String, Model3D> registry = new HashMap<String, Model3D>();
	
	private ModelRegistry() {
	}
	
	
	/**
	 * Add a model to the register.
	 * 
	 * @param modelId The model id.
	 * @param model The model to be registered.
	 * @throws GraphicsException If the modelid ia already in use.
	 */
	public static void registerModel(String modelId, Model3D model) throws GraphicsException {
		synchronized (registry) {			
			if(registry.containsKey(modelId)) {
				throw new GraphicsException("ModelId '" +modelId+ "' is already registered!" );
			}
			
			registry.put(modelId, model);
		}
	}
	
	/**
	 * Remove a model from the register. All entity will be 
	 * removed from the model by this operation.
	 * 
	 * @param modelId The model ID to be removed.
	 */
	public  static void deRegister(String modelId) {
		synchronized (registry) {
			if(!registry.containsKey(modelId)) {
				return;
			}		
			Model3D model = registry.get(modelId);
			registry.remove(modelId);
			model.unbindAll();					
		}
	}
	
	/**
	 * Get the model by it's modelId.
	 * 
	 * @param modelId The modelId to index by.
	 * @return The model.
	 * @throws GraphicsException If no model can be located for the modelId. 
	 */
	public static Model3D getModel(String modelId) throws GraphicsException {
		Model3D model = null;
		synchronized (registry) {
			if(registry.containsKey(modelId)) {
				model = registry.get(modelId);
			}	
		}
		if(model==null) {
			throw new GraphicsException("ModelId '" +modelId+ "' is not registered!" );
		}
		return model;
	}

	/**
	 * @param modelId The model ID to check for.
	 * @return <code>true</code> if the ID is already registered.
	 */
	public static boolean isRegistered(String modelId) {
		return registry.containsKey(modelId);
	}
}
