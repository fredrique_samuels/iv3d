/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.models;

import iv3d.base.math.Matrix44;
import iv3d.graphics.Camera;
import iv3d.graphics.EntityRenderer;
import iv3d.graphics.GraphicsContext;
import iv3d.graphics.GraphicsUtils;
import iv3d.graphics.IndexData;
import iv3d.graphics.Model;
import iv3d.graphics.RenderMode;
import iv3d.graphics.RenderTarget;
import iv3d.graphics.Shader;
import iv3d.graphics.shaders.ShaderProperty;
import iv3d.graphics.shaders.ShaderRegistry;
import iv3d.graphics.shaders.store.SHADER_C;

import java.nio.FloatBuffer;
import java.util.LinkedList;
import java.util.List;

public class LineModel extends Model {
	
	public static final String ID = "iv3d.graphics.models.LineModel";
	private static final DefaultEntityRenderer defaultRenderer = new DefaultEntityRenderer();
	private static FloatBuffer modelViewBuffer;
	private Shader shader;
	private final IndexData indexVbo;
	private final List<LineEntity> entities;
	
	public LineModel() {
		super();
		indexVbo = new IndexData();
		indexVbo.set(new int[]{0, 1}, RenderMode.LINES);
		
		shader = ShaderRegistry.getShader(SHADER_C.ID);
		
		entities = new LinkedList<LineEntity>();
	}

	@Override
	public void render(GraphicsContext gl) {
		render(gl, defaultRenderer);
	}
	
	public LineModel bindEntity(LineEntity... newEntities) {
		for (LineEntity e : newEntities) {
			if(!entities.contains(e)) {
				entities.add(e);
			}
		}
		return this;
	}
	
	public LineModel unbindEntity(LineEntity entity) {
		if(entities.contains(entity)) {
			entities.remove(entity);
		}
		return this;
	}
	
	public void render(GraphicsContext gl, EntityRenderer entityRenderer) {
		shader.bind();
		GraphicsUtils.disableCullFace();
		updateMatrices(gl);

		for (LineEntity e : entities) {
			e.bind(gl);
			entityRenderer.render(null, indexVbo);
		}
	}
	
	private void updateMatrices(GraphicsContext gl) {
		Shader shader = gl.getShader();
		Camera camera = gl.getCamera();
		
		Matrix44 mv = camera.getLookAtMatrix();
		setModelViewBuffer(mv.toArray());
		
		shader.setProperty(ShaderProperty.MODEL_VIEW_MATRIX, getModelViewBuffer());
		shader.setProperty(ShaderProperty.PROJECTION_MATRIX, camera.getProjectionBuffer());

		if (shader.hasProperty(ShaderProperty.CAMERA_MATRIX.getName())) {
			shader.setProperty(ShaderProperty.CAMERA_MATRIX, camera
					.getLookAtBuffer());
		}
		
		if (shader.hasProperty(ShaderProperty.CAMERA_MATRIX_I.getName())) {
			shader.setProperty(ShaderProperty.CAMERA_MATRIX_I, camera.getLookAtBufferI());
		}
	}
	
	private static FloatBuffer getModelViewBuffer() {
		if (modelViewBuffer==null) {
			modelViewBuffer = GraphicsUtils.asFloatBuffer(new float[16]);
		}
		return modelViewBuffer; 
	}
	
	private static void setModelViewBuffer(float[] value) {
		FloatBuffer buffer = getModelViewBuffer();
		buffer.put(value);
		buffer.flip();
	}
	
	static class DefaultEntityRenderer implements EntityRenderer {

		@Override
		public void render(RenderTarget entity, IndexData data) {
			data.render();
		}
	}
	
}
