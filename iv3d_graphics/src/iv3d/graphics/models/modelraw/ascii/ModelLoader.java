/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.models.modelraw.ascii;


import iv3d.base.exceptions.GraphicsException;
import iv3d.base.math.Vector2;
import iv3d.base.math.Vector3;
import iv3d.graphics.GraphicsUtils;
import iv3d.graphics.Material;
import iv3d.graphics.Model3D;
import iv3d.graphics.Object3D;
import iv3d.graphics.RenderMode;
import iv3d.graphics.TextureUtils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;


public class ModelLoader {

	private static enum ReadState {
		VERTEX, NORMALS, TEXCOORD, TEXTURE, INDEX
	}

	private ModelLoader() {

	}
	
	public static Model3D load(String filename) throws GraphicsException {
		InputStream stream = GraphicsUtils.readFileAsInputStream(filename);
		return load(stream);
	}

	public static Model3D load(InputStream stream) throws GraphicsException {

		ReadState readState = null;
		List<Mesh> meshList = new LinkedList<Mesh>();
		Mesh mesh = null;
		MaterialGroup materialGroup = null;

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					stream, "UTF-8"));
			String line;
			while ((line = reader.readLine()) != null) {
				if (line.length() == 0) {
					continue;
				}


				if (line.startsWith("#")) {
					if ("#Mesh".equals(line)) {
						mesh = new Mesh();
						meshList.add(mesh);
						continue;
					} else if ("#Verts".equals(line)) {
						readState = ReadState.VERTEX;
						continue;
					} else if ("#Normals".equals(line)) {
						readState = ReadState.NORMALS;
						continue;
					} else if ("#Texc".equals(line)) {
						readState = ReadState.TEXCOORD;
						continue;
					} else if ("#MatGroup".equals(line)) {
						materialGroup = new MaterialGroup();
						mesh.materialGroups.add(materialGroup);
						continue;
					} else if ("#Texture".equals(line)) {
						readState = ReadState.TEXTURE;
						continue;
					} else if ("#Index".equals(line)) {
						readState = ReadState.INDEX;
						continue;
					}
				}

				if (readState == ReadState.VERTEX) {
					String[] values = line.split(" ");
					Vector3 vector3 = new Vector3(Float.valueOf(values[0]),
							Float.valueOf(values[1]), Float.valueOf(values[2]));
					mesh.verts.add(vector3);
				} else if (readState == ReadState.NORMALS) {
					String[] values = line.split(" ");
					Vector3 vector3 = new Vector3(Float.valueOf(values[0]),
							Float.valueOf(values[1]), Float.valueOf(values[2]));
					mesh.norms.add(vector3);
				} else if (readState == ReadState.TEXCOORD) {
					String[] values = line.split(" ");
					Vector2 vector2 = new Vector2(Float.valueOf(values[0]),
							Float.valueOf(values[1]));
					mesh.texc.add(vector2);
				} else if (readState == ReadState.TEXTURE) {
					materialGroup.texture = line;
				} else if (readState == ReadState.INDEX) {
					materialGroup.indices.add(Integer.valueOf(line));
				}

			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return createModel(meshList);
	}

	private static Model3D createModel(List<Mesh> meshList)
			throws GraphicsException {
		Model3D modelView = new Model3D();
		Object3D model = modelView.createMesh("default");
		System.out.println("Mesh Count " + meshList.size());
		
		int totalCount = 0;
		for (Mesh mesh : meshList) {
			totalCount += mesh.verts.size();
			System.out.println("Vert Count " + mesh.verts.size());
			System.out.println("Texc Count " + mesh.texc.size());
			System.out.println("Norms Count " + mesh.norms.size());

			for (MaterialGroup group : mesh.materialGroups) {
				System.out.println("Index Count " + group.indices.size());
				System.out.println("Image " + group.texture);
			}
		}


		int offset = 0;
		float[] verts = new float[totalCount * 3];
		float[] normals = new float[totalCount * 3];
		float[] texcoords = new float[totalCount * 2];

		int index = 0;
		for (Mesh mesh : meshList) {
			for (Vector3 v : mesh.verts) {
				verts[(offset + index) * 3] = v.getX();
				verts[((offset + index) * 3) + 1] = v.getZ();
				verts[((offset + index) * 3) + 2] = v.getY();
				index++;
			}

			index = 0;
			for (Vector3 v : mesh.norms) {
				normals[(offset + index) * 3] = v.getX();
				normals[((offset + index) * 3) + 1] = v.getY();
				normals[((offset + index) * 3) + 2] = v.getZ();
				index++;
			}

			index = 0;
			for (Vector2 v : mesh.texc) {
				texcoords[(offset + index) * 2] = v.getX();
				texcoords[((offset + index) * 2) + 1] = v.getY();
				index++;
			}

			
			
			for (MaterialGroup group : mesh.materialGroups) {
				Material material = modelView.createMaterial("material"
						+ modelView.getMaterialNames().size());
				int[] indices = new int[group.indices.size()];
				int j=0;
				for(Integer value:group.indices) {
					indices[j]=offset+value.intValue();
					++j;
				}
				model.setIndexData(material, indices, RenderMode.TRIANGLES);
				if (group.texture != null) {
					material.setTexture(group.texture, TextureUtils.TEXTURE_OPTION_FILTER);
				}
			}
		}
		
		model.setData(verts, texcoords, normals);
		return modelView;
	}
}
