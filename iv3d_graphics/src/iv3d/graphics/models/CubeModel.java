/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.models;

import iv3d.graphics.Material;
import iv3d.graphics.Model3D;
import iv3d.graphics.Object3D;
import iv3d.graphics.RenderMode;

public class CubeModel extends Model3D {
	
	public static final String ID = "iv3d.graphics.models.CubeModel";
	
	public static final String MATERIAL_SIDE_1_ID = "side1";
	public static final String MATERIAL_SIDE_2_ID = "side2";
	public static final String MATERIAL_SIDE_3_ID = "side3";
	public static final String MATERIAL_SIDE_4_ID = "side4";
	public static final String MATERIAL_SIDE_5_ID = "side5";
	public static final String MATERIAL_SIDE_6_ID = "side6";
	
	public CubeModel() {
		super();

		float dim = .5f;
		float [][] points = new float[][]{new float[]{dim, dim, dim},
				new float[]{dim, -dim, dim},
				new float[]{-dim, -dim, dim},
				new float[]{-dim, dim, dim},
				new float[]{dim, dim, -dim},
				new float[]{dim, -dim, -dim},
				new float[]{-dim, -dim, -dim},
				new float[]{-dim, dim, -dim}}; 
		
		
		float[] verts = new float[72];
		float[] normals = new float[]{0,0,1, 0,0,1, 0,0,1, 0,0,1,
				-1,0,0,-1,0,0,-1,0,0,-1,0,0,
				0,0,-1, 0,0,-1, 0,0,-1, 0,0,-1,
				1,0,0, 1,0,0, 1,0,0, 1,0,0,
				0,1,0, 0,1,0, 0,1,0, 0,1,0,
				0,-1,0, 0,-1,0, 0,-1,0, 0,-1,0};
		float[] texcoords = new float[] { 0, 1, 1, 1, 1, 0, 0, 0,
				0, 1, 1, 1, 1, 0, 0, 0,
				0, 1, 1, 1, 1, 0, 0, 0,
				0, 1, 1, 1, 1, 0, 0, 0,
				0, 1, 1, 1, 1, 0, 0, 0,
				0, 1, 1, 1, 1, 0, 0, 0};
		
		Object3D model = createMesh("default");
		
		{
			setValue(points, verts, 0, 2);
			setValue(points, verts, 1, 1);
			setValue(points, verts, 2, 0);
			setValue(points, verts, 3, 3);
			int[] indices = new int[] { 3, 2, 1, 0};
			Material material = createMaterial(MATERIAL_SIDE_1_ID);
			model.setIndexData(material, indices, RenderMode.QUADS);
		}
		
		{
			setValue(points, verts, 4, 6);
			setValue(points, verts, 5, 2);
			setValue(points, verts, 6, 3);
			setValue(points, verts, 7, 7);
			int[] indices = new int[] {7, 6, 5, 4};
			Material material = createMaterial(MATERIAL_SIDE_2_ID);
			model.setIndexData(material, indices, RenderMode.QUADS);
		}
		
		{
			setValue(points, verts, 8, 5);
			setValue(points, verts, 9, 6);
			setValue(points, verts, 10, 7);
			setValue(points, verts, 11, 4);
			int[] indices = new int[] {11, 10, 9, 8};
			Material material = createMaterial(MATERIAL_SIDE_3_ID);
			model.setIndexData(material, indices, RenderMode.QUADS);
		}
		
		{
			setValue(points, verts, 12, 1);
			setValue(points, verts, 13, 5);
			setValue(points, verts, 14, 4);
			setValue(points, verts, 15, 0);
			int[] indices = new int[] {15, 14, 13 ,12};
			Material material = createMaterial(MATERIAL_SIDE_4_ID);
			model.setIndexData(material, indices, RenderMode.QUADS);
		}
		
		{
			setValue(points, verts, 16, 3);
			setValue(points, verts, 17, 0);
			setValue(points, verts, 18, 4);
			setValue(points, verts, 19, 7);
			int[] indices = new int[] {19 , 18, 17, 16};
			Material material = createMaterial(MATERIAL_SIDE_5_ID);
			model.setIndexData(material, indices, RenderMode.QUADS);
		}
		
		{
			setValue(points, verts, 20, 6);
			setValue(points, verts, 21, 5);
			setValue(points, verts, 22, 1);
			setValue(points, verts, 23, 2);
			int[] indices = new int[] {23, 22, 21, 20};
			Material material = createMaterial(MATERIAL_SIDE_6_ID);
			model.setIndexData(material, indices, RenderMode.QUADS);
		}
		
		model.setData(verts, texcoords, normals);
	}

	private void setValue(float[][] points, float[] verts,
			int offset, int pointIndex) {
		verts[offset*3]=points[pointIndex][0];
		verts[offset*3+1]=points[pointIndex][1];
		verts[offset*3+2]=points[pointIndex][2];
	}
}
