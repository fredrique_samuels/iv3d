/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.models.model3ds;

import iv3d.base.Color;
import iv3d.base.ResourceLoader;
import iv3d.base.exceptions.GraphicsException;
import iv3d.base.math.Vector2;
import iv3d.base.math.Vector3;
import iv3d.graphics.Material;
import iv3d.graphics.Model3D;
import iv3d.graphics.Object3D;
import iv3d.graphics.RenderMode;
import iv3d.graphics.TextureUtils;
import iv3d.graphics.models.ModelUtils;
import iv3d.graphics.models.ModelUtils.ShadeType;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;


public class Model3DSLoader {

	private static Logger LOG = Logger
			.getLogger(Model3DSLoader.class.getName());
	private static final short HEADER = 0x4d4d;
	
	public static Model3D load(String filename) throws GraphicsException {
		LOG.info("Reader Model file : " + filename);
		InputStream stream = ResourceLoader.read(filename);
		if(stream==null) {
			throw new GraphicsException("File not found : " + filename);
		}
		String parentDir = new File(filename).getParentFile().getAbsolutePath();
		return load(stream, parentDir);
	}

	public static Model3D load(InputStream stream, String parentDir) throws GraphicsException {
		Model3D model = new Model3D();
		Material material = null;
		Object3D mesh = null;
		List<Vector3> verts = new LinkedList<Vector3>();
		List<Vector2> texc = new LinkedList<Vector2>();
		
		Chunk chunk;
		try {
			chunk = readChunk(stream);
		} catch (IOException e) {
			throw new GraphicsException(e);
		}

		if (chunk.id != HEADER) {
			LOG.info("Not a Model .3DS model file");
			throw new GraphicsException("Not a .3DS model file stream.");
		}

//		LOG.info("3DS Chunk id=" + chunk.id + " length=" + chunk.length);
		int colorType = 0;
		try {
			while (stream.available() > 0) {
				chunk = readChunk(stream);
				// LOG.info("3DS Chunk id=" + chunk.id + " length="
				// + chunk.length);

				if (chunk.id == 0x3d3d) {
				} else if (chunk.id == 0x3d3e) {
					readInt(stream);
				} else if (chunk.id == 0xffffafff) {
				} else if (chunk.id == 0xffffa000) {
					String matName = readString(stream);
					material = model.createMaterial(matName);
				} else if (chunk.id == 0xffffa200) {
				} else if (chunk.id == 0xffffa010) {
					colorType = 0;
				} else if (chunk.id == 0xffffa020) {
					colorType = 1;
				} else if (chunk.id == 0xffffa030) {
					colorType = 2;
				} else if (chunk.id == 0xffffa040) {
					colorType = 3;
				} else if (chunk.id == 0xffffa050) {
					colorType = 4;
				} else if (chunk.id == 0x0011) {
					byte[] bytes = read(stream, 3);
					Color color = new Color(byteToInt(bytes[0]),
							byteToInt(bytes[1]), byteToInt(bytes[2]), 255);
					if (colorType == 0) {
						material.setAmbient(color);
					} else if (colorType == 1) {
						material.setDiffuse(color);
					} else if (colorType == 2) {
						material.setSpecular(color);
					}
				} else if (chunk.id == 0x0030) {
					readShort(stream);
				} else if (chunk.id == 0xffffa300) {
					String texture = readString(stream);
					String filePath = parentDir + File.separator + texture;
					material.setTexture(filePath, TextureUtils.TEXTURE_OPTION_FILTER);
				} else if (chunk.id == 0x4000) {
					String modelName = readString(stream);
					mesh = model.createMesh(modelName);
					verts.clear();
					texc.clear();
				} else if (chunk.id == 0x4100) {
				} else if (chunk.id == 0x4110) {
					short vertexCount = readShort(stream);
					for (short i = 0; i < vertexCount; ++i) {
						float x = readFloat(stream);
						float y = readFloat(stream);
						float z = readFloat(stream);
						verts.add(new Vector3(x, z, y));
					}
				} else if (chunk.id == 0x4140) {
					short texcoordCount = readShort(stream);
					for (short i = 0; i < texcoordCount; ++i) {
						float x = readFloat(stream);
						float y = 1.0f - readFloat(stream);
						texc.add(new Vector2(x, y));
					}
				} else if (chunk.id == 0x4120) {
					short faceCount = readShort(stream);
					float[] meshVerts = new float[faceCount * 9];
					float[] meshTexc = null;
					
					if (texc.size() > 0) {
						meshTexc = new float[faceCount * 6];
					}
					
					for (short i = 0; i < faceCount; ++i) {
						short face0 = readShort(stream);
						short face1 = readShort(stream);
						short face2 = readShort(stream);
						readShort(stream);
						
						meshVerts[9*i] =  verts.get(face0).getX();
						meshVerts[9*i+1] =  verts.get(face0).getY();
						meshVerts[9*i+2] =  verts.get(face0).getZ();
						
						meshVerts[9*i+3] =  verts.get(face1).getX();
						meshVerts[9*i+4] =  verts.get(face1).getY();
						meshVerts[9*i+5] =  verts.get(face1).getZ();
						
						meshVerts[9*i+6] =  verts.get(face2).getX();
						meshVerts[9*i+7] =  verts.get(face2).getY();
						meshVerts[9*i+8] =  verts.get(face2).getZ();
						
						if (texc.size() > 0) {
							meshTexc[6*i] =  texc.get(face0).getX();
							meshTexc[6*i+1] =  texc.get(face0).getY();

							meshTexc[6*i+2] =  texc.get(face1).getX();
							meshTexc[6*i+3] =  texc.get(face1).getY();

							meshTexc[6*i+4] =  texc.get(face2).getX();
							meshTexc[6*i+5] =  texc.get(face2).getY();
						}
					}
					float[] meshNorms = ModelUtils.generateNormals(meshVerts, ShadeType.SMOOTH);
					mesh.setData(meshVerts, meshTexc, meshNorms);
				} else if (chunk.id == 0x4130) {
					String matName = readString(stream);

					short indexCount = readShort(stream);
					int[] indices = new int[indexCount*3];

					for (short i = 0; i < indexCount; ++i) {
						short index = readShort(stream);
						indices[i*3] = index*3;
						indices[i*3+1] = index*3+1;
						indices[i*3+2] = index*3+2;
					}
					
					Material material2 = model.getMaterial(matName);
					mesh.setIndexData(material2, indices, RenderMode.TRIANGLES);
				} else {
					read(stream, chunk.length - 6);
				}
			}
		} catch (IOException e) {
			throw new GraphicsException(e);
		}
		return model;
	}

	private static float readFloat(InputStream stream) throws IOException {
		byte[] bytes = read(stream, 4);
		ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[] { bytes[3],
				bytes[2], bytes[1], bytes[0] });
		return byteBuffer.getFloat();
	}

	private static int byteToInt(byte b) {
		byte[] bytes = new byte[] { 0, 0, 0, b };
		return ByteBuffer.wrap(bytes).getInt();
	}

	private static String readString(InputStream stream) throws IOException {
		String value = "";
		char c = readChar(stream);
		while (c != '\0') {
			value += c;
			c = readChar(stream);
		}
		;
		return value;
	}

	private static char readChar(InputStream stream) throws IOException {
		byte[] bytes = read(stream, 1);
		return (char) bytes[0];
	}

	private static int readInt(InputStream stream) throws IOException {
		byte[] bytes = read(stream, 4);
		ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[] { bytes[3],
				bytes[2], bytes[1], bytes[0] });
		return byteBuffer.getInt();
	}

	private static byte[] read(InputStream stream, long l) throws IOException {
		byte[] value = new byte[(int) l];
		stream.read(value);
		return value;
	}

	private static Chunk readChunk(InputStream stream) throws IOException {
		short id = readShort(stream);
		int length = readInt(stream);
		return new Chunk(id, length);
	}

	private static short readShort(InputStream stream) throws IOException {
		byte[] bytes = read(stream, 2);
		ByteBuffer byteBuffer = ByteBuffer
				.wrap(new byte[] { bytes[1], bytes[0] });
		return byteBuffer.getShort();
	}

}
