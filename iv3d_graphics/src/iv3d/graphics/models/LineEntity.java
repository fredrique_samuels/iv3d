package iv3d.graphics.models;

import iv3d.base.Color;
import iv3d.base.math.Vector3;
import iv3d.graphics.GraphicsContext;
import iv3d.graphics.GraphicsUtils;
import iv3d.graphics.Shader;
import iv3d.graphics.VboBuffer;
import iv3d.graphics.shaders.ShaderProperty;

public class LineEntity {
	private VboBuffer vertVbo;
	private Color color = Color.WHITE;
	private float width = 1f;

	public LineEntity(Vector3 start, Vector3 end) {
		vertVbo = new VboBuffer();
		vertVbo.createStaticArray(new float[]{
				start.getX(), 
				start.getY(),
				start.getZ(),
				end.getX(),
				end.getY(),
				end.getZ()});
	}
	
	public void bind(GraphicsContext gl) {
		Shader shader = gl.getShader();
		shader.setProperty(ShaderProperty.VERTS_ATTRIBUTES, vertVbo);
		shader.setUserProperty(ShaderProperty.PROPERTY_COLOR, color);
		GraphicsUtils.setLineWidth(width);
		
	}
	
	public LineEntity setColor(Color color) {
		this.color = color;
		return this;
	}

	public LineEntity setWidth(float width) {
		this.width = width;
		return this;
	}
	
	
}
