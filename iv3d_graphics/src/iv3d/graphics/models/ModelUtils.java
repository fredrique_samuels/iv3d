/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.models;

import iv3d.base.exceptions.GraphicsException;
import iv3d.base.math.Vector3;

import java.util.LinkedList;
import java.util.List;


public class ModelUtils {

	public static enum ShadeType {
		FLAT, SMOOTH
	}

	private ModelUtils() {
	}

	public static Vector3 calculateNormal(Vector3 v0, Vector3 v1, Vector3 v2) {
		Vector3 U = Vector3.subtract(v0, v1);
		Vector3 V = Vector3.subtract(v0, v2);
		return Vector3.cross(U, V).normalize();
	}

	public static float[] generateNormals(float[] vertexData, ShadeType shade)
			throws GraphicsException {
		if (vertexData.length % 9 != 0) {
			throw new GraphicsException(
					"Vertex array must have a size of multiple 9.");
		}
		float[] normals = new float[vertexData.length];
		calculateSufaceNormals(vertexData, normals);

		if (shade == ShadeType.SMOOTH) {
			Face[] faces = new Face[vertexData.length / 9];
			for (int i = 0; i < faces.length; i++) {
				Vector3[] faceVertices = getFaceVertices(i, vertexData);
				faces[i] = new Face(i, calculateNormal(faceVertices[0],
						faceVertices[1], faceVertices[2]));
			}

			assignNeighborData(vertexData, faces);
			calculateVertexNormals(faces, vertexData, normals);
		}
		return normals;
	}

	private static void calculateVertexNormals(Face[] faces,
			float[] vertexData, float[] normals) {
		for (Face face : faces) {
			Vector3 normal0 = calculateVertexNormal(face, 0, faces, vertexData);
			Vector3 normal1 = calculateVertexNormal(face, 1, faces, vertexData);
			Vector3 normal2 = calculateVertexNormal(face, 2, faces, vertexData);
			
			normals[face.index*9] = normal0.getX();
			normals[face.index*9+1] = normal0.getY();
			normals[face.index*9+2] = normal0.getZ();
			
			normals[face.index*9+3] = normal1.getX();
			normals[face.index*9+4] = normal1.getY();
			normals[face.index*9+5] = normal1.getZ();
			
			normals[face.index*9+6] = normal2.getX();
			normals[face.index*9+7] = normal2.getY();
			normals[face.index*9+8] = normal2.getZ();
		}
	}

	private static Vector3 calculateVertexNormal(Face face, int vertexIndex,
			Face[] faces, float[] vertexData) {
		/* Face holder, */
		Face lastface = face;
		Face prevface = face;
		Face n;

		/* The storage for the calculated normal. */
		Vector3 normal = face.normal;

		float curInclude; // the inclusion between two faces.
		float angle; // the angle in radians

		for (int i = 0; i < face.neighbours[vertexIndex].link0.links.size(); i++) {
			n = faces[face.neighbours[vertexIndex].link0.links.get(i)];
			lastface = n;

			/*
			 * Calculate the angle between surface normals. CLip the value to
			 * -1.0 and 1.0.
			 */
			curInclude = (float) Math.sqrt(Vector3.dot(prevface.normal,
					n.normal));
			curInclude = curInclude > 1.0f ? 1.0f : curInclude;
			curInclude = curInclude < -1.0f ? -1.0f : curInclude;

			// Get the angle.
			angle = prevface.normal.equals(n.normal) ? 0.0f : (float) Math
					.acos(curInclude);
			if (angle > 0.8) {
				break;
			}

			/*
			 * If the angle if flat enough, add it to the normal.
			 */
			normal = Vector3.add(n.normal, normal);
			prevface = n;
		}

		prevface = face;
		for (int i = 0; i < face.neighbours[vertexIndex].link1.links.size(); i++) {
			n = faces[face.neighbours[vertexIndex].link1.links.get(i)];

			/*
			 * Do not loop.
			 */
			if (lastface.index == n.index) {
				break;
			}

			/*
			 * Calculate the angle between surface normals. CLip the value to
			 * -1.0 and 1.0.
			 */
			curInclude = (float) Math.sqrt(Vector3.dot(prevface.normal,
					n.normal));
			curInclude = curInclude > 1.0f ? 1.0f : curInclude;
			curInclude = curInclude < -1.0f ? -1.0f : curInclude;

			/*
			 * If the angle if flat enough, add if to the normal.
			 */
			angle = prevface.normal.equals(n.normal) ? 0.0f : (float) Math
					.acos(curInclude);
			if (angle > 0.8) {
				break;
			}

			normal = Vector3.add(n.normal, normal);
			prevface = n;
		}

		return normal.normalize();
	}

	private static void assignNeighborData(float[] vertexData, Face[] faces) {
		for (Face face : faces) {
			findFaceNeighbours(face, faces, vertexData);
		}

		for (Face face : faces) {
			buildNeighbourChain(face, 0, faces, vertexData);
			buildNeighbourChain(face, 1, faces, vertexData);
			buildNeighbourChain(face, 2, faces, vertexData);
		}
	}

	private static void buildNeighbourChain(Face face, int pivotIndex,
			Face[] faces, float[] vertexData) {
		Vector3[] faceVertices = getFaceVertices(face.index, vertexData);
		Vector3 pivot = faceVertices[pivotIndex];
		Vector3 edge0, edge1;

		/*
		 * Find the two edges of the face that share the pivot vertex. A - B \ /
		 * P Edges are A and B.
		 */
		if (pivotIndex == 0) {
			edge0 = faceVertices[1];
			edge1 = faceVertices[2];
		} else if (pivotIndex == 1) {
			edge0 = faceVertices[0];
			edge1 = faceVertices[2];
		} else {
			edge0 = faceVertices[0];
			edge1 = faceVertices[1];
		}
		findNeighborLinks(face, face.neighbours[pivotIndex].link0, pivot,
				edge0, faces, vertexData);
		findNeighborLinks(face, face.neighbours[pivotIndex].link1, pivot,
				edge1, faces, vertexData);

	}

	private static boolean edgeCompare(Vector3 v0, Vector3 v1, Vector3 u0,
			Vector3 u1) {
		boolean t0 = v0.equals(u0) && v1.equals(u1);
		boolean t1 = v0.equals(u1) && v1.equals(u0);
		return t0 || t1;
	}

	private static Vector3 getOppositeEdge(Vector3[] array, Vector3 pivot,
			Vector3 edge) {
		if (edgeCompare(pivot, edge, array[0], array[1])) {
			return array[2];
		}

		if (edgeCompare(pivot, edge, array[0], array[1])) {
			return array[1];
		}

		if (edgeCompare(pivot, edge, array[1], array[2])) {
			return array[0];
		}

		return null;
	}

	private static void findNeighborLinks(Face face, NeighborLink link,
			Vector3 pivot, Vector3 edge, Face[] faces, float[] vertexData) {

		link.links.clear();
		boolean looping = false;

		Face currentFace = findNeighborAtEdge(face, pivot, edge, faces,
				vertexData);

		/* The last face we jumped too. */
		Face prevFace = null;

		/*
		 * Follow the face chain until no more faces are left or we start
		 * looping.
		 */
		while (currentFace != null) {

			/* The the indices are the same then we are looping. */
			if (currentFace.index == face.index) {
				break;
			}

			/* The the faces are the same then we are looping. */
			if (prevFace == currentFace) {
				break;
			}

			/**
			 * Go through all the faces in the chain added so far and check if
			 * the current face is not already in it. If it is then we are
			 * looping.
			 */
			for (int i = 0; i < link.links.size(); i++) {
				if (link.links.get(i) == currentFace.index) {
					looping = true;
					break;
				}
			}

			if (looping) {
				break;
			}

			link.links.add(currentFace.index);

			/* find the next neighbor in the chain */
			edge = getOppositeEdge(currentFace.getVertices(vertexData), pivot,
					edge);

			/* Set the new working faces. */
			prevFace = currentFace;
			if (edge != null) {
				currentFace = findNeighborAtEdge(currentFace, pivot, edge,
						faces, vertexData);
			} else {
				currentFace = null;
			}
		}
	}

	private static Face findNeighborAtEdge(Face face, Vector3 pivot,
			Vector3 edge, Face[] faces, float[] vertexData) {

		for (int i = 0; i < 3; i++) {
			if (face.neighborIndex[i] == -1) {
				break;
			}

			Face n = faces[face.neighborIndex[i]];
			if (facesHasEdge(getFaceVertices(n.index, vertexData), pivot, edge)) {
				return n;
			}
		}
		return null;
	}

	private static void findFaceNeighbours(Face target, Face[] faces,
			float[] vertexData) {

		Vector3[] targetVertices = target.getVertices(vertexData);

		for (Face face : faces) {
			if (face.index == target.index) {
				continue;
			}

			boolean shaderEdge = facesShaderEdge(targetVertices, face
					.getVertices(vertexData));

			if (shaderEdge) {
				if (!target.addNeighbour(face.index)) {
					break;
				}
			}
		}
	}

	private static boolean facesShaderEdge(Vector3[] face1, Vector3[] face2) {
		boolean t0 = containsVector3(face1[0], face2);
		boolean t1 = containsVector3(face1[1], face2);
		boolean t2 = containsVector3(face1[2], face2);
		return (t0 && t1) || (t0 && t2) || (t2 && t1);
	}

	private static boolean facesHasEdge(Vector3[] face, Vector3 p0, Vector3 p1) {
		boolean t0 = containsVector3(p0, face);
		boolean t1 = containsVector3(p1, face);
		return t0 && t1;
	}

	private static boolean containsVector3(Vector3 target, Vector3[] list) {
		boolean found = false;
		for (Vector3 v : list) {
			if (v.equals(target)) {
				found = true;
			}
		}
		return found;
	}

	private static void calculateSufaceNormals(float[] vertexData,
			float[] normals) {
		for (int i = 0; i < vertexData.length; i += 9) {
			Vector3 v0 = new Vector3(vertexData[i], vertexData[i + 1],
					vertexData[i + 2]);

			Vector3 v1 = new Vector3(vertexData[i + 3], vertexData[i + 4],
					vertexData[i + 5]);

			Vector3 v2 = new Vector3(vertexData[i + 6], vertexData[i + 7],
					vertexData[i + 8]);

			Vector3 normal = calculateNormal(v0, v1, v2);

			normals[i] = normal.getX();
			normals[i + 1] = normal.getY();
			normals[i + 2] = normal.getZ();
			normals[i + 3] = normal.getX();
			normals[i + 4] = normal.getY();
			normals[i + 5] = normal.getZ();
			normals[i + 6] = normal.getX();
			normals[i + 7] = normal.getY();
			normals[i + 8] = normal.getZ();
		}
	}

	private static Vector3[] getFaceVertices(int faceIndex, float vertexData[]) {
		int i = faceIndex * 9;
		Vector3 v0 = new Vector3(vertexData[i], vertexData[i + 1],
				vertexData[i + 2]);

		Vector3 v1 = new Vector3(vertexData[i + 3], vertexData[i + 4],
				vertexData[i + 5]);

		Vector3 v2 = new Vector3(vertexData[i + 6], vertexData[i + 7],
				vertexData[i + 8]);
		return new Vector3[] { v0, v1, v2 };
	}

	private static class Face {
		final int index;
		final int[] neighborIndex;
		final Vector3 normal;
		final NeighborChain[] neighbours;

		public Face(int index, Vector3 normal) {
			neighborIndex = new int[] { -1, -1, -1 };
			this.index = index;
			this.normal = normal;
			neighbours = new NeighborChain[] { new NeighborChain(),
					new NeighborChain(), new NeighborChain() };
		}

		public Vector3[] getVertices(float vertexData[]) {
			int i = index * 9;
			Vector3 v0 = new Vector3(vertexData[i], vertexData[i + 1],
					vertexData[i + 2]);

			Vector3 v1 = new Vector3(vertexData[i + 3], vertexData[i + 4],
					vertexData[i + 5]);

			Vector3 v2 = new Vector3(vertexData[i + 6], vertexData[i + 7],
					vertexData[i + 8]);
			return new Vector3[] { v0, v1, v2 };
		}

		public boolean addNeighbour(int n) {
			if (neighborIndex[0] == -1) {
				neighborIndex[0] = n;
				return true;
			} else if (neighborIndex[1] == -1) {
				neighborIndex[1] = n;
				return true;
			} else if (neighborIndex[2] == -1) {
				neighborIndex[2] = n;
				return false;
			}
			return false;
		}
	}

	static class NeighborChain {
		public NeighborChain() {
			link0 = new NeighborLink();
			link1 = new NeighborLink();
		}

		final NeighborLink link0;
		final NeighborLink link1;
	}

	/**
	 * Storage for face indices that form an unbroken chain of faces that share
	 * an edges around a common point.
	 * 
	 * Example : group {A-B-C} or group {D}
	 * 
	 * If the chain is staring at B, than the link will only contain either
	 * {B,A} or {B,C}.
	 * 
	 * |\--------/| | \ B / | | \ / | | A \ / C | |____\/____| |\ | | \ | |D \ |
	 * |___\______|
	 * 
	 */
	static class NeighborLink {
		final List<Integer> links;

		public NeighborLink() {
			links = new LinkedList<Integer>();
		}
	}
}
