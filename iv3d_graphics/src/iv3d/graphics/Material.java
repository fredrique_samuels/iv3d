/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import java.nio.FloatBuffer;

import iv3d.base.Color;
import iv3d.base.ImageSource;
import iv3d.base.exceptions.GraphicsException;
import iv3d.base.math.Matrix44;
import iv3d.graphics.shaders.ShaderProperty;
import iv3d.graphics.shaders.ShaderRegistry;
import iv3d.graphics.shaders.store.SHADER_DEFAULT;

public final class Material implements TextureDescription {

	private static FloatBuffer shadowProj = GraphicsUtils.asFloatBuffer(new float[16]); 
	
	private final String name;
	Shader shader;
	DefaultTextureDescription textures;

	Color ambient;
	Color diffuse;
	Color specular;
	Color emission;

	public Material(String name) {
		this.name = name;
		ambient = new Color(76, 76, 76, 255);
		diffuse = new Color(204, 204, 204, 255);
		specular = new Color(51, 51, 51, 255);
		emission = new Color(0, 0, 0, 255);
		textures = new DefaultTextureDescription();
	}

	public Color getAmbient() {
		return ambient;
	}

	public Material setAmbient(Color ambient) {
		this.ambient = ambient;
		return this;
	}

	public Color getDiffuse() {
		return diffuse;
	}

	public Material setDiffuse(Color diffuse) {
		this.diffuse = diffuse;
		return this;
	}

	public Color getSpecular() {
		return specular;
	}

	public Material setSpecular(Color specular) {
		this.specular = specular;
		return this;
	}

	public Color getEmission() {
		return emission;
	}

	public Material setEmission(Color emission) {
		this.emission = emission;
		return this;
	}
	
	@Override
	public TextureDescription setTexture(Texture texture) {
		return setTexture(texture, TextureLocation.TEXTURE_0);
	}
	
	@Override
	public TextureDescription setTexture(Texture texture, TextureLocation location) {
		return textures.setTexture(texture, location);
	}
	
	@Override
	public TextureDescription setTexture(String filename, int filterOptions) throws GraphicsException {
		return textures.setTexture(filename, filterOptions);
	}
	
	@Override
	public TextureDescription setTexture(String filename, int filterOptions, TextureLocation location)
			throws GraphicsException {
		return textures.setTexture(filename, filterOptions, location);
	}
	
	@Override
	public TextureDescription setTexture(ImageSource imageSource) {
		return textures.setTexture(imageSource);
	}
	
	@Override
	public TextureDescription setTexture(ImageSource imageSource, TextureLocation location) {
		return textures.setTexture(imageSource, location);
	}
	
	@Override
	public Texture getTexture(TextureLocation location) {
		return textures.getTexture(location);
	}
	
	@Override
	public TextureDescription updateTextures() {
		return textures.updateTextures();
	}

	public String getName() {
		return name;
	}

	public Material setShader(Shader shader) {
		this.shader = shader;
		return this;
	}

	public float[] toArray() {
		float[] a = ambient.normalize().toArray();
		float[] d = diffuse.normalize().toArray();
		float[] s = specular.normalize().toArray();
		float[] e = emission.normalize().toArray();
	return new float[] { a[0], a[1], a[2], a[3], d[0], d[1], d[2], d[3],
			s[0], s[1], s[2], s[3], e[0], e[1], e[2], e[3]};
	}

	protected Material bind(GraphicsContext gl) {
		Shader shader = gl.getShader();
		shader.setTextures(textures);
		shader.setProperty(ShaderProperty.MATERIAL, this);
		return this;
	}

	protected void bindShadowProperties(GraphicsContext gl) {
		Shader shader = gl.getShader();
		Integer hasShadow = Shader.FALSE;
		for(int i=0;i<gl.getLightCount();++i) {
			Light light = gl.getLight(i);
			if(!light.isShadowEnabled()) {
				continue;
			}
			
			updateShadowMatrices(shader, light);
			hasShadow = Shader.TRUE;
			break;
		}
		shader.setProperty(ShaderProperty.SHADOW_ENABLED, hasShadow);
	}
	
	protected void disableShadows() {
		shader.setProperty(ShaderProperty.SHADOW_ENABLED, Shader.FALSE);
	}

	private void updateShadowMatrices(Shader shader, Light light) {
		FrameBuffer frameBuffer = light.getFrameBuffer();
		shader.setProperty(TextureLocation.TEXTURE_SHADOW, frameBuffer.getTexture());
		shader.setProperty(ShaderProperty.TEXTURE_SHADOW_ENABLED, Shader.TRUE);
		shader.setProperty(ShaderProperty.SHADOW_VIEW_MATRIX, light.getLookAtBuffer());
		
		Matrix44 projectionMatrix = light.getProjectionMatrix(frameBuffer.getWidth(), frameBuffer.getHeight());
		shadowProj.put(projectionMatrix.toArray());
		shadowProj.flip();
		shader.setProperty(ShaderProperty.SHADOW_PROJ_MATRIX, shadowProj);
	}

	void bindShader() {
		if(shader==null) {
			setShader(ShaderRegistry.getShader(SHADER_DEFAULT.ID));
		}
		shader.bind();
		shader.resetDefaults();
	}
	
	public final Material copy() {
		Material material = new Material(name);
		material.shader = shader;
		material.ambient = new Color(ambient);
		material.diffuse = new Color(diffuse); 
		material.specular = new Color(specular);
		material.emission = new Color(emission);
		material.textures = new DefaultTextureDescription(textures);
		
		return material;
	}
}
