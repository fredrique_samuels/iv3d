/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import iv3d.base.ImageSource;
import iv3d.base.PixelFormat;
import iv3d.base.exceptions.GraphicsException;
import iv3d.graphics.GraphicsUtils;
import iv3d.graphics.TextureUtils;

public final class Texture {

	private int textureId;
	private ImageSource imageSource;
	private boolean forceUpdate = true;
	
	private Integer weight;
	private boolean mask;
	private PixelFormat pixelFormat;

	public Texture() {

	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		destroy();
	}
	
	public Texture destroy() {
		TextureUtils.deleteTexture(textureId);
		textureId = 0;
		return this;
	}

	public Texture bind(int index) {
		if (imageSource.isBindEnabled()) {
			imageSource.bind(index);
		} else {
			TextureUtils.bindTexture(textureId, index);
		}
		return this;
	}

	public static void unbind() {
		for (int index = 0; index < 8; index++) {
			unbind(index);
		}
	}

	public static void unbind(int index) {
		TextureUtils.bindTexture(0, index);
	}

	public Texture setImageSource(ImageSource imageSource) {
		if (this.imageSource == imageSource) {
			return this;
		}
		this.imageSource = imageSource;
		forceUpdate = true;
		return this;
	}

	public ImageSource getImageSource() {
		return this.imageSource;
	}

	public Texture load(ImageSource imageSource) {
		setImageSource(imageSource);
		destroy();
		textureId = TextureUtils.createTexture(imageSource, imageSource
				.getFilterOptions());
		imageSource.clearDirty();
		return this;
	}

	public Texture load(String filename, int filterOptions)
			throws GraphicsException {
		ImageSource imageSource = GraphicsUtils.readImageFromFile(filename,
				filterOptions);
		load(imageSource);
		return this;
	}

	public Texture update(ImageSource imageSource) {
		// TODO create new texture if image properties changed.
		TextureUtils.updateTexture(this.textureId, imageSource);
		return this;
	}

	public Texture updateBuffer() {
		
		if (imageSource == null) {
			destroy();
			return this;
		} else if (imageSource.isBindEnabled()) {
			return this;
		}

		if (textureId == 0) {
			load(imageSource);
			forceUpdate = false;
			return this;
		}

		imageSource.update();
		if (forceUpdate || imageSource.isDirty()) {
			update(imageSource);
			imageSource.clearDirty();
		}

		forceUpdate = false;
		return this;
	}
	
	public final PixelFormat getPixelFormat() {
		if(pixelFormat!=null) {
			return pixelFormat;
		}
		
		if(imageSource!=null) {
			return imageSource.getPixelFormat();
		}
		return PixelFormat.RGBA;
	}
	
	public final void setPixelFormat(PixelFormat pixelFormat){
		this.pixelFormat = pixelFormat;
	}

	public final Integer getWeight() {
		return weight;
	}
	public final void setWeight(Integer weight) {
		this.weight = weight;
	}
	public final boolean isAlphaMask() {
		return mask;
	}
	
	public final void enableAlphaMask() {
		this.mask = true;
	}
	
	public final void disableAlphaMask() {
		this.mask = false;
	}

}
