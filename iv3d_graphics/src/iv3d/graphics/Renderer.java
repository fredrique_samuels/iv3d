/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

public abstract class Renderer implements SceneIterator {

	private final GraphicsContext gl;
	private boolean iterateReversed;

	public Renderer(GraphicsContext gl) {
		this.gl = gl;
		iterateReversed = false;
	}
	
	public final GraphicsContext getGraphics() {
		return gl;
	}
	
	public final void enableDecendingScenes() {
		iterateReversed = true;
	}

	protected abstract boolean render(GraphicsContext gl, Scene scene);
	protected abstract void preRender();
	protected abstract void postRender();

	@Override
	public final boolean processScene(Scene scene) {
		gl.setLights(scene.getContentManager().getLights());
		return render(gl, scene);
	}
	
	public final void render(){
		preRender();
		if(iterateReversed) {
			SceneManager.iterateVisibleScenesReversed(this);
		} else {
			SceneManager.iterateVisibleScenes(this);
		}
		postRender();
	}
}
