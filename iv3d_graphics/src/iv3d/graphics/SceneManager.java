/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;

public class SceneManager {
	private SceneManager() {

	}

	private static Set<Scene> scenes = new HashSet<Scene>();
	private static Set<Scene> scenesToAdd = new HashSet<Scene>();
	private static Set<Scene> scenesToRemove = new HashSet<Scene>();
	private static Set<Scene> scenesToShow = new HashSet<Scene>();
	private static Set<Long> scenesToHide = new HashSet<Long>();
	private static TreeMap<Long, Scene> visibleScenes = new TreeMap<Long, Scene>();

	private static void processRemove() {
		for (Scene scene : scenesToRemove) {
			if (isVisible(scene)) {
				visibleScenes.remove(scene.getLevel());
			}

			if (scenesToShow.contains(scene)) {
				scenesToShow.remove(scene);
			}

			if (scenesToHide.contains(scene)) {
				scenesToHide.remove(scene);
			}

			scenes.remove(scene);
		}

		scenesToRemove.clear();
	}

	public static boolean isVisible(Scene scene) {
		return visibleScenes.containsValue(scene);
	}

	private static void processAdd() {
		for (Scene scene : scenesToAdd) {
			scenes.add(scene);
		}
		scenesToAdd.clear();
	}

	private static void processHide() {
		for (Long level : scenesToHide) {
			visibleScenes.remove(level);
			visibleScenes.get(level).onHide();
		}
		scenesToHide.clear();
	}

	private static void processShow() {
		for (Scene scene : scenesToShow) {
			Long level = scene.getLevel();
			visibleScenes.put(level, scene);
			scene.onShow();
		}
		scenesToShow.clear();
	}

	public static void add(Scene scene) {
		if (scenes.contains(scene)) {
			return;
		}

		scenes.add(scene);
	}

	public static void remove(Scene scene) {
		if (scenesToRemove.contains(scene)) {
			return;
		}

		if (scenesToAdd.contains(scene)) {
			scenesToAdd.remove(scene);
			return;
		}
		scenesToRemove.add(scene);
	}

	public static boolean contains(Scene scene) {
		return scenes.contains(scene) || scenesToAdd.contains(scene);
	}

	public static void show(Scene scene) {
		if(contains(scene)) {
			scenesToShow.add(scene);
		}
	}
	
	public static void hide(Scene scene) {
		if(isVisible(scene)) {
			scenesToHide.add(scene.getLevel());
		}
		
		if (scenesToShow.contains(scene)) {
			scenesToShow.remove(scene);
		}
	}
	
	public static void update() {
		processRemove();
		processAdd();
		processHide();
		processShow();
	}
	
	public static void iterateScenes(SceneIterator iter){
		for (Scene scene : scenes) {
			if (!iter.processScene(scene)) {
				break;
			}
		}
	}
	
	public static void iterateVisibleScenes(SceneIterator iter){
		for (Long level : visibleScenes.keySet()) {
			if (!iter.processScene(visibleScenes.get(level))) {
				break;
			}
		}
	}
	
	public static void iterateVisibleScenesReversed(SceneIterator iter){
		for (Long level : visibleScenes.descendingKeySet()) {
			if (!iter.processScene(visibleScenes.get(level))) {
				break;
			}
		}
	}
}
