/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;


import iv3d.base.Rectangle;
import iv3d.base.math.BoundingBox;
import iv3d.base.math.MathUtils;
import iv3d.base.math.Matrix44;
import iv3d.graphics.shaders.ShaderProperty;

import java.nio.FloatBuffer;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;


public class Object3D {
	private final Map<Material, IndexData> indexVbo;
	VboBuffer vertVbo;
	VboBuffer texcVbo;
	VboBuffer normalVbo;
	static FloatBuffer modelViewBuffer;
	BoundingBox boundingBox;

	public Object3D() {
		super();
		indexVbo = new HashMap<Material, IndexData>(); 
		boundingBox = new BoundingBox();
	}
	
	public Object3D setIndexData(Material material, int[] data, RenderMode mode) {
		if(indexVbo.containsKey(material)) {
			indexVbo.get(material).set(data, mode);
		} else {
			indexVbo.put(material, new IndexData().set(data, mode));
		}
		return this;
	}

	public Object3D setData(float[] verts, float[] texcoords, float[] normals) {
		destroyVboData();
		boundingBox = BoundingBox.fromVerts3(verts);
		if (verts != null) {
			vertVbo = new VboBuffer().createStaticArray(verts);
		}
		if (texcoords != null) {
			texcVbo = new VboBuffer().createStaticArray(texcoords);
		}
		if (normals != null) {
			normalVbo = new VboBuffer().createStaticArray(normals);
		}
		return this;
	}

	public Object3D destroy() {
		destroyVboData();
		destroyMaterialData();
		return this;
	}

	private void destroyMaterialData() {
		for (IndexData data : indexVbo.values()) {
			data.destroy();
		}
		indexVbo.clear();
	}

	private void destroyVboData() {
		boundingBox = new BoundingBox();
		
		if (vertVbo != null) {
			vertVbo.destoy();
			vertVbo = null;
		}
		if (texcVbo != null) {
			texcVbo.destoy();
			texcVbo = null;
		}
		if (normalVbo != null) {
			normalVbo.destoy();
			normalVbo = null;
		}
	}

	public void render(GraphicsContext gl, EntityRenderer entityRenderer,
			Collection<MatrixGroup> collection) {
		if (vertVbo == null) {
			return;
		}
		
		for (Material material : indexVbo.keySet()) {
			IndexData indexData = indexVbo.get(material);
			if (!indexData.isValid()) {
				continue;
			}
			
			Shader shader = null;
			
			for (MatrixGroup  matrixGroup : collection) {
				material.bindShader();
				
				RenderTarget entity = matrixGroup.getTarget();
				if(!entity.isVisible()) {
					continue;
				}
				
				boolean inView = isInView(gl.getViewport(), gl.getCamera(), matrixGroup);
				if(!inView && entity.isBoundingBoxEnabled()) {
					continue;
				}
				
				entity.bindShader();
				boolean receiveShadowsEnabled = entity.isReceiveShadowsEnabled();
				
				material.bind(gl);
				if(receiveShadowsEnabled) {
					material.bindShadowProperties(gl);
				} else {
					material.disableShadows();
				}
				
				if (gl.getShader() != shader) {
					shader = gl.getShader();
					updateStaticValues(gl);
				}
				
				entity.applyShaderProperties(shader);
				updateMatrices(gl, matrixGroup);
				
				preRender(entity);
				entityRenderer.render(entity, indexData);
				postRender(entity);
				
			}
		}
	}

	public void postRender(RenderTarget entity) {
	}

	public void preRender(RenderTarget entity) {
	}

	private boolean isInView(Rectangle viewport, Camera camera, MatrixGroup matrixGroup) {
		Matrix44 modelMatrix = camera.getLookAtMatrix().multiply(matrixGroup.getObjectMatrix());
		Matrix44 projectionMatrix = camera.getProjectionMatrix();
		return MathUtils.isBoxOnScreen(boundingBox, modelMatrix, projectionMatrix, viewport);
	}

	private void updateMatrices(GraphicsContext gl, MatrixGroup matrixGroup) {
		Shader shader = gl.getShader();
		Camera camera = gl.getCamera();
		
		Matrix44 mv = camera.getLookAtMatrix().multiply(matrixGroup.getObjectMatrix());
		setModelViewBuffer(mv.toArray());
		
		shader.setProperty(ShaderProperty.MODEL_VIEW_MATRIX, getModelViewBuffer());
		shader.setProperty(ShaderProperty.PROJECTION_MATRIX, camera.getProjectionBuffer());

		if (shader.hasProperty(ShaderProperty.CAMERA_MATRIX.getName())) {
			shader.setProperty(ShaderProperty.CAMERA_MATRIX, camera
					.getLookAtBuffer());
		}
		
		if (shader.hasProperty(ShaderProperty.CAMERA_MATRIX_I.getName())) {
			shader.setProperty(ShaderProperty.CAMERA_MATRIX_I, camera.getLookAtBufferI());
		}
		

		if (shader.hasProperty(ShaderProperty.NORMAL_MATRIX.getName())) {
			shader.setProperty(ShaderProperty.NORMAL_MATRIX, matrixGroup.getNormalBuffer());
		}
		
		if (shader.hasProperty(ShaderProperty.OBJECT_MATRIX.getName())) {
			shader.setProperty(ShaderProperty.OBJECT_MATRIX, matrixGroup.getObjectBuffer());
		}
		
		if (shader.hasProperty(ShaderProperty.OBJECT_MATRIX_I.getName())) {
			shader.setProperty(ShaderProperty.OBJECT_MATRIX_I, matrixGroup.getObjectBufferI());
		}
	}

	private void updateStaticValues(GraphicsContext gl) {
		updateShaderVbo(gl);
		updateShaderLights(gl);
	}

	private void updateShaderLights(GraphicsContext gl) {
		Shader shader = gl.getShader();
		boolean enabled = false;
		
		for (int index=0;index<8;index++) {
			Light light = gl.getLight(index);
			
			if(light==null) {
				shader.setProperty(ShaderProperty.getLightProperty(index), Integer.valueOf(0));
			} else {
				enabled = true;
				shader.setProperty(ShaderProperty.getLightEnabledProperty(index), Integer.valueOf(1));
				shader.setProperty(ShaderProperty.getLightProperty(index), light);
			}
		}
		shader.setProperty(ShaderProperty.LIGHTS_ENABLED, Integer.valueOf(enabled?1:0));
	}

	private void updateShaderVbo(GraphicsContext gl) {
		Shader shader = gl.getShader();
		shader.setProperty(ShaderProperty.VERTS_ATTRIBUTES, vertVbo);
		shader.setProperty(ShaderProperty.TEXCOORDS_ATTRIBUTES, texcVbo);
		shader.setProperty(ShaderProperty.NORMAL_ATTRIBUTES, normalVbo);
	}

	static class DefaultEntityRenderer implements EntityRenderer {

		@Override
		public void render(RenderTarget entity, IndexData data) {
			data.render();
		}
	}
	
	private static FloatBuffer getModelViewBuffer() {
		if (modelViewBuffer==null) {
			modelViewBuffer = GraphicsUtils.asFloatBuffer(new float[16]);
		}
		return modelViewBuffer; 
	}
	
	private static void setModelViewBuffer(float[] value) {
		FloatBuffer buffer = getModelViewBuffer();
		buffer.put(value);
		buffer.flip();
	}
	
	public final Object3D copy(Map<String, Material> materials) {
		Object3D mesh = new Object3D();
		mesh.vertVbo = vertVbo;
		mesh.texcVbo = texcVbo;
		mesh.normalVbo = normalVbo;
		mesh.boundingBox = boundingBox;
		
		for (Entry<Material, IndexData> entry : indexVbo.entrySet()) {
			mesh.indexVbo.put(materials.get(entry.getKey().getName()), entry.getValue());
		}
		return mesh;
	}
}
