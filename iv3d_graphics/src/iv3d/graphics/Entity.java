/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import iv3d.base.ImageSource;
import iv3d.base.MatrixTransform;
import iv3d.base.Transform;
import iv3d.base.TransformState;
import iv3d.base.events.EventReceiver;
import iv3d.base.exceptions.GraphicsException;
import iv3d.base.math.Matrix44;
import iv3d.base.math.Quaternion;
import iv3d.base.math.Vector3;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public class Entity extends EventReceiver implements Transform, TextureDescription, RenderTarget {
	private final Transform transform;
	private boolean castsShadow = false;
	private Map<String, Object> userProperties;
	private DefaultTextureDescription textures;
	private Shader shader;
	private List<TransformChangeListener> transformChangeListeners;
	private boolean boundingBoxEnabled;
	private boolean receiveShadowsEnabled;
	private boolean visible;

	public Entity() {
		super();
		transform = new MatrixTransform();
		userProperties = new HashMap<String, Object>();
		textures = new DefaultTextureDescription();
		transformChangeListeners = new LinkedList<TransformChangeListener>();
		boundingBoxEnabled = true;
		receiveShadowsEnabled = false;
		visible = true;
	}
	
	public Entity enableBoundingBox() {
		boundingBoxEnabled = true;
		return this;
	}
	
	public Entity disableBoundingBox() {
		boundingBoxEnabled = false;
		return this;
	}
	
	public boolean isBoundingBoxEnabled() {
		return boundingBoxEnabled;
	}
	
	public Entity addTransformChangeListener(TransformChangeListener listener) {
		if(transformChangeListeners.contains(listener)){
			 return this;
		}
		transformChangeListeners.add(listener);
		return this;
	}
	
	public Entity removeTransformChangeListener(TransformChangeListener listener) {
		if(transformChangeListeners.contains(listener)){
			 transformChangeListeners.remove(listener);
		}
		return this;
	}
	
	private void dispatchTransformChanged() { 
		for( TransformChangeListener listener : transformChangeListeners){
			listener.transformChanged(this);
		}
	}

	public final Shader getShader() {
		return shader;
	}
	
	public final void setShader(Shader shader) {
		this.shader = shader;
	}
	
	public final void clearShaderProperties() {
		userProperties.clear();
	}

	public Entity setProperty(String name, Object value) {
		userProperties.put(name, value);
		return this;
	}
	
	public void applyShaderProperties(Shader shader) {
		for(Entry<String, Object> entry : userProperties.entrySet()) {
			shader.setUserProperty(entry.getKey(), entry.getValue());
		}
		shader.updateTextures(this);
	}

	public final Entity enableCastShadow() {
		castsShadow = true;
		return this;
	}

	public final Entity disableCastShadow() {
		castsShadow = false;
		return this;
	}

	public final boolean castShadowEnabled() {
		return castsShadow;
	}

	@Override
	public float[] toArray() {
		return transform.toArray();
	}

	@Override
	public float[] toArrayI() {
		return transform.toArrayI();
	}

	@Override
	public Vector3 getPosition() {
		return transform.getPosition();
	}

	@Override
	public float[] getRotation() {
		return transform.getRotation();
	}

	@Override
	public Vector3 getScale() {
		return transform.getScale();
	}

	@Override
	public Transform setPosition(float x, float y, float z) {
		return setPosition(new Vector3(x, y, z));
	}

	@Override
	public Transform setPosition(Vector3 v) {
		transform.setPosition(v);
		dispatchTransformChanged();
		return transform;
	}

	@Override
	public Transform setRotation(Matrix44 rot) {
		transform.setRotation(rot);
		dispatchTransformChanged();
		return transform;
	}

	@Override
	public Transform setRotation(float[] rot) {
		return setRotation(new Matrix44(rot));
	}

	@Override
	public Transform setRotation(float x, float y, float z) {
		transform.setRotation(x, y, z);
		dispatchTransformChanged();
		return transform;
	}

	@Override
	public Transform setRotation(Quaternion q) {
		transform.setRotation(q);
		dispatchTransformChanged();
		return transform;
	}

	@Override
	public Transform setScale(float x, float y, float z) {
		return setScale(new Vector3(x, y, z));
	}

	@Override
	public Transform setScale(Vector3 v) {
		transform.setScale(v);
		dispatchTransformChanged();
		return transform;
	}

	@Override
	public Transform setRotation(Vector3 v) {
		transform.setRotation(v);
		dispatchTransformChanged();
		return transform;
	}

	@Override
	public TextureDescription setTexture(Texture texture) {
		return setTexture(texture, TextureLocation.TEXTURE_0);
	}
	
	@Override
	public TextureDescription setTexture(Texture texture, TextureLocation location) {
		return textures.setTexture(texture, location);
	}
	
	@Override
	public TextureDescription setTexture(String filename, int filterOptions) throws GraphicsException {
		return textures.setTexture(filename, filterOptions);
	}
	
	@Override
	public TextureDescription setTexture(String filename, int filterOptions, TextureLocation location)
			throws GraphicsException {
		return textures.setTexture(filename, filterOptions, location);
	}
	
	@Override
	public TextureDescription setTexture(ImageSource imageSource) {
		return textures.setTexture(imageSource);
	}
	
	@Override
	public TextureDescription setTexture(ImageSource imageSource, TextureLocation location) {
		return textures.setTexture(imageSource, location);
	}
	
	@Override
	public Texture getTexture(TextureLocation location) {
		return textures.getTexture(location);
	}
	
	@Override
	public TextureDescription updateTextures() {
		return textures.updateTextures();
	}

	@Override
	public Transform setPositionX(Float v) {
		transform.setPositionX(v);
		dispatchTransformChanged();
		return transform;
	}

	@Override
	public Transform setPositionY(Float v) {
		transform.setPositionY(v);
		dispatchTransformChanged();
		return transform;
	}

	@Override
	public Transform setPositionZ(Float v) {
		transform.setPositionZ(v);
		dispatchTransformChanged();
		return transform;
	}

	public void bindShader() {
		if (shader != null) {
			shader.bind();
		}
	}

	@Override
	public TransformState getTransformState() {
		return transform.getTransformState();
	}

	public final boolean isReceiveShadowsEnabled() {
		return receiveShadowsEnabled;
	}
	
	public final void enableReceiveShadows() {
		receiveShadowsEnabled = true;;
	}
	
	public final void disableReceiveShadows() {
		receiveShadowsEnabled = false;
	}

	public final void hide() {
		visible  = false;
	}
	
	public final void show() {
		visible  = true;
	}
	
	public final boolean isVisible() {
		return visible;
	}

	@Override
	public EventReceiver asEventReceiver() {
		return this;
	}
}
