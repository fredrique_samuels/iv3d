/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import iv3d.graphics.GraphicsUtils;
import iv3d.graphics.VboUtils;

public final class VboBuffer {
	
	private static int ARRAY_BUFFER = 300;
	private static int ELEMENT_BUFFER = 400;
	
	private int vboId = 0;
	private int format = 0;
	private int bufferType=0;
	private int size = 0;
	
	public VboBuffer() {
	}
	
	public VboBuffer createStaticArray(float[] data) {
		destoy();
		vboId = VboUtils.createStaticVbo(data);
		format = GraphicsUtils.FLOAT;
		bufferType = ARRAY_BUFFER;
		size = data.length;
		return this;
	}
	
	public VboBuffer createStaticElementArray(int[] data) {
		destoy();
		vboId = VboUtils.createElementVbo(data);
		format = GraphicsUtils.INT;
		bufferType = ELEMENT_BUFFER;
		size = data.length;
		return this;
	}
	
	public VboBuffer destoy() {
		VboUtils.delete(vboId);
		vboId = 0;
		return this;
	}
	
	public VboBuffer bind() {
		if (bufferType==ARRAY_BUFFER) {
			VboUtils.bindArrayBuffer(vboId);
		}else if (bufferType==ELEMENT_BUFFER) {
			VboUtils.bindElementBuffer(vboId);
		}
		return this;
	}
	
	public static void unbind() {
		VboUtils.bindArrayBuffer(0);
	}
	
	public int getFormat() {
		return format;
	}
	
	public int getSize() {
		return size;
	}

	public int getId() {
		return vboId;
	}
}
