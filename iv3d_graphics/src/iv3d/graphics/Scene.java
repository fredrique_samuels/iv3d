/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;


import iv3d.base.Color;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


public class Scene {

	private List<Camera> cameras;
	private SceneContentManager content;
	private final Long level;
	private boolean renderToScreen;
	private boolean renderToImage;

	private FrameBuffer frameBuffer;
	private Color backgroundColor = new Color(0, 0, 0, 0);


	public Scene() {
		this(0);
	}

	public Scene(long level) {
		cameras = new LinkedList<Camera>();
		content = new SceneContentManager();
		this.level = level;
		renderToScreen = true;
		renderToImage = false;
		frameBuffer = new FrameBuffer();
	}
	
	public final boolean canRenderToImage() {
		return renderToImage;
	}

	public final void enabledRenderToImage() {
		if(this.renderToImage) {
			return;
		}
		this.renderToImage = true;
		frameBuffer.createColor();
	}

	public final void disabledRenderToImage() {
		if(!this.renderToImage) {
			return;
		}
		this.renderToImage = false;
		frameBuffer.destoy();
	}

	public final Scene disableRenderToScreen() {
		renderToScreen = false;
		return this;
	}

	public final boolean canRenderToScreen() {
		return renderToScreen;
	}

	public final Scene enableRenderToScreen() {
		this.renderToScreen = true;
		return this;
	}
	
	public final Long getLevel() {
		return level;
	}

	public SceneContentManager getContentManager() {
		return content;
	}

	public Scene setContent(SceneContentManager content) {
		this.content = content;
		return this;
	}

	public Scene addCamera(Camera camera) {
		if (camera == null) {
			return this;
		}

		if (cameras.contains(camera)) {
			return this;
		}

		cameras.add(camera);
		return this;
	}

	public Scene removeLight(Camera camera) {
		if (cameras.contains(camera)) {
			cameras.remove(camera);
		}
		return this;
	}

	public Scene removeAllCameras() {
		cameras.clear();
		return this;
	}

	public boolean isVisible() {
		return SceneManager.isVisible(this);
	}

	public Scene show() {
		SceneManager.show(this);
		return this;
	}

	public Scene hide() {
		SceneManager.hide(this);
		return this;
	}

	public Iterator<Camera> getCameraIterator() {
		return cameras.iterator();
	}

	public final FrameBuffer getFrameBuffer() {
		return frameBuffer; 
	}

	public Scene setBackgroundColor(Color c) {
		backgroundColor  = c;
		return this;
	}
	
	public Color getBackgroundColor() {
		return backgroundColor ;
	}

	public Texture getTexture() {
		return frameBuffer.getTexture();
	}

	protected void onShow() {
	}

	protected void onHide() {
	}
}
