package iv3d.graphics;

public abstract class Model {
	public abstract void render(GraphicsContext gl);
	public abstract void render(GraphicsContext gl, EntityRenderer render);
}
