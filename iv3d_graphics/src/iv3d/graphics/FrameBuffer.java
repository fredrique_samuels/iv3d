/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import iv3d.base.Dimension;
import iv3d.base.ImageSource;
import iv3d.base.PixelFormat;
import iv3d.base.Rectangle;
import iv3d.graphics.FrameBufferUtils;
import iv3d.graphics.TextureUtils;

import java.nio.ByteBuffer;



public final class FrameBuffer {
	
	public static enum BufferMode{ 
		DEPTH_MODE,
		COLOR_MODE
	}
	
	private ImageSource imageSource;
	private int height;
	private int width;
	private final Texture texture;
	private int frameBufferId;
	private int colorTextureId;
	private int renderBufferId;
	private Rectangle usedArea;

	public FrameBuffer() {
		width = 0;
		height = 0;
		imageSource = new FbImageSource();
		texture = new Texture();
		texture.setImageSource(imageSource);
		frameBufferId = 0;
		renderBufferId = 0;
		colorTextureId = 0;
	}

	public final int getHeight() {
		return height;
	}

	public final int getWidth() {
		return width;
	}
	
	public Dimension getDimensions() {
		return new Dimension(width, height);
	}

	public final Texture getTexture() {
		return texture;
	}

	public final ImageSource getImageSource() {
		return imageSource;
	}

	public final void bind() {
		FrameBufferUtils.bind(frameBufferId);
	}

	public void destoy() {
		TextureUtils.deleteTexture(colorTextureId);
		FrameBufferUtils.destroyFrameBuffer(frameBufferId);
		FrameBufferUtils.destroyRenderBuffer(renderBufferId);
		this.width = 0;
		this.height = 0;
		colorTextureId = 0;
		frameBufferId = 0;
		renderBufferId = 0;
	}

	public void create(int width, int height, BufferMode mode) {
		destoy();
		this.width = width;
		this.height = height;
		Texture.unbind();
		
		if(mode==BufferMode.COLOR_MODE){
			colorTextureId = FrameBufferUtils.createColorTexture(width, height);
			frameBufferId = FrameBufferUtils.createFrameBufferColor(colorTextureId);
			renderBufferId = FrameBufferUtils.createRenderBuffer(width, height);
		} else {
			colorTextureId = FrameBufferUtils.createDepthTexture(width, height);
			frameBufferId = FrameBufferUtils.createFrameBufferDepth(colorTextureId);
		}
		
		FrameBufferUtils.unbind();
		Texture.unbind();
		
		texture.setImageSource(imageSource);
	}

	public void createColor() {
		create(512, 512, BufferMode.COLOR_MODE);
	}
	
	public void createDepth() {
		create(512, 512, BufferMode.DEPTH_MODE);
	}
	
	public static void unbind() {
		FrameBufferUtils.unbind();
	}

	class FbImageSource extends ImageSource {
		
		public FbImageSource() {
			super();
			enableBind();
		}

		@Override
		public void bind(int index) {
			TextureUtils.bindTexture(colorTextureId, index);
		}

		@Override
		public int getBytesPerPixel() {
			return 0;
		}

		@Override
		public int getDataFormat() {
			return 0;
		}

		@Override
		public int getHeight() {
			return height;
		}

		@Override
		public int getInternalFormat() {
			return 0;
		}

		@Override
		public ByteBuffer getPixels() {
			return null;
		}

		@Override
		public int getWidth() {
			return width;
		}

		@Override
		public int getFilterOptions() {
			return TextureUtils.TEXTURE_OPTION_CLAMP;
		}

		@Override
		public void update() {			
		}

		@Override
		public PixelFormat getPixelFormat() {
			return PixelFormat.RGBA;
		}
		
	}

	public Rectangle getUsedArea() {
		return usedArea;
	}

	public void setUsedArea(Rectangle usedArea) {
		this.usedArea = usedArea;
	}
}
