/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import iv3d.base.exceptions.GraphicsException;
import iv3d.base.math.Matrix44;
import iv3d.graphics.shaders.ShaderProperty;
import iv3d.graphics.shaders.ShaderRegistry;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public final class Shader {
	private static boolean locked = false;
	private static Shader activeShader;
	
	public static Integer FALSE = new Integer(0);
	public static Integer TRUE = new Integer(1);


	private static String[] LOOKUP_PROPERTIES = new String[] {
			
			ShaderProperty.PROJECTION_MATRIX.getName(),
			ShaderProperty.MODEL_VIEW_MATRIX.getName(),
			ShaderProperty.CAMERA_MATRIX.getName(),
			ShaderProperty.CAMERA_MATRIX_I.getName(),
			ShaderProperty.OBJECT_MATRIX.getName(),
			ShaderProperty.SHADOW_PROJ_MATRIX.getName(),
			ShaderProperty.SHADOW_VIEW_MATRIX.getName(),
			ShaderProperty.OBJECT_MATRIX_I.getName(),
			ShaderProperty.NORMAL_MATRIX.getName(),
			
			ShaderProperty.MATERIAL.getName(),
			
			ShaderProperty.LIGHT_0.getName(), 
			ShaderProperty.LIGHT_1.getName(),
			ShaderProperty.LIGHT_2.getName(), 
			ShaderProperty.LIGHT_3.getName(),
			ShaderProperty.LIGHT_4.getName(),
			ShaderProperty.LIGHT_5.getName(),
			ShaderProperty.LIGHT_6.getName(), 
			ShaderProperty.LIGHT_7.getName(),
			
			ShaderProperty.LIGHT_0_ENABLED.getName(), 
			ShaderProperty.LIGHT_1_ENABLED.getName(),
			ShaderProperty.LIGHT_2_ENABLED.getName(), 
			ShaderProperty.LIGHT_3_ENABLED.getName(),
			ShaderProperty.LIGHT_4_ENABLED.getName(),
			ShaderProperty.LIGHT_5_ENABLED.getName(),
			ShaderProperty.LIGHT_6_ENABLED.getName(), 
			ShaderProperty.LIGHT_7_ENABLED.getName(),
			ShaderProperty.LIGHTS_ENABLED.getName(),
			
			TextureLocation.TEXTURE_0.getName(),
			TextureLocation.TEXTURE_1.getName(),
			TextureLocation.TEXTURE_2.getName(),
			TextureLocation.TEXTURE_3.getName(),
			TextureLocation.TEXTURE_PROJ.getName(),
			TextureLocation.TEXTURE_BUMP.getName(),
			TextureLocation.TEXTURE_SHADOW.getName(),
			TextureLocation.TEXTURE_SPHERE.getName(),
			
			ShaderProperty.TEXTURE_0_ENABLED.getName(),
			ShaderProperty.TEXTURE_1_ENABLED.getName(),
			ShaderProperty.TEXTURE_2_ENABLED.getName(),
			ShaderProperty.TEXTURE_3_ENABLED.getName(),
			ShaderProperty.TEXTURE_PROJ_ENABLED.getName(),
			ShaderProperty.TEXTURE_SPHERE_ENABLED.getName(),
			ShaderProperty.TEXTURE_SHADOW_ENABLED.getName(),
			ShaderProperty.TEXTURE_BUMP_ENABLED.getName(),
			
			ShaderProperty.TEXTURE_0_FORMAT.getName(),
			ShaderProperty.TEXTURE_1_FORMAT.getName(),
			ShaderProperty.TEXTURE_2_FORMAT.getName(),
			ShaderProperty.TEXTURE_3_FORMAT.getName(),
			ShaderProperty.TEXTURE_PROJ_FORMAT.getName(),
			ShaderProperty.TEXTURE_SPHERE_FORMAT.getName(),
			ShaderProperty.TEXTURE_SHADOW_FORMAT.getName(),
			ShaderProperty.TEXTURE_BUMP_FORMAT.getName(),
			
			ShaderProperty.TEXTURE_0_MASK.getName(),
			ShaderProperty.TEXTURE_1_MASK.getName(),
			ShaderProperty.TEXTURE_2_MASK.getName(),
			ShaderProperty.TEXTURE_3_MASK.getName(),
			ShaderProperty.TEXTURE_PROJ_MASK.getName(),
			ShaderProperty.TEXTURE_SPHERE_MASK.getName(),
			ShaderProperty.TEXTURE_SHADOW_MASK.getName(),
			ShaderProperty.TEXTURE_BUMP_MASK.getName(),
			
			ShaderProperty.SHADOW_ENABLED.getName(),
			
			TextureLocation.VERTS_ATTRIBUTES.getName(),
			TextureLocation.NORMAL_ATTRIBUTES.getName(),
			TextureLocation.TEXCOORDS_ATTRIBUTES.getName() };

	private int program = 0;
	private List<String> properties;
	private final Map<String, ShaderProperty> userProperties;
	private String name;

	public final String getName() {
		return name;
	}

	public static void unbind() {
		if (locked) {
			return;
		}
		ShaderUtils.bind(0);
		activeShader = null;
	}

	public Shader() {
		properties = new LinkedList<String>();
		userProperties = new HashMap<String, ShaderProperty>();
	}

	public Shader bind() {
		if (locked || activeShader==this) {
			return this;
		}
		ShaderUtils.bind(program);
		activeShader = this;
		return this;
	}

	public Shader destroy() {
		ShaderUtils.deleteProgram(program);
		program = 0;
		return this;
	}

	public Shader loadFromFiles(String vertexFile, String fragmentFile)
			throws GraphicsException {
		String vertSource = GraphicsUtils.readFileAsString(vertexFile);
		String fragSource = GraphicsUtils.readFileAsString(fragmentFile);
		return loadFromFiles(vertSource, fragSource);
	}

	public Shader loadFromSource(String vertexSource, String fragmentSource)
			throws GraphicsException {
		properties.clear();
		ShaderUtils.deleteProgram(program);
		program = ShaderUtils.createShaderProgram(vertexSource, fragmentSource);
		populateProperties(vertexSource, fragmentSource);
		return this;
	}

	private void populateProperties(String vertexSource, String fragmentSource) {
		for (String p : LOOKUP_PROPERTIES) {
			if (vertexSource.contains(p) || fragmentSource.contains(p)) {
				properties.add(p);
			}
		}
	}
	
	public Shader setUniform1iv(String name, IntBuffer v) {
		ShaderUtils.setUniform1iv(program, name, v);
		return this;
	}
	
	public Shader setUniform2iv(String name, IntBuffer v) {
		ShaderUtils.setUniform2iv(program, name, v);
		return this;
	}
	
	public Shader setUniform3iv(String name, IntBuffer v) {
		ShaderUtils.setUniform3iv(program, name, v);
		return this;
	}
	
	public Shader setUniform4iv(String name, IntBuffer v) {
		ShaderUtils.setUniform4iv(program, name, v);
		return this;
	}

	public Shader setUniform1i(String name, int v0) {
		ShaderUtils.setUniform1i(program, name, v0);
		return this;
	}

	public Shader setUniform2i(String name, int v0, int v1) {
		ShaderUtils.setUniform2i(program, name, v0, v1);
		return this;
	}

	public Shader setUniform3i(String name, int v0, int v1, int v2) {
		ShaderUtils.setUniform3i(program, name, v0, v1, v2);
		return this;
	}

	public Shader setUniform4i(String name, int v0, int v1, int v2, int v3) {
		ShaderUtils.setUniform4i(program, name, v0, v1, v2, v3);
		return this;
	}

	public Shader setUniform4f(String name, float v0, float v1, float v2,
			float v3) {
		ShaderUtils.setUniform4f(program, name, v0, v1, v2, v3);
		return this;
	}

	public Shader setUniform3f(String name, float v0, float v1, float v2) {
		ShaderUtils.setUniform3f(program, name, v0, v1, v2);
		return this;
	}

	public Shader setUniform2f(String name, float v0, float v1) {
		ShaderUtils.setUniform2f(program, name, v0, v1);
		return this;
	}

	public Shader setUniform1f(String name, float v0) {
		ShaderUtils.setUniform1f(program, name, v0);
		return this;
	}

	public Shader setUniformMat3(String name, float[] matrix) {
		setUniformMat3(name, GraphicsUtils.asFloatBuffer(matrix));
		return this;
	}

	public void setUniformMat3(String name, FloatBuffer value) {
		ShaderUtils.setUniformMat3(program, name, value);
	}

	public Shader setUniformMat4(String name, Matrix44 matrix) {
		ShaderUtils.setUniformMat4(program, name, GraphicsUtils
				.asFloatBuffer(matrix.toArray()));
		return this;
	}

	public Shader setUniformMat4(String name, float[] matrix) {
		return setUniformMat4(name, GraphicsUtils
				.asFloatBuffer(matrix));
	}
	
	public Shader setUniformMat4(String name, FloatBuffer matrix) {
		ShaderUtils.setUniformMat4(program, name, matrix);
		return this;
	}

	public Shader setVertexPointer(String name, VboBuffer vbo, int size) {
		if (vbo == null) {
			return this;
		}
		ShaderUtils.setVertexArrayAttrPointer(program, vbo.getId(), name, vbo
				.getFormat(), size);
		return this;
	}

	public int getId() {
		return program;
	}

	public static void lock() {
		locked = true;
	}

	public static void unlock() {
		locked = false;
	}

	public static Shader getActiveShader() {
		return activeShader;
	}

	public boolean hasProperty(String property) {
		return properties.contains(property);
	}

	public Shader setProperty(ShaderProperty property, Object value) {
		if (!properties.contains(property.getName())) {
			return this;
		}

		Shader oldShader = activeShader;
		bind();
		if (activeShader != this) {
			return this;
		}

		property.setValue(this, value);
		
		if(oldShader!=null) {
			oldShader.bind();
		}
		return this;
	}
	
	public Shader setPropertyIgnoreNull(ShaderProperty property, Object value) {
		if( value==null) {
			return this;
		}
		return setProperty(property, value);
	}

	public Shader setUserProperty(String property, Object value) {
		if (value == null) {
			return this;
		}

		if (!userProperties.containsKey(property)) {
			return this;
		}

		Shader oldShader = activeShader;
		bind();
		if (activeShader != this) {			
			return this;
		}

		userProperties.get(property).setValue(this, value);
		
		if(oldShader!=null) {
			oldShader.bind();
		}
		return this;
	}

	public Shader registerProperty(ShaderProperty property) {
		userProperties.put(property.getName(), property);
		if (activeShader == this) {
			setUserProperty(property.getName(), property.getDefaultValue());
		}
		return this;
	}
	
	private void setTexture(TextureDescription textures, int index) {
		TextureLocation location = TextureLocation.fromIndex(index);
		Texture texture = textures.getTexture(location);
		
		setProperty(location, texture);
		if(texture==null) {
			setProperty(ShaderProperty.getTextureEnabledProperty(index), FALSE);
		} else {			
			setProperty(ShaderProperty.getTextureEnabledProperty(index), TRUE);
			setProperty(ShaderProperty.getTextureFormatProperty(index), texture.getPixelFormat());
			setProperty(ShaderProperty.getTextureMaskProperty(index), texture.isAlphaMask() ? Shader.TRUE : Shader.FALSE);
		}
	}
	
	private void updateTexture(TextureDescription textures, int index) {
		TextureLocation location = TextureLocation.fromIndex(index);
		Texture texture = textures.getTexture(location);
		
		setPropertyIgnoreNull(location, texture);
		if(texture!=null){
			setProperty(ShaderProperty.getTextureEnabledProperty(index), TRUE);
			setProperty(ShaderProperty.getTextureFormatProperty(index), texture.getPixelFormat());
			setProperty(ShaderProperty.getTextureMaskProperty(index), texture.isAlphaMask() ? Shader.TRUE : Shader.FALSE);
		}
	}
	
	public void setTextures(TextureDescription textures) {
		textures.updateTextures();
		setTexture(textures, 0);
		setTexture(textures, 1);
		setTexture(textures, 2);
		setTexture(textures, 3);
		setTexture(textures, 4);
		setTexture(textures, 5);
		setTexture(textures, 6);
		setTexture(textures, 7);
	}
	
	public void updateTextures(TextureDescription textures) {
		textures.updateTextures();
		updateTexture(textures, 0);
		updateTexture(textures, 1);
		updateTexture(textures, 2);
		updateTexture(textures, 3);
		updateTexture(textures, 4);
		updateTexture(textures, 5);
		updateTexture(textures, 6);
		updateTexture(textures, 7);
	}

	public boolean isActive() {
		return getActiveShader()==this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void resetDefaults() {
		ShaderRegistry.resetShaderValues(this);
	}
}
