package iv3d.graphics;

import iv3d.base.TransformState;
import iv3d.base.events.EventReceiver;

public interface RenderTarget{

	TransformState getTransformState();

	boolean isReceiveShadowsEnabled();

	void bindShader();

	boolean isBoundingBoxEnabled();

	boolean isVisible();

	void applyShaderProperties(Shader shader);

	boolean castShadowEnabled();

	boolean isEventsEnabled();

	EventReceiver asEventReceiver();
}
