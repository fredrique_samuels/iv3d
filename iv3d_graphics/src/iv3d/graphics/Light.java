/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import iv3d.base.Color;
import iv3d.base.math.Vector3;

import java.nio.FloatBuffer;

public class Light extends Camera {

	private Color ambient;
	private Color diffuse;
	private Color specular;
	private boolean shadow = false;
	private FrameBuffer frameBuffer;
	private FloatBuffer floatBuffer;

	public Light() {
		super();
		ambient = new Color(76, 76, 76, 255);
		diffuse = new Color(204, 204, 204, 255);
		specular = new Color(51, 51, 51, 255);
		frameBuffer = new FrameBuffer();
		floatBuffer = GraphicsUtils.asFloatBuffer(new float[16]);
		updateLightBuffer();
	}

	private void updateLightBuffer() {
		floatBuffer.put(toArray());
		floatBuffer.flip();
	}
	
	@Override
	public Camera setPosition(Vector3 position) {
		super.setPosition(position);
		updateLightBuffer();
		return this;
	}

	public final Color getAmbient() {
		return ambient;
	}

	public final Light setAmbient(Color ambient) {
		this.ambient = ambient;
		updateLightBuffer();
		return this;
	}

	public final Color getDiffuse() {
		return diffuse;
	}

	public final Light setDiffuse(Color diffuse) {
		this.diffuse = diffuse;
		updateLightBuffer();
		return this;
	}

	public final Color getSpecular() {
		return specular;
	}

	public final Light setSpecular(Color specular) {
		this.specular = specular;
		updateLightBuffer();
		return this;
	}


	public float[] toArray() {
		float[] a = ambient.normalize().toArray();
		float[] d = diffuse.normalize().toArray();
		float[] s = specular.normalize().toArray();
		return new float[] { a[0], a[1], a[2], a[3], d[0], d[1], d[2], d[3],
				s[0], s[1], s[2], s[3], getPosition().getX(), getPosition().getY(),
				getPosition().getZ(), 1 };

	}
	

	public Light enableShadow() {
		if(shadow) {
			return this;
		}
		shadow  = true;
		frameBuffer.createDepth();
		return this;
	}
	
	public Light disableShadow() {
		if(!shadow) {
			return this;
		}
		shadow  = false;
		frameBuffer.destoy();
		return this;
	}
	
	public boolean isShadowEnabled() {
		return shadow;
	}

	public FrameBuffer getFrameBuffer() {
		return frameBuffer;
	}

	public FloatBuffer toLightBuffer() {
		return floatBuffer;
	}

}
