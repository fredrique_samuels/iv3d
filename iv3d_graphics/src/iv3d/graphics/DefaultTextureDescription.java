/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import iv3d.base.ImageSource;
import iv3d.base.exceptions.GraphicsException;

public class DefaultTextureDescription implements TextureDescription {

	private final Texture[] textures;
	
	public DefaultTextureDescription() {
		textures = new Texture[] { null, null, null, null, null, null, null,
				null };
	}
	
	public DefaultTextureDescription(TextureDescription source) {
		textures = new Texture[] { source.getTexture(TextureLocation.fromIndex(0)), 
				source.getTexture(TextureLocation.fromIndex(1)),
				source.getTexture(TextureLocation.fromIndex(2)),
				source.getTexture(TextureLocation.fromIndex(3)),
				source.getTexture(TextureLocation.fromIndex(4)),
				source.getTexture(TextureLocation.fromIndex(5)),
				source.getTexture(TextureLocation.fromIndex(6)),
				source.getTexture(TextureLocation.fromIndex(7))};
	}
	
	public TextureDescription setTexture(Texture texture) {
		return setTexture(texture, TextureLocation.TEXTURE_0);
	}

	public TextureDescription setTexture(Texture texture, TextureLocation location) {
		textures[location.getIndex()] = texture;
		return this;
	}

	public TextureDescription setTexture(String filename, int filterOptions) throws GraphicsException {
		return setTexture(filename, filterOptions, TextureLocation.TEXTURE_0);
	}

	public TextureDescription setTexture(String filename, int filterOptions, TextureLocation location)
			throws GraphicsException {
		ImageSource imageSource = GraphicsUtils.readImageFromFile(filename, filterOptions);
		return setTexture(imageSource, location);
	}

	public TextureDescription setTexture(ImageSource imageSource) {
		return setTexture(new Texture().setImageSource(imageSource),
				TextureLocation.TEXTURE_0);
	}

	public TextureDescription setTexture(ImageSource imageSource, TextureLocation location) {
		return setTexture(new Texture().load(imageSource), location);
	}

	public Texture getTexture(TextureLocation location) {
		return textures[location.getIndex()];
	}
	
	@Override
	public TextureDescription updateTextures() {
		for(Texture texture : textures) {
			if(texture!=null) {
				texture.updateBuffer();
			}
		}
		return this;
	}
}
