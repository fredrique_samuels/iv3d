/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.shaders.store;

import iv3d.base.Color;
import iv3d.graphics.shaders.ColorProperty;
import iv3d.graphics.shaders.FloatProperty;
import iv3d.graphics.shaders.ShaderRegistry;

public class SHADER_C_SP {
	private SHADER_C_SP() {
	}
	
	public static final String ID = "SHADER_C_SP";
	
	
	
	private static String V_CODE = "uniform mat4 cg_ProjectionMatrix;"
		+ "uniform mat4 cg_ModelViewMatrix;"
		+ "uniform mat3 cg_NormalMatrix;"
		+ "attribute vec4 cg_Vertex;"
		+ "attribute vec4 cg_Normals;"
		+ "varying vec2 vTextureCoord;"
		+ "varying vec2 vTextureCoordSM;"
		+ "varying vec3 vNormal;"
		+ "varying vec3 vP;"
		+ "varying vec4 u;"
		+ "varying vec3 r;"
		+ "void main() {"
		+ "gl_Position = cg_ProjectionMatrix*cg_ModelViewMatrix*cg_Vertex;"
		+ "vNormal = vec3(cg_NormalMatrix*vec3(cg_Normals));"
		+ "u=cg_ModelViewMatrix*cg_Vertex;"
		+ "r=reflect(u.xyz,vNormal);"
		+ "float m = 2.0*sqrt(r.x*r.x + r.y*r.y + (r.z+1.0)*(r.z+1.0));"
		+ "vTextureCoordSM.s=r.x/m + .5;"
		+ "vTextureCoordSM.t=r.y/m + .5;}";
	
	private static String F_CODE = ""
		+ "uniform sampler2D cg_SphereMap;"
		+ "uniform float brightness;"
		+ "uniform vec4 color;"
		+ "uniform float alpha;"
		+ "varying vec2 vTextureCoordSM;"
		+ "varying vec3 vNormal;"
		+ "varying vec3 vP;"
		+ "void main() {"
		+ "vec4 spColor = texture2D(cg_SphereMap, vTextureCoordSM);"
		
		+ "vec4 finalColor = color*color.a;"
		+ "finalColor += spColor*spColor.a;"
		+ "finalColor *= brightness;"
		
		+ "gl_FragColor = vec4(finalColor.rgb, alpha);\n"
		+ "}";
	
	public static final String PROPERTY_FLOAT_BRIGHTNESS = "brightness";
	public static final String PROPERTY_COLOR = "color";
	public static final String PROPERTY_FLOAT_ALPHA = "alpha";
	public static final void register() {
		ShaderRegistry.registerShader(ID, V_CODE, F_CODE, new FloatProperty(PROPERTY_FLOAT_BRIGHTNESS, 1f),
				new FloatProperty(PROPERTY_FLOAT_ALPHA, 1f),
				new ColorProperty(PROPERTY_COLOR, Color.WHITE));
	}
}
