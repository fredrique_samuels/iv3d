/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.shaders.store;

import iv3d.base.Color;
import iv3d.graphics.shaders.ColorProperty;
import iv3d.graphics.shaders.FloatProperty;
import iv3d.graphics.shaders.IntProperty;
import iv3d.graphics.shaders.ShaderProperty;
import iv3d.graphics.shaders.ShaderRegistry;

public class SHADER_DEFAULT {
	private SHADER_DEFAULT() {
	}
	
	public static final String ID = SHADER_DEFAULT.class.getCanonicalName();
	
	private static String V_CODE = 
		//=======================================
		// Camera Matrix values 
		
		   "uniform mat4 cg_ProjectionMatrix;"
		 + "uniform mat4 cg_ModelViewMatrix;"
		 + "uniform mat3 cg_NormalMatrix;"
		 + "uniform mat4 cg_CameraMatrix;"
		 + "uniform mat4 cg_ObjectMatrix;"
		 + "uniform mat4 cg_ShadowProjMatrix;"
		 + "uniform mat4 cg_ShadowViewMatrix;"
		 
		 //=======================================
		 // Lights
		 + "uniform mat4 cg_Light0;"
		 + "uniform mat4 cg_Light1;"
		 + "uniform mat4 cg_Light2;"
		 + "uniform mat4 cg_Light3;"
		 + "uniform mat4 cg_Light4;"
		 + "uniform mat4 cg_Light5;"
		 + "uniform mat4 cg_Light6;"
		 + "uniform mat4 cg_Light7;"
		 
		 + "uniform int cg_Lights_Enabled;"
		 + "uniform int use_light;"
		
		 + "uniform int cg_Light0_Enabled;"
		 + "uniform int cg_Light1_Enabled;"
		 + "uniform int cg_Light2_Enabled;"
		 + "uniform int cg_Light3_Enabled;"
		 + "uniform int cg_Light4_Enabled;"
		 + "uniform int cg_Light5_Enabled;"
		 + "uniform int cg_Light6_Enabled;"
		 + "uniform int cg_Light7_Enabled;"
		 
		 + "varying vec3 vLight0_Dir;"
		 + "varying vec3 vLight1_Dir;"
		 + "varying vec3 vLight2_Dir;"
		 + "varying vec3 vLight3_Dir;"
		 + "varying vec3 vLight4_Dir;"
		 + "varying vec3 vLight5_Dir;"
		 + "varying vec3 vLight6_Dir;"
		 + "varying vec3 vLight7_Dir;"
		 
		 
		 //=======================================
		 // Vertex attributes
		 + "attribute vec4 cg_Vertex;"
		 + "attribute vec4 cg_Normals;"
		 + "attribute vec2 cg_TexCoord;"
		 
		 + "varying vec2 vTextureCoord;"
		 + "varying vec2 vTextureCoordSM;"
		 + "varying vec4 shadowTextureCoord;"
		 + "varying vec4 vNormal;"
		 + "varying vec3 vNormal3;"
		 + "varying vec3 vEyeVec;"
		 
		 //=======================================
		 // Textures
		 + "uniform int cg_TextureSphereMap_Enabled;"
		 + "uniform int cg_Shadow_Enabled;"
		 
		 //=======================================
		 
		 + "void main() {"
		 
		 + "gl_Position = cg_ProjectionMatrix*cg_ModelViewMatrix*cg_Vertex;"
		 + "vec3 P = vec3(cg_ModelViewMatrix * cg_Vertex);"
		
		 + "vNormal3 = vec3(cg_NormalMatrix*vec3(cg_Normals));"
		 + "vEyeVec = -P;"
		 
		 + "if(cg_Lights_Enabled==1 && use_light==1)"
		 + "{"
		 + "if(cg_Light0_Enabled==1)vLight0_Dir = -1.0*vec3((cg_CameraMatrix*cg_Light0[3]).xyz - P);"
		 + "if(cg_Light1_Enabled==1)vLight1_Dir = -1.0*vec3((cg_CameraMatrix*cg_Light1[3]).xyz - P);"
		 + "if(cg_Light2_Enabled==1)vLight2_Dir = -1.0*vec3((cg_CameraMatrix*cg_Light2[3]).xyz - P);"
		 + "if(cg_Light3_Enabled==1)vLight3_Dir = -1.0*vec3((cg_CameraMatrix*cg_Light3[3]).xyz - P);"
		 + "if(cg_Light4_Enabled==1)vLight4_Dir = -1.0*vec3((cg_CameraMatrix*cg_Light4[3]).xyz - P);"
		 + "if(cg_Light5_Enabled==1)vLight5_Dir = -1.0*vec3((cg_CameraMatrix*cg_Light5[3]).xyz - P);"
		 + "if(cg_Light6_Enabled==1)vLight6_Dir = -1.0*vec3((cg_CameraMatrix*cg_Light6[3]).xyz - P);"
		 + "if(cg_Light7_Enabled==1)vLight7_Dir = -1.0*vec3((cg_CameraMatrix*cg_Light7[3]).xyz - P);"
		 + "}"
		 
		 + "vTextureCoord = cg_TexCoord;"
		 
		 + "if(cg_TextureSphereMap_Enabled==1)"
		 + "{"
		 + "vec4 u=cg_ModelViewMatrix*cg_Vertex;"
		 + "vec3 r=reflect(u.xyz,vNormal);"
		 + "float m = 2.0*sqrt(r.x*r.x + r.y*r.y + (r.z+1.0)*(r.z+1.0));"
		 + "vTextureCoordSM.s=r.x/m + .5;"
		 + "vTextureCoordSM.t=r.y/m + .5;"
		 + "}"
		 
		 //===============
		 // Shadow stuff
		 + "if(cg_Shadow_Enabled==1)"
		 + "{"
		 + "mat4 bias = mat4(0.5, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.5, 0.5, 0.5, 1.0);"
		 + "mat4 lmv = cg_ShadowViewMatrix*cg_ObjectMatrix;"
		 + "shadowTextureCoord = ((bias*cg_ShadowProjMatrix)*lmv)*cg_Vertex;"
		 + "}"
		 
		 + "}";
	
	private static String F_CODE = "" 
		 // =====================================
		 // Textures
		 + "uniform sampler2D cg_Texture0;"
		 + "uniform sampler2D cg_Texture1;"
		 + "uniform sampler2D cg_Texture2;"
		 + "uniform sampler2D cg_Texture3;"
		 + "uniform sampler2D cg_ProjMap;"
		 + "uniform sampler2D cg_SphereMap;"
		 + "uniform sampler2D cg_BumpMap;"
		 + "uniform sampler2D cg_ShadowMap;"
		 
		 + "uniform int cg_Texture0_Enabled;"
		 + "uniform int cg_Texture1_Enabled;"
		 + "uniform int cg_Texture2_Enabled;"
		 + "uniform int cg_Texture3_Enabled;"
		 + "uniform int cg_TextureProjMap_Enabled;"
		 + "uniform int cg_TextureSphereMap_Enabled;"
		 + "uniform int cg_TextureBumpMap_Enabled;"
		 + "uniform int cg_TextureShadowMap_Enabled;"
		 
		 + "uniform int cg_Texture0_Mask;"
		 + "uniform int cg_Texture1_Mask;"
		 + "uniform int cg_Texture2_Mask;"
		 + "uniform int cg_Texture3_Mask;"
		 + "uniform int cg_TextureProjMap_Mask;"
		 + "uniform int cg_TextureSphereMap_Mask;"
		 + "uniform int cg_TextureBumpMap_Mask;"
		 + "uniform int cg_TextureShadowMap_Mask;"

		 + "uniform int cg_Shadow_Enabled;"
		 
		 + "uniform ivec4 cg_Texture0_Format;"
		 + "uniform ivec4 cg_Texture1_Format;"
		 + "uniform ivec4 cg_Texture2_Format;"
		 + "uniform ivec4 cg_Texture3_Format;"
		 + "uniform ivec4 cg_TextureProjMap_Format;"
		 + "uniform ivec4 cg_TextureSphereMap_Format;"
		 + "uniform ivec4 cg_TextureBumpMap_Format;"
		 + "uniform ivec4 cg_TextureShadowMap_Format;"
		 
		 
		 
		 + "varying vec2 vTextureCoord;"
		 + "varying vec2 vTextureCoordSM;"
		 + "varying vec4 shadowTextureCoord;"

		 + "uniform mat4 cg_Material;"

		 //=======================================
		 // Variables
		 + "uniform mat3 cg_NormalMatrix;"
		 + "uniform float shininess;"
		 + "uniform vec4 color;"
		 + "uniform float alpha;"
		 
		 + "varying vec3 vNormal3;"
		 + "varying vec3 vEyeVec;" 
		 
		 //=======================================
		 // Lights
		 + "uniform mat4 cg_Light0;"
		 + "uniform mat4 cg_Light1;"
		 + "uniform mat4 cg_Light2;"
		 + "uniform mat4 cg_Light3;"
		 + "uniform mat4 cg_Light4;"
		 + "uniform mat4 cg_Light5;"
		 + "uniform mat4 cg_Light6;"
		 + "uniform mat4 cg_Light7;"
		 
		 + "uniform int cg_Lights_Enabled;"
		 + "uniform int use_light;"

		 + "uniform int cg_Light0_Enabled;"
		 + "uniform int cg_Light1_Enabled;"
		 + "uniform int cg_Light2_Enabled;"
		 + "uniform int cg_Light3_Enabled;"
		 + "uniform int cg_Light4_Enabled;"
		 + "uniform int cg_Light5_Enabled;"
		 + "uniform int cg_Light6_Enabled;"
		 + "uniform int cg_Light7_Enabled;"
		 
		 + "varying vec3 vLight0_Dir;"
		 + "varying vec3 vLight1_Dir;"
		 + "varying vec3 vLight2_Dir;"
		 + "varying vec3 vLight3_Dir;"
		 + "varying vec3 vLight4_Dir;"
		 + "varying vec3 vLight5_Dir;"
		 + "varying vec3 vLight6_Dir;"
		 + "varying vec3 vLight7_Dir;"
		 
		 //=======================================
		 // texture functions
		 + "void applyPixelFormat(in ivec4 format, inout vec4 colorIn) {"
		 + "		float pixels[4]; "
		 + "		pixels[0] = colorIn.r; "
		 + "		pixels[1] = colorIn.g; "
		 + "		pixels[2] = colorIn.b; "
		 + "		pixels[3] = colorIn.a; "
		 + "		colorIn = vec4(pixels[format.r], pixels[format.g], pixels[format.b], pixels[format.a]);"
		 + "}"
		
			
		 + "void addTexture(in sampler2D tex, inout vec4 final_color,in ivec4 format, inout int firstTexture, in vec2 texcoord, in int mask)" 
		 + "{"
		 + "vec4 tex_color = texture2D(tex, texcoord);"
		 + "applyPixelFormat(format, tex_color);"
		 + "if(mask==1)" 
		 + "{" 
		 + "final_color=vec4(final_color.r, final_color.g,final_color.b, tex_color.a*final_color.a);"
		 + "}"
		 + "else if(firstTexture==1)" 
		 + "{" 
		 + "final_color=tex_color;" 
		 + "}" 
		 + "else if(tex_color.a > 0.0)" 
		 + "{" 
		 + "final_color=vec4(final_color.rgb*(tex_color.rgb*tex_color.a), mask==1?tex_color.a:final_color.a);" 
		 + "}"
		 + "firstTexture=0;"
		 + "}"
		 
		 + "void addShadow(in sampler2D tex, inout vec4 final_color,in ivec4 format, inout int firstTexture, in vec4 shadowTextureCoord)" 
		 + "{"
		 	+ "vec4 shadowCoordinateWdivide = shadowTextureCoord / shadowTextureCoord.w ;"
		 	+ "shadowCoordinateWdivide.z -= 0.0001;"
			+ "vec2 coords = vec2(shadowCoordinateWdivide.s, shadowCoordinateWdivide.t);"

			+ "float distanceFromLight = texture2D(tex,coords).z;"
			+ "float shadow = 1.0;"
			+ "float shadowIntensity = 0.9;"
			+ "if (shadowTextureCoord.w > 0.0)" 
			+ "{"
			+ "shadow = distanceFromLight < shadowCoordinateWdivide.z ? shadowIntensity : 1.0;" 
			+ "}"
			+ "final_color = final_color*vec4(shadow,shadow,shadow,1);"
		 + "}"

		 + "void addTextures(inout vec4 final_color)" 
		 + "{"	 
		 + "int firstTexture=1;"
		 + "if(cg_Texture0_Enabled==1)addTexture(cg_Texture0, final_color, cg_Texture0_Format, firstTexture, vTextureCoord, cg_Texture0_Mask);" 
		 + "if(cg_Texture1_Enabled==1)addTexture(cg_Texture1, final_color, cg_Texture1_Format, firstTexture, vTextureCoord, cg_Texture1_Mask);" 
		 + "if(cg_Texture2_Enabled==1)addTexture(cg_Texture2, final_color, cg_Texture2_Format, firstTexture, vTextureCoord, cg_Texture2_Mask);" 
		 + "if(cg_Texture3_Enabled==1)addTexture(cg_Texture3, final_color, cg_Texture3_Format, firstTexture, vTextureCoord, cg_Texture3_Mask);" 
		 + "if(cg_TextureProjMap_Enabled==1)addTexture(cg_ProjMap, final_color, cg_TextureProjMap_Format, firstTexture, vTextureCoord, cg_TextureProjMap_Mask);" 
		 + "if(cg_TextureSphereMap_Enabled==1)addTexture(cg_SphereMap, final_color, cg_TextureSphereMap_Format, firstTexture, vTextureCoordSM, cg_TextureSphereMap_Mask);" 
		 + "if(cg_TextureShadowMap_Enabled==1 && cg_Shadow_Enabled==1)addShadow(cg_ShadowMap, final_color, cg_TextureShadowMap_Format, firstTexture, shadowTextureCoord);" 
		 + "}"
		 
		 //=======================================
		 // light functions
		 + "void addLight(inout vec4 final_color," +
		 		"in mat4 light," +
		 		"in vec3 light_dir," +
		 		"in vec4 mat_ambient," +
		 		"in vec4 mat_diffuse," +
		 		"in vec4 mat_specular," +
		 		"in vec4 mat_emissive)" 
		 + "{"
		 // light values
		 + "vec4 light_ambient  = vec4(light[0]);"
		 + "vec4 light_diffuse  = vec4(light[1]);"
		 + "vec4 light_specular = vec4(light[2]);"
		 
		 // light color
		 + "vec4 light_color = vec4(1,1,1,1);"
		 + "light_color = (light_ambient * mat_ambient);"
		 + "vec3 N = normalize(vNormal3);"
		 + "if(cg_TextureBumpMap_Enabled==1)"
		 + "{"
	 	 + "vec3 norm =  texture2D(cg_BumpMap, vTextureCoord).rgb-.5; "
		 + "N = normalize(vec3(cg_NormalMatrix*cross(vNormal3, norm)));" 
		 + "}"
		 + "vec3 L = normalize(light_dir);"
		 + "float lambertTerm = dot(N,L);"
		 
		 + "if(lambertTerm > 0.0) " 
		 + "{"
		 + "light_color += light_diffuse * mat_diffuse * lambertTerm;"
		 + "vec3 E = normalize(vEyeVec);" + "vec3 R = reflect(-L, N);"
		 + "float specular = pow( max(dot(R, E), 0.0), shininess );"
		 + "light_color += light_specular * mat_specular *  specular;"
		 + "}"
		 + "final_color *= light_color;"
		 + "}"
		 

		 + "void addLights(inout vec4 final_color)" 
		 + "{"
		 
		 + "vec4 mat_ambient  = vec4(cg_Material[0]);"
		 + "vec4 mat_diffuse  = vec4(cg_Material[1]);"
		 + "vec4 mat_specular = vec4(cg_Material[2]);"
		 + "vec4 mat_emissive = vec4(cg_Material[3]);"
		 
		 + "if(cg_Light0_Enabled==1)addLight(final_color, cg_Light0, vLight0_Dir, mat_ambient, mat_diffuse, mat_specular, mat_emissive);"
		 + "if(cg_Light1_Enabled==1)addLight(final_color, cg_Light1, vLight1_Dir, mat_ambient, mat_diffuse, mat_specular, mat_emissive);"
		 + "if(cg_Light2_Enabled==1)addLight(final_color, cg_Light2, vLight2_Dir, mat_ambient, mat_diffuse, mat_specular, mat_emissive);"
		 + "if(cg_Light3_Enabled==1)addLight(final_color, cg_Light3, vLight3_Dir, mat_ambient, mat_diffuse, mat_specular, mat_emissive);"
		 + "if(cg_Light4_Enabled==1)addLight(final_color, cg_Light4, vLight4_Dir, mat_ambient, mat_diffuse, mat_specular, mat_emissive);"
		 + "if(cg_Light5_Enabled==1)addLight(final_color, cg_Light5, vLight5_Dir, mat_ambient, mat_diffuse, mat_specular, mat_emissive);"
		 + "if(cg_Light6_Enabled==1)addLight(final_color, cg_Light6, vLight6_Dir, mat_ambient, mat_diffuse, mat_specular, mat_emissive);"
		 + "if(cg_Light7_Enabled==1)addLight(final_color, cg_Light7, vLight7_Dir, mat_ambient, mat_diffuse, mat_specular, mat_emissive);"
		 + "}"
		 
		 //=======================================
		 + "void main() " 
		 + "{"
	 	 
		 // Light values
		 + "vec4 light_color = vec4(1,1,1,1);"
		 + "if(cg_Lights_Enabled==1 && use_light==1)addLights(light_color);"
		 
		 // texture color
		 + "vec4 textureColor = vec4(1,1,1,1);"
		 + "addTextures(textureColor);"
		 
		 + "vec4 final_color=textureColor*light_color*color;"
		 + "gl_FragColor = vec4(final_color.rgb, final_color.a*alpha);" 
		 
		 + "}";
	
	
	public static final void register() {
		ShaderRegistry.registerShader(ID, V_CODE, F_CODE, 
				new FloatProperty(ShaderProperty.PROPERTY_FLOAT_SHININESS, Float.valueOf(100.0f)),
				new FloatProperty(ShaderProperty.PROPERTY_FLOAT_ALPHA, Float.valueOf(1.0f)),
				new ColorProperty(ShaderProperty.PROPERTY_COLOR, Color.WHITE),
				new IntProperty(ShaderProperty.PROPERTY_USE_LIGHT, Integer.valueOf(0)));
	}
}
