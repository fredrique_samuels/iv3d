/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.shaders.store;

import iv3d.graphics.shaders.ShaderRegistry;

public class SHADER_1T {
	private SHADER_1T() {
	}
	
	public static final String ID = SHADER_1T.class.getCanonicalName();
	
	private static String V_CODE = "uniform mat4 cg_ModelViewMatrix;"
		+ "uniform mat4 cg_ProjectionMatrix;"
		+ "attribute vec4 cg_Vertex;"
		+ "attribute vec4 cg_TexCoord;"
		+ "varying vec2 vTextureCoord;"
		+ "void main() {"
		+ "gl_Position = cg_ProjectionMatrix*cg_ModelViewMatrix*cg_Vertex;"
		+ "vTextureCoord = cg_TexCoord;" 
		+ "}";
	
	private static String F_CODE = "varying vec2 vTextureCoord;"
		+ "uniform sampler2D cg_Texture0;" 
		+ "void main() {"
		+ "gl_FragColor = texture2D(cg_Texture0, vTextureCoord);" 
		+ "}";
	
	
	public static final void register() {
		ShaderRegistry.registerShader(ID, V_CODE, F_CODE);
	}
}
