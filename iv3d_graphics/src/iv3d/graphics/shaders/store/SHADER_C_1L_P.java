/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.shaders.store;

import iv3d.base.Color;
import iv3d.graphics.shaders.ColorProperty;
import iv3d.graphics.shaders.FloatProperty;
import iv3d.graphics.shaders.ShaderRegistry;

public class SHADER_C_1L_P {
	private SHADER_C_1L_P() {
	}
	
	public static final String ID = "SHADER_C_1L_P";
	
	private static String V_CODE = "uniform mat4 cg_ProjectionMatrix;"
		 + "uniform mat4 cg_ModelViewMatrix;"
		 + "uniform mat3 cg_NormalMatrix;"
		 + "uniform mat4 cg_Light0;"
		 + "uniform mat4 cg_CameraMatrix;"
		 + "attribute vec4 cg_Vertex;"
		 + "attribute vec4 cg_Normals;"
		 + "varying vec4 vNormal;"
		 + "varying vec3 normal, lightDir, eyeVec;"  
		 + "void main() {"
		 + "gl_Position = cg_ProjectionMatrix*cg_ModelViewMatrix*cg_Vertex;"
		 + "normal = vec3(cg_NormalMatrix*vec3(cg_Normals));"
		 + "vec3 P = vec3(cg_ModelViewMatrix * cg_Vertex);"
		 + "eyeVec = -P;"
		 + "lightDir = -1*vec3((cg_CameraMatrix*cg_Light0[3]).xyz - P);"
		 + "}";
	
	private static String F_CODE = ""
		 + "uniform mat4 cg_Light0;" + "uniform mat4 cg_Material;"
		 + "uniform float shininess;"
		 + "uniform vec4 color;"
		 + "uniform float alpha;"
		 + "varying vec3 normal, lightDir, eyeVec;" + "void main() {"
		 + "vec4 mat_ambient  = vec4(cg_Material[0]);"
		 + "vec4 mat_diffuse  = vec4(cg_Material[1]);"
		 + "vec4 mat_specular = vec4(cg_Material[2]);"
		 + "vec4 mat_emissive = vec4(cg_Material[3]);"
		 + "vec4 light_ambient  = vec4(cg_Light0[0]);"
		 + "vec4 light_diffuse  = vec4(cg_Light0[1]);"
		 + "vec4 light_specular = vec4(cg_Light0[2]);"
		 + "vec4 final_color = (light_ambient * mat_ambient);"
		 + "vec3 N = normalize(normal);"
		 + "vec3 L = normalize(lightDir);"
		 + "float lambertTerm = dot(N,L);"
		 + "if(lambertTerm > 0.0) {"
		 + "final_color += light_diffuse * mat_diffuse * lambertTerm;"
		 + "vec3 E = normalize(eyeVec);" + "vec3 R = reflect(-L, N);"
		 + "float specular = pow( max(dot(R, E), 0.0), shininess );"
		 + "final_color +=  light_specular * mat_specular *  specular;}"
		 + "vec4 surfacecolor = final_color*color;\n"
		 + "gl_FragColor = vec4(surfacecolor.rgb, alpha);}";
	
	public static final String PROPERTY_FLOAT_SHININESS = "shininess";
	public static final String PROPERTY_COLOR = "color";
	public static final String PROPERTY_FLOAT_ALPHA = "alpha";
	public static final void register() {
		ShaderRegistry.registerShader(ID, V_CODE, F_CODE, new FloatProperty(PROPERTY_FLOAT_SHININESS, 30f),
				new FloatProperty(PROPERTY_FLOAT_ALPHA, 1.0f),
				new ColorProperty(PROPERTY_COLOR, Color.WHITE));
	}
}
