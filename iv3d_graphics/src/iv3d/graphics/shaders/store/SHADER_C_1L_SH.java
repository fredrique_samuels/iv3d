/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.shaders.store;

import iv3d.base.Color;
import iv3d.graphics.shaders.ColorProperty;
import iv3d.graphics.shaders.ShaderRegistry;

public class SHADER_C_1L_SH {
	private SHADER_C_1L_SH() {
	}
	
	public static final String ID = "SHADER_C_1L_SH";
	
	private static String V_CODE = "uniform mat4 cg_ModelViewMatrix;"
		+ "uniform mat4 cg_ProjectionMatrix;"
		+ "uniform mat4 cg_ObjectMatrix;"
		+ "uniform mat4 cg_ShadowProjMatrix;"
		+ "uniform mat4 cg_ShadowViewMatrix;"
		+ "attribute vec4 cg_Vertex;"
		+ "varying vec4 shadowTextureCoord;"
		+ "void main() {"
		+ "gl_Position = cg_ProjectionMatrix*cg_ModelViewMatrix*cg_Vertex;"

		+ "mat4 bias = mat4(0.5, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.5, 0.5, 0.5, 1.0);"
		+ "mat4 lmv = cg_ShadowViewMatrix*cg_ObjectMatrix;"
		+ "shadowTextureCoord = ((bias*cg_ShadowProjMatrix)*lmv)*cg_Vertex;"
		+ "}";

	private static String F_CODE = ""
		+ "uniform vec4 color;"
		+ "uniform vec4 shadowColor;"
		+ "uniform sampler2D cg_ShadowMap;"
		+ "varying vec4 shadowTextureCoord;"
		+ "void main() {"
		+ "vec4 shadowCoordinateWdivide = shadowTextureCoord / shadowTextureCoord.w ;"
		+ "shadowCoordinateWdivide.z -= 0.0001;"
		+ "vec2 coords = vec2(shadowCoordinateWdivide.s, shadowCoordinateWdivide.t);"

		+ "float distanceFromLight = texture2D(cg_ShadowMap,coords).z;"
		+ "vec4 finalShadow = vec4(0, 0, 0, 0);"
		+ "if (shadowTextureCoord.w > 0.0){"
		+ "		finalShadow = distanceFromLight < shadowCoordinateWdivide.z ? shadowColor : finalShadow;}"

		+ "vec4 finalColor = color;"
		+ "if(finalShadow.a>0.0){finalColor *= finalShadow;}"
		
		+ "gl_FragColor = finalColor;" + "}";

	public static final String PROPERTY_COLOR = "color";
	public static final String PROPERTY_SHADOW_COLOR = "shadowColor";
	public static final void register() {
		ShaderRegistry.registerShader(ID, V_CODE, F_CODE, new ColorProperty(PROPERTY_COLOR, Color.WHITE),
				new ColorProperty(PROPERTY_SHADOW_COLOR, Color.GRAY));
	}
}
