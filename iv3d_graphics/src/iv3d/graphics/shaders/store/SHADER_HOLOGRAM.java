/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.shaders.store;

import iv3d.base.Color;
import iv3d.graphics.shaders.ColorProperty;
import iv3d.graphics.shaders.FloatProperty;
import iv3d.graphics.shaders.ShaderRegistry;

public class SHADER_HOLOGRAM {
	private SHADER_HOLOGRAM() {
	}
	
	
	private static final String V_CODE = "uniform mat4 cg_ProjectionMatrix;"
		+ "uniform mat4 cg_ModelViewMatrix;"
		+ "uniform mat3 cg_NormalMatrix;"
		+ "attribute vec4 cg_Vertex;"
		+ "attribute vec4 cg_Normals;"
		+ "varying vec3 P;"
		+ "varying vec3 N;"
		+ "varying vec3 I;"
		+ "void main() {"
		+ "gl_Position = cg_ProjectionMatrix*cg_ModelViewMatrix*cg_Vertex;"
		+ "P = vec3(cg_ModelViewMatrix * cg_Vertex);"
		+ "N = vec3(cg_NormalMatrix * vec3(cg_Normals));" + "I = P;"
		+ "}";
	private static final String F_CODE = "varying vec3 P;" + "varying vec3 N;"
		+ "varying vec3 I;"
		+ "uniform float intensity;"
		+ "uniform vec4 color;"
		+ "void main() {"
		+ "	float opacity = dot(normalize(N), normalize(-I));"
		+ "opacity = abs(opacity);"
		+ "opacity = 1.0 - pow(opacity, intensity);"
		+ "gl_FragColor = vec4(color.rgb *opacity ,color.a);" + "}";
	
	
	public static final String ID = "SHADER_HOLOGRAM";
	public static final String PROPERTY_FLOAT_INTENSITY = "intensity";
	public static final String PROPERTY_COLOR = "color";
	public static final void register() {
		ShaderRegistry.registerShader(ID, V_CODE, F_CODE, new FloatProperty(PROPERTY_FLOAT_INTENSITY, .5f),
				new ColorProperty(PROPERTY_COLOR, new Color(0, 162, 232, 255)));
	}
}
