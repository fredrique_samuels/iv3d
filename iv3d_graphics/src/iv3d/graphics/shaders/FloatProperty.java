/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.shaders;

import iv3d.graphics.Shader;

public class FloatProperty extends ShaderProperty{
	private Float defaultValue;
	
	public FloatProperty(String name) {
		this(name, null);
	}

	public FloatProperty(String name, Float f) {
		super(name);
		defaultValue = f;
	}

	@Override
	public void setValue(Shader shader, Object value) {
		if (!(value instanceof Float)) {
			return;
		}
		shader.setUniform1f(getName(), ((Float)value).floatValue());
	}
	
	@Override
	public Object getDefaultValue() {
		return defaultValue;
	}
}
