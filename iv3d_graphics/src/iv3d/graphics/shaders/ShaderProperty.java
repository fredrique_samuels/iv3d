/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.shaders;

import iv3d.graphics.Shader;

public abstract class ShaderProperty {
	public static final String PROPERTY_FLOAT_SHININESS = "shininess";
	public static final String PROPERTY_FLOAT_BRIGHTNESS = "brightness";
	public static final String PROPERTY_COLOR = "color";
	public static final String PROPERTY_FLOAT_ALPHA = "alpha";
	public static final String PROPERTY_USE_LIGHT = "use_light";

	public static final ShaderProperty MODEL_VIEW_MATRIX = new Matrix44Property(
			"cg_ModelViewMatrix");
	public static final ShaderProperty PROJECTION_MATRIX = new Matrix44Property(
			"cg_ProjectionMatrix");
	public static final ShaderProperty CAMERA_MATRIX = new Matrix44Property(
			"cg_CameraMatrix");
	public static final ShaderProperty CAMERA_MATRIX_I = new Matrix44Property(
			"cg_CameraMatrixI");
	public static final ShaderProperty OBJECT_MATRIX = new Matrix44Property(
			"cg_ObjectMatrix");
	public static final ShaderProperty  OBJECT_MATRIX_I = new Matrix44Property(
			"cg_ObjectMatrixI");;
	public static final ShaderProperty SHADOW_PROJ_MATRIX = new Matrix44Property(
			"cg_ShadowProjMatrix");
	public static final ShaderProperty SHADOW_VIEW_MATRIX = new Matrix44Property(
			"cg_ShadowViewMatrix");
	public static final ShaderProperty MATERIAL = new MaterialProperty(
			"cg_Material");
	public static final ShaderProperty LIGHT_0 = new LightProperty("cg_Light0");
	public static final ShaderProperty LIGHT_1 = new LightProperty("cg_Light1");
	public static final ShaderProperty LIGHT_2 = new LightProperty("cg_Light2");
	public static final ShaderProperty LIGHT_3 = new LightProperty("cg_Light3");
	public static final ShaderProperty LIGHT_4 = new LightProperty("cg_Light4");
	public static final ShaderProperty LIGHT_5 = new LightProperty("cg_Light5");
	public static final ShaderProperty LIGHT_6 = new LightProperty("cg_Light6");
	public static final ShaderProperty LIGHT_7 = new LightProperty("cg_Light7");
	
	public static final ShaderProperty LIGHT_0_ENABLED = new IntProperty("cg_Light0_Enabled");
	public static final ShaderProperty LIGHT_1_ENABLED = new IntProperty("cg_Light1_Enabled");
	public static final ShaderProperty LIGHT_2_ENABLED = new IntProperty("cg_Light2_Enabled");
	public static final ShaderProperty LIGHT_3_ENABLED = new IntProperty("cg_Light3_Enabled");
	public static final ShaderProperty LIGHT_4_ENABLED = new IntProperty("cg_Light4_Enabled");
	public static final ShaderProperty LIGHT_5_ENABLED = new IntProperty("cg_Light5_Enabled");
	public static final ShaderProperty LIGHT_6_ENABLED = new IntProperty("cg_Light6_Enabled");
	public static final ShaderProperty LIGHT_7_ENABLED = new IntProperty("cg_Light7_Enabled");
	public static final ShaderProperty LIGHTS_ENABLED = new IntProperty("cg_Lights_Enabled");
	
	public static final ShaderProperty TEXTURE_0_ENABLED = new IntProperty("cg_Texture0_Enabled");
	public static final ShaderProperty TEXTURE_1_ENABLED = new IntProperty("cg_Texture1_Enabled");
	public static final ShaderProperty TEXTURE_2_ENABLED = new IntProperty("cg_Texture2_Enabled");
	public static final ShaderProperty TEXTURE_3_ENABLED = new IntProperty("cg_Texture3_Enabled");
	public static final ShaderProperty TEXTURE_PROJ_ENABLED = new IntProperty("cg_TextureProjMap_Enabled");
	public static final ShaderProperty TEXTURE_SPHERE_ENABLED = new IntProperty("cg_TextureSphereMap_Enabled");
	public static final ShaderProperty TEXTURE_BUMP_ENABLED = new IntProperty("cg_TextureBumpMap_Enabled");
	public static final ShaderProperty TEXTURE_SHADOW_ENABLED = new IntProperty("cg_TextureShadowMap_Enabled");
	
	public static final ShaderProperty TEXTURE_0_FORMAT = new PixelFormatProperty("cg_Texture0_Format");
	public static final ShaderProperty TEXTURE_1_FORMAT = new PixelFormatProperty("cg_Texture1_Format");
	public static final ShaderProperty TEXTURE_2_FORMAT = new PixelFormatProperty("cg_Texture2_Format");
	public static final ShaderProperty TEXTURE_3_FORMAT = new PixelFormatProperty("cg_Texture3_Format");
	public static final ShaderProperty TEXTURE_PROJ_FORMAT = new PixelFormatProperty("cg_TextureProjMap_Format");
	public static final ShaderProperty TEXTURE_SPHERE_FORMAT = new PixelFormatProperty("cg_TextureSphereMap_Format");
	public static final ShaderProperty TEXTURE_BUMP_FORMAT = new PixelFormatProperty("cg_TextureBumpMap_Format");
	public static final ShaderProperty TEXTURE_SHADOW_FORMAT = new PixelFormatProperty("cg_TextureShadowMap_Format");
	
	public static final ShaderProperty TEXTURE_0_MASK = new IntProperty("cg_Texture0_Mask");
	public static final ShaderProperty TEXTURE_1_MASK = new IntProperty("cg_Texture1_Mask");
	public static final ShaderProperty TEXTURE_2_MASK = new IntProperty("cg_Texture2_Mask");
	public static final ShaderProperty TEXTURE_3_MASK = new IntProperty("cg_Texture3_Mask");
	public static final ShaderProperty TEXTURE_PROJ_MASK = new IntProperty("cg_TextureProjMap_Mask");
	public static final ShaderProperty TEXTURE_SPHERE_MASK = new IntProperty("cg_TextureSphereMap_Mask");
	public static final ShaderProperty TEXTURE_BUMP_MASK = new IntProperty("cg_TextureBumpMap_Mask");
	public static final ShaderProperty TEXTURE_SHADOW_MASK = new IntProperty("cg_TextureShadowMap_Mask");

	public static final ShaderProperty SHADOW_ENABLED = new IntProperty("cg_Shadow_Enabled");;
	
	public static final ShaderProperty NORMAL_MATRIX = new Matrix33Property(
			"cg_NormalMatrix");
	public static final ShaderProperty VERTS_ATTRIBUTES = new VertexAttributeProperty(
			"cg_Vertex", 3);
	public static final ShaderProperty NORMAL_ATTRIBUTES = new VertexAttributeProperty(
			"cg_Normals", 3);
	public static final ShaderProperty TEXCOORDS_ATTRIBUTES = new VertexAttributeProperty(
			"cg_TexCoord", 2);

	private final String name;
	
	private static ShaderProperty[] lights = new ShaderProperty[]{
		LIGHT_0,
		LIGHT_1,
		LIGHT_2,
		LIGHT_3,
		LIGHT_4,
		LIGHT_5,
		LIGHT_6,
		LIGHT_7
	};
	
	private static ShaderProperty[] lightsEnabled = new ShaderProperty[]{
		LIGHT_0_ENABLED,
		LIGHT_1_ENABLED,
		LIGHT_2_ENABLED,
		LIGHT_3_ENABLED,
		LIGHT_4_ENABLED,
		LIGHT_5_ENABLED,
		LIGHT_6_ENABLED,
		LIGHT_7_ENABLED
	};
	
	private static ShaderProperty[] textureEnabled = new ShaderProperty[]{
		TEXTURE_0_ENABLED,
		TEXTURE_1_ENABLED,
		TEXTURE_2_ENABLED,
		TEXTURE_3_ENABLED,
		TEXTURE_PROJ_ENABLED,
		TEXTURE_SPHERE_ENABLED,
		TEXTURE_BUMP_ENABLED,
		TEXTURE_SHADOW_ENABLED
	};
	
	private static ShaderProperty[] textureFormat = new ShaderProperty[]{
		TEXTURE_0_FORMAT,
		TEXTURE_1_FORMAT,
		TEXTURE_2_FORMAT,
		TEXTURE_3_FORMAT,
		TEXTURE_PROJ_FORMAT,
		TEXTURE_SPHERE_FORMAT,
		TEXTURE_BUMP_FORMAT,
		TEXTURE_SHADOW_FORMAT
	};
	
	private static ShaderProperty[] textureMask = new ShaderProperty[]{
		TEXTURE_0_MASK,
		TEXTURE_1_MASK,
		TEXTURE_2_MASK,
		TEXTURE_3_MASK,
		TEXTURE_PROJ_MASK,
		TEXTURE_SPHERE_MASK,
		TEXTURE_BUMP_MASK,
		TEXTURE_SHADOW_MASK
	};

	public ShaderProperty(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public static ShaderProperty getLightProperty(int index) {
		return lights[index];
	}
	
	public static ShaderProperty getLightEnabledProperty(int index) {
		return lightsEnabled[index];
	}
	
	public static ShaderProperty getTextureEnabledProperty(int index) {
		return textureEnabled[index];
	}
	
	public static ShaderProperty getTextureFormatProperty(int index) {
		return textureFormat[index];
	}
	
	public static ShaderProperty getTextureMaskProperty(int index) {
		return textureMask[index];
	}

	public abstract void setValue(Shader shader, Object value);
	public Object getDefaultValue(){return null;}
}
