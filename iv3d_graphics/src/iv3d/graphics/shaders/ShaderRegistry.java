/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics.shaders;


import iv3d.base.exceptions.GraphicsException;
import iv3d.graphics.Shader;
import iv3d.graphics.shaders.store.SHADER_1L_P_1T;
import iv3d.graphics.shaders.store.SHADER_1L_SH_1T;
import iv3d.graphics.shaders.store.SHADER_1T;
import iv3d.graphics.shaders.store.SHADER_B_1L_P_1T;
import iv3d.graphics.shaders.store.SHADER_C;
import iv3d.graphics.shaders.store.SHADER_CAIRO_1T;
import iv3d.graphics.shaders.store.SHADER_C_1L_P;
import iv3d.graphics.shaders.store.SHADER_C_1L_SH;
import iv3d.graphics.shaders.store.SHADER_C_SP;
import iv3d.graphics.shaders.store.SHADER_DEFAULT;
import iv3d.graphics.shaders.store.SHADER_HOLOGRAM;
import iv3d.graphics.shaders.store.SHADER_RED;
import iv3d.graphics.shaders.store.SHADER_SP;
import iv3d.graphics.shaders.store.SHADER_SP_1T;
import iv3d.graphics.shaders.store.SHADER_WHITE;

import java.util.HashMap;
import java.util.Map;


public class ShaderRegistry {
	
	private static Map<String, ShaderEntry> shaders = new HashMap<String, ShaderEntry>();

	private ShaderRegistry() {
	}

	public static void registerShader(String key, String vertsSource,
			String fragSource, ShaderProperty... properties) {
		if (shaders.containsKey(key)) {
			shaders.get(key).destroy();
		}
		shaders.put(key, new ShaderEntry(key, vertsSource, fragSource, properties));
	}

	public static Shader getShader(String key) {
		if (shaders.containsKey(key)) {
			return shaders.get(key).getShader();
		}
		return null;
	}
	
	public static Shader createNew(String key) {
		if (shaders.containsKey(key)) {
			return shaders.get(key).create();
		}
		return null;
	}
	
	public static void resetShaderValues(Shader shader) {
		if (shaders.containsKey(shader.getName())) {
			shaders.get(shader.getName()).resetValues(shader);
		}
	}

	private static class ShaderEntry {
		private String key;
		private Shader shader;
		private String vertexSource;
		private String fragmentSource;
		private ShaderProperty[] properties;

		public ShaderEntry(String key, String vertsSource,
				String fragSource, ShaderProperty[] properties) {
			this.key = key;
			this.vertexSource = vertsSource;
			this.fragmentSource = fragSource;
			this.properties = properties;
		}

		public void destroy() {
			if (shader != null) {
				shader.destroy();
			}
		}
		
		public Shader getShader() {
			if (shader == null) {
				shader = create();
			}
			return shader;
		}
		
		private Shader create() {
			Shader activeShader = Shader.getActiveShader();
			Shader newShader = new Shader();
			newShader.setName(key);
			try {
				newShader.loadFromSource(vertexSource, fragmentSource);
				resetValues(newShader);
			} catch (GraphicsException e) {
				e.printStackTrace();
				if (activeShader != null) {
					activeShader.bind();
				}
			}
			return newShader;
		}

		public void resetValues(Shader shader) {
			if(shader==null) {
				return;
			}
			
			Shader activeShader = Shader.getActiveShader();

			shader.bind();
			if(shader.isActive()) {
				for (ShaderProperty property : properties) {
					shader.registerProperty(property);
				}
			}
			
			if (activeShader != null) {
				activeShader.bind();
			}
		}
	}

	public static void init() {
		SHADER_B_1L_P_1T.register();
		SHADER_C_1L_P.register();
		SHADER_1L_P_1T.register();
		SHADER_SP.register();
		SHADER_SP_1T.register();
		SHADER_1L_SH_1T.register();
		SHADER_C_1L_SH.register();
		SHADER_C_SP.register();
		SHADER_1T.register();
		SHADER_CAIRO_1T.register();
		SHADER_RED.register();
		SHADER_WHITE.register();
		SHADER_C.register();
		SHADER_HOLOGRAM.register();
		SHADER_DEFAULT.register();
	}
}
