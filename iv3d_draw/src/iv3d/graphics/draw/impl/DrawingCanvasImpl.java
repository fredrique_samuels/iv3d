package iv3d.graphics.draw.impl;

import iv3d.graphics.draw.base.GraphicsCanvas;
import iv3d.graphics.draw.base.GraphicsPainter;
import iv3d.graphics.draw.base.ImagePainter;
import iv3d.graphics.draw.impl.adpaters.GraphicsPainterAdapter;

import java.awt.image.BufferedImage;
import java.util.logging.Logger;

public class DrawingCanvasImpl implements GraphicsCanvas {

	Logger logger = Logger.getLogger(DrawingCanvasImpl.class.getCanonicalName());
	
	private final BufferedImage target;
	GraphicsPainterAdapter painter;
	
	public DrawingCanvasImpl(int width, int height) {
		target = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		painter = new GraphicsPainterAdapter(target.createGraphics());
	}
	
	@Override
	public final GraphicsPainter getPainter() {
		return painter;
	}

	@Override
	public int getHeight() {
		return target.getHeight();
	}

	@Override
	public int getWidth() {
		return target.getWidth();
	}

	@Override
	public ImagePainter getImage() {
		return new ImagePainterImpl(target);
	}
}
