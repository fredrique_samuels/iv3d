package iv3d.graphics.draw.impl.adpaters;

import iv3d.graphics.draw.base.FontMetrics;

public class FontMetricsAdapter implements FontMetrics {

	private final java.awt.FontMetrics awtMetrics;
	
	public FontMetricsAdapter(java.awt.FontMetrics fontMetrics) {
		awtMetrics = fontMetrics;
	}

	@Override
	public float getAscent() {
		return awtMetrics.getAscent();
	}

	@Override
	public float getDescent() {
		return awtMetrics.getDescent();
	}

	@Override
	public float getHeight() {
		return awtMetrics.getHeight();
	}

	@Override
	public float getLeading() {
		return awtMetrics.getLeading();
	}

	@Override
	public float stringWidth(String str) {
		return awtMetrics.stringWidth(str);
	}

}
