package iv3d.graphics.draw.impl.adpaters;

import iv3d.base.Bounds;
import iv3d.base.Color;
import iv3d.graphics.draw.base.Font;
import iv3d.graphics.draw.base.FontMetrics;
import iv3d.graphics.draw.base.GraphicsPainter;
import iv3d.graphics.draw.base.ImagePainter;
import iv3d.graphics.draw.base.Options;
import iv3d.graphics.draw.base.Shape;
import iv3d.graphics.draw.base.Stroke;
import iv3d.graphics.draw.impl.ImagePainterImpl;
import iv3d.graphics.draw.impl.ShapeBaseImpl;
import iv3d.graphics.draw.impl.shapes.ArcShape;
import iv3d.graphics.draw.impl.shapes.LineShape;
import iv3d.graphics.draw.impl.shapes.OvalShape;
import iv3d.graphics.draw.impl.shapes.RectangleShape;

import java.awt.BasicStroke;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.RadialGradientPaint;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.util.Stack;
import java.util.logging.Logger;

public class GraphicsPainterAdapter implements GraphicsPainter {

	Logger logger = Logger.getLogger(GraphicsPainter.class.getCanonicalName());
	
	private final Graphics2D graphics;
	private Graphics2D currentGraphics;
	private final Stack<Graphics2D> stack;
	
	public GraphicsPainterAdapter(Graphics2D g) {
		graphics = g;
		currentGraphics = graphics;
		stack = new Stack<Graphics2D>();
	}


	@Override
	public void drawArc(float x, float y, float width, float height, float startAngle,
			float arcAngle, Options options) {
		applyOptions(options, getGraphics());

		Stroke strokeType = options.getStrokeType();
		switch (strokeType) {
		case FILL:
			getGraphics().fillArc(Math.round(x), Math.round(y), 
					Math.round(width), Math.round(height), 
					Math.round(startAngle), Math.round(arcAngle));
			break;
		case LINE:
			getGraphics().drawArc(Math.round(x), Math.round(y), 
					Math.round(width), Math.round(height), 
					Math.round(startAngle), Math.round(arcAngle));
			break;
		default:
			logger.warning("Trying to use unknown stroke type " + strokeType);
		}	    
	}

	@Override
	public void drawCircle(float x, float y, float radius, Options options) {
		float dim = radius*2;
		drawOval(x, y, dim, dim, options);
	}

	@Override
	public void drawImage(ImagePainter image, float x, float y) {
		drawImage(image, x, y, image.getWidth(), image.getHeight());
	}

	@Override
	public void drawLine(float px0, float py0, float px1, float py1,
			Options options) {
		Graphics2D g = getGraphics();
		applyOptions(options, g);
		g.drawLine(Math.round(px0), Math.round(py0), Math.round(px1), Math.round(py1));
	}

	@Override
	public void drawRect(float x, float y, float width, float height,
			Options options) {
		Graphics2D g = getGraphics();
		
		applyOptions(options, g);
		
	    Stroke strokeType = options.getStrokeType();
		switch (strokeType) {
		case FILL:
			g.fillRect(Math.round(x), Math.round(y), Math.round(width), Math.round(height));
			break;
		case LINE:
			g.drawRect(Math.round(x), Math.round(y), Math.round(width), Math.round(height));
			break;
		default:
			logger.warning("Trying to use unknown stroke type " + strokeType);
		}
	}

	private void applyOptions(Options options, Graphics2D g) {
		Color color = options.getColor();
		g.setColor(transformColor(color));
		
		BasicStroke wideStroke = new BasicStroke(options.getStrokeSize());  
	    g.setStroke(wideStroke);
	}

	@Override
	public void drawRect(Bounds bounds, Options options) {
		drawRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight(), options);
	}

	private java.awt.Color transformColor(Color color) {
		return new java.awt.Color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
	}

	@Override
	public void drawOval(float x, float y, float width, float height,
			Options options) {
		Graphics2D g = getGraphics();
		
		applyOptions(options, g);
		
	    Stroke strokeType = options.getStrokeType();
		
		switch (strokeType) {
		case FILL:
			g.fillOval(Math.round(x-width*.5f), Math.round(y-height*.5f), 
					Math.round(width), Math.round(height));
			break;
		case LINE:
			g.drawOval(Math.round(x-width*.5f), Math.round(y-height*.5f), 
					Math.round(width), Math.round(height));
			break;
		default:
			logger.warning("Trying to use unknown stroke type " + strokeType);
		}
		
	}

	@Override
	public void fillLinearGradient(float startX, float startY,
			Color startColor, float endX, float endY, Color endColor,
			Bounds bounds) {
		Graphics2D g = getGraphics();
		GradientPaint paint = new GradientPaint(startX, 
				startY, 
				transformColor(startColor),
				endX, 
				endY, 
				transformColor(endColor));
		g.setPaint(paint);
		g.fill(transformRectangle2D(bounds));
	}
	
	Rectangle2D transformRectangle2D(Bounds bounds) {
		return new Rectangle2D.Double(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
	}

	@Override
	public void fillRadialGradient(float cx, float cy, float radius,
			Color startColor, Color endColor, Bounds bounds) {
		Graphics2D g = getGraphics();
		RadialGradientPaint paint = new RadialGradientPaint(cx, cy, radius, new float[]{0,1}, 
				new java.awt.Color[]{transformColor(startColor), transformColor(endColor)});
		g.setPaint(paint);
		g.fill(transformRectangle2D(bounds));
	}

	@Override
	public void drawImage(ImagePainter image, float x, float y, float width, float height) {
		boolean isImageType = image instanceof ImagePainterImpl;
		if (!isImageType) {
			throw new IllegalArgumentException(String.format("Image should be an instance of %s got %s instead!", 
					ImagePainterImpl.class.getCanonicalName(),
					image.getClass().getCanonicalName()));
		}
		
		ImagePainterImpl impl = (ImagePainterImpl) image;
		Graphics2D g = getGraphics();
		g.drawImage(impl.getImage(), 
				Math.round(x), 
				Math.round(y), 
				Math.round(width), 
				Math.round(height), 
				null);
	}

	@Override
	public void clip(float x, float y, float width, float height) {
		Graphics2D g = getGraphics();
		g.clipRect(Math.round(x), 
				Math.round(y), 
				Math.round(width), 
				Math.round(height));
	}

	@Override
	public void rotate(float theta, float x, float y) {
		getGraphics().rotate(theta, x, y);
	}

	@Override
	public void scale(float x, float y) {
		getGraphics().scale(x, y);
	}

	@Override
	public void translate(float x, float y) {
		getGraphics().translate(x, y);
	}

	@Override
	public GraphicsPainter create(float x, float y, float width,
			float height) {
		Graphics2D subPainter = (Graphics2D) getGraphics().create(
				Math.round(x), 
				Math.round(y), 
				Math.round(width), 
				Math.round(height));
		return new GraphicsPainterAdapter(subPainter);
	}

	@Override
	public void setClip(float x, float y, float width, float height) {
		getGraphics().setClip(
				Math.round(x), 
				Math.round(y), 
				Math.round(width), 
				Math.round(height));
	}

	@Override
	public void clipShape(Shape shape) {
		java.awt.Shape awtShape = getShapeFromImpl(shape);
		getGraphics().clip(awtShape);
	}

	private Graphics2D getGraphics() {
		return currentGraphics;
	}

	@Override
	public Shape createOvalShape(float x, float y, float width, float height) {
		return new OvalShape(x, y, width, height);
	}

	@Override
	public Shape createPathShape(Shape[] shapes) {
		GeneralPath path = new GeneralPath();
		for(Shape s:shapes) {
			java.awt.Shape awtShape = getShapeFromImpl(s);
			path.append(awtShape, true);
		}
		return null;
	}

	private java.awt.Shape getShapeFromImpl(Shape s) {
		boolean isType = s instanceof ShapeBaseImpl;
		if (!isType) {
			throw new IllegalArgumentException(String.format("Shape should be an instance of %s got %s instead!", 
					ShapeBaseImpl.class.getCanonicalName(),
					s.getClass().getCanonicalName()));
		}
		
		ShapeBaseImpl shape = (ShapeBaseImpl)s;
		return shape.getShape();
	}

	@Override
	public Shape createRectangleShape(float x, float y, float width, float height) {
		return new RectangleShape(x, y, width, height);
	}

	@Override
	public void drawShape(Shape shape, Options options) {
		Graphics2D g = getGraphics();
		applyOptions(options, g);

		java.awt.Shape awtShape = getShapeFromImpl(shape);		
		g.draw(awtShape);
	}

	@Override
	public GraphicsPainter create() {
		return new GraphicsPainterAdapter((Graphics2D) getGraphics().create());
	}

	@Override
	public Shape createArcShape(float x, float y, float width, float height,
			float startAngle, float sweepAngle, int type) {
		return new ArcShape(x, y, width, height, startAngle, sweepAngle, type);
	}

	@Override
	public Shape createLineShape(float px0, float py0, float px1, float py1) {
		return new LineShape(px0, py0, px1, py1);
	}

	@Override
	public void dispose() {
		getGraphics().dispose();
	}

	@Override
	public void restore() {
		if(stack.isEmpty()) {
			throw new IllegalStateException("Cannot restore state. No states saved.");
		}
		
		Graphics2D g = stack.pop();
		g.dispose();
		
		if(stack.isEmpty()) {
			currentGraphics = graphics;
		} else {
			currentGraphics = stack.peek();
		}
	}

	@Override
	public void save() {
		Graphics2D g = (Graphics2D) getGraphics().create();
		currentGraphics = g;
		stack.push(g);
	}

	@Override
	public void drawText(float x, float y, String text, Font font, Options options) {
		Graphics2D g = getGraphics();
		java.awt.Font awtFont = getAwtFont(font);
		
		applyOptions(options, g);
		
		g.setFont(awtFont);
		g.drawString(text, x, y);
	}


	private java.awt.Font getAwtFont(Font font) {
		boolean isType = font instanceof FontAdapter;
		if (!isType) {
			throw new IllegalArgumentException(String.format("Font should be an instance of %s got %s instead!", 
					FontAdapter.class.getCanonicalName(),
					font.getClass().getCanonicalName()));
		}
		
		FontAdapter fontAdapter = (FontAdapter)font;
		java.awt.Font awtFont = fontAdapter.get();
		return awtFont;
	}


	@Override
	public void clear(float x, float y, float width , float height) {
		Graphics2D g = getGraphics();
		g.clearRect( Math.round(x), 
				Math.round(y), 
				Math.round(width), 
				Math.round(height));
	}


	@Override
	public FontMetrics getFontMetrics(Font font) {
		java.awt.Font awtFont = getAwtFont(font);
		java.awt.FontMetrics fontMetrics = getGraphics().getFontMetrics(awtFont);
		return new FontMetricsAdapter(fontMetrics);
	}

}
