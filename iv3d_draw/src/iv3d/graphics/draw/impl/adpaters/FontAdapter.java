package iv3d.graphics.draw.impl.adpaters;

import iv3d.graphics.draw.base.Font;

public class FontAdapter extends Font {

	private final java.awt.Font awtFont;

	public FontAdapter(java.awt.Font awtFont) {
		this.awtFont = awtFont;
	}
	
	public final java.awt.Font get() {
		return awtFont;
	}

}
