package iv3d.graphics.draw.impl.shapes;

import java.awt.geom.Rectangle2D;

import iv3d.graphics.draw.impl.ShapeBaseImpl;

public class RectangleShape extends ShapeBaseImpl{

	public RectangleShape(float x, float y, float width, float height) {
		super(new Rectangle2D.Float(x, y, width, height));
	}
	
}
