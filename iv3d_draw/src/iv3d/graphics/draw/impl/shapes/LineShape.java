package iv3d.graphics.draw.impl.shapes;

import iv3d.graphics.draw.impl.ShapeBaseImpl;

import java.awt.geom.Line2D;

public class LineShape extends ShapeBaseImpl {
	public LineShape(float px0, float py0, float px1, float py1) {
		super(new Line2D.Float(px0, py0, px1, py1));
	}
}
