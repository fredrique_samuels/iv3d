package iv3d.graphics.draw.impl.shapes;

import iv3d.graphics.draw.impl.ShapeBaseImpl;

import java.awt.geom.Ellipse2D;

public class OvalShape extends ShapeBaseImpl {

	public OvalShape(float x, float y, float width, float height) {
		super(new Ellipse2D.Float(x, y, width, height));
	}

}
