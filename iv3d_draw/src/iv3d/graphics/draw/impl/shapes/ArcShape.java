package iv3d.graphics.draw.impl.shapes;

import iv3d.graphics.draw.impl.ShapeBaseImpl;

import java.awt.geom.Arc2D;

public class ArcShape extends ShapeBaseImpl {
	
	public static int OPEN = Arc2D.OPEN;
	public static int PIE = Arc2D.PIE;

	public ArcShape(float x, float y, float width, float height,
			float startAngle, float sweepAngle, int type) {
		super(new Arc2D.Float(x, y, width, height, startAngle, sweepAngle, type));
	}

}
