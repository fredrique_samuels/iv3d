package iv3d.graphics.draw.impl;

import iv3d.base.exceptions.GraphicsException;
import iv3d.graphics.draw.base.ImagePainter;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImagePainterImpl extends ImagePainter {

	private final BufferedImage image;
	
	public ImagePainterImpl(BufferedImage image) {
		super(image.getWidth(), image.getHeight());
		this.image = image;
	}

	public BufferedImage getImage() {
		return image;
	}

	@Override
	public ImagePainter subImage(int x, int y, int width, int height) {
		return new ImagePainterImpl(image.getSubimage(x, y, width, height));
	}

	@Override
	public void save(String filename) throws GraphicsException{
		try {
			File outputfile = new File(filename);
			ImageIO.write(image, "png", outputfile);
		} catch (IOException ex) {
			throw new GraphicsException(ex);
		}
	}
}
