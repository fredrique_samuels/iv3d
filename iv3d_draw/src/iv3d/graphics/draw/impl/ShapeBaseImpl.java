package iv3d.graphics.draw.impl;

import iv3d.graphics.draw.base.Shape;

public class ShapeBaseImpl implements Shape{
	
	protected final java.awt.Shape awtShape;
	
	public ShapeBaseImpl(java.awt.Shape shape) {
		this.awtShape = shape;
	}

	public final java.awt.Shape getShape() {
		return awtShape;
	}
	
}
