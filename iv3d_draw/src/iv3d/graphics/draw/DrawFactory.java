package iv3d.graphics.draw;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import iv3d.base.ResourceLoader;
import iv3d.base.exceptions.GraphicsException;
import iv3d.graphics.draw.base.IDrawFactory;
import iv3d.graphics.draw.base.Font;
import iv3d.graphics.draw.base.GraphicsCanvas;
import iv3d.graphics.draw.base.ImagePainter;
import iv3d.graphics.draw.impl.DrawingCanvasImpl;
import iv3d.graphics.draw.impl.ImagePainterImpl;
import iv3d.graphics.draw.impl.adpaters.FontAdapter;

public class DrawFactory implements IDrawFactory {
	
	private static DrawFactory instance;
	
	private DrawFactory() {
	}
	
	public static DrawFactory getInstance() {
		if(instance==null) {
			instance = new DrawFactory();
		}
		return instance;
	}

	@Override
	public GraphicsCanvas create(int width, int height) {
		return new DrawingCanvasImpl(width, height);
	}


	@Override
	public Font createFont(String name, int size, int style) {
		int awtStyle = java.awt.Font.PLAIN;
		if((style & Font.BOLD) == 0) {
			awtStyle |= java.awt.Font.BOLD;
		}
		
		if((style & Font.ITALIC) == 0) {
			awtStyle |= java.awt.Font.ITALIC;
		}
		
		java.awt.Font font = new java.awt.Font(name, awtStyle, size);
		return new FontAdapter(font);
	}

	@Override
	public ImagePainter createImage(int width, int height) {
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
		return new ImagePainterImpl(image);
	}

	@Override
	public ImagePainter createImage(String filename) throws GraphicsException {
		InputStream in = ResourceLoader.read(filename);
		
		if(in==null) {
			throw new GraphicsException(String.format("Unable to load resource %s", filename));
		}
		try {
			BufferedImage image = ImageIO.read(in);
			return new ImagePainterImpl(image);
		} catch (IOException e) {
			throw new GraphicsException(e);
		}
	}

}
