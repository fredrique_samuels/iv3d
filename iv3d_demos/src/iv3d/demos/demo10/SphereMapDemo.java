/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.demos.demo10;

import iv3d.base.ImageSource;
import iv3d.base.animation.AnimationManager;
import iv3d.base.animation.LinearVector3Animation;
import iv3d.base.exceptions.GraphicsException;
import iv3d.base.math.Vector3;
import iv3d.display.Window;
import iv3d.graphics.Camera;
import iv3d.graphics.Entity;
import iv3d.graphics.GraphicsUtils;
import iv3d.graphics.Model3D;
import iv3d.graphics.Scene;
import iv3d.graphics.SceneManager;
import iv3d.graphics.Shader;
import iv3d.graphics.TextureLocation;
import iv3d.graphics.TextureUtils;
import iv3d.graphics.models.CubeModel;
import iv3d.graphics.models.PlaneModel;
import iv3d.graphics.shaders.ShaderProperty;
import iv3d.graphics.shaders.ShaderRegistry;
import iv3d.graphics.shaders.store.SHADER_1T;

public class SphereMapDemo extends Scene {
	public SphereMapDemo() throws GraphicsException {
		super();
		addCamera(new Camera().setPosition(new Vector3(0, 2, 2)));
		
		createFloor();
		createTarget();
	}

	private void createTarget() throws GraphicsException {
		Entity entity = new Entity();
		entity.setPosition(0, 1, 0);
		entity.setProperty(ShaderProperty.PROPERTY_USE_LIGHT, Shader.TRUE);
		

		LinearVector3Animation animator = new LinearVector3Animation(entity, 
				"setRotation", new Vector3(), new Vector3(0, 360, 0), 5000, 0, true);
		AnimationManager.registerAnimation(animator);
		
		ImageSource sphereMap = GraphicsUtils.readImageFromFile("/images/Env_Lat-Lon.png", TextureUtils.TEXTURE_OPTION_NONE);
		
		Model3D model =new CubeModel()
			.setTexture(sphereMap, TextureLocation.TEXTURE_SPHERE)
			.bindEntity(entity);
		getContentManager().addModel(model);
	}

	private void createFloor() throws GraphicsException {
		/** Create the image. */
		ImageSource floorTexture = GraphicsUtils.readImageFromFile("/images/CedarBoards-ColorMap.png", TextureUtils.TEXTURE_OPTION_NONE);
		
		/** Create the floor. */
		Entity floor = new Entity();
		floor.setRotation(-90, 0, 0).setScale(5, 5, 1);
		
		/** Create the floor model. */
		Model3D model = new PlaneModel().setShader(ShaderRegistry.getShader(SHADER_1T.ID))
			.bindEntity(floor)
			.setTexture(floorTexture);
		getContentManager().addModel(model);
	}
	
	/**
	 * Application Entry.
	 * @throws GraphicsException 
	 */
	public static void main(String[] args) throws GraphicsException {
		
		/**Create a new window.*/
		int width = 640;
		int height = 480;
		String caption = "Sphere Map Demo";

		Window window = new Window(caption, width, height);

		/** Create the demo scene. */
		SphereMapDemo graphicsDemo = new SphereMapDemo();
		SceneManager.add(graphicsDemo);
		graphicsDemo.show();
		
		/** Start the application. */
//		window.fullscreen();
		window.run();
	}

}
