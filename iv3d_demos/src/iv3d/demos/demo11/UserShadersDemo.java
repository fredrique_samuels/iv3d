/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.demos.demo11;

import iv3d.base.exceptions.GraphicsException;
import iv3d.base.math.Vector3;
import iv3d.display.Window;
import iv3d.graphics.Camera;
import iv3d.graphics.Entity;
import iv3d.graphics.Model3D;
import iv3d.graphics.Scene;
import iv3d.graphics.SceneManager;
import iv3d.graphics.Shader;
import iv3d.graphics.models.PlaneModel;


public class UserShadersDemo extends Scene {

	
	private static String V_CODE = "uniform mat4 cg_ModelViewMatrix;"
		+ "uniform mat4 cg_ProjectionMatrix;"
		+ "attribute vec4 cg_Vertex;"
		+ "void main() {"
		+ "gl_Position = cg_ProjectionMatrix*cg_ModelViewMatrix*cg_Vertex;"
		+ "}";
	
	private static String F_CODE = "void main() {"
		+ "gl_FragColor = vec4(.5, 4., 1, 1);" 
		+ "}";
	
	public UserShadersDemo() {
		super();
		addCamera(new Camera().setPosition(new Vector3(0, 0, 2)));
		
		Shader shader = null;
		try {
			shader = new Shader().loadFromSource(V_CODE, F_CODE);
		} catch (GraphicsException e) {
			e.printStackTrace();
		}
		
		Model3D model = new PlaneModel().setShader(shader).bindEntity(new Entity());
		getContentManager().addModel(model);
	}
	
	/**
	 * Application Entry.
	 */
	public static void main(String[] args) {
		
		/**Create a new window.*/
		int width = 640;
		int height = 480;
		String caption = "Graphics Demo";
		Window window = new Window(caption, width, height);

		/** Create the demo scene. */
		UserShadersDemo graphicsDemo = new UserShadersDemo();
		SceneManager.add(graphicsDemo);
		graphicsDemo.show();
		
		/** Start the application. */
//		window.fullscreen();
		window.run();
	}
}
