/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.demos.demo3;

import iv3d.base.ImageSource;
import iv3d.base.exceptions.GraphicsException;
import iv3d.base.math.Vector3;
import iv3d.display.Window;
import iv3d.graphics.Camera;
import iv3d.graphics.Entity;
import iv3d.graphics.GraphicsUtils;
import iv3d.graphics.Model3D;
import iv3d.graphics.Scene;
import iv3d.graphics.SceneManager;
import iv3d.graphics.Texture;
import iv3d.graphics.TextureUtils;
import iv3d.graphics.models.PlaneModel;

/**
 * A demonstration on rendering a simple plane.
 * 
 * Color can be added to models using {@link Texture} using
 * {@link Model3D#setTexture(Texture)}.
 * 
 * Textures require an {@link ImageSource} object that contain the image data. 
 */
public class TextureDemo extends Scene {
	public TextureDemo() throws GraphicsException {
		super();
		addCamera(new Camera().setPosition(new Vector3(0, 0, 2)));
		
		/** Create the image. */
		ImageSource faceImage = GraphicsUtils.readImageFromFile("/images/face.jpg", TextureUtils.TEXTURE_OPTION_NONE);
		ImageSource logoImage = GraphicsUtils.readImageFromFile("/images/opengl_logo.jpg", TextureUtils.TEXTURE_OPTION_NONE);
		
		/** Create the model. */
		Model3D model = new PlaneModel()
			.setTexture(faceImage);
		getContentManager().addModel(model);
		
		/** Create the entity. */
		Entity entity1 = new Entity();
		entity1.setPosition(.6f, 0, 0);
		entity1.setTexture(logoImage);
		model.bindEntity(entity1);
		
		Entity entity2 = new Entity();
		entity2.setPosition(-.6f, 0, 0);
		model.bindEntity(entity2);
		
	}
	
	/**
	 * Application Entry.
	 * @throws GraphicsException 
	 */
	public static void main(String[] args) throws GraphicsException {
		
		/**Create a new window.*/
		int width = 640;
		int height = 480;
		String caption = "Texture Demo";
		Window window = new Window(caption, width, height);

		/** Create the demo scene. */
		TextureDemo graphicsDemo = new TextureDemo();
		SceneManager.add(graphicsDemo);
		graphicsDemo.show();
		
		/** Start the application. */
//		window.fullscreen();
		window.run();
	}
}
