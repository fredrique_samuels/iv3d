/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.demos.demo2;

import iv3d.base.math.Vector3;
import iv3d.display.Window;
import iv3d.graphics.Camera;
import iv3d.graphics.Entity;
import iv3d.graphics.Model3D;
import iv3d.graphics.Scene;
import iv3d.graphics.SceneManager;
import iv3d.graphics.models.PlaneModel;

/**
 * A demonstration on rendeing a simple plane.
 * 
 * Rending objects require 3 components. 
 * <ul>
 * <li>A Shader</li>
 * <li>A Model</li>
 * <li>An Entity</li>
 * </ul>
 * 
 * <h1>Shader</h1>
 * <p>
 * An object that describes how the pixels for the model will be rendered.
 * <a href="http://en.wikipedia.org/wiki/OpenGL_Shading_Language">more info.</a>
 * </p>
 * 
 * <h1>Model</h1>
 * <p>
 * An object that holds the vertex information for rendering entities to the screen.
 * Entities that look the same use the same {@link iv3d.graphics.Model3D}  to represent how they look.
 * </p>
 * 
 * <h1>Entity</h1>
 * <p>
 * An entity represents a single object rendered to the screen.
 * How it looks on screen depends on which {@link iv3d.graphics.Model3D}s it is
 * linked to.
 * 
 * @see {@link Model3D#bindEntity(iv3d.graphics.Entity)}
 * </p>
 */
public class GraphicsDemo extends Scene {
	
	public GraphicsDemo() {
		super();
		Camera camera = new Camera();
		addCamera(camera.setPosition(new Vector3(0, 0, 2)));
		Model3D model = new PlaneModel().bindEntity(new Entity());
		getContentManager().addModel(model);
	}
	
	/**
	 * Application Entry.
	 */
	public static void main(String[] args) {
		
		/**Create a new window.*/
		int width = 640;
		int height = 480;
		String caption = "Graphics Demo";

		Window window = new Window(caption, width, height);

		/** Create the demo scene. */
		GraphicsDemo graphicsDemo = new GraphicsDemo();
		SceneManager.add(graphicsDemo);
		graphicsDemo.show();
		
		/** Start the application. */
//		window.fullscreen();
		window.run();
	}
}
