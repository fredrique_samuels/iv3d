/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.demos.demo5;

import iv3d.base.ImageSource;
import iv3d.base.exceptions.GraphicsException;
import iv3d.base.math.Vector3;
import iv3d.display.Window;
import iv3d.graphics.Camera;
import iv3d.graphics.Entity;
import iv3d.graphics.GraphicsUtils;
import iv3d.graphics.Model3D;
import iv3d.graphics.Scene;
import iv3d.graphics.SceneManager;
import iv3d.graphics.TextureUtils;
import iv3d.graphics.models.PlaneModel;
import iv3d.graphics.models.model3ds.Model3DSLoader;

public class ModelDemo extends Scene {
	public ModelDemo() throws GraphicsException {
		super();
		addCamera(new Camera().setPosition(new Vector3(2, 2, 2)));
		
		createFloor();
		createTarget();
	}
 
	private void createTarget() throws GraphicsException {
		Entity entity = new Entity();
		entity.setPosition(0, .5f, 0);
		
		Model3D model = Model3DSLoader.load("/models/sphere.3ds")
			.bindEntity(entity);
		getContentManager().addModel(model);
	}

	private void createFloor() throws GraphicsException {
		/** Create the image. */
		ImageSource floorTexture = GraphicsUtils.readImageFromFile("/images/CedarBoards-ColorMap.png", TextureUtils.TEXTURE_OPTION_NONE);
		
		/** Create the floor. */
		Entity floor = new Entity();
		floor.setRotation(-90, 0, 0).setScale(5, 5, 1);
		
		/** Create the floor model. */
		Model3D model = new PlaneModel()
			.bindEntity(floor)
			.setTexture(floorTexture);
		getContentManager().addModel(model);
	}
	
	/**
	 * Application Entry.
	 * @throws GraphicsException 
	 */
	public static void main(String[] args) throws GraphicsException {
		
		/**Create a new window.*/
		int width = 640;
		int height = 480;
		String caption = "Model Demo";

		Window window = new Window(caption, width, height);

		/** Create the demo scene. */
		ModelDemo graphicsDemo = new ModelDemo();
		SceneManager.add(graphicsDemo);
		graphicsDemo.show();
		
		/** Start the application. */
//		window.fullscreen();
		window.run();
	}

}
