/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.demos.demo9;

import iv3d.base.ImageSource;
import iv3d.base.animation.AnimationCalculator;
import iv3d.base.animation.AnimationManager;
import iv3d.base.animation.LinearVector3Animation;
import iv3d.base.exceptions.GraphicsException;
import iv3d.base.math.Vector3;
import iv3d.display.Window;
import iv3d.graphics.Camera;
import iv3d.graphics.Entity;
import iv3d.graphics.GraphicsUtils;
import iv3d.graphics.Light;
import iv3d.graphics.Model3D;
import iv3d.graphics.Scene;
import iv3d.graphics.SceneManager;
import iv3d.graphics.Shader;
import iv3d.graphics.TextureLocation;
import iv3d.graphics.TextureUtils;
import iv3d.graphics.models.PlaneModel;
import iv3d.graphics.shaders.ShaderProperty;

public class BumpMapDemo extends Scene {
	public BumpMapDemo() throws GraphicsException {
		super();
		addCamera(new Camera().setPosition(new Vector3(0, 8, 4)));
		
		createLight();
		createFloor();
	}

	private void createLight() throws GraphicsException {
		Light light = new Light();
		light.setPosition(new Vector3(0, 8, 0));
		getContentManager().addLight(light);
		

		LinearVector3Animation animator = new LinearVector3Animation(light, 
				"setPosition", new Vector3(), new Vector3(), 2000, 0, true);
		
		animator.setAnimationCalculator(new AnimationCalculator<Vector3>() {
			
			@Override
			public Vector3 calculate(float progress, Vector3 startValue,
					Vector3 endValue) {
				float radius = 15;
				float x = (float) (Math.sin(6.28*progress)*radius);
				float y = 2;
				float z = (float) (Math.cos(6.28*progress)*radius);
				return new Vector3(x, y, z);
			}
		});
		AnimationManager.registerAnimation(animator);
	}

	private void createFloor() throws GraphicsException {
		Entity floor = new Entity();
		floor.setRotation(-90, 0, 0)
			.setScale(15, 15, 1);
		floor.setProperty(ShaderProperty.PROPERTY_USE_LIGHT, Shader.TRUE);
		
		ImageSource normalMap = GraphicsUtils.readImageFromFile(
				"/images/CarvedSandstone-NormalMap.png", TextureUtils.TEXTURE_OPTION_NONE);
		ImageSource diffuseMap = GraphicsUtils.readImageFromFile(
				"/images/CarvedSandstone-ColorMap.png", TextureUtils.TEXTURE_OPTION_NONE);

		Model3D model = new PlaneModel();
		model.setTexture(diffuseMap, TextureLocation.TEXTURE_0)
			.setTexture(normalMap, TextureLocation.TEXTURE_BUMP)
			.bindEntity(floor);
		
		getContentManager().addModel(model);
	}

	/**
	 * Application Entry.
	 * 
	 * @throws GraphicsException
	 */
	public static void main(String[] args) throws GraphicsException {

		/** Create a new window. */
		int width = 640;
		int height = 480;
		String caption = "BumpMap Demo";
		Window window = new Window(caption, width, height);

		/** Create the demo scene. */
		BumpMapDemo graphicsDemo = new BumpMapDemo();
		SceneManager.add(graphicsDemo);
		graphicsDemo.show();

		/** Start the application. */
		// window.fullscreen();
		window.run();
	}

}
