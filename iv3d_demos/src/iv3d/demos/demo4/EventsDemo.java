/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.demos.demo4;

import iv3d.base.ImageSource;
import iv3d.base.events.Event;
import iv3d.base.exceptions.GraphicsException;
import iv3d.base.math.Vector3;
import iv3d.display.Window;
import iv3d.graphics.Camera;
import iv3d.graphics.Entity;
import iv3d.graphics.GraphicsUtils;
import iv3d.graphics.Model3D;
import iv3d.graphics.Scene;
import iv3d.graphics.SceneManager;
import iv3d.graphics.TextureUtils;
import iv3d.graphics.models.PlaneModel;

import java.util.EventListener;

/**
 * A demonstration on receiving events.
 * 
 * All {@link Entity} can receive events. Events handling is enabled
 * using {@link Entity#enableEvents()}. {@link Event}s are handled using {@link EventListener}s objects.
 * 
 * @see {@link Entity#addEventListener(iv3d.base.events.EventListener)} 
 */
public class EventsDemo extends Scene implements iv3d.base.events.EventListener {
	public EventsDemo() throws GraphicsException {
		super();
		addCamera(new Camera().setPosition(new Vector3(0, 0, 2)));
		
		
		/** Create the image. */
		ImageSource image = GraphicsUtils.readImageFromFile("/images/face.jpg", TextureUtils.TEXTURE_OPTION_NONE);
		
		/** Create the entity1. */
		Entity entity1 = new Entity();
		entity1.enableEvents();
		entity1.addEventListener(this);
		entity1.setPosition(.6f, 0, 0);
		entity1.setRotation(0, 45, 0);
		
		/** Create the entity2. */
		Entity entity2 = new Entity();
		entity2.setPosition(-.6f, 0, 0);
		
		/** Create the model. */
		Model3D model = new PlaneModel()
			.bindEntity(entity1)
			.bindEntity(entity2)
			.setTexture(image);
		getContentManager().addModel(model);
	}
	
	@Override
	public void onEventStateChanged(Event event) {
		System.out.println(event);
	}
	
	/**
	 * Application Entry.
	 * @throws GraphicsException 
	 */
	public static void main(String[] args) throws GraphicsException {
		
		/**Create a new window.*/
		int width = 640;
		int height = 480;
		String caption = "Events Demo";

		Window window = new Window(caption, width, height);

		/** Create the demo scene. */
		EventsDemo graphicsDemo = new EventsDemo();
		SceneManager.add(graphicsDemo);
		graphicsDemo.show();
		
		/** Start the application. */
//		window.fullscreen();
		window.run();
	}

}
