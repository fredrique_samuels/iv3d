/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.beanfactory.impl;

import iv3d.base.beanfactory.IBeanFactory;
import iv3d.base.exceptions.FactoryException;

/**
 * Bean factory that loads Java classes using <code>ClassLoader.getSystemClassLoader()</code>.
 * 
 * @author Fredrique Samuels
 */
public final class DefaultBeanFactory implements IBeanFactory {

	@Override
	public Object newInstance(String clazz, Class<?> cast) throws FactoryException {		
		ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();

		try {
			Class<?> loadClass = systemClassLoader.loadClass(clazz);
			return cast.cast(loadClass.newInstance());
		} catch (ClassNotFoundException e) {
			throw new FactoryException(e);
		} catch (InstantiationException e) {
			throw new FactoryException(e);
		} catch (IllegalAccessException e) {
			throw new FactoryException(e);
		}
	}

	@Override
	public String getType() {
		return DefaultBeanFactory.class.getCanonicalName();
	}
}
