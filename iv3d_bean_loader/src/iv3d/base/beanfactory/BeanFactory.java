/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.beanfactory;

import iv3d.base.beanfactory.impl.DefaultBeanFactory;
import iv3d.base.beanfactory.impl.JythonBeanFactory;
import iv3d.base.exceptions.ExceptionUtils;
import iv3d.base.exceptions.FactoryException;
import iv3d.base.exceptions.TransformException;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.logging.Logger;


/**
 * <p>
 * Bean factory used to create objects from class only the class name.
 * The class type must have a default constructor otherwise a {@link TransformException} will be thrown. 
 * </p>
 * 
 * <code>
 * {@link BeanFactory#registerFactory(String)}
 * </code>
 * 
 * @author Fredrique Samuels
 */
public final class BeanFactory {
	
	/**
	 * Class name for a Jython bean loader.
	 */
	public static final String JYTHON_BEAN_FACTORY = JythonBeanFactory.class.getCanonicalName(); 

	private static Logger logger = Logger.getLogger(BeanFactory.class.getCanonicalName());
	private final Map<String, IBeanFactory> factories;
	
	private static BeanFactory instance;
	
	private BeanFactory() {
		factories = new HashMap<String, IBeanFactory>();
		factories.put("default", new DefaultBeanFactory());
	}
	
	/**
	 * Register a new {@link IBeanFactory} implementation.
	 *  
	 * @param clazz The implementation class name.
	 */
	public static void registerFactory(String clazz) {
		BeanFactory target = getInstance();
		if(target.factories.containsKey(clazz)) {
			return;
		}
		
		Object factory = null;
		try {
			factory = newBean(clazz, IBeanFactory.class);
		} catch (FactoryException e) {
			String msg = "Not register class loader %s as the it is not an instance of %s!";
			logger.warning(String.format(msg, clazz, IBeanFactory.class.getCanonicalName()));
		}

		IBeanFactory beanFactory = (IBeanFactory)factory;
		if(target.factories.containsKey(beanFactory.getType())) {
			String msg = "Not register class loader %s as the key '%s' is already being used!";
			logger.warning(String.format(msg, clazz));
			return;
		}
		
		logger.info(String.format("Registering new BeanFactory %s", clazz));
		target.factories.put(clazz, beanFactory);
	}

	/**
	 * Create a new bean from the class name.
	 * 
	 * @param clazz The implementation class name.
	 * @param cast An interface or superclass type that the object must implement.
	 * @return A new bean object.
	 * @throws FactoryException if the class could not be found, the class has no default constructor or the bean does not implement the @cast type.
	 */ 
	public static Object newBean(String clazz, Class<?> cast) throws FactoryException {
		BeanFactory target = getInstance();
		Set<Entry<String, IBeanFactory>> entrySet = target.factories.entrySet();
		Object newInstance = null;
		StringBuilder exceptionList = new StringBuilder();
		
		for(Entry<String, IBeanFactory> entry : entrySet) {
			IBeanFactory factory = entry.getValue();
			
			try {
				newInstance = factory.newInstance(clazz, cast);
			} catch (FactoryException e) {
				exceptionList.append("\nloader name :");
				exceptionList.append(entry.getKey());
				exceptionList.append('\n');
				exceptionList.append(ExceptionUtils.getStackTrace(e));
			}
			
			if(newInstance!=null) {
				break;
			}
		}
		
		if(newInstance==null) {
			throw new FactoryException("Unable to create new instance of " + clazz + exceptionList.toString());
		}
		
		return newInstance;
	}

	private static BeanFactory getInstance() {
		if(instance==null) {
			instance = new BeanFactory();
		}
		return instance;
	}
}
