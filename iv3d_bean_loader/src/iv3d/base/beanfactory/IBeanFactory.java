/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.beanfactory;

import iv3d.base.exceptions.FactoryException;

/**
 * Interface for all Bean Loader implementations.
 * 
 * @see BeanFactory
 * @author Fredrique Samuels
 */
public interface IBeanFactory {
	
	/**
	 * Create a new bean from the class name.
	 * 
	 * @param clazz The implementation class name.
	 * @param cast An interface or superclass type that the object must implement.
	 * @return A new bean object.
	 * @throws FactoryException if the class could not be found, the class has no default constructor or the bean does not implement the @cast type.
	 */ 
	Object newInstance(String clazz, Class<?> cast) throws FactoryException;
	
	/**
	 * The bean loader type.
	 * @return The bean loader type
	 */
	String getType();
}
