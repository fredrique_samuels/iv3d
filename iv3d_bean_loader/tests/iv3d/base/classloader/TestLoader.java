package iv3d.base.classloader;

import iv3d.base.beanfactory.BeanFactory;
import iv3d.base.beanfactory.impl.JythonBeanFactory;
import iv3d.base.exceptions.FactoryException;
import junit.framework.TestCase;

public class TestLoader extends TestCase {
	public void testJavaLoader() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
		Class<?> loadClass = systemClassLoader.loadClass("iv3d.base.classloader.TestImpl");
		ITest newInstance = (ITest) loadClass.newInstance();
		System.out.println(newInstance);
	}
	
	public void testDefaultLoaderFail() throws FactoryException {
		BeanFactory.newBean("iv3d.base.classloader.TestIpl", ITest.class);
	}
	
	public void testDefaultLoader() throws FactoryException {
		BeanFactory.newBean("iv3d.base.classloader.TestImpl", ITest.class);
	}
	
	public void testJythonLoader() {
		BeanFactory.registerFactory(JythonBeanFactory.class.getCanonicalName());
	}
}
