/**
 * Copyright (c) 2002-2008 LWJGL Project
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * * Neither the name of 'LWJGL' nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package iv3d.display;

import iv3d.base.Dimension;
import iv3d.base.events.Event;
import iv3d.base.events.EventType;
import iv3d.base.exceptions.ExceptionUtils;
import iv3d.graphics.GraphicsContext;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

/**
 * Window manager for computer rendering.
 */
public class Window {

	private static Logger log = Logger.getLogger(Window.class.getCanonicalName());

	final GraphicsContext gl;
	int[] buttonStates = new int[] { 0, 0, 0 };
	int mouseX = -1;
	int mouseY = -1;
	private boolean quitRequested = false;

	private void pollInput(List<Event> events) {
		events.clear();
		int x = Mouse.getX();
		int y = Mouse.getY();

		pollMouseButton(events, x, y, Event.MOUSE_BUTTON0);
		pollMouseButton(events, x, y, Event.MOUSE_BUTTON1);
		pollMouseButton(events, x, y, Event.MOUSE_BUTTON2);

		if (mouseX != x || mouseY != y) {
			int button = firstDownButton();
			if (button == -1) {
				events.add(new Event(EventType.MOUSE_MOVE, button, -1, x, y, x
						- mouseX, y - mouseX, null, null));
			} else {
				events.add(new Event(EventType.MOUSE_DRAG, button, -1, x, y, x
						- mouseX, y - mouseX, null, null));
			}
		}

		while (Keyboard.next()) {
			if (Keyboard.getEventKeyState()) {
				events.add(new Event(EventType.KEY_DOWN,
						Keyboard.getEventKey(), -1, x, y, 0, 0, null, null));
				onKeyDown(Keyboard.getEventKey());
			} else {
				events.add(new Event(EventType.KEY_UP, Keyboard.getEventKey(),
						-1, x, y, 0, 0, null, null));
			}
		}
	}

	public void onKeyDown(int eventKey) {
	}

	private int firstDownButton() {
		for (int i = 0; i < buttonStates.length; ++i) {
			if (buttonStates[i] == 1) {
				return i;
			}
		}
		return -1;
	}

	private void pollMouseButton(List<Event> events, int x, int y, int button) {
		if (Mouse.isButtonDown(button) && buttonStates[button] == 0) {
			buttonStates[button] = 1;
			events.add(new Event(EventType.MOUSE_DOWN, button, -1, x, y, 0, 0,
					null, null));
		} else if (!Mouse.isButtonDown(button) && buttonStates[button] == 1) {
			buttonStates[button] = 0;
			events.add(new Event(EventType.MOUSE_UP, button, -1, x, y, 0, 0,
					null, null));
		}
	}

	/**
	 * Create a new graphics window.
	 * @param caption
	 *            The window caption, if not fulscreen.
	 * @param width
	 *            The window width.
	 * @param height
	 *            The window height.
	 */
	public Window(String caption, int width, int height) {
		try {
			create_window(caption, width, height);
		} catch (LWJGLException e) {
			e.printStackTrace();
		}

		gl = GraphicsContext.getInstance();
		gl.init(width, height);
	}
	
	/**
	 * Set the window to display fullscreen.
	 */
	public void fullscreen() {
		log.info("Setting display fullscreen");
		try {
			Display.setFullscreen(true);
		} catch (LWJGLException e) {
			e.printStackTrace();
			log.severe(ExceptionUtils.getStackTrace(e));
		}
	}
	
	/**
	 * Set the display to be windowed. 
	 */
	public void windowed() {
		log.info("Setting display to window mode");
		try {
			Display.setFullscreen(false);
		} catch (LWJGLException e) {
			e.printStackTrace();
			log.severe(ExceptionUtils.getStackTrace(e));
		}
	}

	/**
	 * Start the widnow main loop.
	 * 
	 * This method blocks untill the windoe is closed.
	 */
	public void run() {
		loop(gl);
		destroy();
	}

	/**
	 *
	 */
	private void destroy() {
		Display.destroy();
		System.exit(1);
	}
	
	public void exit() {
		quitRequested = true;
	}
	
	public static Dimension[] getScreenSizes() {  
		DisplayMode[] modes = new DisplayMode[0];
		
		try {
			modes = Display.getAvailableDisplayModes();
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		Dimension[] sizes = new Dimension[modes.length];
		
		for (int i = 0; i < modes.length; i++) {
			DisplayMode current = modes[i];
			sizes[i] = new Dimension(current.getWidth(), current.getHeight());
		}
		
		return sizes;
	}

	/**
	 *
	 */
	private void loop(final GraphicsContext gl) {

//		long startTime = System.currentTimeMillis() + 5000;
//		long fps = 0;
		List<Event> events = new LinkedList<Event>();

		while (!Display.isCloseRequested() && !quitRequested) {
			pollInput(events);
			gl.render(events);

			Display.update();

  			try {
				Thread.sleep(2);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

//			if (startTime > System.currentTimeMillis()) {
//				fps++;
//			} else {
//				long timeUsed = 5000 + (startTime - System.currentTimeMillis());
//				startTime = System.currentTimeMillis() + 5000;
//				System.out.println(fps + " frames in " + timeUsed / 1000f
//						+ " seconds = " + (fps / (timeUsed / 1000f)));
//				fps = 0;
//			}
		}
	}

	private void create_window(String caption, int w, int h)
			throws LWJGLException {

		DisplayMode[] modes = Display.getAvailableDisplayModes();
		DisplayMode mode = null;
		for (int i = 0; i < modes.length; i++) {
			DisplayMode current = modes[i];
			if ( current.getWidth()==w && current.getHeight()==h ) {
				mode = current;
			}
		}
		
		if(mode==null) {
			mode = new DisplayMode(w, h);
		}

		Display.setLocation(0, 0);
		Display.setDisplayMode(mode);
		Display.setTitle(caption);
		
		String msg = String.format("Creating window : mode=%s caption=%s", mode.toString(), caption);
		log.info(msg);
		
		Display.create();
	}
	
	public static Dimension getLargestDimension() {
		Dimension[] screenSizes = getScreenSizes();
		Dimension d = screenSizes[0];
		for(Dimension e:screenSizes) {
			if(d.getWidth() < e.getWidth()) {
				d = e;
			}
		}
		return d;
	}
	
	
}