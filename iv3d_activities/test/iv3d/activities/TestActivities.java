package iv3d.activities;

import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;

import iv3d.activities.channels.InputChannel;
import iv3d.activities.channels.InputChannelBlock;
import iv3d.activities.channels.OutputChannel;
import iv3d.activities.channels.OutputChannelBlock;
import iv3d.activities.factory.ActivityFactory;
import iv3d.activities.factory.ChannelDescription;
import iv3d.activities.factory.DataProcessor;
import iv3d.activities.factory.ParameterDescription;
import iv3d.activities.factory.ValueContainer;

public class TestActivities extends TestCase {
	
	public void testActivityValueBullet() {
		ActivityFactory activityFactory = new ActivityFactory();
		
		DataProcessor processor = new DataProcessor() {
			
			@Override
			public Object[] execute(ValueContainer[] args) {
				Integer integer = args[0].get(Integer.class);
				System.out.println(integer);
				return toResult(new Double(integer.floatValue()));
			}

			@Override
			public String getName() {
				return "Test Processor";
			}
		};
		
		ChannelDescription input = new ChannelDescription("Input1", Integer.class);
		ChannelDescription[] outputs = new ChannelDescription[]{new ChannelDescription("Output1", Double.class)};
		ParameterDescription[] parameters = null;
		
		Activity activity = activityFactory.createActivity("Activiy 1", processor, input, outputs, parameters);
		InputChannelBlock inputChannels = activity.getInputChannels();
		InputChannel channel = inputChannels.getChannel(0);

		OutputChannelBlock outputChannelBlock = activity.getOutputChannels();
		OutputChannel channel2 = outputChannelBlock.getChannel(0);
		
		activity.start();
		
		channel.push(new Integer(5));
		
		Object value = channel2.pop(5000, TimeUnit.MILLISECONDS);
		assertEquals(new Double(5.0), (Double)value, .001);
		
		activity.stop();
	}
}
