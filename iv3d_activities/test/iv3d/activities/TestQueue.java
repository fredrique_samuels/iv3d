package iv3d.activities;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;

public class TestQueue extends TestCase {
	public void testQueue() {
		BlockingQueue<Integer> q = new ArrayBlockingQueue<Integer>(1);
		boolean success = q.offer(Integer.valueOf(2));
		try {
			q.poll(2, TimeUnit.SECONDS);
			System.out.println(q.poll(2, TimeUnit.SECONDS));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
