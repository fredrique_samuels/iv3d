package iv3d.activities;

import iv3d.activities.exceptions.CalibrationException;

public interface CalibrationParameter {
	public Object get();
	public Class<?> getType();
	public void set(Object value) throws CalibrationException;
}
