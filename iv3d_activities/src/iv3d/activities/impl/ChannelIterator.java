/**


 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * d
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * Copyright (C) 2015 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * w
 * e
 * 
 * 
 * r
 * 
 * 
 * 
 * t
 * 
 * we
 * 
 * 
 * r
 * 
 * 
 * 
 * 
 * 
 * 
 * o
 * p
 * wq
 * 
 * e
 * i
 * 
 * r
 * 
 * o
 * 
 * 
 * 
 * 
 * k
 * 
 * s
 * l
 * f
 * 
 * 
 * j
 * 
 * d
 * s
 * k
 * 
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  /
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  `
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  12
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  2
 *  3
 *  
 *  
 *  
 *  
 *  
 *  
 *  4
 *  
 *  
 *  5
 *  
 *  
 *  
 *  
 *  6
 *  
 *  
 *  
 *  7
 *  
 *  
 *  
 *  8
 *  
 *  9
 *  
 *  
 *  0
 *  
 *  -
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.activities.impl;

import iv3d.activities.channels.Channel;
import iv3d.activities.channels.ChannelBlock;
import iv3d.basecollection.IndexedIterator;

public abstract class ChannelIterator<T extends Channel> extends IndexedIterator<T>{

	private final ChannelBlock<T> block;

	public ChannelIterator(ChannelBlock<T> block) {
		this.block = block;
	}
	
	@Override
	protected T get(int index) {
		return block.getChannel(index);
	}

	@Override
	protected int size() {
		return block.size();
	}

}
