package iv3d.activities.impl;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class ChannelQueue {
	private ArrayBlockingQueue<Object> queue;

	public ChannelQueue(int arg0) {
		queue = new ArrayBlockingQueue<Object>(arg0);
	}
	
	public Object pop(long timeout, TimeUnit unit) {
		try {
			return queue.poll(timeout, unit);
		} catch (InterruptedException e) {
			return null;
		}
	}
	
	public void push(Object obj) {
		if(queue.remainingCapacity()==0) {
			return;
		}
		queue.add(obj);
	}

	private static final long serialVersionUID = 1L;
}
