/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.activities.impl;

import iv3d.activities.Summary;
import iv3d.activities.channels.InputChannel;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class DefaultInputChannel implements InputChannel {
	
	private final BlockingQueue<Object> queue;
	private final Class<?> type;
	private final String name; 

	public DefaultInputChannel(Class<?> type, 
			String name, int maxQueueSize) {
		this.type = type;
		this.name = name;
		queue = new ArrayBlockingQueue<Object>(maxQueueSize);
	}
	
	@Override
	public Class<?> getDataType() {
		return type;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Summary getSummary() {
		return null;
	}

	@Override
	public Object pop(long timeout, TimeUnit unit) {
		try {
			return queue.poll(timeout, unit);
		} catch (InterruptedException e) {
			return null;
		}
	}

	@Override
	public void push(Object object) {
		if(queue.remainingCapacity()==0) {
			return;
		}
		try {
			type.cast(object);
		} catch (Exception e) {
			return;
		}
		queue.add(object);
	}

	@Override
	public void open() {
		
	}

	@Override
	public void close() {
	}

	@Override
	public void clear() {
		queue.clear();
	}

}
