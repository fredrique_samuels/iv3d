package iv3d.activities.impl;

import iv3d.activities.Summary;
import iv3d.activities.channels.OutputChannel;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class DefaultOutputChannel implements OutputChannel {
	
	private final String name;
	private final Class<?> type;
	private ArrayBlockingQueue<Object> queue;
	private Object cache;

	public DefaultOutputChannel(Class<?> type, String name) {
		this.type = type;
		this.name = name;
		this.queue = new ArrayBlockingQueue<Object>(1);
	}

	@Override
	public void clear() {
	
	}

	@Override
	public void close() {
	}

	@Override
	public Class<?> getDataType() {
		return type;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Summary getSummary() {
		return null;
	}

	@Override
	public void open() {
	}

	@Override
	public Object pop(long timeout, TimeUnit unit) {
		try {
			return queue.poll(timeout, unit);
		} catch (InterruptedException e) {
			return null;
		}
	}

	@Override
	public void push(Object object) {
		if(queue.remainingCapacity()==0) {
			return;
		}
		try {
			type.cast(object);
		} catch (Exception e) {
			return;
		}
		cache = object;
		queue.add(object);
	}

	@Override
	public Object getCached() {
		return cache;
	}

}
