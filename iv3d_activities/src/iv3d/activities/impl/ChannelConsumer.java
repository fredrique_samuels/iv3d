/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.activities.impl;

import java.util.concurrent.TimeUnit;

import iv3d.activities.channels.Channel;
import iv3d.base.threads.WorkerThread;

/**
 * Constantly requests
 */
public abstract class ChannelConsumer extends WorkerThread {
	
	private final Channel channel;

	public ChannelConsumer(Channel channel) {
		setDelay(0);
		this.channel = channel;
		this.channel.open();
	}
	
	public abstract void onValue(Object value);

	@Override
	protected void execute() {
		Object value = channel.pop(50, TimeUnit.MILLISECONDS);
		if(value==null) {return;}
		onValue(value);
	}

	@Override
	public void onExit() {
		channel.close();
	}
	
}
