package iv3d.activities.impl;

import iv3d.activities.channels.OutputChannel;
import iv3d.activities.channels.OutputChannelBlock;

public class DefaultOutputChannelBlock implements OutputChannelBlock {
	private final OutputChannel[] outputChannels;

	public DefaultOutputChannelBlock(OutputChannel[] outputChannels) {
		this.outputChannels = outputChannels;
	}

	@Override
	public OutputChannel getChannel(int index) {
		return outputChannels[index];
	} 

	@Override
	public int size() {
		return outputChannels.length;
	}

}
