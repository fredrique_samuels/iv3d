package iv3d.activities;

public interface Calibrator {
	public CalibrationParameter[] getParameters();
}
