/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.activities.channels;

import iv3d.activities.Summary;

import java.util.concurrent.TimeUnit;

/**
 * Public interface for all channels.
 */
public interface Channel {
	
	/**
	 * @return The channel's name
	 */
	public String getName();
	
	/**
	 * @return The channels status.
	 */
	public Summary getSummary();
	
	/**
	 * @return The data type for the channel.
	 */
	public Class<?> getDataType();
	
	
	/**
	 * Push an object through the channel.
	 * 
	 * @param object The object to be pushed.
	 */
	public void push(Object object);
	
	/**
	 * Retrieve an item from the channel.
	 * Return <code>null</code> to indicate to item available or
	 * the polling period expired.
	 *  
	 * @return The item retrieved.
	 */
	public Object pop(long timeout, TimeUnit unit);
	
	/**
	 * Start the channel thread.
	 */
	public void open();
	
	/**
	 * Stop the channel thread.
	 */
	public void close();
	
	/**
	 * Clear the channel;
	 */
	public void clear();
}
