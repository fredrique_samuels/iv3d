package iv3d.activities.channels;

public interface OutputChannelBlock {
	public int size();
	public OutputChannel getChannel(int index);
}
