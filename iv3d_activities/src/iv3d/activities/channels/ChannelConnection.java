/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.activities.channels;


/**
 * Public interface for objects that connect {@link Channel} outputs to
 * channel inputs.
 * 
 * Pulls data from output channels and pushes then to 
 * the input channel. 
 */
public interface ChannelConnection {
	
	/**
	 * @return <code>false</code> if the connection is no longer valid.
	 */
	public boolean isValid();
	
	/**
	 * @return <code>true</code> if the connection has experienced an error.
	 */
	public boolean hasError();
	
	/**
	 * @return The last error that this connection experienced or <code>null</code>.
	 * @see #hasError()
	 */
	public Exception getError();
	
	/**
	 * Request that the connection be broken.
	 * {@link #isValid()} must return <code>false</code> 
	 * after this method completes.  
	 */
	public void disconnect();
}
