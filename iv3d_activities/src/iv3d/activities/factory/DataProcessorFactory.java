package iv3d.activities.factory;

import iv3d.base.exceptions.ExceptionUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Logger;

public final class DataProcessorFactory {
	
	private static Logger logger = Logger.getLogger(DataProcessorFactory.class.getName());
	
	private DataProcessorFactory(){}

	public static DataProcessor get(Class<?> clazz, String methodName, Class<?> ... inputs) {
		try {
			final Method method = clazz.getMethod(methodName, inputs);
			final String name = String.format("%s.%s", clazz.getName(), method.getName());
			new DataProcessor() {
				
				@Override
				public Object[] execute(ValueContainer[] args) {
					try {
						Object result = method.invoke(null, getValues(args));
						toResult(result);
					} catch (IllegalArgumentException e) {
						logger.warning(ExceptionUtils.getStackTrace(e));
					} catch (IllegalAccessException e) {
						logger.warning(ExceptionUtils.getStackTrace(e));
					} catch (InvocationTargetException e) {
						logger.warning(ExceptionUtils.getStackTrace(e));
					}
					return null;
				}

				@Override
				public String getName() {
					return null;
				}
			};
		} catch (SecurityException e) {
			logger.warning(ExceptionUtils.getStackTrace(e));
		} catch (NoSuchMethodException e) {
			logger.warning(ExceptionUtils.getStackTrace(e));
		}
		
		return null;
	}
}
