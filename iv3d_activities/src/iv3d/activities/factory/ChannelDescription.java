/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.activities.factory;

/**
 * Container class for information needed to construct
 * a {@link Channel}. 
 */
public final class ChannelDescription {
	private final String name;
	private final String typeName;
	private final Class<?> type;

	/**
	 * Create a new {@link ChannelDescription}.
	 * 
	 * @param name
	 * @param typeName The fully  qualified class type name.
	 */
	public ChannelDescription(String name, String typeName) {
		super();
		this.name = name;
		this.typeName = typeName;
		this.type = null;
	}

	/**
	 * Create a new {@link ChannelDescription}.
	 * 
	 * @param name
	 * @param type
	 */
	public ChannelDescription(String name, Class<?> type) {
		super();
		this.name = name;
		this.type = type;
		this.typeName = null;
	}

	/**
	 * @return The channel name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return The channel name.
	 */
	public String getTypeName() {
		return typeName;
	}

	
	/**
	 * @return The channel type.
	 */
	public Class<?> getType() {
		return type;
	}
}
