/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
/**
 * Copyright (C) 2015 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.activities.factory;

import iv3d.activities.Activity;
import iv3d.activities.channels.InputChannel;
import iv3d.activities.channels.InputChannelBlock;
import iv3d.activities.channels.OutputChannel;
import iv3d.activities.channels.OutputChannelBlock;
import iv3d.activities.impl.DefaultChannelBlockFactory;
import iv3d.activities.impl.DefaultChannelFactory;

import java.util.ArrayList;

/**
 * Factory for all {@link Activity} objects. 
 */
public final class ActivityFactory {
	private ChannelFactoryMethod channelFactoryMethod;
//	private CalibrationFactoryMethod calibrationFactoryMethod;
	private ChannelBlockFactoryMethod channelBlockFactoryMethod; 

	/**
	 * Default Constructor
	 */
	public ActivityFactory(){
		channelFactoryMethod = new DefaultChannelFactory();
		channelBlockFactoryMethod = new DefaultChannelBlockFactory();
	}
	

	/**
	 * Set the {@link ChannelFactoryMethod} implementation to be used.
	 * 
	 * @param channelFactory
	 */
	public final void setChannelFactory(ChannelFactoryMethod channelFactoryMethod) {
		this.channelFactoryMethod = channelFactoryMethod;
	}

	/**
	 * Set the {@link CalibrationFactoryMethod} to be used.
	 * 
	 * @param calibrationFactory
	 */
	public final void setCalibrationFactory(CalibrationFactoryMethod calibrationFactoryMethod) {
//		this.calibrationFactoryMethod = calibrationFactoryMethod;
	}

	/**
	 * Create a new {@link Activity}.
	 * 
	 * @param processor The data processor to be used.
	 * @param inputs The inputs for the activity.
	 * @param outputs The outputs for the activity.
	 * @param parameters The calibration variables for the activity.
	 * @return The new {@link Activity}.
	 */
	public final Activity createActivity(String name, DataProcessor processor, 
			ChannelDescription input,
			ChannelDescription[] outputs,
			ParameterDescription[] parameters) {
		
		InputChannel[] inputChannel = getInputChannels(new ChannelDescription[]{input});
		OutputChannel[] outputChannels = getOutputChannels(outputs);
		
		InputChannelBlock inputChannelBlock = channelBlockFactoryMethod.createInputChannelBlock(inputChannel);
		OutputChannelBlock outputChannelBlock = channelBlockFactoryMethod.createOutputChannelBlock(outputChannels);
		
		return new Activity(name, inputChannelBlock, outputChannelBlock, processor);
	}

	private InputChannel[] getInputChannels(ChannelDescription[] channels) {
		ArrayList<InputChannel> result = new ArrayList<InputChannel>();
		
		for (ChannelDescription channelDescription : channels) {			
			result.add(channelFactoryMethod.createInputChannel(channelDescription));
		}
		
		return result.toArray(new InputChannel[0]);
	}
	
	private OutputChannel[] getOutputChannels(ChannelDescription[] channels) {
		ArrayList<OutputChannel> result = new ArrayList<OutputChannel>();
		
		for (ChannelDescription channelDescription : channels) {			
			result.add(channelFactoryMethod.createOutputChannel(channelDescription));
		}
		
		return result.toArray(new OutputChannel[0]);
	}
}
