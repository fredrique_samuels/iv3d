package iv3d.activities;

import iv3d.activities.channels.InputChannel;
import iv3d.activities.channels.InputChannelBlock;
import iv3d.activities.channels.OutputChannel;
import iv3d.activities.channels.OutputChannelBlock;
import iv3d.activities.factory.DataProcessor;
import iv3d.activities.factory.ValueContainer;
import iv3d.activities.impl.ChannelConsumer;
import iv3d.activities.impl.ChannelIterator;
import iv3d.base.threads.ExecutionThread;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public final class Activity {
	private static Logger logger = Logger.getLogger(Activity.class.getName());
	
	private final InputChannelBlock inputChannelBlock;
	private final OutputChannelBlock outputChannelBlock;
	private final DataProcessor dataProcessor;
	private final List<ChannelConsumer> inputConsumers;
	private final ExecutionThread processorThread;
	private ValueContainer[] inputCache;
	private final String name;

	public Activity(String name, InputChannelBlock inputChannelBlock,
					OutputChannelBlock outputChannelBlock,
					final DataProcessor dataProcessor) {
		this.name = name;
		this.inputChannelBlock = inputChannelBlock;
		this.outputChannelBlock = outputChannelBlock;
		this.dataProcessor = dataProcessor;
		this.inputConsumers = new ArrayList<ChannelConsumer>();
		this.processorThread = new ExecutionThread() {
			
			@Override
			protected void process() {
				Object[] values = dataProcessor.execute(inputCache);
				setOutput(values);
			}
			
			@Override
			protected void onExit() {
				
			}
		};
	}
	
	public final String getName() {
		return name;
	}

	public final InputChannelBlock getInputChannels() {
		return inputChannelBlock;
	}
	
	public final OutputChannelBlock getOutputChannels() {
		return outputChannelBlock;
	}
	
	protected final DataProcessor getDataProcessor() {
		return dataProcessor;
	}
	
	public final void start() {
		logger.info("Starting activity " + getName());
		init();
		startInputConsumers();
	}

	private void startInputConsumers() {
		logger.info("Starting input consumers for activity " + getName());
		for(ChannelConsumer channelConsumer : inputConsumers) {
			channelConsumer.start();
		}
	}

	private void init() {
		createInputConsumers();
		processorThread.start();
	}
	
	private void createInputConsumers() {
		InputChannelBlock inputChannels = getInputChannels();
		int size = inputChannels.size();
		inputCache = new ValueContainer[size];
		final String activityName = getName();
		
		new ChannelIterator<InputChannel>(inputChannels) {

			@Override
			protected boolean execute(InputChannel c, final int index) {
				ChannelConsumer channelConsumer = new ChannelConsumer(c){

					@Override
					public void onValue(Object value) {
						setCache(value, index);
						processorThread.execute();
					}
				};
				channelConsumer.setName(activityName +  ":ConsumerThread:" + channelConsumer.getName());
				inputConsumers.add(channelConsumer);
				return true;
			}
		}.iterate();
	}

	private void setCache(Object value, int index) {
		inputCache[index] = new ValueContainer(value);
	}

	public final void stop() {
		processorThread.terminate();
		closeInputConsumers();
	}

	private void closeInputConsumers() {
		logger.info("Closing input consumers for activity " + getName());
		for(ChannelConsumer channelConsumer : inputConsumers) {
			channelConsumer.terminate();
		}
	}
		
	private void setOutput(Object[] values) {
		OutputChannelBlock outputChannels = getOutputChannels();
		int size = outputChannels.size();
		
		for(int index=0;index<values.length && index<size;index++) {
			OutputChannel channel = outputChannels.getChannel(index);
			channel.push(values[index]);
		}
	}
	
//	public Calibrator getCalibrator();
//	public Summary getSummary();
//	public void addHook(ActivityHook hook);
}
