package iv3d.graphics.draw.base;

import iv3d.base.exceptions.GraphicsException;

public interface IDrawFactory {
	public GraphicsCanvas create(int width, int height);
	public Font createFont(String name, int size, int style);
	public ImagePainter createImage(int width, int height);
	public ImagePainter createImage(String filename) throws GraphicsException ;
}
