package iv3d.graphics.draw.base;

public interface GraphicsCanvas {
	GraphicsPainter getPainter();
	int getWidth();
	int getHeight();
	ImagePainter getImage();
}
