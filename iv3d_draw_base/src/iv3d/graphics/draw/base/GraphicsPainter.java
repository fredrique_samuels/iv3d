package iv3d.graphics.draw.base;

import iv3d.base.Bounds;
import iv3d.base.Color;

public interface GraphicsPainter {
	public void drawRect(float x, float y, float width, float height, Options options);
	public void drawRect(Bounds bounds, Options options);
	public void drawCircle(float cx, float cy, float radius, Options options);
	public void drawOval(float cx, float cy, float width, float height, Options options);
	public void drawLine(float px0, float py0, float px1, float py1, Options options);
	public void drawImage(ImagePainter image, float x, float y);
	public void drawImage(ImagePainter image, float x, float y, float width, float height);
	
	/**
	 A NOTE ON ARCS FOR ANDROID 
	 float radius = 20;
	final RectF oval = new RectF();
	oval.set(point1.x - radius, point1.y - radius, point1.x + radius, point1.y+ radius);
	Path myPath = new Path();
	myPath.arcTo(oval, startAngle, -(float) sweepAngle, true);
	
	To calculate startAngle, use this code:
	 int startAngle = (int) (180 / Math.PI * Math.atan2(point.y - point1.y, point.x - point1.x));
	 
	 Here, point1 means where you want to start drawing the Arc. sweepAngle means the angle between two lines. We have to calculate that by using two points like the blue points in my Question image.
	 */
	public void drawArc(float x, float y, float width, float height, float startAngle, float sweepAngle, Options options);
	
	public void fillLinearGradient(float startX, float startY, Color startColor, float endX, float endY, Color endColor, Bounds bounds);
	public void fillRadialGradient(float cx, float cy, float radius, Color startColor, Color endColor, Bounds bounds);
	public void clip(float x, float y, float width, float height);
	public void setClip(float x, float y, float width, float height);
	public void translate(float x, float y);
	public void rotate(float theta, float x, float y);
	public void scale(float x, float y);
	
	
	public GraphicsPainter create(float x, float y, float width, float height);
	public GraphicsPainter create();
	public void dispose();
	public void save();
	public void restore();
	
	public Shape createRectangleShape(float x, float y, float width, float height);
	public Shape createOvalShape(float cx, float cy, float width, float height);
	public Shape createArcShape(float x, float y, float width, float height, float startAngle, float sweepAngle, int type);
	public Shape createLineShape(float px0, float py0, float px1, float py1);
	public Shape createPathShape(Shape[] shapes);
	public void clipShape(Shape shape);
	public void drawShape(Shape shape, Options options);
	public void drawText(float x, float y, String text, Font font, Options options);
	public void clear(float x, float y, float width , float height);
	public FontMetrics getFontMetrics(Font font);
	
}
