package iv3d.graphics.draw.base;

public abstract class Font {
	public final static int NORMAL = 0;
	public final static int ITALIC = 2;
	public final static int BOLD = 4; 
}
