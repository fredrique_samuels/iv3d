package iv3d.graphics.draw.base;

public interface FontMetrics {
	float getAscent();
	float getHeight();
	float getDescent();
	float getLeading();
	float stringWidth(String str);
}
