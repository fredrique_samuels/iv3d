package iv3d.graphics.draw.base;

import iv3d.base.exceptions.GraphicsException;

public abstract class ImagePainter {
	private final int width;
	private final int height;
	
	public ImagePainter(int width, int height) {
		super();
		this.width = width;
		this.height = height;
	}
	
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}

	public abstract void save(String filename) throws GraphicsException;
	public abstract ImagePainter subImage(int x, int y, int width, int height);
}
