package iv3d.graphics.draw.base;

import iv3d.base.Color;

public class Options {
	
	private Color color = Color.BLACK;
	private Stroke strokeType = Stroke.LINE;
	private float strokeSize = 1.0f;
	
	public Options() {
	}

	public Color getColor() {
		return color;
	}

	public Options setColor(Color color) {
		this.color = color;
		return this;
	}

	public Stroke getStrokeType() {
		return strokeType;
	}

	public Options setStrokeType(Stroke stroke) {
		this.strokeType = stroke;
		return this;
	}

	public float getStrokeSize() {
		return strokeSize;
	}

	public Options setStrokeSize(float strokeSize) {
		this.strokeSize = strokeSize;
		return this;
	}
	
}
