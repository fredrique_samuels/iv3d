/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base;

import iv3d.base.math.Matrix44;
import iv3d.base.math.Quaternion;
import iv3d.base.math.Vector3;
import iv3d.base.math.Vector4;

/**
 * A {@link Matrix44} implementation of {@link Transform}. 
 */
public class MatrixTransform implements Transform {

	private Vector3 position;
	private Vector3 scale;
	private Matrix44 rotation;

	public MatrixTransform() {
		position = new Vector3();
		scale = new Vector3(1, 1, 1);
		rotation = new Matrix44();
	}

	@Override
	public final Transform setPosition(float x, float y, float z) {
		return setPosition(new Vector3(x, y, z));
	}
	
	@Override
	public final Transform setPosition(Vector3 v) {
		synchronized (position) {
			position = v;
		}
		return this;
	}

	@Override
	public final Vector3 getPosition() {
		Vector3 result;
		synchronized (position) {
			result = new Vector3(position);			
		}
		return result;
	}

	@Override
	public final Transform setScale(float x, float y, float z) {
		return setScale(new Vector3(x, y, z));
	}

	@Override
	public final Transform setScale(Vector3 v) {
		synchronized (scale) {
			scale = v;			
		}
		return this;
	}

	@Override
	public final Vector3 getScale() {
		Vector3 result;
		synchronized (scale) {
			result = new Vector3(scale);			
		}
		return result;
	}

	@Override
	public final Transform setRotation(Matrix44 rot) {
		return setRotation(rot.toArray());
	}

	@Override
	public final Transform setRotation(float[] rot) {
		synchronized (rotation) {
			rotation.set(rot); 			
		}
		return this;
	}

	@Override
	public final Transform setRotation(float x, float y, float z) {
		synchronized (rotation) {
			rotation.identity();
			rotation.rotate(new Vector4(1, 0, 0, x));
			rotation.rotate(new Vector4(0, 1, 0, y));
			rotation.rotate(new Vector4(0, 0, 1, z));
		}
		return this;
	}

	@Override
	public final Transform setRotation(Quaternion q) {
		synchronized (rotation) {
			rotation.makeRotate(q);
		}
		return this;
	}

	@Override
	public final float[] getRotation() {
		float[] array;
		synchronized (position) {
			array = rotation.toArray();
		}
		return array;
	}

	@Override
	public final float[] toArray() {
		Matrix44 m = new Matrix44();
		m.translate(getPosition());
		m.multiply(getRotation());
		m.scale(getScale());
		return m.toArray();
	}

	@Override
	public final float[] toArrayI() {
		Matrix44 m = new Matrix44();
		m.translate(getPosition());
		m.multiply(getRotation());
		m.scale(getScale());
		m.invert();
		return m.toArray();
	}

	@Override
	public Transform setRotation(Vector3 v) {
		return setRotation(v.getX(), v.getY(), v.getZ());
	}

	@Override
	public Transform setPositionX(Float v) {
		return setPosition(v, position.getY(), position.getZ());
	}

	@Override
	public Transform setPositionY(Float v) {
		return setPosition(position.getX(),v, position.getZ());
	}

	@Override
	public Transform setPositionZ(Float v) {
		return setPosition(position.getX(), position.getY(), v);
	}

	@Override
	public TransformState getTransformState() {
		return new TransformState(getPosition(), getScale(), new Matrix44(getRotation()));
	}

}
