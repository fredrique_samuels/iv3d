package iv3d.base.net.server.exception;


public class DataServerException extends Exception {

	private static final long serialVersionUID = 1L;

	public DataServerException(Exception e) {
		super(e);
	}

}
