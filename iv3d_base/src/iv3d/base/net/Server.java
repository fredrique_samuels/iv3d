/**
 * Copyright (C) 2011-2012 F. Samuels.       
 * All rights reserved.  Email: fredriquesamuels@gmail.com          
 *                                                                       
 * This library is free software; you can redistribute it and/or         
 * modify it under the terms of EITHER:                                  
 *   (1) The GNU Lesser General Public License as published by the Free  
 *       Software Foundation; either version 2.1 of the License, or (at  
 *       your option) any later version. The text of the GNU Lesser      
 *       General Public License is included with this library in the     
 *       file LICENSE.TXT.                                               
 *   (2) The BSD-style license that is included with this library in     
 *       the file LICENSE-BSD.TXT.                                       
 *                                                                       
 * This library is distributed in the hope that it will be useful,       
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files    
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.           
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.net;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;

/**
 * <p>A {@link ServerConnection} implementation that provided
 * an interface for receiving and sending data from client connections.</p><br>
 * <br>
  * <b>Example :</b><br><br>
 * <code>
 * try {<br>&ensp;&ensp;
 * 	ScNetServer server = new ScNetServer(12345){<br>&ensp;&ensp;&ensp;&ensp; 
 * 		public void onNewConnection(ScNetConnection connection) {}<br>&ensp;&ensp;&ensp;&ensp;
 * 		public void onDataReceived(ScNetConnection connection, String data) {}<br>&ensp;&ensp;&ensp;&ensp;
 * 		public void onConnectionClosed(ScNetConnection connection) {}<br>&ensp;&ensp;
 * 		};<br>&ensp;&ensp;
 * 	Thread.sleep(2000);<br>&ensp;&ensp;
 * 	server.close();<br>
 * } catch (IOException e) {<br>&ensp;&ensp;
 *  e.printStackTrace();<br>
 * } catch (InterruptedException e) {<br>&ensp;&ensp;
 *  e.printStackTrace();<br>
 * }
 * </code> 
 *  
 * @see Connection
 * @see Client
 * @see ServerConnection
 * @see ServerBeacon
 */
public abstract class Server extends ServerConnection{

	
	/**
	 * This thread is dedicated to waiting to client connection
	 * and then calling ScNetServer#onNewConnection(ScNetConnection)
	 * when a connection is made.
	 */
	class NetDataListenThread extends Thread {
		
		/**
		 * A termination flag.
		 */
		private boolean mTerminate = false;

		/**
		 * Terminate the thread.
		 */
		public synchronized void terminate() {
			mTerminate = true;
		}

		@Override
		public void run() {
			while (!mTerminate) {
				listen();
			}
			mTerminate = true;
		}
	};

	/**
	 * Create a new server on the requested port.
	 * @param port The port on which data will be served.
	 * @throws NetworkException 
	 */
	public Server(int port) throws NetworkException {
		super(port);
		listenThread = new NetDataListenThread();

		addClientFoundListener(new ClientConnectedListener() {
			
			@Override
			public void onClientConnected(ServerConnection server,
					Connection scNetConnection) {
				try {
					mConnectionListSema.acquire();
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				mConnectionList.add(scNetConnection);
				mConnectionListSema.release();
				onNewConnection(scNetConnection);
			}
		});
	}
	
	@Override
	public final void onConnectionLost(Connection connection) {
		try {
			mConnectionListSema.acquire();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		mConnectionList.remove(connection);
		mConnectionListSema.release();
		onConnectionClosed(connection);
	}
	
	/**
	 * Called when a connection is lost or closed.
	 * @param connection The connection object that was lost or closed.
	 */
	public abstract void onConnectionClosed(Connection connection);

	
	/**
	 * Start the server.
	 * @see #stop()
	 */
	public void start() {
		if (!isRunning()){
			startListenThread();
		}
	}

	/**
	 * Stop the server.
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @see start()
	 */
	public void stop() throws InterruptedException, IOException {
		this.close();
		listenThread.terminate();
		listenThread.join();
	}
	
	/**
	 * @return <tt>true</tt> if the server is still running.
	 */
	public boolean isRunning() {
		return !isClosed() && listenThread.isAlive();
	}

	/**
	 * Called when a new connection is made the server.
	 * The server keeps an internal list of al connection that
	 * are made to it. They are also removed when they are disconnected.
	 * 
	 * @param connection The connection object.
	 */
    public abstract void onNewConnection(Connection connection);

    
    /**
     * Start the listening for connections ot this server.
     */
	private void startListenThread() {
		listenThread.start();
	}
	
	/**
	 * The thread that waits for client connections.
	 */
	private NetDataListenThread listenThread;
	
	/**
	 * A semaphore to make the internal connection list thread safe.
	 */
	private Semaphore mConnectionListSema = new Semaphore(1);
	
	/**
	 * A list of active connections to this server.
	 */
	private ArrayList<Connection> mConnectionList = new ArrayList<Connection>();

}
