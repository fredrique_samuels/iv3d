/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.net;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public class SerializationUtils {
	private SerializationUtils() {
	}
	
	public static byte[] toBytes(Object obj) throws NetworkException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		try {
		  out = new ObjectOutputStream(bos);   
		  out.writeObject(obj);
		  return bos.toByteArray();
		} catch (IOException e) {
			throw new NetworkException("Unable to serialize object!", e);
		} finally {
		  try {
		    if (out != null) {
		      out.close();
		    }
		  } catch (IOException ex) {
		  }
		  try {
		    bos.close();
		  } catch (IOException ex) {
		     
		  }
		}
	}
	
	public static Object fromBytes(byte[] bytes) throws NetworkException {
		ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
		ObjectInput in = null;
		try {
		  in = new ObjectInputStream(bis);
		  Object o;
		try {
			o = in.readObject();
		} catch (ClassNotFoundException e) {
			throw new NetworkException("Unable to deserialize object.", e);
		} 
		  return o;
		} catch (IOException e) {
			throw new NetworkException("Unable to serialize object!", e);
		} finally {
		  try {
		    if (in != null) {
		    	in.close();
		    }
		  } catch (IOException ex) {
		  }
		  try {
			  bis.close();
		  } catch (IOException ex) {
		     
		  }
		}
	}
	
}
