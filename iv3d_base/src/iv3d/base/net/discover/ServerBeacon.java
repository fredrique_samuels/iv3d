/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.net.discover;

import iv3d.base.exceptions.ExceptionUtils;
import iv3d.base.net.NetworkException;
import iv3d.base.threads.WorkerThread;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Logger;

public abstract class ServerBeacon {

	protected static Logger logger = Logger.getLogger(ServerBeacon.class
			.getCanonicalName());

	private final DatagramSocket socket;
	private WorkerThread datalistenThread;

	public ServerBeacon(int port) throws NetworkException {
		try {
			socket = new DatagramSocket(port, InetAddress.getByName("0.0.0.0"));
			socket.setBroadcast(true);
		} catch (SocketException e) {
			throw new NetworkException(e);
		} catch (UnknownHostException e) {
			throw new NetworkException(e);
		}
	}
	
	protected abstract byte[] getServerResponse(byte[] data);
	protected abstract boolean isClientRequest(byte[] data);

	@Override
	protected void finalize() throws Throwable {
		terminate();
		super.finalize();
	}
	
	private final void sendData(byte[] data, InetAddress address, int port) {
		DatagramPacket sendPacket = new DatagramPacket(data, data.length, address, port);
		logger.info(">>>Sent packet to: " + address.getHostAddress());
		try {
			socket.send(sendPacket);
		} catch (IOException e) {
			logger.warning(ExceptionUtils.getStackTrace(e));
		}
	}

	public void terminate() {
		if (datalistenThread != null) {
			datalistenThread.terminate();
			datalistenThread = null;
		}
	}
		
	public final void start() {
		terminate();
		datalistenThread = new WorkerThread() {
			
			@Override
			public void onExit() {
			}
			
			@Override
			public void execute() {
				logger.info("Ready to receive broadcast packets!");
				
		        DatagramPacket packet = receivePacket();
		        if(packet==null) {
		        	return;
		        }
				
		        logger.info(">>>Discovery packet received from: " + packet.getAddress().getHostAddress());
		        if(isClientRequest(packet.getData())) {
		        	byte[] response = getServerResponse(packet.getData());
		        	logger.info(">>>Sending server response to " + packet.getAddress().getHostAddress());
		        	sendData(response, packet.getAddress(), packet.getPort());
		        }
			        
				logger.info("Closing server beacon");
			}

			private DatagramPacket receivePacket() {
				byte[] recvBuf = new byte[15000];
				DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
				try {
					socket.receive(packet);
				} catch (IOException e) {
					logger.info(ExceptionUtils.getStackTrace(e));
					return null;
				}
				return packet;
			}
		};
		
		datalistenThread.setDelay(10);
		datalistenThread.start();
	}
	
	public void close() {
		socket.close();
		terminate();
	}
}
