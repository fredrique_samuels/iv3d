/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.net.discover;

import iv3d.base.exceptions.ExceptionUtils;
import iv3d.base.net.NetworkException;
import iv3d.base.threads.WorkerThread;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.logging.Logger;

public abstract class ServerFinder {
	protected static Logger logger = Logger.getLogger(ServerFinder.class
			.getCanonicalName());

	private String defaultBroadcastIp;
	private int port;
	private DatagramSocket socket;
	private WorkerThread datalistenThread;

	public ServerFinder(int port) throws NetworkException {
		this("255.255.255.255", port);
	}

	public ServerFinder(String defaultBroadcastIp, int port)
			throws NetworkException {
		this.defaultBroadcastIp = defaultBroadcastIp;
		this.port = port;

		try {
			socket = new DatagramSocket();
			socket.setBroadcast(true);
		} catch (SocketException e) {
			throw new NetworkException(e);
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		terminate();
		super.finalize();
	}

	public final void probeServers() {
		byte[] data = getProbeMessage();

		probeDefaultNetwork(data);
		probeAllNetworkDevices(data);
	}

	private void probeAllNetworkDevices(byte[] data) {
		Enumeration<NetworkInterface> interfaces;
		try {
			interfaces = NetworkInterface.getNetworkInterfaces();
		} catch (SocketException e) {
			return;
		}

		while (interfaces.hasMoreElements()) {
			NetworkInterface networkInterface = interfaces.nextElement();

			try {
				if (networkInterface.isLoopback() || !networkInterface.isUp()) {
					continue;
				}
			} catch (SocketException e) {
			}

			for (InterfaceAddress interfaceAddress : networkInterface
					.getInterfaceAddresses()) {
				InetAddress broadcast = interfaceAddress.getBroadcast();
				if (broadcast == null) {
					continue;
				}

				probeNetwork(data, broadcast, port);

				logger.info(">>> Request packet sent to: "
						+ broadcast.getHostAddress() + "; Interface: "
						+ networkInterface.getDisplayName());
			}
		}
	}

	private void probeDefaultNetwork(byte[] data) {
		logger.info(">>> Request packet sent to: " + defaultBroadcastIp
				+ " (DEFAULT)");
		try {
			probeNetwork(data, InetAddress.getByName(defaultBroadcastIp), port);
		} catch (UnknownHostException e) {
			logger.warning(ExceptionUtils.getStackTrace(e));
		}
	}

	private void probeNetwork(byte[] data, InetAddress address, int port) {
		DatagramPacket sendPacket = new DatagramPacket(data, data.length,
				address, port);
		try {
			socket.send(sendPacket);
		} catch (IOException e) {
			logger.warning(ExceptionUtils.getStackTrace(e));
		}
	}

	public synchronized final void start() {
		terminate();
		datalistenThread = new WorkerThread() {

			@Override
			public void execute() {
				logger.info("Ready to receive broadcast packets!");

				DatagramPacket packet = receivePacket();
				if (packet == null) {
					return;
				}

				String hostAddress = packet.getAddress().getHostAddress();
				byte[] data = packet.getData();

				logger
						.info(">>>Discovery packet received from: "
								+ hostAddress);
				if (isServerReply(data)) {
					onServerReply(hostAddress, data);
				}

			}

			private DatagramPacket receivePacket() {
				byte[] recvBuf = new byte[15000];
				DatagramPacket packet = new DatagramPacket(recvBuf,
						recvBuf.length);
				try {
					socket.receive(packet);
				} catch (IOException e) {
					logger.info(ExceptionUtils.getStackTrace(e));
					return null;
				}
				return packet;
			}

			@Override
			public void onExit() {
			}
		};

		datalistenThread.setDelay(0);
		datalistenThread.start();
	}

	public void terminate() {
		if (datalistenThread != null) {
			datalistenThread.terminate();
			datalistenThread = null;
		}
	}

	protected abstract byte[] getProbeMessage();

	protected abstract void onServerReply(String hostAddress, byte[] data);

	protected abstract boolean isServerReply(byte[] data);

}
