/**
 * Copyright (C) 2011-2012 F. Samuels.       
 * All rights reserved.  Email: fredriquesamuels@gmail.com          
 *                                                                       
 * This library is free software; you can redistribute it and/or         
 * modify it under the terms of EITHER:                                  
 *   (1) The GNU Lesser General Public License as published by the Free  
 *       Software Foundation; either version 2.1 of the License, or (at  
 *       your option) any later version. The text of the GNU Lesser      
 *       General Public License is included with this library in the     
 *       file LICENSE.TXT.                                               
 *   (2) The BSD-style license that is included with this library in     
 *       the file LICENSE-BSD.TXT.                                       
 *                                                                       
 * This library is distributed in the hope that it will be useful,       
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files    
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.           
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.net;

import iv3d.base.net.adapters.SocketDataPathAdapter;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;

/**
 * A thin wrapper for the {@link java.net.Socket} object.
 * This implementation provides abstract methods for data reception
 * and connection connectivity and breaking.
 * 
 * @see Server
 */
public abstract class ServerConnection {

	/**
	 * Create a new server socket on the requested port.
	 * 
	 * @param port The port to run the server on.
	 * @throws NetworkException 
	 * @throws IOException
	 */
    public ServerConnection(int port) throws NetworkException {
        try {
			serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			throw new NetworkException(e);
		}
    }

    /**
     * Listen for any incoming connections. Calls
     * {@link ClientConnectedListener#onClientConnected(ServerConnection, Connection)} 
     * when a client connects to the server.
     * 
     * @see ClientConnectedListener
     */
    synchronized public void listen() {
        try {
            mSocketSema.acquire();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
        try {
            serverSocket.setSoTimeout(mTimeout);
            Socket socket = serverSocket.accept();
            for (ClientConnectedListener l : mClientConnectedListeners) {
                l.onClientConnected(this, new Connection(new SocketDataPathAdapter(socket), new DataReceiver() {
					
					@Override
					public void onConnectionDataReceived(Connection connection, byte[] data) {
						ServerConnection.this.onDataReceived(connection, data);
					}
					
					@Override
					public void onConnectionClosed(Connection connection) {
						ServerConnection.this.onConnectionLost(connection);
					}
				}));
            }
        } catch (IOException e) {
        } finally {
            mSocketSema.release();
        }
    }
    
    
    /**
     * Called when a data is received from a client.
     * 
     * @param connection The connection that send the data.
     * @param data The data received.
     */
    public abstract void onDataReceived(Connection connection, byte[] data);
    
    /**
     * Called when a connection is lost.
     * @param connection The lost conenction.
     */
    public abstract void onConnectionLost(Connection connection);

    
    /**
     * @return The underlying {@link Socket} object. The socket will not be protected by the internal 
     * 		semaphore.
     */
    public final ServerSocket getSocket() {
        return serverSocket;
    }

    /**
     * Add a listener to be notified of any new connections.
     * @param l The listener to be added.
     */
    public void addClientFoundListener(ClientConnectedListener l) {
        if (!mClientConnectedListeners.contains(l))
            mClientConnectedListeners.add(l);
    }

    /**
     * Set the time in milliseconds to wait on a new connection. 
     * @param timeout The time to wait in milliseconds.
     * @throws InterruptedException
     * @see {@link #listen()}
     */
    public void setConnectionTimeout(int timeout) throws InterruptedException {
        mSocketSema.acquire();
        mTimeout = timeout;
        mSocketSema.release();
    }

    /**
     * Close the socket connection.
     * @throws IOException
     */
    public void close() {
        try {
			serverSocket.close();
		} catch (IOException e) {
			// ignore close exception
		}
    }
    
    /**
     * @return <tt>true</tt> if the server is not running.
     */
    public boolean isClosed(){
    	return serverSocket.isClosed();
    }

    /**
     * The server socket.
     */
    private ServerSocket serverSocket;
    
    /**
     * A semaphore to lock the socket.
     */
    private Semaphore mSocketSema = new Semaphore(1);
    
    /**
     * A list of {@link ClientConnectedListener} listeners.
     */
    private final ArrayList<ClientConnectedListener> mClientConnectedListeners =
            new ArrayList<ClientConnectedListener>();
    
    /**
     * The socket timeout time.
     */
    private int mTimeout = 10000;
}
