/**
 * Copyright (C) 2011-2012 F. Samuels.       
 * All rights reserved.  Email: fredriquesamuels@gmail.com          
 *                                                                       
 * This library is free software; you can redistribute it and/or         
 * modify it under the terms of EITHER:                                  
 *   (1) The GNU Lesser General Public License as published by the Free  
 *       Software Foundation; either version 2.1 of the License, or (at  
 *       your option) any later version. The text of the GNU Lesser      
 *       General Public License is included with this library in the     
 *       file LICENSE.TXT.                                               
 *   (2) The BSD-style license that is included with this library in     
 *       the file LICENSE-BSD.TXT.                                       
 *                                                                       
 * This library is distributed in the hope that it will be useful,       
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files    
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.           
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.net;

import iv3d.base.net.adapters.SocketDataPathAdapter;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Abstract Client object used to communicate with local or remote
 * {@link Server} objects.<br>
 * <br>
 * <b>Example :</b><br><br>
 * <code>
 * try {<br>&ensp;&ensp;
 * 	ScNetClient client = new ScNetClient(127.0.0.1, 12345){<br>&ensp;&ensp;&ensp;&ensp;
 *		public void onDataReceived(String data) {<br>&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;
 *					System.out.println("Data from server : " + data);<br>&ensp;&ensp;&ensp;&ensp;
 *				}<br>&ensp;&ensp;&ensp;&ensp;
 *		public void onConnectionClosed() {}<br>&ensp;&ensp;
 * };<br>&ensp;&ensp;
 *            
 *  client.sendData("Hello from client");<br>&ensp;&ensp;
 *  Thread.sleep(2000);<br>&ensp;&ensp;
 *  client.close();<br>
 * } catch (IOException e) {<br>&ensp;&ensp;
 *  e.printStackTrace();<br>
 * } catch (InterruptedException e) {<br>&ensp;&ensp;
 *  e.printStackTrace();<br>
 * }
 * </code>
 * 
 * @see Server
 */
public abstract class Client implements DataReceiver {

	
	/**
	 * Create a new Client connection.
	 * 
	 * @param serverName The server address
	 * @param port The port number.
	 * @throws UnknownHostException
	 * @throws IOException
	 */
    public Client(String serverName, int port) throws UnknownHostException, IOException {
        Socket socket = new Socket(serverName, port);
		mConnection = new Connection(new SocketDataPathAdapter(socket), this);
    }

    /**
     * Send data to the server.
     * 
     * @param data The data to be sent.
     * @throws IOException
     */
    public void sendData(String data) throws IOException {
        mConnection.sendData(data);
    }
    
    @Override
	public final void onConnectionDataReceived(Connection connection, byte[] data) {
    	onDataReceived(data);
	}

    /**
     * Called when data is received from the server.
     * 
     * @param data The data received.
     */
    public abstract void onDataReceived(byte[] data);

    /**
     * Called when the connection is closed.
     */
    public abstract void onConnectionClosed();
    
    /**
     * Close the connection.
     * 
     * @see #isConnected()
     */
    public synchronized void close() {
        try {
            mConnection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    
    /**
     * @return <tt>true</tt> if connection is active.
     * @see Connection#isConnected()
     */
    public boolean isConnected(){
    	return mConnection.isConnected();
    }

    @Override
    public final void onConnectionClosed(Connection connection) {
    	onConnectionClosed();
    }
    
    /**
     * The connection instance.
     */
    private final Connection mConnection;

}
