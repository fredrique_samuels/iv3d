/**
 * Copyright (C) 2011-2012 F. Samuels.       
 * All rights reserved.  Email: fredriquesamuels@gmail.com          
 *                                                                       
 * This library is free software; you can redistribute it and/or         
 * modify it under the terms of EITHER:                                  
 *   (1) The GNU Lesser General Public License as published by the Free  
 *       Software Foundation; either version 2.1 of the License, or (at  
 *       your option) any later version. The text of the GNU Lesser      
 *       General Public License is included with this library in the     
 *       file LICENSE.TXT.                                               
 *   (2) The BSD-style license that is included with this library in     
 *       the file LICENSE-BSD.TXT.                                       
 *                                                                       
 * This library is distributed in the hope that it will be useful,       
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files    
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.           
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * A thin wrapper for the {@link java.net.Socket} implementation. This class 
 * provides threads for receiving and methods for sending data.<br><br>
 * <b>Example :</b><br><br>
 * <code>
 * 
 * </code>
 * 
 * @see Client
 * @see Server
 */
public class Connection {

	/**
	 * A thread dedicated to receiving data from the socket connection.
	 */
    class ScNetReceiveDataThread extends Thread {

    	/**
    	 * A termination flag.
    	 */
        private boolean mTerminate = false;

        
        /**
         * Request that the thread be terminated.
         */
        public synchronized void terminate() {
            mTerminate = true;
        }

        @Override
        public void run() {
            InputStream inFromServer = null;
            try {
                inFromServer = mSocket.getInputStream();
            } catch (IOException e) {
            	e.printStackTrace();
            } 
            
            DataInputStream in =
                    new DataInputStream(inFromServer);
            while (!mTerminate) {
                try {
                	int length = in.available();
                	if(length==0) {
                		try {
							Thread.sleep(10);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						continue;
                	}
                    byte[] bs = new byte[length];
					in.readFully(bs);
                    mComms.onConnectionDataReceived(Connection.this, bs);
                } catch (IOException e) {
                    mComms.onConnectionClosed(Connection.this);
                    break;
                }
            }
            mTerminate = true;
        }
    };

    
    /**
     * Create a new connection object from an existing socket.
     * @param socket The socket object.
     * @param comms The communication interface.
     * @throws IOException
     */
    public Connection(DataPath socket, DataReceiver comms) throws IOException {
        mSocket = socket;
        mComms = comms;
        mOut = new DataOutputStream(socket.getOutputStream());
        mInDataThread.start();
    }
    
    
    /**
     * @return <tt>true</tt> is the socket is connected. <tt>false</tt> otherwise.
     */
    public boolean isConnected(){
    	return !mSocket.isClosed();
    }

    /**
     * Send data to the connecting server.
     * @param data The data to send.
     * @throws IOException
     */
    public void sendData(String data) throws IOException {
        mOut.writeUTF(data);
    }
    
    /**
     * Send data to the connecting server.
     * @param data The data to send.
     * @throws IOException
     */
    public void sendData(byte[] data) throws IOException {
        mOut.write(data);
    }

    /**
     * Close the socket.
     * @throws IOException
     */
    public synchronized void close() throws IOException {
        mSocket.close();
        mInDataThread.terminate();
        mComms.onConnectionClosed(this);
    }

    /**
     * The socket connection.
     */
    private final DataPath mSocket;
    
    /**
     * The communication object.
     */
    private final DataReceiver mComms;
    
    /**
     * The output data stream.
     */
    private final DataOutputStream mOut;
    
    /**
     * The input data stream.
     */
    private final ScNetReceiveDataThread mInDataThread = new ScNetReceiveDataThread();
    
}

