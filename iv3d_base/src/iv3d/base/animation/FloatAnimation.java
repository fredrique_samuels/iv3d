/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.animation;

import iv3d.base.exceptions.GraphicsException;

import java.lang.reflect.Method;

/**
 * An animation object for {@link Float} values.
 * 
 * @see AnimationCalculator
 * @see AnimationManager
 * @see Animator
 */
public class FloatAnimation extends Animator<Float>{

	/**
	 * Value constructor.
	 * 
	 * @param target The object that will be has the call-back method. 
	 * @param callback The name of the method to be called when the value is updated.
	 * 			The method must take only one argument of type {@link Float}. 
	 * @param startValue The start value.
	 * @param endValue The end value.
	 * @param duration The duration of the animation in milliseconds.
	 * @param delay The amount of time to wait before the animation starts in milliseconds.
	 * @param loop Set <code>true</code> if the animation should loop.
	 * @throws GraphicsException If the <code>target</code> does not have a method 
	 * 		<code>callback</code> or the method signature is not valid.
	 */
	public FloatAnimation(Object target, String callback, Float startValue,
			Float endValue, long duration, long delay, boolean loop) throws GraphicsException {
		super(target, callback, startValue, endValue, duration, delay, loop);
	}

	@Override
	protected Method getMethod(Object source, String methodName) throws GraphicsException {
		try {
			return source.getClass().getMethod(methodName, Float.class);
		} catch (SecurityException e) {
			throw new GraphicsException(e);
		} catch (NoSuchMethodException e) {
			throw new GraphicsException(e);
		}
	}

}
