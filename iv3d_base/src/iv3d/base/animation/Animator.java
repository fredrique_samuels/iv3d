/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.animation;

import iv3d.base.exceptions.GraphicsException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


/**
 * An abstract animation description object that manages animation
 * progress and updates the target object via reflection. 
 * 
 * @param <DataType> Data type that will be animated.
 */
public abstract class Animator<DataType> {

	private final Object target;
	private Method callback;
	private boolean done;
	private final long duration;
	private final long delay;
	private long activeDelay;
	private final boolean loop;
	private final DataType startValue;
	private final DataType endValue;
	private long startTime;
	private AnimationCalculator<DataType> animationCalculator;
	private AnimationDoneListener listener;

	/**
	 * <p>
	 * Value constructor.
	 * </p>
	 * 
	 * @param target The object that will receive the animation notifications.
	 * @param callback The name of the method that should be called when the animation is updated.
	 * 			{@link #callback} must have the signature callback(DataType).
	 * @param startValue The start value of the animation.
	 * @param endValue The target value of the animation.
	 * @param duration The time it should take to reach the end of the animation in milliseconds.
	 * @param delay The time that should be waited before the animatio starts. Value in milliseconds.
	 * @param loop Set <code>true</code> to have the animation loop. 
	 * @throws GraphicsException If the callback signature does not match callback(DataType).
	 */
	public Animator(Object target, String callback, DataType startValue, DataType endValue,
			long duration, long delay, boolean loop) throws GraphicsException {
		this.target = target;
		this.callback = getMethod(target, callback);
		this.duration = duration;
		this.delay = delay;
		this.activeDelay = delay;
		this.startValue = startValue;
		this.endValue = endValue;
		this.loop = loop;
	}

	/**
	 * @return The target object.
	 */
	public final Object getTarget() {
		return target;
	}

	/**
	 * Get the callback method that should be notified on animation updates.
	 *   
	 * @param source The object that the method will be invoked on.
	 * @param methodName The name of the method.
	 * @return The method.
	 * @throws GraphicsException If the method with the correct signature could not be found.
	 */
	protected abstract Method getMethod(Object source, String methodName)
			throws GraphicsException;

	/**
	 * Calculate the value of the animation givne the start value, end value and progress.
	 * 
	 * @param progress A value from 0.0-1.0. Where 0.0 is the start of the animation aand 1.0 is the end.
	 * @param startValue The start value for the animation.
	 * @param endValue The target value for the animation.
	 * @return The value of the animation. 
	 */
	private DataType animate(float progress, DataType startValue, DataType endValue) {
		if(animationCalculator==null) {
			return startValue;
		}
		return animationCalculator.calculate(progress, startValue, endValue);
	}

	/**
	 * Update the animation and notify the target objects of the new value.
	 *  
	 * @param clock The application running time. 
	 */
	public final void update(long clock) {
		if (isDone()) {
			return;
		}

		long runningTime = clock - startTime;

		if (runningTime < this.activeDelay) {
			return;
		}
		runningTime -= this.activeDelay;
		
		updateStatus(clock, runningTime);

//		long progressTime = runningTime % duration;
		float progress = done ? 1.0f : (float) runningTime / (float) duration;
		DataType value = animate(progress, startValue, endValue);

		try {
			callback.invoke(target, value);
		} catch (IllegalArgumentException e) {
			callback = null;
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			callback = null;
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			callback = null;
			e.printStackTrace();
		}
	}
	
	public void setAnimationDoneListener(AnimationDoneListener listener) {
		this.listener = listener;
	}

	/**
	 * Start the annimation.
	 */
	public final void start() {
		this.startTime = AnimationManager.getClock();
		this.activeDelay = delay;
		this.done=false;
	}

	private void updateStatus(long clock, long runningTime) {
		if (runningTime > duration) {
			done = true;
			this.activeDelay = 0;
		}

		if (done && loop) {
			done = false;
			long dur = (runningTime) % duration;
			startTime = clock - dur;
		}
	}

	/**
	 * @return <code>true</code> if the animation has completed, the target is <code>null</code> or the 
	 * 		callback method could not be located. 
	 */
	public final boolean isDone() {
		return done || target == null || duration == 0 || callback == null;
	}
	
	/**
	 * Set the calculation method for the animations.
	 * 
	 * @param animationCalculator The calculator.
	 * @see AnimationCalculator
	 */
	public final void setAnimationCalculator(
			AnimationCalculator<DataType> animationCalculator) {
		this.animationCalculator = animationCalculator;
	}
	
	public final void fireDoneEvent() {
		if(listener!=null) {
			listener.animationComplete();
		}
	}
}
