/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.animation;

import iv3d.base.Clock;
import iv3d.base.SystemClock;

import java.util.LinkedList;
import java.util.List;


/**
 * An utility class for managing animations.
 * If you wish to include animation you need to 
 * call {@link AnimationManager#start()}.
 */
public class AnimationManager {
	private static List<Animator<?>> animations = new LinkedList<Animator<?>>();
	private static List<Animator<?>> animationsToAdd = new LinkedList<Animator<?>>();
	private static List<Animator<?>> animationsToRemove = new LinkedList<Animator<?>>();
	private static AnimationUpdateThread updateThead; 
	private static Clock clock = new SystemClock();
	private static Long pausedTime;

	private AnimationManager() {
	}
	
	/**
	 * Set the {@link Clock} to be used for animation updates.
	 * @param arg0 The clock used for animation updates.
	 */
	public static void setClock(Clock arg0) { 
		clock = arg0;
	}
	
	/**
	 * Register a new {@link Animator}.
	 * 
	 * The animation will automatically start when registered and will remove itself when 
	 * done.
	 * 
	 * @param animator The animator to be registered.
	 */
	public static void registerAnimation(Animator<?>... animators) {
		if (animators == null) {
			return;
		}
		
		synchronized (animationsToAdd) {
			for(Animator<?> animator : animators) {
				synchronized (animations) {
					if(animations.contains(animator)) {
						continue;
					}
				}
			
				if(!animationsToAdd.contains(animator)) {
					animationsToAdd.add(animator);
				}
			}
		}
	}
	
	
	/**
	 * De-register an {@link Animator}.
	 * 
	 * @param animator The {@link Animator} to be removed.
	 */
	public static void deregisterAnimation(Animator<?> animator) {
		if (animator == null) {
			return;
		}
		
		synchronized (animationsToAdd) {
			if(animationsToAdd.contains(animator)) {
				animationsToAdd.remove(animator);
				return;
			}
		}
		
		synchronized (animationsToRemove) {
			if(!animationsToRemove.contains(animator)) {
				animationsToRemove.add(animator);
			}
		}
	}
	
	/**
	 * Update all animations.
	 */
	public static void update() {
		
		processAnimationRemoving();
		processAnimationAdding();
		
		long clock = getClock();
		
		for(Animator<?> animator : animations) {
			
			synchronized (animationsToRemove) {		
				if(animationsToRemove.contains(animator)) {
					continue;
				}
			}
			
			animator.update(clock);
			if (animator.isDone()) {
				synchronized (animationsToRemove) {					
					animationsToRemove.add(animator);
				}
				animator.fireDoneEvent();
			}
		}
		processAnimationRemoving();
	}

	private static void processAnimationRemoving() {
		synchronized (animationsToRemove) {			
			for(Animator<?> animator : animationsToRemove) {
				animations.remove(animator);
			}
			animationsToRemove.clear();
		}
	}

	private static void processAnimationAdding() {
		pauseClock();
		synchronized (animationsToAdd) {
			for(Animator<?> animator : animationsToAdd) {
				animations.add(animator);
				animator.start();
			}
			animationsToAdd.clear();
		}
		unpauseClock();
	}
	
	private static void pauseClock() {
		pausedTime = Long.valueOf(getClock());
	}
	
	private static void unpauseClock() {
		pausedTime = null;
	}

	/**
	 * @return The current time in milliseconds.
	 */
	public static long getClock() {
		return pausedTime==null?clock.getTime():pausedTime;
	}
	
	/**
	 * Remove all {@link Animator}s that are linked to a target object.
	 * 
	 * @param target The object animations to be removed.
	 */
	public static void removeTarget(Object target) {
		Animator<?> a = getAnimatorFromTarget(target);
		if(a!=null) {
			deregisterAnimation(a);
		} else {			
			a = getAnimatorFromTargetInAddPendingList(target);
			synchronized (animationsToAdd) {				
				animationsToAdd.remove(a);
			}
		}
		
	}

	private static Animator<?> getAnimatorFromTarget(Object target) {
		synchronized (animations) {
			for(Animator<?> animator : animations) {
				if (animator.getTarget().equals(target) || target==animator.getTarget()) {
					return animator;
				}
			}
		}
		return null;
	}
	
	private static Animator<?> getAnimatorFromTargetInAddPendingList(Object target) {
		synchronized (animationsToAdd) {
			for(Animator<?> animator : animationsToAdd) {
				if (animator.getTarget().equals(target) || target==animator.getTarget()) {
					return animator;
				}
			}
		}
		return null;
	}

	/**
	 * Initialize the animation threads.
	 */
	public static void start() {
		if(updateThead==null) {
			updateThead = new AnimationUpdateThread();
			updateThead.start();
		}
	}
	
	/**
	 * Initialize the animation threads.
	 */
	public static void stop() {
		if(updateThead!=null) {
			updateThead.stopThread();
			updateThead= null;
		}
	}
	
	/**
	 * The animation update thread.
	 */
	static class AnimationUpdateThread extends Thread {
		
		boolean stop;
		
		public AnimationUpdateThread() {
			stop = false;
		}
		
		public void stopThread()  {
			stop = true;
		}
		
		@Override
		public void run() {
			while (!stop) {
				
				AnimationManager.update();
				
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
