/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base;


import java.nio.ByteBuffer;

public class StaticImageSource extends ImageSource {

	final int width;
	final int height;
	final int bytesPerPixel;
	final int format;
	final int internalFormat;
	final ByteBuffer pixels;
	final int filterOptions;

	public StaticImageSource(int width, int height, int bytesPerPixel,
			int format, int internalFormat, ByteBuffer pixels, int filterOptions) {
		super();
		this.width = width;
		this.height = height;
		this.bytesPerPixel = bytesPerPixel;
		this.format = format;
		this.internalFormat = internalFormat;
		this.pixels = pixels;
		this.filterOptions = filterOptions;
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		if(pixels!=null) {
			pixels.clear();
		}
	}

	@Override
	public void bind(int index) {
	}

	@Override
	public int getBytesPerPixel() {
		return bytesPerPixel;
	}

	@Override
	public int getDataFormat() {
		return format;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public int getInternalFormat() {
		return internalFormat;
	}

	@Override
	public ByteBuffer getPixels() {
		return pixels;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getFilterOptions() {
		return filterOptions;
	}

	@Override
	public void update() {
	}

	@Override
	public PixelFormat getPixelFormat() {
		return PixelFormat.RGBA;
	}
}
