/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base;

import iv3d.base.math.Matrix44;
import iv3d.base.math.Quaternion;
import iv3d.base.math.Vector3;

/**
 * Any object that contains Transform data must implement {@link Transform}.
 * 
 * @see MatrixTransform
 */
public interface Transform {
	
	/**
	 * Set the internal position value.
	 * 
	 * @param x The <code>x</code> value.
	 * @param y The <code>y</code> value.
	 * @param z The <code>z</code> value.
	 * @return <code>this</code>.
	 */
	public Transform setPosition(float x, float y, float z);
	
	/**
	 * Set the position X component.
	 * 
	 * @param v The value. 
	 * @return <code>this</code>.
	 */
	public Transform setPositionX(Float v);
	
	/**
	 * Set the position Y component.
	 * 
	 * @param v The value. 
	 * @return <code>this</code>.
	 */
	public Transform setPositionY(Float v);
	
	/**
	 * Set the position Z component.
	 * 
	 * @param v The value. 
	 * @return <code>this</code>.
	 */
	public Transform setPositionZ(Float v);

	/**
	 * Set the internal position value.
	 * 
	 * @param v The new position value.
	 * @return <code>this</code>.
	 */
	public Transform setPosition(Vector3 v);

	/**
	 * @return The current position value.
	 */
	public Vector3 getPosition();

	/**
	 *  Set the new scale value.
	 * @param x The <code>x</code> value.
	 * @param y The <code>y</code> value.
	 * @param z The <code>z</code> value.
	 * @return <code>this</code>.
	 */
	public Transform setScale(float x, float y, float z);

	/**
	 * Set the internal scale value.
	 * 
	 * @param v The new scale value.
	 * @return <code>this</code>.
	 */
	public Transform setScale(Vector3 v);
	
	/**
	 * @return The current scale value.
	 */
	public Vector3 getScale();

	/**
	 * Set the internal rotation value.
	 * 
	 * @param rot The new rotation value.
	 * @return <code>this</code>.
	 */
	public Transform setRotation(Matrix44 rot);
	
	/**
	 * Set the internal rotation value.
	 * 
	 * @param rot The new rotation value.
	 * 	rot must be an array of 16 floats.
	 * @return <code>this</code>.
	 */
	public Transform setRotation(float[] rot);

	/**
	 * Set the internal rotation value.
	 * 
	 * @param x The <code>x</code> value.
	 * @param y The <code>y</code> value.
	 * @param z The <code>z</code> value.
	 * @return <code>this</code>.
	 */
	public Transform setRotation(float x, float y, float z);

	/**
	 * Set the internal rotation value.
	 * 
	 * @param v The new rotation value.
	 * @return <code>this</code>.
	 */
	public Transform setRotation(Vector3 v);
	
	/**
	 * Set the internal rotation value.
	 * 
	 * @param q The new rotation value.
	 * @return <code>this</code>.
	 */
	public Transform setRotation(Quaternion q);

	/**
	 * @return The rotation matrix as an array of 16 floats.
	 */
	public float[] getRotation();

	/**
	 * @return The complete transform as a array.
	 */
	public float[] toArray();

	/**
	 * @return The inverse of the complete transform as a array.
	 */
	public float[] toArrayI();
	
	/**
	 * @return The position, rotation and scale of the transform.
	 */
	public TransformState getTransformState();
}
