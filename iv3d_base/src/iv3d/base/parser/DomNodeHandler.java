package iv3d.base.parser;

import org.w3c.dom.Node;

public interface DomNodeHandler {
	public void handle(Node node);
}
