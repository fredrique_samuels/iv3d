package iv3d.base.parser;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public final class DomParser {
	
	private Exception error;
	private final InputStream inputStream;
	private final DomNodeHandler handler;
	
	public DomParser(InputStream inputStream, DomNodeHandler handler) {
		this.inputStream = inputStream;
		this.handler = handler;
	}
	
	public Exception getError(){return error;}
	
	public boolean parse() throws IOException {
		if(handler==null) {return true;}
		
        final Document doc = getDoc(inputStream);
        if(doc==null){return false;}
        
        boolean result = processDoc(handler, doc);
        inputStream.close();
		return result;
    }

	private boolean processDoc(DomNodeHandler handler, final Document doc) {
		Element documentElement = doc.getDocumentElement();
        try {
	        handler.handle(documentElement);
			parse(documentElement, handler);
			return true;
        } catch (Exception e) {
			error = e;
			return false;
		}
	}

	private Document getDoc(InputStream inputStream) {
		final DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
		try {
			docBuilder = dbfac.newDocumentBuilder();
			return docBuilder.parse(inputStream);
		} catch (ParserConfigurationException e) {
			error = e;
		} catch (SAXException e) {
			error = e;
		} catch (IOException e) {
			error = e;
		}
		return null;
	}

    private void parse(final Element e, DomNodeHandler handler) {
        final NodeList children = e.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            final Node n = children.item(i);
	        handler.handle(n);
	        
            if (n.getNodeType() == Node.ELEMENT_NODE) {
                parse((Element) n, handler);
            }
        }
    }
    
}
