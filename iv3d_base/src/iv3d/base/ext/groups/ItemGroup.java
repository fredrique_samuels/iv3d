/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.ext.groups;

import iv3d.base.events.Event;

import java.util.LinkedList;
import java.util.List;

/**
 * Management object for animated menus. 
 */
public class ItemGroup <ItemType extends Item, ItemViewType extends ItemView<ItemType>> {
	private final ItemGroupView<ItemViewType, ItemType> view;
	private final List<ItemGroupEventListener<ItemType>> listeners;
	
	public ItemGroup(Long animationDuration,
			ItemTransform[] transforms, 
			ItemFactory<ItemViewType> factory) {
		factory.init();
		this.view = new ItemGroupView<ItemViewType, ItemType>(transforms, 
				factory) {

			@Override
			protected void dispatchEvent(ItemView<ItemType> item,
					Event event) {
				disptachItemEvent(item, event);
			}
		};
		
		hideAll();
		setAnimationTime(animationDuration);
		listeners = new LinkedList<ItemGroupEventListener<ItemType>>();
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		listeners.clear();
	}
	
	public void addMenuEventListener(ItemGroupEventListener<ItemType> l) {
		if(listeners.contains(l)){
			return;
		}
		listeners.add(l);
	}
	
	public void removeMenuEventListener(ItemGroupEventListener<ItemType> l) {
		if(listeners.contains(l)){
			listeners.remove(l);
		}
	}

	protected void disptachItemEvent(ItemView<ItemType> item, Event event) {
		for(ItemGroupEventListener<ItemType> listener : listeners) {
			listener.onEvent(item, event);
		}
	}
	
	public void setTransforms(ItemTransform... transforms) { 
		setTransforms(0, transforms);
	}
	
	public void setTransforms(int offset, ItemTransform... transforms) { 
		view.setTransforms(offset, transforms);
	}
	
	public void animateTransforms(ItemTransform[] transforms) { 
		view.animateTransforms(transforms);
	}
	
	protected final ItemGroupView<ItemViewType, ItemType> getView() {
		return view;
	}
	
	private final void setAnimationTime(long ms) {
		view.setAnimationTime(ms);
	}

	protected void setItemValues(ItemType[] values) {
		view.setValues(values);
	}
	
	public final ItemTransform[] getCurrentItemTransforms() {
		return view.getCurrentItemTransforms();
	}

	public final void animateItem(int index, ItemTransform start,
			ItemTransform end) {
		view.animateItem(index, start, end);
	}

	public final void setItemValue(int index, ItemType value) {
		view.setItemValue(index, value);
	}

	public final void setItemTransform(int index, ItemTransform transform) {
		view.setItemTransform(index, transform);
	}
	
	public final void hideAll() {
		view.hideAll();
	}

}
