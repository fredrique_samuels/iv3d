/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.ext.groups;

import iv3d.base.events.Event;

import java.util.LinkedList;

public abstract class ItemGroupView <ViewItemType extends ItemView<ItemType>, ItemType extends Item>{
	private LinkedList<ItemView<ItemType>> items;
	private ItemFactory<ViewItemType> factory;

	public ItemGroupView(ItemTransform[] transforms, ItemFactory<ViewItemType> factory) {
		this.factory = factory;
		createViewObjects(factory, transforms);
	}
	
	public  final ItemFactory<ViewItemType> getFactory() {
		return factory;
	}
		
	@SuppressWarnings("unchecked")
	private void createViewObjects(ItemFactory<ViewItemType> factory, ItemTransform[] transforms) {
		if(transforms==null) {
			items = null;
			return;
		}
		
		items = new LinkedList<ItemView<ItemType>>();
		
		int i=0;
		while(i<transforms.length) {
			ViewItemType item = factory.createMenuItem();
			item.setParent((ItemGroupView<ItemView<ItemType>, ItemType>) this);
			item.setTransform(transforms[i]);
			items.add(item);
			i++;
		}
	}
	
	protected abstract void dispatchEvent(ItemView<ItemType> item, Event event);

	public final void setTransforms(ItemTransform[] transforms) {
		setTransforms(0, transforms);
	}

	public final void setTransforms(int offset, ItemTransform[] transforms) {
		int length = transforms.length;
		if(length!=items.size()) {
			return;
		}
		
		for (int i=0;i<length;i++) {
			int index=i+offset;
			ItemTransform transform = index<0||index>=length?ItemTransform.NOT_VISIBLE:transforms[index];
			items.get(i).setTransform(transform);
		}
	}
	
	public final void animateTransforms(ItemTransform[] transforms) {
		animateTransforms(0, transforms);
	}
	
	public final void animateTransforms(int offset, ItemTransform[] transforms) {
		int length = transforms.length;
		if(length!=items.size()) {
			return;
		}
		
		for (int i=0;i<length;i++) {
			int index=i+offset;
			ItemTransform dest = index<0||index>=length?ItemTransform.NOT_VISIBLE:transforms[index];
			items.get(i).setTransitionValue(dest);
		}
	}
	
	public final void setValues(ItemType[] values) {
		for (int i=0;i<values.length;i++) {
			items.get(i).setValue(values[i]);
		}
	}

	public final ItemTransform[] getCurrentItemTransforms() {
		ItemTransform[] transforms = new ItemTransform[items.size()];
		for(int i=0;i<items.size();i++) {
			transforms[i] = items.get(i).getTransform();
		}
		return transforms;
	}

	public final void setAnimationTime(long ms) {
		for(ItemView<ItemType> item : items) {
			item.setAnimationDuration(ms);
		}
	}

	public final void animateItem(int index, ItemTransform start, ItemTransform end) {
		items.get(index).setTransitionValues(start, end);
	}
	
	public final void animateItem(int index, ItemTransform end) {
		ItemView<ItemType> itemView = items.get(index);
		itemView.setTransitionValues(itemView.getTransform(), end);
	}
	
	public final void setItemValue(int index, ItemType value) {
		items.get(index).setValue(value);
	}

	public final void setItemTransform(int index, ItemTransform transform) {
		items.get(index).setTransform(transform);
	}

	public final void hideAll() {
		for(ItemView<ItemType> item : items) {
			item.setTransform(ItemTransform.NOT_VISIBLE);
		}
	}
}
