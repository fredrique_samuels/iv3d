/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.ext.groups;

import iv3d.base.animation.AnimationCalculator;
import iv3d.base.animation.AnimationManager;
import iv3d.base.animation.FloatAnimation;
import iv3d.base.animation.LinearFloatAnimation;
import iv3d.base.events.Event;
import iv3d.base.events.EventListener;
import iv3d.base.exceptions.GraphicsException;
import iv3d.base.math.Vector3;

public abstract class ItemView <ItemType extends Item> implements EventListener {

	private ItemTransform sourceTrans;
	private ItemTransform destTrans;
	private ItemGroupView<ItemView<ItemType>, ItemType> view;
	private ItemTransform transform;
	private final TransitionCalculator calculator;
	private FloatAnimation animation;
	private AnimationUpdater animationUpdater;

	public abstract boolean isVisible();

	public abstract void show();

	public abstract void hide();

	public abstract void applyTransform(ItemTransform transform);
	
	public abstract void setValue(ItemType value);
	
	public abstract void addEventListener(EventListener listener);
	
	protected class AnimationUpdater { 
		public void update(Float value) {
			ItemView.this.update(value);
		}
	}
	
	public ItemView() {
		transform = new ItemTransform();
		calculator = new TransitionCalculator();
		animationUpdater = new AnimationUpdater();
		setAnimationDuration(200);
	}

	private void update(Float progress) {
		if (isVisible()) {
			updateTransition(progress, calculator);
		}
	}

	protected final void setupEventHooks() {
		addEventListener(this);
	}
	
	protected final void setParent(ItemGroupView< ItemView<ItemType>, ItemType > view) {
		this.view = view;
	}
	public final void setTransform(ItemTransform transform) {
		this.transform = transform;
		applyTransform(transform);
	}
	
	public void stopAnimation() {
		AnimationManager.deregisterAnimation(animation);
	}
	
	private void startAnimation() {
		AnimationManager.registerAnimation(animation);
	}

	protected final void setTransitionValues(ItemTransform source,
			ItemTransform dest) {
		stopAnimation();
		this.sourceTrans = source;
		this.destTrans = dest;
		startAnimation();
	} 
	
	protected void setTransitionValue(ItemTransform dest) {
		setTransitionValues(getTransform(), dest);
	} 

	public final ItemTransform getTransform() {
		return transform;
	}

	private final void updateTransition(float progress, TransitionCalculator calculator) {
		AnimationCalculator<Vector3> positionCalculator = calculator.getPositionCalculator();
		
		Vector3 position = positionCalculator.calculate(progress, sourceTrans.getPosition(), destTrans.getPosition());
		Vector3 rotation = calculator.getRotationCalculator().calculate(progress, sourceTrans.getRotation(), destTrans.getRotation());
		Vector3 scale = calculator.getScaleCalculator().calculate(progress, sourceTrans.getScale(), destTrans.getScale());
		float alpha = calculator.getAlphaCalculator().calculate(progress, sourceTrans.getAlpha(), destTrans.getAlpha());
		setTransform(new ItemTransform(position, rotation, scale, alpha));
	}
	
	@Override
	public final void onEventStateChanged(Event event) {
		view.dispatchEvent(this, event);
	}

	public final void setAnimationDuration(long ms) {
		stopAnimation();
		try {
			animation = new LinearFloatAnimation(animationUpdater, "update", Float.valueOf(0f), Float.valueOf(1), ms, 0, false);
		} catch (GraphicsException e) {
			e.printStackTrace();
		}
	}
}
