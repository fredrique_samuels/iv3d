/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.ext.groups;

import iv3d.base.animation.AnimationCalculator;
import iv3d.base.math.Vector3;

public final class ItemTransform {
	
	public static final ItemTransform NOT_VISIBLE = new ItemTransform(new Vector3(),
			new Vector3(),
			new Vector3(),
			0f);
	
	private final Vector3 rot;
	private final Vector3 pos;
	private final Vector3 scale;
	private final float alpha;

	public ItemTransform() {
		this(new Vector3(), new Vector3(), new Vector3(1, 1, 1), 1.0f);
	}
	
	public ItemTransform(Vector3 pos, Vector3 rot, Vector3 scale, float alpha) {
		this.rot = rot;
		this.pos = pos;
		this.scale = scale;
		this.alpha = alpha;
	}

	public ItemTransform(ItemTransform other, float alpha) {
		this.rot = other.rot;
		this.pos = other.pos;
		this.scale = other.scale;
		this.alpha = alpha;
	}

	public final Vector3 getRotation() {
		return rot;
	}

	public final Vector3 getPosition() {
		return pos;
	}

	public final Vector3 getScale() {
		return scale;
	}

	public final float getAlpha() {
		return alpha;
	}
	
	public static ItemTransform animate(ItemTransform from, ItemTransform to, 
			float progress,
			AnimationCalculator<Vector3> posCalc,
			AnimationCalculator<Vector3> rotCalc,
			AnimationCalculator<Vector3> scaleCalc,
			AnimationCalculator<Float> alphaCalc){
		Vector3 pos = posCalc.calculate(progress, from.getPosition(), to.getPosition());
		Vector3 rot = rotCalc.calculate(progress, from.getRotation(), to.getRotation());
		Vector3 scale = scaleCalc.calculate(progress, from.getScale(), to.getScale());
		Float alpha = alphaCalc.calculate(progress, from.getAlpha(), to.getAlpha());
		return new ItemTransform(pos, rot, scale, alpha);
	}
	
	public final ItemTransform offset(ItemTransform item){
		return new ItemTransform(pos.add(item.getPosition()),  
				rot.add(item.getPosition()), 
				Vector3.scale(scale, item.getScale()), 
				alpha*item.getAlpha());
	}
	
	public final String toString() {
		return String.format("ItemTransform p=%s r=%s s=%s a=%.3f", pos.toString(),
				rot.toString(),
				scale.toString(),
				alpha);
	}
}