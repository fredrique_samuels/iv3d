package iv3d.base;

public class Pair <F, S> {
	
	private F first;
	private S second;
	
	public Pair(F first, S second) {
		this.first = first;
		this.second = second;
	}

	public final F getFirst() {
		return first;
	}
	
	public final S getSecond() {
		return second;
	}
}
