/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base;

/**
 * Size of an object. 
 */
final public class Size  {
	private final float width;
	private final float height;
	
	/**
	 * Default constructor.
	 */
	public Size() {
		this(0, 0);
	}

	/**
	 * Value constructor.
	 * @param width The <code>width</code> value.
	 * @param height The <code>height</code> value.
	 */
	public Size(float width, float height) {
		this.width = width;
		this.height = height;
	}

	/**
	 * @return The <code>width</code> value.
	 */
	public final float getWidth() {
		return width;
	}

	/**
	 * @return The <code>height</code> value.
	 */
	public final float getHeight() {
		return height;
	}
	
	@Override
	public boolean equals(Object other) {
		if(other instanceof Size){
			return ((Size)other).width==width && ((Size)other).height==height; 
		}
		return this==other;
	}
	
	/**
	 * Calculate the aspect ratio for this size.
	 * <code>width/height</code>.
	 * 
	 * @return The aspect ratio as <code>width/height</code>.
	 */
	public final float getAspectRatio() {
		return width/height;
	}
	
	/**
	 * Calculate the size that will either fill the 
	 * <tt>destination</tt> space and either have the same height or width or both, but 
	 * has the aspect ratio of the <tt>source</tt>
	 * 
	 * @param source The source size.
	 * @param destination The space to be filled. 
	 * @return The size that will fill the destination.
	 */
	public static Size scaleToFill(final Size source,final Size destination){
		float ar = source.getAspectRatio();
		float dstWidth = destination.getWidth();
		float dstHeight = destination.getHeight();
		
		if(ar*dstHeight>=dstWidth) {
			return new Size(ar*dstHeight, dstHeight);
		} 		
		return new Size(dstWidth, dstWidth/ar);
	}
	
	/**
	 * Calculate the size of the source that will have it fit in the destination space.
	 * 
	 * @param source The source size. 
	 * @param destination The destination space.
	 * @return
	 */
	public static Size scaleToFit(final Size source,final Size destination) {
		float ar = source.getAspectRatio();
		float dstWidth = destination.getWidth();
		float dstHeight = destination.getHeight();
		
		if(dstWidth/ar<=dstHeight) {
			return new Size(dstWidth, dstWidth/ar);
		} 	
		
		return new Size(ar*dstHeight, dstHeight);
	}
	
	/**
	 * @return The size normalized to 1.0.
	 */
	public final Size normalize() {
		return scaleToFit(this, new Size(1, 1));
	}
}
