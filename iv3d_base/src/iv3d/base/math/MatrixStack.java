/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.math;

import java.util.Stack;

public class MatrixStack{
	private Matrix44 matrix;
	private Stack<Matrix44> stack;

	public MatrixStack() {
		matrix = new Matrix44();
		stack = new Stack<Matrix44>();
	}

	public final Matrix44 get() {
		return new Matrix44(matrix);
	}

	public final void multiMatrixf(Matrix44 m) {
		matrix.multiply(m);
	}

	public final void multiMatrixf(final float[] m) {
		matrix.multiply(new Matrix44(m));
	}

	public final void pushMatrix() {
		stack.add(new Matrix44(matrix));
	}

	public final void popMatrix() {
		matrix = stack.pop();
	}

	public final void translate(float x, float y, float z) {
		matrix.translate(new Vector3(x, y, z));
	}

	public final void translate(Vector3 v) {
		matrix.translate(v);
	}

	public final void translate(float x, float y) {
		translate(x, y, 0);
	}

	public final void translate(Vector2 v) {
		translate(v.getX(), v.getY());
	}

	public final void loadIdentity() {
		matrix.identity();
	}

	public final void setLookAtMatrix(float eyex, float eyey, float eyez,
			float centerx, float centery, float centerz, float upx, float upy,
			float upz) {
		matrix.makeLookAt(eyex, eyey, eyez, centerx, centery, centerz, upx,
				upy, upz);
	}

	public final void setOrthoView(float left, float right, float bottom,
			float top, float clipnear, float clipfar) {
		matrix.makeProjectionOrtho(left, right, bottom, top, clipnear, clipfar);
	}

	public final void setPerspectiveView(float arc, float aspect,
			float clipnear, float clipfar) {
		matrix.makeProjectionPersp(arc, aspect, clipnear, clipfar);
	}

	public final void loadMatrix(Matrix44 m) {
		matrix = new Matrix44(m);
	}

	public final void loadMatrix(float[] m) {
		matrix = new Matrix44(m);
	}

	public final void rotateEuler(Vector3 v) {
		matrix.rotate(v);
	}
	
	public final void rotateEuler(float x, float y, float z) {
		rotateEuler(new Vector3(x, y, z));
	}
	
	public final void rotateAxis(Vector4 v){
		matrix.rotate(v);
	}
	
	public final void rotateAxis(float x, float y, float z, float w){
		matrix.rotate(new Vector4(x, y, z, w));
	}
	
	public final void scale(Vector3 v) {
		matrix.scale(v);
	}
	
	public final void scale(float x, float y, float z) {
		matrix.scale(new Vector3(x, y, z));
	}
}
