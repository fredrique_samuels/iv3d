/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.math;

public final class BoundingBox {
	float xMin;
	float xMax;
	float yMin;
	float yMax;
	float zMin;
	float zMax;
	
	public BoundingBox(float xMin, float xMax, float yMin, float yMax,
			float zMin, float zMax) {
		this.xMin = xMin;
		this.xMax = xMax;
		this.yMin = yMin;
		this.yMax = yMax;
		this.zMin = zMin;
		this.zMax = zMax;
	}
	
	public BoundingBox () { 
		this(0, 0, 0, 0, 0, 0);
	}

	public final float getXMin() {
		return xMin;
	}

	public final float getXMax() {
		return xMax;
	}

	public final float getYMin() {
		return yMin;
	}

	public final float getYMax() {
		return yMax;
	}

	public final float getZMin() {
		return zMin;
	}

	public final float getZMax() {
		return zMax;
	}
	
	public final Vector3[] getVertices() {
		return new Vector3[]{new Vector3(xMax, yMax, zMax),
							new Vector3(xMax, yMin, zMax),
							new Vector3(xMin, yMin, zMax),
							new Vector3(xMin, yMax, zMax),
							new Vector3(xMax, yMax, zMin),
							new Vector3(xMax, yMin, zMin),
							new Vector3(xMin, yMin, zMin),
							new Vector3(xMin, yMax, zMin)};
	}
	
	public Vector3 getCenter() {
		return new Vector3(xMin+(xMax-xMin)*.5f,
				yMin+(yMax-yMin)*.5f,
				zMin+(zMax-zMin)*.5f);
	}

	public static BoundingBox fromVerts3(float[] verts) {
		if(verts.length==0) {
			return new BoundingBox();
		}
		
		if(verts.length%3!=0) {
			return new BoundingBox();
		}
		
		float xMin = verts[0];
		float xMax = verts[0];
		float yMin = verts[1];
		float yMax = verts[1];
		float zMin = verts[2];
		float zMax = verts[2];
		
		for (int i=0;i<verts.length;i+=3) { 
			float x = verts[i];
			if(x<xMin) {
				xMin = x;
			} else if (x>xMax) {
				xMax = x;
			}
			
			float y = verts[i+1];
			if(y<yMin) {
				yMin = y;
			} else if (y>yMax) {
				yMax = y;
			}
			
			float z = verts[i+2];
			if(z<zMin) {
				zMin = z;
			} else if (z>zMax) {
				zMax = z;
			}
		}
		return new BoundingBox(xMin, xMax, yMin, yMax, zMin, zMax);
	}	
	
	@Override
	public String toString() {
		return String.format("<%s xMin=%.3f xMax=%.3f yMin=%.3f yMax=%.3f zMin=%.3f zMax=%.3f  >", BoundingBox.class.toString(),
				xMin, xMax, yMin, yMax, zMin, zMax);
	}
}
