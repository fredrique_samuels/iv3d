/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.math;

import iv3d.base.Point;
import iv3d.base.Rectangle;


public class MathUtils {
	private MathUtils(){}
	
	public static boolean isPointOnScreen(Vector3 point3d, Matrix44 modelMatrix,
			Matrix44 projMatrix, Rectangle viewport) {
		
		Matrix44 mvp = projMatrix.multiply(modelMatrix);
		return isPointOnScreen(point3d, mvp, viewport);
	}

	private static boolean isPointOnScreen(Vector3 point3d, Matrix44 mvp,
			Rectangle viewport) {
		Vector3 project = project(point3d, mvp, viewport);
		
		if(project.getZ() < 1.0) {
			Point point2d = new Point((int)point3d.getX(),(int)project.getY());
			return viewport.containsPoint(point2d);
		}
		return false;
	}
	
	public static boolean isBoxOnScreen(BoundingBox box, Matrix44 modelMatrix,
			Matrix44 projMatrix, Rectangle viewport) {
		Matrix44 mvp = projMatrix.multiply(modelMatrix);
		
		boolean onScreen = false;
		Vector3[] vertices = box.getVertices();
		
		for (Vector3 vert : vertices) {
			if(isPointOnScreen(vert, mvp, viewport)) {
				onScreen = true;
				break;
			}
			onScreen = isPointOnScreen(box.getCenter(), mvp, viewport);
		}
		
		return onScreen;
	}
	
	public static Vector3 unProject(float winx, float winy, float winz, Matrix44 modelMatrix,
			Matrix44 projMatrix, Rectangle viewport)
	{
	    Matrix44 mvpInv = Matrix44.multiply(projMatrix, modelMatrix).getInverse();

	    /* Map x and y from window coordinates */
	    float normX = (winx - viewport.getX()) / viewport.getWidth();
	    float normY = (winy - viewport.getY()) / viewport.getHeight();

	    /* Map to range -1 to 1 */
	    Vector4 windowCoord = new Vector4(normX * 2 - 1,
	    		normY * 2 - 1,
	    		winz * 2 - 1,
	    		1f);

	    Vector4 out = mvpInv.multiply(windowCoord);
	    if (out.getW() == 0.0) {
	        return new Vector3();
	    }

	    out.devide(out.getW());
	    return new Vector3(out);
	}
	
	public static Vector3 project(Vector3 point, Matrix44 modelMatrix,
			Matrix44 projMatrix, Rectangle viewport)
	{
		Matrix44 mvp = projMatrix.multiply(modelMatrix);
		return project(point, mvp, viewport);
	}

	private static Vector3 project(Vector3 point, Matrix44 mvp, Rectangle viewport) {
		Vector4 in = mvp.multiply(new Vector4(point)); 
		
		in = in.multiply(1.0f/in.getW());
		
		float x = (float) (viewport.getX() + (1 + in.getX()) * viewport.getWidth() / 2);
	    float y = (float) (viewport.getY() + (1 + in.getY()) * viewport.getHeight() / 2);
	    float z = (1 + in.getZ()) / 2;
	    return new Vector3(x, y, z);
	}
	
	
}
