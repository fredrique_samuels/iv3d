/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.math;

// TODO Vec3(Quat)
//TODO mult(Matrix4)

final public class Vector3 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final float x;
	private final float y;
	private final float z;

	public Vector3() {
		this(0, 0, 0);
	}

	public Vector3(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vector3(Vector3 other) {
		this(other.getX(), other.getY(), other.getZ());
	}

	public Vector3(Vector4 other) {
		this(other.getX(), other.getY(), other.getZ());
	}

	public Vector3(float[] in) {
		this(in[0], in[1], in[2]);
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public float getZ() {
		return z;
	}
	
	public Vector3 identity() {
		return new Vector3();
	}

	@Override
	public boolean equals(Object obj) {
		Vector3 other = (Vector3) obj;
		return Float.compare(x, other.getX()) == 0
				&& Double.compare(y, other.getY()) == 0
				&& Double.compare(z, other.getZ()) == 0;
	}

	public Vector3 add(Vector3 other) {
		return add(this, other);
	}

	public Vector3 subtract(Vector3 other) {
		return subtract(this, other);
	}
	
	public static Vector3 add(Vector3 v0, Vector3 v1) {
		return new Vector3(v0.getX() + v1.getX(), v0.getY() + v1.getY(), v0.getZ() + v1.getZ());
	}

	public static Vector3 subtract(Vector3 v0, Vector3 v1) {
		return new Vector3(v0.getX() - v1.getX(), v0.getY() - v1.getY(), v0.getZ() - v1.getZ());
	}

	public Vector3 multiply(float d) {
		return new Vector3(x*d, y*d, z*d);
	}
	
	public static Vector3 scale(Vector3 v0, Vector3 v1) {
		return  new Vector3(v0.getX()*v1.getX(),
				v0.getY()*v1.getY(),
				v0.getZ()*v1.getZ());
	}

	public float length() {
		return (float) Math.sqrt(x * x + y * y + z * z);
	}

	public Vector3 normalize() {
		float len = length();
		if (len != 0) {
			float fac = (float) (1.0 / len);
			return multiply(fac);
		}
		return identity();
	}

	public float dot(Vector3 other) {
		return x * other.x + y * other.y + z * other.z;
	}

	public Vector3 cross(Vector3 other) {
		return new Vector3(y * other.z - z * other.y,
				z * other.x - x * other.z, x * other.y - y * other.x);
	}
	
	public static float dot(Vector3 v0, Vector3 other) {
		return v0.x * other.x + v0.y * other.y + v0.z * other.z;
	}

	public static Vector3 cross(Vector3 v0, Vector3 other) {
		return new Vector3(v0.y * other.z - v0.z * other.y,
				v0.z * other.x - v0.x * other.z, v0.x * other.y - v0.y * other.x);
	}

	public float distance(Vector3 other) {
		return subtract(other).length();
	}
		
	@Override
	public String toString() {
		return String.format("<%s x=%.3f y=%.3f z=%.3f>", Vector3.class.toString(),
				x, y, z);
	}
}
