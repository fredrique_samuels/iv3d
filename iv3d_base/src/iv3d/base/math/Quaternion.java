/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.math;

public class Quaternion {
	private float x;
	private float y;
	private float z;
	private float w;
	static int[] s_iNext = new int[] { 1, 2, 0 };

	public Quaternion() {
		this(0, 0, 0, 1);
	}

	public Quaternion(float x, float y, float z, float w) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	public Quaternion(final Quaternion other) {
		this(other.x, other.y, other.z, other.w);
	}

	public Quaternion(Vector3 other) {
		set(other);
	}

	public Quaternion(Vector4 other) {
		set(other);
	}

	public Quaternion set(float x, float y, float z, float w) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
		return this;
	}

	private Quaternion set(Quaternion other) {
		return set(other.x, other.y, other.z, other.w);
	}

	public Quaternion set(Vector3 other) {
		return makeEuler(other);
	}

	public Quaternion set(Vector4 other) {
		return makeAxisAngle(other);
	}

	public Quaternion add(Quaternion other) {
		x += other.getX();
		y += other.getY();
		z += other.getZ();
		w += other.getW();
		return this;
	}

	public Quaternion subtract(Quaternion other) {
		x -= other.getX();
		y -= other.getY();
		z -= other.getZ();
		w -= other.getW();
		return this;
	}

	public Quaternion multiply(Quaternion other) {
		float w = -this.x * other.x - this.y * other.y - this.z * other.z
				+ this.w * other.w;
		float x = this.x * other.w + this.y * other.z - this.z * other.y
				+ this.w * other.x;
		float y = -this.x * other.z + this.y * other.w + this.z * other.x
				+ this.w * other.y;
		float z = this.x * other.y - this.y * other.x + this.z * other.w
				+ this.w * other.z;
		return set(x, y, z, w);
	}

	public Quaternion divide(Quaternion other) {
		float w = (this.w * other.w + this.x * other.x + this.y * other.y + this.z
				* other.z)
				/ (other.w * other.w + other.x * other.x + other.y * other.y + other.z
						* other.z);
		float x = (this.x * other.w - this.w * other.x - this.z * other.y + this.y
				* other.z)
				/ (other.w * other.w + other.x * other.x + other.y * other.y + other.z
						* other.z);
		float y = (this.y * other.w + this.z * other.x - this.w * other.y - this.x
				* other.z)
				/ (other.w * other.w + other.x * other.x + other.y * other.y + other.z
						* other.z);
		float z = (this.z * other.w - this.y * other.x + this.x * other.y - this.w
				* other.z)
				/ (other.w * other.w + other.x * other.x + other.y * other.y + other.z
						* other.z);
		return set(x, y, z, w);
	}

	public Quaternion identity() {
		return set(0, 0, 0, 1);
	}

	public Quaternion multiply(float d) {
		return set(x * d, y * d, z * d, w * d);
	}

	public Quaternion divide(float d) {
		return set(x / d, y / d, z / d, w / d);
	}

	public Quaternion makeEuler(Vector3 rot) {
		float fSinPitch = (float) Math.sin(rot.getX() * Math.PI / 360.0f);
		float fCosPitch = (float) Math.cos(rot.getX() * Math.PI / 360.0f);
		float fSinYaw = (float) Math.sin(rot.getY() * Math.PI / 360.0f);
		float fCosYaw = (float) Math.cos(rot.getY() * Math.PI / 360.0f);
		float fSinRoll = (float) Math.sin(rot.getZ() * Math.PI / 360.0f);
		float fCosRoll = (float) Math.cos(rot.getZ() * Math.PI / 360.0f);
		float fCosPitchCosYaw = (fCosPitch * fCosYaw);
		float fSinPitchSinYaw = (fSinPitch * fSinYaw);
		x = fSinRoll * fCosPitchCosYaw - fCosRoll * fSinPitchSinYaw;
		y = fCosRoll * fSinPitch * fCosYaw + fSinRoll * fCosPitch * fSinYaw;
		z = fCosRoll * fCosPitch * fSinYaw - fSinRoll * fSinPitch * fCosYaw;
		w = fCosRoll * fCosPitchCosYaw + fSinRoll * fSinPitchSinYaw;
		return normalize();
	}

	public Quaternion makeEuler(Vector4 rot) {
		return makeEuler(new Vector3(rot.getX(), rot.getY(), rot.getZ()));
	}

	public Quaternion makeAxisAngle(Vector3 axis, float ang) {
		float halfang = (float) (ang * Math.PI / 360.0f);
		float fsin = (float) Math.sin(halfang);
		w = (float) Math.cos(halfang);
		x = fsin * axis.getX();
		y = fsin * axis.getY();
		z = fsin * axis.getZ();

		normalize();
		return this;
	}

	public Quaternion makeAxisAngle(Vector4 axis) {
		return makeAxisAngle(new Vector3(axis), axis.getW());
	}

	public Quaternion makeLookAt(Vector3 dir, Vector3 up) {
		// Setup basis vectors describing the rotation given the input vector
		dir.normalize();
		Vector3 right = up.cross(dir); // The perpendicular vector to Up and
		// Direction
		right.normalize();
		up = dir.cross(right); // The actual up vector given the direction and
		// the right vector
		up.normalize();

		/*
		 * w = sqrt(fmax(0.0, 1.0+right.x+up.y+dir.z))/2.0; x = sqrt(fmax(0.0,
		 * 1.0+right.x-up.y-dir.z))/2.0; y = sqrt(fmax(0.0,
		 * 1.0-right.x+up.y-dir.z))/2.0; z = sqrt(fmax(0.0,
		 * 1.0-right.x-up.y+dir.z))/2.0; x = copysign(x, up.z-dir.y); y =
		 * copysign(y, dir.x-right.z); z = copysign(z, right.y-up.x);
		 */

		// Algorithm in Ken Shoemake's article in 1987 SIGGRAPH course notes
		// article "Quaternion Calculus and Fast Animation".
		// Implementation taken from Ogre3D.

		float[][] kRot = new float[3][3];
		kRot[0][0] = right.getX();
		kRot[1][0] = right.getY();
		kRot[2][0] = right.getZ();
		kRot[0][1] = up.getX();
		kRot[1][1] = up.getY();
		kRot[2][1] = up.getZ();
		kRot[0][2] = dir.getX();
		kRot[1][2] = dir.getY();
		kRot[2][2] = dir.getZ();

		float fTrace = kRot[0][0] + kRot[1][1] + kRot[2][2];
		float fRoot;
		if (fTrace > 0.0) {
			// |w| > 1/2, may as well choose w > 1/2
			fRoot = (float) Math.sqrt(fTrace + 1.0f); // 2w
			w = 0.5f * fRoot;
			fRoot = 0.5f / fRoot; // 1/(4w)
			x = (kRot[2][1] - kRot[1][2]) * fRoot;
			y = (kRot[0][2] - kRot[2][0]) * fRoot;
			z = (kRot[1][0] - kRot[0][1]) * fRoot;
		} else {
			// |w| <= 1/2
			int i = 0;
			if (kRot[1][1] > kRot[0][0])
				i = 1;
			if (kRot[2][2] > kRot[i][i])
				i = 2;
			int j = s_iNext[i];
			int k = s_iNext[j];

			fRoot = (float) Math.sqrt(kRot[i][i] - kRot[j][j] - kRot[k][k]
					+ 1.0f);
			// float* apkQuat[3] = { &x, &y, &z };
			float[] apkQuat = new float[3];
			apkQuat[i] = 0.5f * fRoot;
			fRoot = 0.5f / fRoot;
			w = (kRot[k][j] - kRot[j][k]) * fRoot;
			apkQuat[j] = (kRot[j][i] + kRot[i][j]) * fRoot;
			apkQuat[k] = (kRot[k][i] + kRot[i][k]) * fRoot;

			x = apkQuat[0];
			y = apkQuat[1];
			z = apkQuat[2];
		}

		/*
		 * float tx = up.z-dir.y; float ty = dir.x-right.z; float tz =
		 * right.y-up.x; sgLog("%f %f %f, %f %f %f", tx, ty, tz, x, y, z);
		 */

		normalize();
		return this;
	}

	public Quaternion makeLerpS(final Quaternion quat1, final Quaternion quat2,
			float fac) {
		Quaternion q1 = new Quaternion(quat1);
		Quaternion q2 = new Quaternion(quat2);

		float angle = q1.dot(q2);
		if (angle < 0.0f) {
			q1.multiply(-1.0f);
			angle *= -1.0f;
		}

		float scale;
		float invscale;

		if ((angle + 1.0f) > 0.05f) {
			if ((1.0f - angle) >= 0.05f) // spherical interpolation
			{
				float theta = (float) Math.acos(angle);
				float invsintheta = (float) (1.0f / Math.sin(theta));
				scale = (float) (Math.sin(theta * (1.0f - fac)) * invsintheta);
				invscale = (float) (Math.sin(theta * fac) * invsintheta);
			} else // linear interploation
			{
				scale = 1.0f - fac;
				invscale = fac;
			}
		} else {
			q2.set(-q1.y, q1.x, -q1.w, q1.z);
			scale = (float) Math.sin(Math.PI * (0.5f - fac));
			invscale = (float) Math.sin(Math.PI * fac);
		}

		return set((q1.multiply(scale)).add((q2.multiply(invscale))));
	}

	public Quaternion makeLerpN(Quaternion quat1, Quaternion quat2, float fac) {
		Quaternion q1 = new Quaternion(quat1);
		Quaternion q2 = new Quaternion(quat2);
		q2.multiply(fac);
		q1.multiply(1.0f - fac);
		return set(q2.add(q1));
	}

	public Quaternion makeLerpS(Quaternion other, float fac) {
		return makeLerpS(this, other, fac);
	}

	public Quaternion makeLerpN(Quaternion other, float fac) {
		return makeLerpN(this, other, fac);
	}

	public Quaternion lerpS(Quaternion other, float fac) {
		Quaternion res = new Quaternion(this);
		return res.makeLerpS(other, fac);
	}

	public Quaternion lerpN(Quaternion other, float fac) {
		Quaternion res = new Quaternion(this);
		return res.makeLerpN(other, fac);
	}

	public Quaternion normalize() {
		float len = length();
		if (len != 0) {
			float fac = 1 / len;
			w *= fac;
			x *= fac;
			y *= fac;
			z *= fac;
		}
		return this;
	}

	public Quaternion conjugate() {
		x = -x;
		y = -y;
		z = -z;
		return this;
	}

	public float length() {
		return (float) Math.sqrt(x * x + y * y + z * z + w * w);
	}

	public float dot(Quaternion other) {
		return x * other.x + y * other.y + z * other.z + w * other.w;
	}

	public Vector3 rotate(Vector3 vec) {
		return getMatrix().transform(vec);
	}

	public Vector4 rotate(Vector4 vec) {
		return getMatrix().transform(vec);
	}

	public Matrix44 getMatrix() {
		float[] mat = new float[16];
		Matrix44.identity(mat);

		mat[0] = 1.0f - 2.0f * (y * y + z * z);
		mat[4] = 2.0f * (x * y - z * w);
		mat[8] = 2.0f * (x * z + y * w);
		mat[1] = 2.0f * (x * y + z * w);
		mat[5] = 1.0f - 2.0f * (x * x + z * z);
		mat[9] = 2.0f * (y * z - x * w);
		mat[2] = 2.0f * (x * z - y * w);
		mat[6] = 2.0f * (y * z + x * w);
		mat[10] = 1.0f - 2.0f * (x * x + y * y);

		return new Matrix44(mat);
	}

	public Vector3 getEuler() {

		float sqx = x * x;
		float sqy = y * y;
		float sqz = z * z;

		float clamped = 2.0f * (x * y + z * w);
		if (clamped > 0.99999f) {
			float x = (float) (2.0f * Math.atan2(this.x, this.w) * 180 / Math.PI);
			float y = 90.0f;
			float z = 0.0f;
			return new Vector3(x, y, z);
		}
		if (clamped < -0.99999f) {
			float x = (float) (-2.0f * Math.atan2(this.x, this.w) * 180 / Math.PI);
			float y = -90.0f;
			float z = 0.0f;
			return new Vector3(x, y, z);
		}

		float con = (float) (180.0f / Math.PI);
		float x = (float) ((Math.atan2(
				2.0 * (this.y * this.w - this.x * this.z),
				1.0 - 2 * (sqy + sqz))) * con);
		float y = (float) (Math.asin(clamped) * con);
		float z = (float) ((Math.atan2(
				2.0 * (this.x * this.w - this.y * this.z),
				1.0 - 2 * (sqx + sqz))) * con);
		return new Vector3(x, y, z);
	}

	public Vector4 getAxisAngle() {
		float scale = (float) Math.sqrt(x * x + y * y + z * z);
		if (scale == 0.0f || w > 1.0f || w < -1.0f) {
			return new Vector4(0, 0, 1, 0);
		}

		float invscale = 1.0f / scale;

		return new Vector4((float) ((360 / Math.PI) * Math.acos(w)), x
				* invscale, y * invscale, z * invscale);
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public float getZ() {
		return z;
	}

	public float getW() {
		return w;
	}

	@Override
	public String toString() {
		return String.format("<%s x=%.3f y=%.3f z=%.3f w=%.3f >", Quaternion.class.toString(),
				x, y, z, w);
	}
}
