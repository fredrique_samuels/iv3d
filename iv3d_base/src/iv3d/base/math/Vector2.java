/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.math;

final public class Vector2 {

	private float y;
	private float x;

	public Vector2(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public Vector2(Vector2 other) {
		this(other.getX(), other.getY());
	}

	public Vector2() {
		this(0, 0);
	}

	public Vector2 identity() {
		x = 0;
		y = 0;
		return this;
	}
	
	public Vector2 set(float x, float y) {
		this.x = x;
		this.y = y;
		return this;
	}
	
	public Vector2 set(Vector2 other) {
		this.x = other.getX();
		this.y = other.getY();
		return this;
	}
	
	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public Vector2 add(Vector2 other) {
		x = x + other.getX();
		y = y + other.getY();
		return this;
	}

	public Vector2 subtract(Vector2 other) {
		x = x - other.getX();
		y = y - other.getY();
		return this;
	}

	@Override
	public boolean equals(Object obj) {
		Vector2 other = (Vector2) obj;
		return Float.compare(x, other.getX()) == 0
				&& Float.compare(y, other.getY()) == 0;
	}

	public Vector2 multiply(float d) {
		x = x * d;
		y = y * d;
		return this;
	}

	public float length() {
		return (float) Math.sqrt(x * x + y * y);
	}

	public Vector2 normalize() {
		float len = length();
		if (len != 0) {
			float fac = (float) (1.0 / len);
			x *= fac;
			y *= fac;
			return this;
		}
		x = 0;
		y = 0;
		return this;
	}

	public float dot(Vector2 other) {
		return x * other.x + y * other.y;
	}

	public float distance(Vector2 other) {
		return subtract(other).length();
	}
	
	@Override
	public String toString() {
		return String.format("<%s x=%.3f y=%.3f >", Vector2.class.toString(),
				x, y);
	}

}
