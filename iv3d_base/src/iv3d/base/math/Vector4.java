/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.math;


// TODO Vector4(Quat)
final public class Vector4 {
	private float x;
	private float y;
	private float z;
	private float w;

	public Vector4() {
		this(0, 0, 0, 0);
	}

	public Vector4(float x, float y, float z, float w) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	public Vector4(Vector4 other) {
		this(other.getX(), other.getY(), other.getZ(), other.getW());
	}

	public Vector4(Vector3 other) {
		this(other.getX(), other.getY(), other.getZ(), 1);
	}

	public Vector4(Vector3 other, float w) {
		this(other.getX(), other.getY(), other.getZ(), w);
	}
	
	public Vector4(float[] in) {
		this(in[0], in[1], in[2], in[3]);
	}

	public Vector4 identity() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
		this.w = 0;
		return this;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public float getZ() {
		return z;
	}

	public float getW() {
		return w;
	}
	
	@Override
	public boolean equals(Object obj) {
		Vector4 other = (Vector4) obj;
		return Float.compare(x, other.getX()) == 0
				&& Float.compare(y, other.getY()) == 0
				&& Float.compare(z, other.getZ()) == 0
				&& Float.compare(w, other.getW()) == 0;
	}

	public Vector4 add(Vector4 other) {
		x+=other.getX();
		y+=other.getY();
		z+=other.getZ();
		w+=other.getW();
		return this;
	}

	public Vector4 subtract(Vector4 other) {
		x-=other.getX();
		y-=other.getY();
		z-=other.getZ();
		w-=other.getW();
		return this;
	}

	public Vector4 multiply(float d) {
		x*=d;
		y*=d;
		z*=d;
		w*=d;
		return this;
	}
	
	public Vector4 devide(float d) {
		x/=d;
		y/=d;
		z/=d;
		w/=d;
		return this;
	}

	public float length() {
		return (float) Math.sqrt(x*x+y*y+z*z+w*w);
	}

	public Vector4 normalize() {
		float len = length();
		if (len != 0) {
			float fac = (float) (1.0 / len);
			return multiply(fac);
		}
		identity();
		return this;
	}

	public float dot(Vector4 other) {
		return x*other.x+y*other.y+z*other.z+w*other.w;
	}

	public Vector4 cross(Vector4 other) {
		return new Vector4(y * other.z - z * other.y,
				z * other.x - x * other.z, x * other.y - y * other.x, 0.0f);
	}

	public float distance(Vector4 other) {
		return subtract(other).length();
	}
	
	@Override
	public String toString() {
		return String.format("<%s x=%.3f y=%.3f z=%.3f w=%.3f >", Vector4.class.toString(),
				x, y, z, w);
	}
	
	public float[] toArray() {
		return new float[]{x, y, z, w};
	}

}
