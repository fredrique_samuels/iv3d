/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.math;

import java.util.Arrays;

final public class Matrix44 {
	public static final Matrix44 IDENTITY = new Matrix44();
	private float[] mat;

	public Matrix44() {
		mat = new float[16];
		identity();
	}

	public Matrix44(final float[] mat) {
		this.mat = new float[] { mat[0], mat[1], mat[2], mat[3], mat[4], mat[5], mat[6],
				mat[7], mat[8], mat[9], mat[10], mat[11], mat[12], mat[13], mat[14], mat[15] };
	}

	public Matrix44(final Matrix44 other) {
		this.mat = new float[] { other.mat[0], other.mat[1], other.mat[2],
				other.mat[3], other.mat[4], other.mat[5], other.mat[6],
				other.mat[7], other.mat[8], other.mat[9], other.mat[10],
				other.mat[11], other.mat[12], other.mat[13], other.mat[14],
				other.mat[15] };
	}

	public Matrix44 multiply(final Matrix44 other) {		
		return set(multiply(this, other));
	}
	
	public Matrix44 multiply(final float[] other) {		
		return set(multiply(this, new Matrix44(other)));
	}
	
	public static Matrix44 multiply(Matrix44 mat1, Matrix44 mat2) {
		Matrix44 matrix44 = new Matrix44();

		matrix44.mat[0] = mat1.mat[0] * mat2.mat[0] + mat1.mat[4]
				* mat2.mat[1] + mat1.mat[8] * mat2.mat[2] + mat1.mat[12]
				* mat2.mat[3];
		matrix44.mat[1] = mat1.mat[1] * mat2.mat[0] + mat1.mat[5]
				* mat2.mat[1] + mat1.mat[9] * mat2.mat[2] + mat1.mat[13]
				* mat2.mat[3];
		matrix44.mat[2] = mat1.mat[2] * mat2.mat[0] + mat1.mat[6]
				* mat2.mat[1] + mat1.mat[10] * mat2.mat[2] + mat1.mat[14]
				* mat2.mat[3];
		matrix44.mat[3] = mat1.mat[3] * mat2.mat[0] + mat1.mat[7]
				* mat2.mat[1] + mat1.mat[11] * mat2.mat[2] + mat1.mat[15]
				* mat2.mat[3];

		matrix44.mat[4] = mat1.mat[0] * mat2.mat[4] + mat1.mat[4]
				* mat2.mat[5] + mat1.mat[8] * mat2.mat[6] + mat1.mat[12]
				* mat2.mat[7];
		matrix44.mat[5] = mat1.mat[1] * mat2.mat[4] + mat1.mat[5]
				* mat2.mat[5] + mat1.mat[9] * mat2.mat[6] + mat1.mat[13]
				* mat2.mat[7];
		matrix44.mat[6] = mat1.mat[2] * mat2.mat[4] + mat1.mat[6]
				* mat2.mat[5] + mat1.mat[10] * mat2.mat[6] + mat1.mat[14]
				* mat2.mat[7];
		matrix44.mat[7] = mat1.mat[3] * mat2.mat[4] + mat1.mat[7]
				* mat2.mat[5] + mat1.mat[11] * mat2.mat[6] + mat1.mat[15]
				* mat2.mat[7];

		matrix44.mat[8] = mat1.mat[0] * mat2.mat[8] + mat1.mat[4]
				* mat2.mat[9] + mat1.mat[8] * mat2.mat[10] + mat1.mat[12]
				* mat2.mat[11];
		matrix44.mat[9] = mat1.mat[1] * mat2.mat[8] + mat1.mat[5]
				* mat2.mat[9] + mat1.mat[9] * mat2.mat[10] + mat1.mat[13]
				* mat2.mat[11];
		matrix44.mat[10] = mat1.mat[2] * mat2.mat[8] + mat1.mat[6]
				* mat2.mat[9] + mat1.mat[10] * mat2.mat[10] + mat1.mat[14]
				* mat2.mat[11];
		matrix44.mat[11] = mat1.mat[3] * mat2.mat[8] + mat1.mat[7]
				* mat2.mat[9] + mat1.mat[11] * mat2.mat[10] + mat1.mat[15]
				* mat2.mat[11];

		matrix44.mat[12] = mat1.mat[0] * mat2.mat[12] + mat1.mat[4]
				* mat2.mat[13] + mat1.mat[8] * mat2.mat[14] + mat1.mat[12]
				* mat2.mat[15];
		matrix44.mat[13] = mat1.mat[1] * mat2.mat[12] + mat1.mat[5]
				* mat2.mat[13] + mat1.mat[9] * mat2.mat[14] + mat1.mat[13]
				* mat2.mat[15];
		matrix44.mat[14] = mat1.mat[2] * mat2.mat[12] + mat1.mat[6]
				* mat2.mat[13] + mat1.mat[10] * mat2.mat[14] + mat1.mat[14]
				* mat2.mat[15];
		matrix44.mat[15] = mat1.mat[3] * mat2.mat[12] + mat1.mat[7]
				* mat2.mat[13] + mat1.mat[11] * mat2.mat[14] + mat1.mat[15]
				* mat2.mat[15];

		return matrix44;
	}

	public Matrix44 set(float[] m) {
		this.mat = new float[] { m[0], m[1], m[2], m[3], m[4], m[5], m[6],
				m[7], m[8], m[9], m[10], m[11], m[12], m[13], m[14], m[15] };
		return this;
	}

	public Matrix44 set(Matrix44 other) {
		return set(other.mat);
	}

	public Vector4 multiply(Vector4 other) {
		float x = mat[0] * other.getX() + mat[4] * other.getY() + mat[8]
				* other.getZ() + mat[12] * other.getW();
		float y = mat[1] * other.getX() + mat[5] * other.getY() + mat[9]
				* other.getZ() + mat[13] * other.getW();
		float z = mat[2] * other.getX() + mat[6] * other.getY() + mat[10]
				* other.getZ() + mat[14] * other.getW();
		float w = mat[3] * other.getX() + mat[7] * other.getY() + mat[11]
				* other.getZ() + mat[15] * other.getW();
		return new Vector4(x, y, z, w);
	}

	public Vector3 multiply(Vector3 other) {
		float x = mat[0] * other.getX() + mat[4] * other.getY() + mat[8]
				* other.getZ() + mat[12];
		float y = mat[1] * other.getX() + mat[5] * other.getY() + mat[9]
				* other.getZ() + mat[13];
		float z = mat[2] * other.getX() + mat[6] * other.getY() + mat[10]
				* other.getZ() + mat[14];
		return new Vector3(x, y, z);
	}

	public Matrix44 getInverse() {
		Matrix44 result = new Matrix44();
		invert(mat, result.mat);
		return result;
	}

	public Matrix44 invert() {
		float inv[] = new float[16];
		invert(mat, inv);
		for (int i = 0; i < 16; ++i)
			mat[i] = inv[i];
		return this;
	}

	public Matrix44 identity() {
		for (int i = 0; i < 16; ++i)
			mat[i] = 0.0f;
		mat[0] = 1.0f;
		mat[5] = 1.0f;
		mat[10] = 1.0f;
		mat[15] = 1.0f;
		return this;
	}

	public static void identity(float[] mat) {
		for (int i = 0; i < 16; ++i)
			mat[i] = 0.0f;
		mat[0] = 1.0f;
		mat[5] = 1.0f;
		mat[10] = 1.0f;
		mat[15] = 1.0f;
	}

	static private boolean invert(float[] m, float[] invOut) {
		float[] inv = new float[16];
		float det;
		int i;

		inv[0] = m[5] * m[10] * m[15] - m[5] * m[11] * m[14] - m[9] * m[6]
				* m[15] + m[9] * m[7] * m[14] + m[13] * m[6] * m[11] - m[13]
				* m[7] * m[10];
		inv[4] = -m[4] * m[10] * m[15] + m[4] * m[11] * m[14] + m[8] * m[6]
				* m[15] - m[8] * m[7] * m[14] - m[12] * m[6] * m[11] + m[12]
				* m[7] * m[10];
		inv[8] = m[4] * m[9] * m[15] - m[4] * m[11] * m[13] - m[8] * m[5]
				* m[15] + m[8] * m[7] * m[13] + m[12] * m[5] * m[11] - m[12]
				* m[7] * m[9];
		inv[12] = -m[4] * m[9] * m[14] + m[4] * m[10] * m[13] + m[8] * m[5]
				* m[14] - m[8] * m[6] * m[13] - m[12] * m[5] * m[10] + m[12]
				* m[6] * m[9];
		inv[1] = -m[1] * m[10] * m[15] + m[1] * m[11] * m[14] + m[9] * m[2]
				* m[15] - m[9] * m[3] * m[14] - m[13] * m[2] * m[11] + m[13]
				* m[3] * m[10];
		inv[5] = m[0] * m[10] * m[15] - m[0] * m[11] * m[14] - m[8] * m[2]
				* m[15] + m[8] * m[3] * m[14] + m[12] * m[2] * m[11] - m[12]
				* m[3] * m[10];
		inv[9] = -m[0] * m[9] * m[15] + m[0] * m[11] * m[13] + m[8] * m[1]
				* m[15] - m[8] * m[3] * m[13] - m[12] * m[1] * m[11] + m[12]
				* m[3] * m[9];
		inv[13] = m[0] * m[9] * m[14] - m[0] * m[10] * m[13] - m[8] * m[1]
				* m[14] + m[8] * m[2] * m[13] + m[12] * m[1] * m[10] - m[12]
				* m[2] * m[9];
		inv[2] = m[1] * m[6] * m[15] - m[1] * m[7] * m[14] - m[5] * m[2]
				* m[15] + m[5] * m[3] * m[14] + m[13] * m[2] * m[7] - m[13]
				* m[3] * m[6];
		inv[6] = -m[0] * m[6] * m[15] + m[0] * m[7] * m[14] + m[4] * m[2]
				* m[15] - m[4] * m[3] * m[14] - m[12] * m[2] * m[7] + m[12]
				* m[3] * m[6];
		inv[10] = m[0] * m[5] * m[15] - m[0] * m[7] * m[13] - m[4] * m[1]
				* m[15] + m[4] * m[3] * m[13] + m[12] * m[1] * m[7] - m[12]
				* m[3] * m[5];
		inv[14] = -m[0] * m[5] * m[14] + m[0] * m[6] * m[13] + m[4] * m[1]
				* m[14] - m[4] * m[2] * m[13] - m[12] * m[1] * m[6] + m[12]
				* m[2] * m[5];
		inv[3] = -m[1] * m[6] * m[11] + m[1] * m[7] * m[10] + m[5] * m[2]
				* m[11] - m[5] * m[3] * m[10] - m[9] * m[2] * m[7] + m[9]
				* m[3] * m[6];
		inv[7] = m[0] * m[6] * m[11] - m[0] * m[7] * m[10] - m[4] * m[2]
				* m[11] + m[4] * m[3] * m[10] + m[8] * m[2] * m[7] - m[8]
				* m[3] * m[6];
		inv[11] = -m[0] * m[5] * m[11] + m[0] * m[7] * m[9] + m[4] * m[1]
				* m[11] - m[4] * m[3] * m[9] - m[8] * m[1] * m[7] + m[8] * m[3]
				* m[5];
		inv[15] = m[0] * m[5] * m[10] - m[0] * m[6] * m[9] - m[4] * m[1]
				* m[10] + m[4] * m[2] * m[9] + m[8] * m[1] * m[6] - m[8] * m[2]
				* m[5];

		det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];
		if (det == 0)
			return false;

		det = (float) (1.0 / det);

		for (i = 0; i < 16; i++)
			invOut[i] = inv[i] * det;

		return true;
	}

	public Matrix44 makeProjectionOrtho(float left, float right, float bottom,
			float top, float clipnear, float clipfar) {
		float r_l = right - left;
		float t_b = top - bottom;
		float f_n = clipfar - clipnear;
		float tx = -(right + left) / (right - left);
		float ty = -(top + bottom) / (top - bottom);
		float tz = -(clipfar + clipnear) / (clipfar - clipnear);

		mat[0] = 2.0f / r_l;
		mat[5] = (float) (2.0 / t_b);
		mat[10] = -2.0f / f_n;

		mat[12] = tx;
		mat[13] = ty;
		mat[14] = tz;
		mat[15] = 1.0f;
		return this;
	}

	public Matrix44 makeProjectionPersp(float arc, float aspect,
			float clipnear, float clipfar) {
		float xFac, yFac;
		yFac = (float) Math.tan(arc * (Math.PI / 360.0));
		xFac = yFac * aspect;

		mat[0] = 1 / xFac;
		mat[5] = 1 / yFac;
		mat[10] = -(clipfar + clipnear) / (clipfar - clipnear);
		mat[11] = -1;
		mat[14] = -(2 * clipfar * clipnear) / (clipfar - clipnear);
		return this;
	}

	public Matrix44 makeInvProjectionPersp(float arc, float aspect,
			float clipnear, float clipfar) {
		float xFac, yFac;
		yFac = (float) Math.tan(arc * (Math.PI / 360.0));
		xFac = yFac * aspect;

		// Thanks to Superku!!!!!!
		mat[0] = xFac;
		mat[5] = yFac;
		mat[11] = -(clipfar - clipnear) / (2 * clipfar * clipnear);
		mat[14] = -1;
		mat[15] = (clipfar + clipnear) / (2 * clipfar * clipnear);
		return this;
	}

	public Matrix44 makeTranslate(Vector4 other) {
		identity();
		mat[12] = other.getX();
		mat[13] = other.getY();
		mat[14] = other.getZ();
		mat[15] = other.getW();
		return this;
	}

	public Matrix44 makeTranslate(Vector3 other) {
		identity();
		mat[12] = other.getX();
		mat[13] = other.getY();
		mat[14] = other.getZ();
		mat[15] = 1.0f;
		return this;
	}

	public Matrix44 makeScale(Vector4 other) {
		identity();
		mat[0] = other.getX();
		mat[5] = other.getY();
		mat[10] = other.getZ();
		mat[15] = other.getW();
		return this;
	}

	public Matrix44 makeScale(Vector3 other) {
		identity();
		mat[0] = other.getX();
		mat[5] = other.getY();
		mat[10] = other.getZ();
		mat[15] = 1.0f;
		return this;
	}

	public Matrix44 makeRotate(Vector4 other) {
		return set(new Quaternion(other).getMatrix());
	}

	public Matrix44 makeRotate(Vector3 other) {
		return set(new Quaternion(other).getMatrix());
	}

	public Matrix44 makeRotate(Quaternion other) {
		return set(other.getMatrix());
	}

	public Vector4 transform(Vector4 other) {
		return multiply(other);
	}

	public Vector3 transform(Vector3 other) {
		return multiply(other);
	}

	public Matrix44 translate(Vector4 other) {
		return multiply(new Matrix44().makeTranslate(other));
	}

	public Matrix44 translate(Vector3 other) {
		return multiply(new Matrix44().makeTranslate(other));
	}

	public Matrix44 scale(Vector3 other) {
		return multiply(new Matrix44().makeScale(other));
	}

	public Matrix44 scale(Vector4 other) {
		return multiply(new Matrix44().makeScale(other));
	}

	public Matrix44 rotate(Vector4 other) {
		return multiply(new Matrix44().makeRotate(other));
	}

	public Matrix44 rotate(Vector3 other) {
		return multiply(new Matrix44().makeRotate(other));
	}

	public Matrix44 rotate(Quaternion other) {
		return multiply(other.getMatrix());
	}

	public Matrix44 transpose() {
		float[] temp = new float[16];

		temp[0] = mat[0];
		temp[1] = mat[4];
		temp[2] = mat[8];
		temp[3] = mat[12];

		temp[4] = mat[1];
		temp[5] = mat[5];
		temp[6] = mat[9];
		temp[7] = mat[13];

		temp[8] = mat[2];
		temp[9] = mat[6];
		temp[10] = mat[10];
		temp[11] = mat[14];

		temp[12] = mat[3];
		temp[13] = mat[7];
		temp[14] = mat[11];
		temp[15] = mat[15];

		return set(temp);
	}
	
	public Matrix44 getTranspose() {
		float[] temp = new float[16];

		temp[0] = mat[0];
		temp[1] = mat[4];
		temp[2] = mat[8];
		temp[3] = mat[12];

		temp[4] = mat[1];
		temp[5] = mat[5];
		temp[6] = mat[9];
		temp[7] = mat[13];

		temp[8] = mat[2];
		temp[9] = mat[6];
		temp[10] = mat[10];
		temp[11] = mat[14];

		temp[12] = mat[3];
		temp[13] = mat[7];
		temp[14] = mat[11];
		temp[15] = mat[15];

		return new Matrix44(temp);
	}

	public Matrix44 makeLookAt(float eyex, float eyey, float eyez,
			float centerx, float centery, float centerz, float upx, float upy,
			float upz) {

		float[] x = new float[3];
		float[] y = new float[3];
		float[] z = new float[3];

		float mag;

		/* Make rotation matrix */

		/* Z vector */
		z[0] = eyex - centerx;
		z[1] = eyey - centery;
		z[2] = eyez - centerz;
		mag = (float) Math.sqrt(z[0] * z[0] + z[1] * z[1] + z[2] * z[2]);
		if (mag > 0) { /* mpichler, 19950515 */
			z[0] /= mag;
			z[1] /= mag;
			z[2] /= mag;
		}

		/* Y vector */
		y[0] = upx;
		y[1] = upy;
		y[2] = upz;

		/* X vector = Y cross Z */
		x[0] = y[1] * z[2] - y[2] * z[1];
		x[1] = -y[0] * z[2] + y[2] * z[0];
		x[2] = y[0] * z[1] - y[1] * z[0];

		/* Recompute Y = Z cross X */
		y[0] = z[1] * x[2] - z[2] * x[1];
		y[1] = -z[0] * x[2] + z[2] * x[0];
		y[2] = z[0] * x[1] - z[1] * x[0];

		/* mpichler, 19950515 */
		/*
		 * cross product gives area of parallelogram, which is < 1.0 for
		 * non-perpendicular unit-length vectors; so normalize x, y here
		 */

		mag = (float) Math.sqrt(x[0] * x[0] + x[1] * x[1] + x[2] * x[2]);
		if (mag > 0) {
			x[0] /= mag;
			x[1] /= mag;
			x[2] /= mag;
		}

		mag = (float) Math.sqrt(y[0] * y[0] + y[1] * y[1] + y[2] * y[2]);
		if (mag > 0) {
			y[0] /= mag;
			y[1] /= mag;
			y[2] /= mag;
		}

		// #define M(row,col) m[col*4+row]
		mat[0 * 4 + 0] = x[0];
		mat[1 * 4 + 0] = x[1];
		mat[2 * 4 + 0] = x[2];
		mat[3 * 4 + 0] = 0.0f;
		mat[0 * 4 + 1] = y[0];
		mat[1 * 4 + 1] = y[1];
		mat[2 * 4 + 1] = y[2];
		mat[3 * 4 + 1] = 0.0f;
		mat[0 * 4 + 2] = z[0];
		mat[1 * 4 + 2] = z[1];
		mat[2 * 4 + 2] = z[2];
		mat[3 * 4 + 2] = 0.0f;
		mat[0 * 4 + 3] = 0.0f;
		mat[1 * 4 + 3] = 0.0f;
		mat[2 * 4 + 3] = 0.0f;
		mat[3 * 4 + 3] = 1.0f;

		return translate(new Vector3(-eyex, -eyey, -eyez));
	}

	public float[] toArray() {
		return Arrays.copyOf(mat, 16);
	}
	
	public float[] toMatrix33() {
		float[] m = new float[9];
		m[0] = mat[0];
		m[1] = mat[1];
		m[2] = mat[2];
		
		m[3] = mat[4];
		m[4] = mat[5];
		m[5] = mat[6];
		
		m[6] = mat[8];
		m[7] = mat[9];
		m[8] = mat[10];
		return m;
	}
	
	@Override
	public String toString() {
		return String.format("<%s \n[[%.3f, %.3f, %.3f, %.3f]\n" +
				"[%.3f, %.3f, %.3f, %.3f]\n" +
				"[%.3f, %.3f, %.3f, %.3f]\n" +
				"[%.3f, %.3f, %.3f, %.3f]>", Matrix44.class.toString(),
				mat[0], mat[1], mat[2], mat[3], 
				mat[4], mat[5], mat[6], mat[7], 
				mat[8], mat[9], mat[10], mat[11], 
				mat[12], mat[13], mat[14], mat[15]);
	}

	public final Matrix44 copy() {
		return new Matrix44(mat);
	}

}
