/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base;

import iv3d.base.math.Matrix44;
import iv3d.base.math.Vector3;

/**
 * <p>
 * An object that stores a snapshot of the a {@link Transform}'s state.
 * </p>
 */
public final class TransformState {
	final Vector3 position;
	final Vector3 scale;
	final Matrix44 rotation;

	/**
	 * Value constructor.
	 * 
	 * @param position The position value.
	 * @param scale The scale value.
	 * @param rotation The rotation.
	 */
	public TransformState(Vector3 position, Vector3 scale, Matrix44 rotation) {
		super();
		this.position = position;
		this.scale = scale;
		this.rotation = rotation;
	}

	/**
	 * @return The position.
	 */
	public final Vector3 getPosition() {
		return position;
	}

	/**
	 * @return The scale value.
	 */
	public final Vector3 getScale() {
		return scale;
	}

	/**
	 * @return The rotation.
	 */
	public final Matrix44 getRotation() {
		return rotation;
	}
}
