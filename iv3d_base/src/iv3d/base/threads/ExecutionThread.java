/**
 * Copyright (C) 2014 Fredrique Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.base.threads;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * 
 */
public abstract class ExecutionThread extends Thread {
	private boolean teminated;
	private final Semaphore semaphore; 
	
	public ExecutionThread() {
		teminated = false;
		semaphore = new Semaphore(1);
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		while(!teminated) {
			try {
				boolean tryAcquire = semaphore.tryAcquire(100, TimeUnit.MILLISECONDS);
				if(!tryAcquire) {
					continue;
				}
			} catch (InterruptedException e) {
				continue;
			}
			
			process();
		}
		
		onExit();
	}
	
	/**
	 * Run the process.
	 */
	protected abstract void onExit();
	
	/**
	 * Run the process.
	 */
	protected abstract void process();

	public final void execute() {
		semaphore.release();
	}
	
	/**
	 * Terminated the thread.
	 */
	public final void terminate() {
		teminated = true;
	}
	
	public static void main(String[] args) throws InterruptedException {
		ExecutionThread executionThread = new ExecutionThread() {
			
			@Override
			protected void process() {
				System.out.println("This is an output!");
			}
			
			@Override
			protected void onExit() {
				System.out.println("Exiting thread");
			}
		};
		executionThread.start();
		Thread.sleep(2000);
		executionThread.execute();
		Thread.sleep(2000);
		executionThread.terminate();
		Thread.sleep(3000);
		
	}
}
