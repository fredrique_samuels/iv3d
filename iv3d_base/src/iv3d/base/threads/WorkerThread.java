/**
 * Copyright (C) 2014 Fredrique Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.threads;


/**
 * A thread that invokes it's execute() method every X milliseconds.
 */
public abstract class WorkerThread extends Thread {
	private WorkerState state;

	/**
	 * Default constructor
	 */
	public WorkerThread() {
		setName("WorkerThread " + getId());
		state = new WorkerState();
	}

	/**
	 * Set the delay between executions.
	 * 
	 * @param delay The time in milliseconds.
	 */
	public final void setDelay(long delay) {
		synchronized (state) {
			state.setDelay(delay <= 0 ? 0 : delay);
		}
	}

	/**
	 * Request that the current thread terminate.
	 */
	public final void terminate() {
		synchronized (state) {
			state.terminate();
		}
	}

	@Override
	public void run() {
		while (!terminateRequested()) {
			boolean hasSlept = sleepForDelayTime();
			if (!hasSlept) {
				execute();
			}
		}
		onExit();
	}

	protected final boolean terminateRequested() {
		boolean result = false;
		synchronized (state) {
			result = state.terminateRequested;	
		}
		return result;
	}

	/**
	 * Called when the worked thread exits.
	 */
	public abstract void onExit();

	/**
	 * Sleep for 100ms at a time or less if the remaining time is less that
	 * 100ms.
	 * 
	 * @return <code>false</code> if no sleep is performed.
	 */
	private boolean sleepForDelayTime() {
		long sleep_time = 0;

		synchronized (state) {
			sleep_time = state.getSleepTime();
		}
		
        if (sleep_time == 0)
            return false;
		else {
			try {
				Thread.sleep(sleep_time);
			} catch (InterruptedException e) {
				e.printStackTrace();
				synchronized (state) {
					state.terminate();
				}
			}
		}
        return true;
	}

	/**
	 * This method must be overridden and is called after every sleep duration.
	 */
	protected abstract void execute();

	private class WorkerState {

		long delay;
		long remainingSleepTime;
		boolean terminateRequested;

		WorkerState() {
			this.delay = 500;
			this.remainingSleepTime = 500;
			this.terminateRequested = false;
		}

		public long getSleepTime() {
			if(delay==0) {
				return 0;
			}
			
			long sleepTime=0;
			if(remainingSleepTime>100) {
				remainingSleepTime-=100;
				sleepTime=100;
			} else if(remainingSleepTime>0){
				sleepTime = remainingSleepTime;
				remainingSleepTime=0;
			} else {
				remainingSleepTime = delay;
			}
			return sleepTime;
		}

		void setDelay(long delay) {
			this.delay = delay;
			this.remainingSleepTime = delay;
		}

		void terminate() {
			terminateRequested = true;
		}
	}
}
