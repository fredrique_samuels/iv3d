/**
 * Copyright (C) 2014 Fredrique Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
/**
 * Copyright (C) 2014 Fredrique Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.threads;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.Semaphore;

public abstract class ServingQueue <T> {
	
	private final Queue<T> items;
	private final Semaphore semaphore;
	private Integer maxBufferSize;
	private boolean terminated;
	
	public ServingQueue() throws InterruptedException {
		items = new PriorityQueue<T>();
		maxBufferSize=10;

		semaphore = new Semaphore(1);
		semaphore.acquire();
	}
	
	public final int getMaxBufferSize() {
		int result=0;
		synchronized (maxBufferSize) {
			result = maxBufferSize;
		}
		return result;
	}

	public final void setMaxBufferSize(int m) {
		synchronized (maxBufferSize) {
			maxBufferSize = m<=1?1:m;
		}
	}

	public final void push(T item) {
		if(item==null) {
			return;
		}
		
		int maxSize = getMaxBufferSize();
		synchronized (items) {
			int remove = items.size()-maxSize;
			while(remove>0) {
				items.remove();
				remove--;
			}
			
			items.add(item);
			if(items.size()==1) {
				semaphore.release();
			}
		}
	}
	
	public final T pop() {
		boolean wait = false;
		T item;
		synchronized (items) {
			if(items.size()==0) {
				wait = true;
			} else {
				item = items.poll();
			}
		}
		
		if(wait) {
			try {
				semaphore.acquire();
				semaphore.release();
			} catch (InterruptedException e) {
				return null;
			}
			
			if(terminated) {
				terminated=false;
				return null;
			}
		}
		

		synchronized (items) {
			item = items.poll();
		}
		
		return item;
	}
	
	public final void terminate() {
		terminated = true;
	}
}
