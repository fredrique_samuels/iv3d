/**
 * Copyright (C) 2014 Fredrique Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.threads;

import java.util.PriorityQueue;
import java.util.Queue;

public class CommandWorkerThread<CommandType extends Command> extends WorkerThread {

	final Queue<CommandType> commands;
	final int consumptionPerCycle;
	
	public CommandWorkerThread(int consumptionPerCycle) {
		commands = new PriorityQueue<CommandType>();
		this.consumptionPerCycle = consumptionPerCycle;
		setDelay(300);
	}
	
	@Override
	protected void execute() {
		int itemsLeft = 0;
		synchronized (commands) {			
			itemsLeft = commands.size();
		}
		
		if(itemsLeft>consumptionPerCycle) {
			itemsLeft = consumptionPerCycle;
		}
		
		while(!terminateRequested() && itemsLeft>0 ) {
			CommandType command;
			synchronized (commands) {
				command = commands.remove();
			}
			
			if(command!=null) {
				try {
					command.execute();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			itemsLeft--;
		}
		
	}
	
	@Override
	public void onExit() {
		commands.clear();
	}
	
	public final void pushCommand(CommandType command) {
		synchronized (commands) {
			commands.add(command);
		}
	}

}
