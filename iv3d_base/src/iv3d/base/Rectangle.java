/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base;


/**
 * <p>
 * A representation of a rectangular bounding area with an 
 * origin location and dimensions.
 * </p> 
 */
final public class Rectangle {

	final int x;
	final int y;
	final int width; 
	final int height;

	/**
	 * <p>Default constructor.</p>
	 */
	public Rectangle() {
		this(0, 0, 0, 0);
	}
	
	/**
	 * <p>Copy constructor.</p>
	 * 
	 * @param other The {@link Rectangle} to be copied.
	 */
	public Rectangle(Rectangle other) {
		this(other.x, other.y, other.width, other.height);
	}

	/**
	 * <p>
	 * Value constructor.
	 * </p>
	 * @param x The origin location X component.
	 * @param y The origin location Y component.
	 * @param width The area width.
	 * @param height The area height.
	 */
	public Rectangle(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	/**
	 * @return The <i>x</i> component.
	 */
	public final int getX() {
		return x;
	}

	/**
	 * @return The <i>y</i> component.
	 */
	public final int getY() {
		return y;
	}

	/**
	 * @return The area <i>width</i>.
	 */
	public final int getWidth() {
		return width;
	}

	/**
	 * @return The area <i>height</i>.
	 */
	public final int getHeight() {
		return height;
	}
	
	/**
     * Get the rectangle area.
     * Returns width*height.
     */
	public final float getArea() {
	    return width * height;
	}

	/**
     * Returns true if @point is
     * within the limits of the rectangle.
     */
	public final boolean containsPoint(Point point) {
	    return point.getX() >= x && point.getY() >= y && point.getX()
	            <= (x + width) && point.getY() <= (y + height);
	}

	
	private final Point getLinesIntersection(Point p0, Point p1) {
	    int p0_min = p0.getX() < p0.getY() ? p0.getX() : p0.getY();
	    int p0_max = p0.getX() > p0.getY() ? p0.getX() : p0.getY();
	    int p1_min = p1.getX() < p1.getY() ? p1.getX() : p1.getY();
	    int p1_max = p1.getX() > p1.getY() ? p1.getX() : p1.getY();

	    //make p0 the line with the lower minimum
	    if (p0_min > p1_min)
	    {
	    	int tmp_min = p0_min;
	        int tmp_max = p0_max;

	        p0_min = p1_min;
	        p0_max = p1_max;
	        p1_min = tmp_min;
	        p1_max = tmp_max;
	    }

	    if (p0_min < p1_max && p0_max > p1_min)
	    {
	        return new Point(p0_min > p1_min ? p0_min : p1_min,
	                p0_max < p1_max ? p0_max : p1_max);
	    }

	    return new Point(0, 0);
	}
	
	/**
     * Returns the rectangle space where this
     * rectangle overlaps @r.
     *
     * Intersection are calculated with the assumption that
     * (x,y) is the top left corner of the rectangle.
     */
	public final Rectangle intersect(Rectangle r)
	{
	    Rectangle ret = new Rectangle();
	    if (this.getArea()==0 || r.getArea()==0) {
	        return ret;
	    }

	    Point px = getLinesIntersection(new Point(x, x + width), new Point(r.x, r.x
	            + r.width));
	    Point py = getLinesIntersection(new Point(y, y + height), new Point(r.y, r.y
	            + r.height));
	    return new Rectangle(px.getX(), py.getX(), px.getY() - px.getX(), py.getY()
	            - py.getX());
	}
	
	/**
	 * @return The rectangle as an array of <code>int[]{x, y, width, height}</code>.
	 */
	public final int[] toArray() {
		return new int[]{x, y, width, height};
	}
	
	/**
	 * @return The size of the rectangle.
	 */
	public final Size toSize() {
		return new Size(width, height);
	}
}
