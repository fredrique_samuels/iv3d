/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base;

import java.util.LinkedList;

public class LinkedSelectionList<T> implements LoopCollection {
		LinkedList<T> items;
		private int currentIndex;
		
		public LinkedSelectionList() {
			items = new LinkedList<T>();
			currentIndex = 0;
		}
		
		public int size() {
			return items.size();
		}

		public void prev() {
			currentIndex-=1;
			if(currentIndex<-1) {
				currentIndex = -1;
			}
		}

		public boolean hasPrevious() {
			return currentIndex-1>-1;
		}

		public void next() {
			currentIndex+=1;
			if(currentIndex>=size()) {
				currentIndex=size()-1;
			}
		}

		public boolean hasNext() {
			return currentIndex+1<size();
		}

		public T get(int index) {
			return items.get(index);
		}
		
		public T get() {
			return items.size()==0?null:get(currentIndex);
		}

		public int selectedIndex() {
			return currentIndex;
		}

		public void remove(T item) {
			if(items.contains(item)) {
				items.remove(item);
				
				// TODO be smarter here.
				// Adjust the index to the same item 
				// that was selected before.
				while (currentIndex>=items.size()) {
					currentIndex--;
				}
			}
		}

		public void add(T item) {
			if(items.contains(item)) {
				return;
			}
			items.add(item);			
		}

		public void setSelectedIndex(int i) {
			currentIndex = i;
		}
	}