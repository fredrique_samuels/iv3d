/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base;

import java.nio.ByteBuffer;

/**
 * <p>
 * Basic image data source structure.
 * </p>
 * <p>
 * Any object that provides image data must be a extends {@link ImageSource}.
 * </p>
 */
public abstract class ImageSource {
	private boolean canBind;
	private boolean dirty;

	/**
	 * <p>
	 * Default constructor.
	 * <>
	 */
	public ImageSource() {
		canBind = false;
		dirty = true;
	}

	/**
	 * <p>
	 * Sets the internal flag to indicate that this object will
	 * manage it's own context binding.
	 * 
	 * Used for Opnegl graphics mapping.
	 * </p>
	 */
	public final void enableBind() {
		canBind = true;
	}

	/**
	 * <p>
	 * Clears the internal flag that indicates this object will
	 * manage it's own context binding.
	 * 
	 * Used for Opnegl graphics mapping.
	 * </p>
	 */
	public final void disableBind() {
		canBind = false;
	}

	/**
	 * @return <code>true</code> if {@link #enableBind()} has been invoked.
	 */
	public final boolean isBindEnabled() {
		return canBind;
	}

	/**
	 * <p>
	 * Set the internal flag to indicate that the image data has changed
	 * </p>
	 */
	public final void setDirty() {
		dirty = true;
	}

	/**
	 * <p>
	 * CLear the internal flag. Use this after the internal data has been read.
	 * </p>
	 */
	public final void clearDirty() {
		dirty = false;
	}

	/**
	 * @return <code>true</code> if {@link #setDirty()} has been invoked.
	 */
	public final boolean isDirty() {
		return dirty;
	}

	public abstract PixelFormat getPixelFormat();
	public abstract int getWidth();
	public abstract int getHeight();
	public abstract int getDataFormat();
	public abstract int getInternalFormat();
	public abstract ByteBuffer getPixels();
	public abstract int getBytesPerPixel();
	public abstract void bind(int index);
	public abstract int getFilterOptions();
	public abstract void update();
}
