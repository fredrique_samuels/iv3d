package iv3d.base;

public final class Percentage {
	
	private final int value;

	public Percentage() {
		this(0);
	}

	public Percentage(int value) {
		this.value = formatValue(value);
	}

	public final int get() {
		return value;
	}
	
	@Override
	public String toString() {
		return String.format("<%s %d>", Percentage.class.getName(), value);
	}
	
	private int formatValue(int value) {
		if(value<0) {
			return 0;
		}
		
		if(value>100) {
			return 100;
		}
		
		return value;
	}
}
