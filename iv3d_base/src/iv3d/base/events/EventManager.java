/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.events;

import iv3d.base.events.handlers.KeyEventHandler;
import iv3d.base.events.handlers.MouseDownEventHandler;
import iv3d.base.events.handlers.MouseMoveEventHandler;
import iv3d.base.events.handlers.MouseUpEventHandler;

/**
 * <p>
 * An object for keeping track of which {@link EventReceiver} should 
 * be sent an event based on previous event states.
 * </p>
 * 
 * <b>Usage:<b>
 * <p>
 * <code>
 * EventManager manager = new EventManager;
 * manager.processEvent(new Event(EventType.MOUSE_DOWN), new SelectionInfo());
 * manager.processEvent(new Event(EventType.MOUSE_UP), new SelectionInfo());
 * </code>
 * </p>
 * 
 * @see SelectionInfo
 * @see MouseDownEventHandler
 * @see MouseUpEventHandler
 * @see MouseMoveEventHandler
 */
public class EventManager extends EventTargetGroup {
	private final MouseMoveEventHandler mouseMoveEventHandler;
	private final MouseDownEventHandler mouseDownEventHandler;
	private final MouseUpEventHandler mouseUpEventHandler;
	private KeyEventHandler keyEventHandler;

	/**
	 * Default constructor.
	 */
	public EventManager() {
		super();
		mouseMoveEventHandler = new MouseMoveEventHandler(this);
		mouseDownEventHandler = new MouseDownEventHandler(this);
		mouseUpEventHandler = new MouseUpEventHandler(this);
		keyEventHandler = new KeyEventHandler(this);
	}
	
	/**
	 * Update the current event states based on the new 
	 * {@link SelectionInfo}.
	 *  
	 * @param event The new event.
	 * @param selectionInfo The selection data.
	 */
	public final void processEvent(Event event, SelectionInfo selectionInfo) {
		if (selectionInfo==null) {
			return;
		}
		
		if (event==null) {
			if (getLastEvent()!=null){
				delegateToHandlers(getLastEvent().copy(EventType.MOUSE_MOVE, -1, null, null, null, 0, 0, selectionInfo, null), selectionInfo);
				setLastEvent(event);
			} 
			return;
		}
		
		delegateToHandlers(event, selectionInfo);
		setLastEvent(event);
	}

	private void delegateToHandlers(Event event, SelectionInfo pick) {
		if (event.getEventType()==EventType.MOUSE_MOVE) {
			mouseMoveEventHandler.process(getRelCoordEvent(event), pick);
		}else if (event.getEventType()==EventType.MOUSE_DRAG) {
			mouseMoveEventHandler.process(getRelCoordEvent(event), pick);
		}else if (event.getEventType()==EventType.MOUSE_DOWN) {
			mouseDownEventHandler.process(event, pick);
		}else if (event.getEventType()==EventType.MOUSE_UP) {
			mouseUpEventHandler.process(event, pick);
		}else if (event.getEventType()==EventType.KEY_DOWN) {
			keyEventHandler.process(event, pick);
		}else if (event.getEventType()==EventType.KEY_UP) {
			keyEventHandler.process(event, pick);
		}
	}

	private Event getRelCoordEvent(Event event) {
		Event lastEvent = getLastEvent();
		if(lastEvent==null) {
			return event;
		}
		int oldX = lastEvent.getX();
		int oldY = lastEvent.getY();
		int curX = event.getX();
		int curY = event.getY();
		return event.copy(null, null, null, null, null, curX-oldX, curY-oldY, null, null);
	}

}
