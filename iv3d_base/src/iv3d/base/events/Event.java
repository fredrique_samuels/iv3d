/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.events;

/**
 * An event info container.
 */
public final class Event {
	/**
	 * A value used to indicate that an {@link EventReceiver} has no events 
	 * associated with it. 
	 */
	public static final int STATE_NONE = 0x0000;
	
	/**
	 * Used to indicate the {@link EventReceiver} is currently being pressed.
	 */
	public static final int STATE_PRESSED = 0x0001;
	
	/**
	 * Used to indicate the {@link EventReceiver} is currently being dragged.
	 */
	public static final int STATE_DRAG = 0x0002;
	
	/**
	 * Used to indicate the {@link EventReceiver} is currently has focus.
	 * Only objects that have focus will receive key input events.
	 */
	public static final int STATE_FOCUS = 0x0004;
	
	/**
	 * Used to indicate the mouse cursor is hovering over the {@link EventReceiver}.
	 */
	public static final int STATE_HOVER = 0x0008;
	
	/**
	 * Used to indicate the {@link EventReceiver} is currently requesting focus.
	 */
	public static final int STATE_FOCUS_REQUEST = 0x0010;

	/**
	 *  A constant to indicate the left mouse button.
	 */
	public static final int MOUSE_BUTTON0 = 0;
	
	/**
	 *  A constant to indicate the center mouse button.
	 */
	public static final int MOUSE_BUTTON1 = 1;
	
	/**
	 *  A constant to indicate the right mouse button.
	 */
	public static final int MOUSE_BUTTON2 = 2;

	private final EventType event;
	private final int button;
	private final int key;
	private final int x;
	private final int y;
	private final int relX;
	private final int relY;
	private final SelectionInfo selectionInfo;
	private final EventReceiver droppedReceiver;

	/**
	 * Create a new event.
	 * 
	 * @param event The event type.
	 * @param button The mouse button pressed.
	 * @param key The key pressed.
	 * @param x The location of the mouse cursor.
	 * @param y The location of the mouse cursor.
	 * @param relX The relative movement of the mouse cursor
	 * @param relY The relative movement of the mouse cursor
	 * @param selectionInfo Information about the object that is under the cursor.
	 * @param droppedReceiver The object that the drag target was dropped onto. 
	 */
	public Event(EventType event, int button, int key, int x, int y, int relX,
			int relY, SelectionInfo selectionInfo, EventReceiver droppedReceiver) {
		this.event = event;
		this.button = button;
		this.key = key;
		this.x = x;
		this.y = y;
		this.relX = relX;
		this.relY = relY;
		this.selectionInfo = selectionInfo;
		this.droppedReceiver = droppedReceiver;
	}

	protected Event(EventType event) {
		this(event, -1, -1, -1, -1, 0, 0, null, null);
	}

	protected Event(EventType event, int x, int y) {
		this(event, -1, x, y, -1, 0, 0, null, null);
	}

	/**
	 * @return The event type.
	 * @see EventType
	 */
	public final EventType getEventType() {
		return event;
	}

	/**
	 * @return The mouse button.
	 */
	public final int getButton() {
		return button;
	}

	/**
	 * @return The key for the event.
	 */
	public final int getKey() {
		return key;
	}

	/**
	 * @return The mouse cursor location.
	 */
	public final int getX() {
		return x;
	}

	/**
	 * @return The mouse cursor location.
	 */
	public final int getY() {
		return y;
	}

	/**
	 * @return The relative movement of the mouse cursor
	 */
	public final int getRelX() {
		return relX;
	}

	/**
	 * @return The relative movement of the mouse cursor
	 */
	public final int getRelY() {
		return relY;
	}
	
	/**
	 * @return Information about the object that is under the cursor.
	 */
	public final SelectionInfo getSelectionInfo() {
		return selectionInfo;
	}

	/**
	 * @return The object that the drag target was dropped onto. 
	 */
	public final EventReceiver getDroppedReceiver() {
		return droppedReceiver;
	}

	/**
	 * <p>
	 * Create a copy of an event.
	 * </p> 
	 * 
	 * <p>
	 * If any argument is not null, then that value will be used in the 
	 * return object instead of the original value.
	 * </p>
	 * 
	 * 
	 * @param event The event type.
	 * @param button The mouse button pressed.
	 * @param key The key pressed.
	 * @param x The location of the mouse cursor.
	 * @param y The location of the mouse cursor.
	 * @param relX The relative movement of the mouse cursor
	 * @param relY The relative movement of the mouse cursor
	 * @param selectionInfo Information about the object that is under the cursor.
	 * @param droppedReceiver The object that the drag target was dropped onto. 
	 * @return
	 */
	public final Event copy(EventType event, Integer button, Integer key,
			Integer x, Integer y, Integer relX, Integer relY, SelectionInfo pick,
			EventReceiver droppedReceiver) {
		return new Event(event == null ? this.event : event,
				button == null ? this.button : button.intValue(),
				key == null ? this.key : key.intValue(), x == null ? this.x : x
						.intValue(), y == null ? this.y : y.intValue(),
				relX == null ? this.relX : relX.intValue(),
				relY == null ? this.relY : relY.intValue(),
				pick == null ? this.selectionInfo : pick,
				droppedReceiver == null ? this.droppedReceiver
						: droppedReceiver);
	}

	@Override
	public String toString() {
		return String.format("<%s event=%s x=%d y=%d relx=%d rely=%d>", Event.class.toString(),
				event.toString(), x, y, relX, relY);
	}

	/**
	 * @param event The event to compare with.
	 * @return <code>true</code> if the event have the same 
	 * mouse cursor location.
	 */
	public boolean equalsPosition(Event event) {
		if (event == null) {
			return false;
		}
		return event.x == x && event.y == y;
	}
}
