/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.events;

public enum EventType {
	KEY_UP(100),
	KEY_DOWN(101),
	
	MOUSE_MOVE(102),
	MOUSE_DOWN(103),
	MOUSE_UP(104),
	MOUSE_DRAG(105),
	MOUSE_ENTER(106),
	MOUSE_EXIT(107),
	FOCUS_GAINED(108),
	FOCUS_LOST(109),
	MOUSE_CLICK(110),
	MOUSE_DROP(111),
	
	BUTTON_UP(200),
	BUTTON_DOWN(201); 
	
	private int value;
	private EventType(int value) {
		this.value = value;
	}
	public int getValue(){
		return value;
	}
}
