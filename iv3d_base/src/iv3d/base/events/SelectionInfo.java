/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.events;

import iv3d.base.math.Matrix44;
import iv3d.base.math.Vector3;

public class SelectionInfo {
	final EventReceiver target;
	final Vector3 position;
	final Matrix44 modelView;
	final Matrix44 projection;
	final Matrix44 modelViewProjection;

	public SelectionInfo(EventReceiver target, Vector3 position,
			Matrix44 modelView, Matrix44 projection, Matrix44 modelViewProjection) {
		if(target!=null) {
			this.target = target.isEventsEnabled() ? target : null;
		}else {
			this.target = target;
		}
		this.position = new Vector3(position);
		this.modelView = new Matrix44(modelView);
		this.projection = new Matrix44(projection);
		this.modelViewProjection = new Matrix44(modelViewProjection);
	}
	
	public SelectionInfo(EventReceiver target) {
		this(target, new Vector3(), new Matrix44(), new Matrix44(), new Matrix44());
	}
	
	public SelectionInfo() {
		this(null);
	}

	public final Object getTarget() {
		return target;
	}

	public final Vector3 getPosition() {
		return position;
	}

	public final Matrix44 getModelView() {
		return modelView;
	}

	public final Matrix44 getProjection() {
		return projection;
	}

	public final Matrix44 getModelViewProjection() {
		return modelViewProjection;
	}
}
