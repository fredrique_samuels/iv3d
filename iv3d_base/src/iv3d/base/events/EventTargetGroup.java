/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.events;

public class EventTargetGroup {
	private EventReceiver focusTarget;
	private EventReceiver hoverTarget;
	private EventReceiver pressedTarget;
	private EventReceiver focusRequestTarget;
	private EventReceiver dragTarget;
	private Event lastEvent;
	
	public EventTargetGroup() {
	}

	public int getEventState(EventReceiver target) {
		if (target == null) {
			return 0;
		}
		
		if (target==dragTarget){
			return Event.STATE_DRAG;
		}
		
		if (target==hoverTarget){
			return Event.STATE_HOVER;
		}

		if (target==pressedTarget){
			return Event.STATE_PRESSED;
		}
		
		if (target==focusTarget){
			return Event.STATE_FOCUS;
		}
		
		return 0;
	}
	
	
	public final EventReceiver getFocusTarget() {
		return focusTarget;
	}

	public final void setFocusTarget(EventReceiver focusTarget) {
		this.focusTarget = focusTarget;
		set(focusTarget);
	}

	public final EventReceiver getHoverTarget() {
		return hoverTarget;
	}

	public final void setHoverTarget(EventReceiver hoverTarget) {
		this.hoverTarget = hoverTarget;
		set(hoverTarget);
	}

	public final EventReceiver getPressedTarget() {
		return pressedTarget;
	}

	public final void setPressedTarget(EventReceiver pressedTarget) {
		this.pressedTarget = pressedTarget;
		set(pressedTarget);
	}

	public final EventReceiver getFocusRequestTarget() {
		return focusRequestTarget;
	}

	public final void setFocusRequestTarget(EventReceiver focusRequestTarget) {
		this.focusRequestTarget = focusRequestTarget;
		set(focusRequestTarget);
	}

	public final EventReceiver getDragTarget() {
		return dragTarget;
	}

	public final void setDragTarget(EventReceiver dragTarget) {
		this.dragTarget = dragTarget;
		set(dragTarget);
	}
	
	private void set(EventReceiver target){
		if(target!=null) {
			target.eventGroup = this;
		}
		
	}

	public void setLastEvent(Event lastEvent) {
		this.lastEvent = lastEvent;
	}

	public Event getLastEvent() {
		return lastEvent;
	}
}
