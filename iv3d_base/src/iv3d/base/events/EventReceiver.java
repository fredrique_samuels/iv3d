/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.events;

import java.util.LinkedList;

/**
 * An object that can receive events from an {@link EventManager}. 
 * 
 * This object will only receive events if {@link #isEventsEnabled()}
 * returns <code>true</code>.
 * 
 * @see SelectionInfo
 * @see EventManager
 * @see EventListener
 */
public class EventReceiver {
	protected EventTargetGroup eventGroup;
	private boolean eventsEnabled;
	private int eventState;
	private final LinkedList<EventListener> eventListeners;

	/**
	 * Default constructor.
	 */
	public EventReceiver() {
		eventsEnabled = false;
		eventGroup = null;
		eventListeners = new LinkedList<EventListener>();
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		removeAllEventListeners();
	}

	/**
	 * Check of events has been enabled for this object.
	 * 
	 * @return <code>true</code> if this object may receive events.
	 * @see #enableEvents()
	 * @see #disableEvents()
	 */
	public final boolean isEventsEnabled() {
		return eventsEnabled;
	}

	/**
	 * Enabled events for this object.
	 * 
	 * @return <code>this</code>
	 * @see EventReceiver#isEventsEnabled()
	 * @see EventReceiver#disableEvents()
	 */
	public final EventReceiver enableEvents() {
		this.eventsEnabled = true;
		return this;
	}

	/**
	 * Disable events for this object.
	 * 
	 * @return <code>this</code>
	 * @see EventReceiver#isEventsEnabled()
	 * @see EventReceiver#enableEvents()
	 */
	public final EventReceiver disableEvents() {
		this.eventsEnabled = false;
		return this;
	}
	
	/**
	 * Add a new {@link EventListener} to this list
	 * of object that must be notified when this object receives an event.
	 * 
	 * @param listener The object to be notified.
	 * @return <code>this</code>
	 */
	public final EventReceiver addEventListener(EventListener listener){
		eventListeners.add(listener);
		return this;
	}
	
	/**
	 * Remove an {@link EventListener} from the list of object to be notified
	 * of events.
	 * 
	 * @param listener The object to be removed. 
	 * @return <code>this</code>
	 * @see #addEventListener(EventListener)
	 */
	public final EventReceiver removeEventListener(EventListener listener){
		if(eventListeners.contains(listener)){
			eventListeners.remove(listener);
		}
		return this;
	}
	
	public final EventReceiver removeAllEventListeners(){
		eventListeners.clear();
		return this;
	}
	
	public final EventReceiver disptachEvent(Event event){
		int newState = getEventState();
		if(eventState!=newState){
			eventState = newState; 
			onEventStateChanged(event);
		}
		for(EventListener l : eventListeners) {
			l.onEventStateChanged(event);
		}
		return this;
	}
	
	/**
	 * A method to be overridden if needed. 
	 * called before {@link #disptachEvent(Event)}.
	 * @param event Then new event.
	 */
	public void onEventStateChanged(Event event) {
		
	}
	
	/**
	 * 
	 * @param state The event state to check for.
	 * <ul>
	 * 	<li>Event.STATE_NONE</li>
	 * 	<li>Event.STATE_PRESSED</li>
	 * 	<li>Event.STATE_DRAG</li>
	 * 	<li>Event.STATE_FOCUS</li>
	 * 	<li>Event.STATE_HOVER</li>
	 * <ul>
	 * @return true if this object is in the requested state.
	 */
	public final boolean isInState(int state) {
		boolean result = false;
		if(eventGroup==null) {
			if(state==Event.STATE_NONE)
				return true;
			return false;
		}
		
		EventReceiver dragTarget = eventGroup.getDragTarget();
		EventReceiver focusTarget = eventGroup.getFocusTarget();
		EventReceiver hoverTarget = eventGroup.getHoverTarget();
		EventReceiver pressedTarget = eventGroup.getPressedTarget();
		
		switch (state) {
		case Event.STATE_DRAG:
			result = dragTarget==this;
			break;
		case Event.STATE_FOCUS:
			result = focusTarget==this;
			break;
		case Event.STATE_HOVER:
			result = hoverTarget==this;
			break;
		case Event.STATE_PRESSED:
			result = pressedTarget==this;
			break;
		case Event.STATE_NONE:
			result = dragTarget!=this && focusTarget!=this && hoverTarget!=this && pressedTarget!=this;
			break;
		default:
			break;
		}

		return result;
	}
	
	/**
	 * @return The current event state of the widget.
	 * 	The widget may be in more that one state, but this return the 
	 *   highest priority state.
	 */
	public final int getEventState() { 
		if(eventGroup==null) {
			return Event.STATE_NONE;
		}
		
		if(isInState(Event.STATE_DRAG)) {
			return Event.STATE_DRAG;
		}else if(isInState(Event.STATE_PRESSED)) {
			return Event.STATE_PRESSED;
		}else if(isInState(Event.STATE_HOVER)) {
			return Event.STATE_HOVER;
		}else if(isInState(Event.STATE_FOCUS)) {
			return Event.STATE_FOCUS;
		}

		return Event.STATE_NONE;
	}

}
