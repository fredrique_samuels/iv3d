/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.events.handlers;

import iv3d.base.events.Event;
import iv3d.base.events.EventReceiver;
import iv3d.base.events.EventTargetGroup;
import iv3d.base.events.EventType;
import iv3d.base.events.SelectionInfo;

public class MouseMoveEventHandler extends EventHandler {

	public MouseMoveEventHandler(EventTargetGroup targets) {
		super(targets);
	}

	@Override
	public void process(Event event, SelectionInfo pick) {
		if (processMouseDown(event, pick)) {
			return;
		}
		processMouseUp(event, pick);
	}

	private void processMouseUp(Event event, SelectionInfo pick) {
		EventReceiver target = (EventReceiver) pick.getTarget();
		dispatchExitEvent(event, pick, target);
		if (target != null) {
			dispatchEnterEvent(event, pick, target);
			dispatchMoveEvent(event, pick, target);
		}
	}

	private void dispatchMoveEvent(Event event, SelectionInfo pick,
			EventReceiver target) {
		if(event.equalsPosition(getTargets().getLastEvent())){
			return;
		}
		if(target==getTargets().getHoverTarget()) {
			target.disptachEvent(event.copy(EventType.MOUSE_MOVE, null, null,
					null, null, null, null, pick, null));
		}
	}

	private void dispatchEnterEvent(Event event, SelectionInfo pick,
			EventReceiver target) {
		if (target != getTargets().getHoverTarget()) {
			getTargets().setHoverTarget(target);
			target.disptachEvent(event.copy(EventType.MOUSE_ENTER, null, null,
					null, null, null, null, pick, null));
		}
	}

	private void dispatchExitEvent(Event event, SelectionInfo pick,
			EventReceiver target) {
		EventReceiver hoverTarget = getTargets().getHoverTarget();
		if (target != hoverTarget && hoverTarget != null) {
			getTargets().setHoverTarget(null);
			hoverTarget.disptachEvent(event.copy(EventType.MOUSE_EXIT, null,
					null, null, null, null, null, pick, null));
		}
	}

	private boolean processMouseDown(Event event, SelectionInfo pick) {
		setDragTarget(event);
		return dispatchDragEvent(event, pick);
	}

	private boolean dispatchDragEvent(Event event, SelectionInfo pick) {
		if(event.equalsPosition(getTargets().getLastEvent())){
			return false;
		}
		
		EventReceiver target = getTargets().getDragTarget();
		if (target != null) {
			target.disptachEvent(event.copy(EventType.MOUSE_DRAG, null, null,
					null, null, null, null, pick, null));
			return true;
		}
		return false;
	}

	private void setDragTarget(Event event) {
		if(Math.abs(event.getRelX())<3 && Math.abs(event.getRelY())< 3) {
			return;
		}
		EventReceiver target = getTargets().getPressedTarget();
		if (target != null) {
			System.out.println("Draging : " );
			getTargets().setPressedTarget(null);
			getTargets().setDragTarget(target);
		}
	}

}
