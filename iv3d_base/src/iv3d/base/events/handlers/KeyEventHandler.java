/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.events.handlers;

import iv3d.base.events.Event;
import iv3d.base.events.EventReceiver;
import iv3d.base.events.EventTargetGroup;
import iv3d.base.events.SelectionInfo;

public class KeyEventHandler extends EventHandler {

	public KeyEventHandler(EventTargetGroup targets) {
		super(targets);
	}

	@Override
	public void process(Event event, SelectionInfo pick) {
		EventReceiver focusTarget = getTargets().getFocusTarget();
		if (focusTarget != null) {
			focusTarget.disptachEvent(event);
		}
	}

}
