/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.events.handlers;

import iv3d.base.events.Event;
import iv3d.base.events.EventReceiver;
import iv3d.base.events.EventTargetGroup;
import iv3d.base.events.EventType;
import iv3d.base.events.SelectionInfo;

public class MouseUpEventHandler extends EventHandler{

	public MouseUpEventHandler(EventTargetGroup targets) {
		super(targets);
	}

	@Override
	public void process(Event event, SelectionInfo pick) {
		EventReceiver pressedTarget = getTargets().getPressedTarget();
		EventReceiver dragTarget = getTargets().getDragTarget();
		EventReceiver hoverTarget = getTargets().getHoverTarget();
		
		getTargets().setDragTarget(null);
		getTargets().setPressedTarget(null);
		
		dispatchUpEvent(event, pick, dragTarget, pressedTarget);
		dispatchExitEvent(event, pick, hoverTarget);
		dispatchEnterEvent(event, pick, hoverTarget);
	}

	private void dispatchEnterEvent(Event event, SelectionInfo pick,
			EventReceiver hoverTarget) {
		EventReceiver target = (EventReceiver) pick.getTarget();
		if(target!=null && target!=hoverTarget) {
			getTargets().setHoverTarget(target);
			target.disptachEvent(event.copy(EventType.MOUSE_ENTER, null, null,
					null, null, null, null, pick, null));
		}
	}

	private void dispatchExitEvent(Event event, SelectionInfo pick,
			EventReceiver hoverTarget) {
		if(hoverTarget!=null && hoverTarget!=pick.getTarget()){
			getTargets().setHoverTarget(null);
			hoverTarget.disptachEvent(event.copy(EventType.MOUSE_EXIT, null, null,
					null, null, null, null, pick, null));
		}
	}

	private void dispatchUpEvent(Event event, SelectionInfo pick,
			EventReceiver dragTarget, EventReceiver pressedTarget) {
		if(pressedTarget!=null) {
			pressedTarget.disptachEvent(event.copy(EventType.MOUSE_UP, null, null,
					null, null, null, null, pick, null));
			pressedTarget.disptachEvent(event.copy(EventType.MOUSE_CLICK, null, null,
					null, null, null, null, pick, null));
		} else if(dragTarget!=null) {
			dragTarget.disptachEvent(event.copy(EventType.MOUSE_UP, null, null,
					null, null, null, null, pick, null));
			dragTarget.disptachEvent(event.copy(EventType.MOUSE_DROP, null, null,
					null, null, null, null, pick, (EventReceiver) pick.getTarget()));
		}
	}

}
