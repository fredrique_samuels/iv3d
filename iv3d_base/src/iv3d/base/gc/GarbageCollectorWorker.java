/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.gc;

import java.util.logging.Logger;

import iv3d.base.threads.WorkerThread;

public class GarbageCollectorWorker extends WorkerThread {
	
	public static Logger log = Logger.getLogger(GarbageCollectorWorker.class.getCanonicalName());
	
	public GarbageCollectorWorker(long period) {
		setDelay(period);
	}

	@Override
	protected void execute() {
		Runtime rs = Runtime.getRuntime();
		long totalMemory = rs.totalMemory();

		long freeMemory = rs.freeMemory();
		{
			String msg = String.format(
					"Free memory in JVM before Garbage Collection = %s (%.3f)",
					Long.valueOf(freeMemory),
					((float) freeMemory / totalMemory) * 100);
			log.info(msg);
		}

		rs.gc();

		{
			String msg = String.format(
					"Free memory in JVM after Garbage Collection = %s (%.3f)",
					Long.valueOf(rs.freeMemory()),
					((float) rs.freeMemory() / totalMemory) * 100);
			log.info(msg);
			log.info(rs.freeMemory() - freeMemory
					+ " bytes collected");
		}
	}

	@Override
	public void onExit() {

	}

}