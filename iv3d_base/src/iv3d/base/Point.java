/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base;


/**
 * 2 Dimensional point object.
 */
final public class Point {
	private final int x;
	private final int y;

	/**
	 * Default constructor.
	 */
	public Point() {
		this(0, 0);
	}

	/**
	 * Value constructor.
	 * 
	 * @param x
	 *            The <code>x</code> value.
	 * @param y
	 *            The <code>y</code> value.
	 */
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * @return The <code>x</code> value.
	 */
	public final int getX() {
		return x;
	}

	/**
	 * @return The <code>y</code> value.
	 */
	public final int getY() {
		return y;
	}

	@Override
	public String toString() {
		return String.format("<%s x=%d y=%d>", Point.class.toString(), x, y);
	}
	
}
