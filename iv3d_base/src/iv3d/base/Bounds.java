/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base;

/**
 * <p>
 * A representation of a rectangular bounding area with an 
 * origin location and dimensions.
 * </p> 
 */
final public class Bounds {

	final float x;
	final float y;
	final float w;
	final float h;

	/**
	 * <p>Default constructor.</p>
	 */
	public Bounds() {
		this(0, 0, 0, 0);
	}

	/**
	 * <p>
	 * Value constructor.
	 * </p>
	 * @param x The origin location X component.
	 * @param y The origin location Y component.
	 * @param width The area width.
	 * @param height The area height.
	 */
	public Bounds(float x, float y, float width, float height) {
		this.x = x;
		this.y = y;
		this.w = width;
		this.h = height;
	}

	/**
	 * @return The <i>x</i> component.
	 */
	public final float getX() {
		return x;
	}

	/**
	 * @return The <i>y</i> component.
	 */
	public final float getY() {
		return y;
	}

	/**
	 * @return The area <i>width</i>.
	 */
	public final float getWidth() {
		return w;
	}

	/**
	 * @return The area <i>height</i>.
	 */
	public final float getHeight() {
		return h;
	}
	
}
