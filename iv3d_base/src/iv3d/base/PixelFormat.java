package iv3d.base;

import iv3d.base.exceptions.GraphicsException;

public final class PixelFormat {
	
	public static enum Component {
		R(0), G(1), B(2), A(3);
		
		int value;
		Component(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return value;
		}
	}
	
	public static final PixelFormat RGBA = new PixelFormat(Component.R, Component.G, Component.B, Component.A);
	public static final PixelFormat ARGB = new PixelFormat(Component.A, Component.R, Component.G, Component.B);
	
	private final Component component1;
	private final Component component2;
	private final Component component3;
	private final Component component4;
	
	public PixelFormat(String pixels) throws GraphicsException {
		validatePixelFormat(pixels);
		this.component1 = getComponentFromChar(pixels.charAt(0));
		this.component2 = getComponentFromChar(pixels.charAt(1));
		this.component3 = getComponentFromChar(pixels.charAt(2));
		this.component4 = getComponentFromChar(pixels.charAt(3));
	}
	

	public PixelFormat(Component component1, Component component2, Component component3, Component component4) {
		this.component1 = component1;
		this.component2 = component2;
		this.component3 = component3;
		this.component4 = component4;
	}

	private Component getComponentFromChar(char c) {
		if(c=='r') { 
			return Component.R;
		}
		
		if(c=='g') { 
			return Component.G;
		}
		
		if(c=='b') { 
			return Component.B;
		}
		
		if(c=='a') { 
			return Component.A;
		}
			
		return null;
	}
	
	public static void validatePixelFormat(String pixelFormat) throws GraphicsException {
		if(pixelFormat==null) {
			return;
		}
		
		if(pixelFormat.length()!=4) { 
			String message = String.format("Pixel format must be atleast 4 long,  but received '%s'", pixelFormat);
			throw new GraphicsException(message);
		}

		for(int i=0; i<4; i++) {
			boolean valid = "rgba".contains(""+pixelFormat.charAt(i));
			if(!valid) {
				String message = String.format("Pixel format must contain only the characters 'r', 'g', 'b' and 'a' : received '%s'", pixelFormat);
				throw new GraphicsException(message);
			}
		}
	}

	public final Component getComponent1() {
		return component1;
	}

	public final Component getComponent2() {
		return component2;
	}

	public final Component getComponent3() {
		return component3;
	}

	public final Component getComponent4() {
		return component4;
	}
	
}
