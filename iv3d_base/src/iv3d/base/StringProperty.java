package iv3d.base;

import java.util.HashMap;
import java.util.Map;

public class StringProperty extends Pair<String, String> {
	
	public static final String ACCOUNT_ID = "Account ID";
	public static final String NAME = "Name";
	public static final String LAST_NAME = "Surname";
	public static final String DOB = "DOB";
	public static final String GENDER = "Gender";
	public static final String LOCALE = "Locale";
	public static final String TIME_ZONE = "Time Zone";

	public StringProperty(String first, String second) {
		super(first, second);
	}
	
	public static Map<String, String> toMap(String separator, StringProperty... properties) {
		HashMap<String, String> map = new HashMap<String, String>();
		for(StringProperty p : properties) 
			updateMap(map, p, separator);
		return map;
	}                                                             
	
	

	private static void updateMap(HashMap<String, String> map, StringProperty p, String separator) {
		String key = p.getFirst();
		if(map.containsKey(key))
			map.put(key, String.format("%s%s%s", map.get(key), separator, p.getSecond()));
		else
			map.put(key, p.getSecond());
	}
	
}
