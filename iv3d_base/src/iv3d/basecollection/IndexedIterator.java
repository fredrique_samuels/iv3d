/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package iv3d.basecollection;

/**
 * Iterator used to iterate over an indexed collection.
 */
public abstract class IndexedIterator<T> extends Iterator<T> {
	
	private int index = -1;
	
	public IndexedIterator() {
		
	}
	
	/**
	 * @return The size of the collection.
	 */
	protected abstract int size();

	/**
	 * @return The indexed item or <code>null</code>.
	 */
	protected abstract T get(int index);

	/**
	 * @return <code>false</code> if the loop should break.
	 */
	protected abstract boolean execute(T v, int index);
	
	@Override
	protected final boolean execute(T v) {
		return execute(v, index);
	}

	@Override
	public final boolean hasNext() {
		return size() > 0 && index+1 < size() ;
	}

	@Override
	public final T next() {
		if(!hasNext()) {return null;}
		++index;
		return get(index);
	}
	
}
