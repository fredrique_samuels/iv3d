/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.basecollection;

/**
 * Interface for iterating over collections.
 */
public abstract class Iterator <T> {
	
	/**
	 * @return <code>true</code> if there is a next item in the collection.
	 */
	public abstract boolean hasNext();
	
	/**
	 * @return The next item in the collection or <code>null</code>.
	 */
	public abstract T next();
	
	/**
	 * Iterate through all the items in the collection.
	 */
	public final void iterate() {
		while (hasNext()) {
			T v = next();
			if(v==null) {break;}
			if(!execute(v)) {break;}
		}
	}
	
	protected abstract boolean execute(T v);
}
