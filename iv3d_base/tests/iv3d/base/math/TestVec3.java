/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.math;

import iv3d.base.math.Vector3;
import iv3d.base.math.Vector4;
import junit.framework.TestCase;

public class TestVec3 extends TestCase{
	
	public void testInit() {
		Vector3 vec2 = new Vector3();

		assertEquals(0, vec2.getX(), 0.001);
		assertEquals(0, vec2.getY(), 0.001);
		assertEquals(0, vec2.getZ(), 0.001);
	}
	
	public void testInitWithValues() {
		Vector3 vec2 = new Vector3(2.3f, 4.6f, 6.8f);

		assertEquals(2.3, vec2.getX(), 0.001);
		assertEquals(4.6, vec2.getY(), 0.001);
		assertEquals(6.8, vec2.getZ(), 0.001);
	}
	
	public void testInitWithOther() {
		Vector3 vec1 = new Vector3(2.3f, 4.6f, 6.8f);
		Vector3 vec2 = new Vector3(vec1);

		assertEquals(2.3, vec2.getX(), 0.001);
		assertEquals(4.6, vec2.getY(), 0.001);
		assertEquals(6.8, vec2.getZ(), 0.001);
	}
	
	public void testInitWithVec4() {
		Vector4 vec1 = new Vector4(2.3f, 4.6f, 6.8f, 5.9f);
		Vector3 vec2 = new Vector3(vec1);

		assertEquals(2.3, vec2.getX(), 0.001);
		assertEquals(4.6, vec2.getY(), 0.001);
		assertEquals(6.8, vec2.getZ(), 0.001);
	}
	
	public void testEquals() {
		Vector3 vec1 = new Vector3(3.3f, 5.0f, 4.5f);
		Vector3 vec2 = new Vector3(3.2f, 5.3f, 4.6f);
		Vector3 vec3 = new Vector3(3.2f, 5.3f, 4.6f);

		assertFalse(vec1.equals(vec2));
		assertTrue(vec2.equals(vec3));
	}
	
	public void testAdd() {
		// given
		Vector3 vec1 = new Vector3(1, 2, 3);
		Vector3 vec2 = new Vector3(3, 5, 7);

		// when
		Vector3 vec3 = vec1.add(vec2);

		// then
		assertEquals(4, vec3.getX(), 0.001);
		assertEquals(7, vec3.getY(), 0.001);
		assertEquals(10, vec3.getZ(), 0.001);
	}
	
	public void testSubtract() {
		// given
		Vector3 vec1 = new Vector3(1, 2, 3);
		Vector3 vec2 = new Vector3(3, 5, 7);

		// when
		Vector3 vec3 = vec1.subtract(vec2);

		// then
		assertEquals(-2, vec3.getX(), 0.001);
		assertEquals(-3, vec3.getY(), 0.001);
		assertEquals(-4, vec3.getZ(), 0.001);
	}

	public void testScale() {
		// given
		Vector3 vec1 = new Vector3(1, 2, 3);

		// when
		Vector3 vec2 = vec1.multiply(3.0f);

		// then
		assertEquals(3, vec2.getX(), 0.001);
		assertEquals(6, vec2.getY(), 0.001);
		assertEquals(9, vec2.getZ(), 0.001);
	}
	
	public void testLength() {
		// given
		Vector3 vec1 = new Vector3(2, 4, 6);

		// when
		double actual = vec1.length();

		// then
		double expected = 7.483314991;
		assertEquals(expected, actual, 0.001);
	}
	
	public void testNomalize() {
		// given
		Vector3 vec1 = new Vector3(2, 4, 6);

		// when
		Vector3 vec2 = vec1.normalize();

		// then
		assertEquals(0.267261236906, vec2.getX(), 0.001);
		assertEquals(0.534522473812, vec2.getY(), 0.001);
		assertEquals(0.801783680916, vec2.getZ(), 0.001);
	}
	
	public void testDot() {
		// given
		Vector3 vec1 = new Vector3(2, 4, 6);
		Vector3 vec2 = new Vector3(1, 3, 5);

		// when
		double actual = vec1.dot(vec2);

		// then
		double expected = 44.0;
		assertEquals(expected, actual, 0.001);
	}
	
	public void testCross() {
		// given
		Vector3 vec1 = new Vector3(2, 4, 6);
		Vector3 vec2 = new Vector3(1, 3, 5);

		// when
		Vector3 vec3 = vec1.cross(vec2);

		// then
		assertEquals(2.0, vec3.getX(), 0.001);
		assertEquals(-4.0, vec3.getY(), 0.001);
		assertEquals(2.0, vec3.getZ(), 0.001);
	}
	
	public void testDistance() {
		// given
		Vector3 vec1 = new Vector3(2, 4, 6);
		Vector3 vec2 = new Vector3(1, 3, 5);

		// when
		double actual = vec1.distance(vec2);

		// then
		float expected = 1.73205077648f;
		assertEquals(expected, actual, 0.001);
	}
}
