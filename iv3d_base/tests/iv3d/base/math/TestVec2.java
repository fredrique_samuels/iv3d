/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.math;

import iv3d.base.math.Vector2;
import junit.framework.TestCase;

public class TestVec2 extends TestCase {

	public void testInit() {
		Vector2 vec2 = new Vector2();

		assertEquals(0, vec2.getX(), 0.001);
		assertEquals(0, vec2.getY(), 0.001);
	}

	public void testInitWithValues() {
		Vector2 vec2 = new Vector2(2.3f, 4.6f);

		assertEquals(2.3, vec2.getX(), 0.001);
		assertEquals(4.6, vec2.getY(), 0.001);
	}
	
	public void testInitWithOther() {
		Vector2 vec1 = new Vector2(2.3f, 4.6f);
		Vector2 vec2 = new Vector2(vec1);

		assertEquals(2.3, vec2.getX(), 0.001);
		assertEquals(4.6, vec2.getY(), 0.001);
	}

	public void testAdd() {
		// given
		Vector2 vec1 = new Vector2(1, 2);
		Vector2 vec2 = new Vector2(3, 5);

		// when
		Vector2 vec3 = vec1.add(vec2);

		// then
		assertEquals(4, vec3.getX(), 0.001);
		assertEquals(7, vec3.getY(), 0.001);
	}

	public void testEquals() {
		Vector2 vec1 = new Vector2(3.3f, 5.0f);
		Vector2 vec2 = new Vector2(3.2f, 5.3f);
		Vector2 vec3 = new Vector2(3.2f, 5.3f);

		assertFalse(vec1.equals(vec2));
		assertTrue(vec2.equals(vec3));
	}

	public void testSubtract() {
		// given
		Vector2 vec1 = new Vector2(1, 2);
		Vector2 vec2 = new Vector2(3, 5);

		// when
		Vector2 vec3 = vec1.subtract(vec2);

		// then
		assertEquals(-2, vec3.getX(), 0.001);
		assertEquals(-3, vec3.getY(), 0.001);
	}

	public void testScale() {
		// given
		Vector2 vec1 = new Vector2(1, 2);

		// when
		Vector2 vec2 = vec1.multiply(3.0f);

		// then
		assertEquals(3, vec2.getX(), 0.001);
		assertEquals(6, vec2.getY(), 0.001);
	}

	public void testLength() {
		// given
		Vector2 vec1 = new Vector2(2, 4);

		// when
		double actual = vec1.length();

		// then
		double expected = 4.47213602066;
		assertEquals(expected, actual, 0.001);
	}
	
	public void testNormalize() {
		// given
		Vector2 vec1 = new Vector2(2, 4);

		// when
		Vector2 vec2 = vec1.normalize();

		// then
		assertEquals(0.447213590145, vec2.getX(), 0.001);
		assertEquals(0.89442718029, vec2.getY(), 0.001);
	}
	
	public void testDot() {
		// given
		Vector2 vec1 = new Vector2(2, 4);
		Vector2 vec2 = new Vector2(1, 3);

		// when
		double actual = vec1.dot(vec2);

		// then
		double expected = 14.0;
		assertEquals(expected, actual, 0.001);
	}
	
	public void testDistance() {
		// given
		Vector2 vec1 = new Vector2(2, 4);
		Vector2 vec2 = new Vector2(1, 3);

		// when
		double actual = vec1.distance(vec2);

		// then
		double expected = 1.41421353817;
		assertEquals(expected, actual, 0.001);
	}
}
