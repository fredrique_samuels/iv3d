/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.base.events;

import iv3d.base.events.Event;
import iv3d.base.events.EventListener;
import iv3d.base.events.EventManager;
import iv3d.base.events.EventReceiver;
import iv3d.base.events.EventType;
import iv3d.base.events.SelectionInfo;

import java.util.LinkedList;
import java.util.List;

import junit.framework.TestCase;

public class TestEvents extends TestCase {
	
	class TestTarget extends EventReceiver implements EventListener {

		private List<Event> events = new LinkedList<Event>();
		
		public TestTarget() {
			addEventListener(this);
		}

		@Override
		public void onEventStateChanged(Event event) {
			events.add(event);
		}
	}
	
    public void testMouseUpOverTargets(){
        // Given
    	EventManager manager = new EventManager();
        TestTarget target = new TestTarget();
        TestTarget target2 = new TestTarget();

        // When
        manager.processEvent(new Event(EventType.MOUSE_DOWN), new SelectionInfo(target));
        SelectionInfo pick = new SelectionInfo(target2);
		manager.processEvent(new Event(EventType.MOUSE_MOVE, 10, 10), pick);

		// And
        assertNull(manager.getPressedTarget());
        assertEquals(target, manager.getDragTarget());
        target.events.clear();
        assertEquals(target.events.size(), 0);
        target2.events.clear();
        assertEquals(target2.events.size(), 0);

        // When
        Event event = new Event(EventType.MOUSE_UP);
		manager.processEvent(event, pick);

        // Then
        assertNull(manager.getPressedTarget());
        assertNull(manager.getDragTarget());

        assertEquals(target, manager.getFocusTarget());
        assertEquals(2, target.events.size());
        assertEquals(EventType.MOUSE_UP, target.events.get(0).getEventType());
        assertEquals(EventType.MOUSE_DROP, target.events.get(1).getEventType());
        assertTrue(target.isInState(Event.STATE_FOCUS));
    }
	

	public void testMouseUpOverNoTarget() {
        // Given
        EventManager manager = new EventManager();
        TestTarget target = new TestTarget();

        // And
        manager.processEvent(new Event(EventType.MOUSE_DOWN), new SelectionInfo(target));
        manager.processEvent(new Event(EventType.MOUSE_MOVE), new SelectionInfo());

        // And
        assertNull(manager.getPressedTarget());
        assertEquals(target, manager.getDragTarget());
        target.events.clear();
        assertEquals(len(target.events), 0);

        // When
        manager.processEvent(new Event(EventType.MOUSE_UP), new SelectionInfo());

        // Then
        assertNull(manager.getPressedTarget());
        assertNull(manager.getDragTarget());
        assertEquals(target, manager.getFocusTarget());
        assertEquals(len(target.events), 2);
        assertEquals(EventType.MOUSE_UP, target.events.get(0).getEventType());
        assertEquals(EventType.MOUSE_DROP, target.events.get(1).getEventType());
        assertTrue(target.isInState(Event.STATE_FOCUS));
	}
        
	public void  testMouseUpOverPressedTarget() {
        // Given
        EventManager manager = new EventManager();
        TestTarget target = new TestTarget();

        // And
        manager.processEvent(new Event(EventType.MOUSE_DOWN), new SelectionInfo(target));
        manager.processEvent(new Event(EventType.MOUSE_MOVE), new SelectionInfo(target));

        // And
        assertNull(manager.getPressedTarget());
        assertEquals(target, manager.getDragTarget());
        target.events.clear();
        assertEquals(len(target.events), 0);

        // When
        manager.processEvent(new Event(EventType.MOUSE_UP), new SelectionInfo(target));

        // Then
        assertNull(manager.getPressedTarget());
        assertNull(manager.getDragTarget());
        assertEquals(target, manager.getFocusTarget());
        assertEquals(len(target.events), 2);
        assertEquals(EventType.MOUSE_UP, target.events.get(0).getEventType());
        assertEquals(EventType.MOUSE_DROP, target.events.get(1).getEventType());
        assertTrue(target.isInState(Event.STATE_HOVER));
        assertTrue(target.isInState(Event.STATE_FOCUS));
	}
	
	public void test_lose_focus() {
		// Given
        EventManager manager = new EventManager();
        TestTarget target0 = new TestTarget();
        TestTarget target1 = new TestTarget();

        manager.setFocusTarget(target0);
        assertNull(manager.getPressedTarget());
        assertEquals(target0, manager.getFocusTarget());

        // And
        target0.events.clear();
        target1.events.clear();
        assertEquals(0, len(target0.events));
        assertEquals(0, len(target1.events));

        // When
        manager.processEvent(new Event(EventType.MOUSE_DOWN), new SelectionInfo(target1));

        // Then
        assertEquals(len(target0.events), 1);
        assertEquals(EventType.FOCUS_LOST, target0.events.get(0).getEventType());

        assertEquals(target1, manager.getPressedTarget());
        assertEquals(target1, manager.getFocusTarget());
        assertEquals(len(target1.events), 2);
        assertEquals(EventType.MOUSE_DOWN, target1.events.get(0).getEventType());
        assertEquals(EventType.FOCUS_GAINED, target1.events.get(1).getEventType());
        assertTrue(target1.isInState(Event.STATE_PRESSED));
        assertTrue(target1.isInState(Event.STATE_FOCUS));
	}
	
	private int len(List<Event> events) {
		return events.size();
	}

	public void testMousePressed(){
        // Given
        EventManager manager = new EventManager();
        TestTarget target = new TestTarget();

        manager.setHoverTarget(target);
        assertNull(manager.getPressedTarget());
        assertNull(manager.getFocusTarget());

        // And
        target.events.clear();
        assertEquals(0, target.events.size());

        // When
        manager.processEvent(new Event(EventType.MOUSE_DOWN), new SelectionInfo(target));

        // Then
        assertEquals(target, manager.getPressedTarget());
        assertEquals(target, manager.getFocusTarget());
        assertEquals(3, target.events.size());
        assertEquals(EventType.MOUSE_EXIT, target.events.get(0).getEventType());
        assertEquals(EventType.MOUSE_DOWN, target.events.get(1).getEventType());
        assertEquals(EventType.FOCUS_GAINED, target.events.get(2).getEventType());
        assertTrue(target.isInState(Event.STATE_PRESSED));
        assertTrue(target.isInState(Event.STATE_FOCUS));

        // Given
        target.events.clear();
        assertEquals(0, target.events.size());

        // When
        manager.processEvent(new Event(EventType.MOUSE_MOVE, 1, 1), new SelectionInfo(target));

        // Then
        assertNull(manager.getPressedTarget());
        assertEquals(target, manager.getDragTarget());
        assertEquals(target, manager.getFocusTarget());
        assertEquals(1, target.events.size());
        assertEquals(EventType.MOUSE_DRAG, target.events.get(0).getEventType());
        assertTrue(target.isInState(Event.STATE_DRAG));
        assertTrue(target.isInState(Event.STATE_FOCUS));

        // Given
        target.events.clear();
        assertEquals(0, target.events.size());

        // When
        manager.processEvent(new Event(EventType.MOUSE_MOVE, 10, 10), new SelectionInfo());

        // Then
        assertNull(manager.getPressedTarget());
        assertEquals(target, manager.getDragTarget());
        assertEquals(target, manager.getFocusTarget());
        assertEquals(1, target.events.size());
        assertEquals(EventType.MOUSE_DRAG, target.events.get(0).getEventType());
        assertTrue(target.isInState(Event.STATE_DRAG));
        assertTrue(target.isInState(Event.STATE_FOCUS));
        
	}
	
    public void testMouseMovingOver2Targets(){
        // Given
    	EventManager manager = new EventManager();
        TestTarget target0 = new TestTarget();
        TestTarget target1 = new TestTarget();

        // Then
        assertNull(manager.getHoverTarget());

        // When
        manager.processEvent(new Event(EventType.MOUSE_MOVE), new SelectionInfo());

        // Then
        assertNull(manager.getHoverTarget());

        // When
        target0.events.clear();
        assertEquals(0, target0.events.size());
        manager.processEvent(new Event(EventType.MOUSE_MOVE, 10, 10), new SelectionInfo(target0));

        // Then
        assertEquals(target0, manager.getHoverTarget());
        assertEquals(2, target0.events.size());
        assertEquals(EventType.MOUSE_ENTER, target0.events.get(0).getEventType());
        assertEquals(EventType.MOUSE_MOVE, target0.events.get(1).getEventType());
        assertTrue(target0.isInState(Event.STATE_HOVER));

        // When
        target0.events.clear();
        assertEquals(0, target0.events.size());
        manager.processEvent(new Event(EventType.MOUSE_MOVE), new SelectionInfo(target0));

        // Then
        assertEquals(1, target0.events.size());
        assertEquals(EventType.MOUSE_MOVE, target0.events.get(0).getEventType());
        assertTrue(target0.isInState(Event.STATE_HOVER));

        // When
        target0.events.clear();
        target1.events.clear();
        assertEquals(0, target0.events.size());
        assertEquals(0, target1.events.size());
        manager.processEvent(new Event(EventType.MOUSE_MOVE, 10, 10), new SelectionInfo(target1));

        // Then
        assertEquals(1, target0.events.size());
        assertEquals(EventType.MOUSE_EXIT, target0.events.get(0).getEventType());
        assertTrue(target0.isInState(Event.STATE_NONE));
        
        assertEquals(target1, manager.getHoverTarget());
        assertEquals(2, target1.events.size());
        assertEquals(EventType.MOUSE_ENTER, target1.events.get(0).getEventType());
        assertEquals(EventType.MOUSE_MOVE, target1.events.get(1).getEventType());
        assertTrue(target1.isInState(Event.STATE_HOVER));

        // When
        target1.events.clear();
        assertEquals(0, target1.events.size());
        manager.processEvent(new Event(EventType.MOUSE_MOVE), new SelectionInfo());

        // Then
        assertEquals(1, target1.events.size());
        assertEquals(EventType.MOUSE_EXIT, target1.events.get(0).getEventType());
        assertTrue(target1.isInState(Event.STATE_NONE));
        assertNull(manager.getHoverTarget());
    }
}
