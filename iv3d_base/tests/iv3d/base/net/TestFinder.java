package iv3d.base.net;

import iv3d.base.net.discover.ServerFinder;

public class TestFinder extends ServerFinder {

	public TestFinder(int port) throws NetworkException {
		super(port);
	}

	@Override
	protected byte[] getProbeMessage() {
		return "DISCOVER_FUIFSERVER_REQUEST".getBytes();
	}

	@Override
	protected boolean isServerReply(byte[] data) {
		String message = new String(data).trim();
		return "DISCOVER_FUIFSERVER_RESPONSE".equals(message);
	}

	@Override
	protected void onServerReply(String hostAddress, byte[] data) {
		System.out.println("Server IP :" + hostAddress);
	}

}
