package iv3d.base.net;

import iv3d.base.net.discover.ServerBeacon;
import iv3d.base.net.discover.ServerFinder;

public class BroadcastTest {

	static ServerBeacon beacon;

	public static void main(String[] args) throws NetworkException {
		
		startServer();
		probeServers();
		
		System.out.println("Done !");
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static void probeServers() {
		
		try {
			ServerFinder finder = new TestFinder(8888);
			finder.start();
			finder.probeServers();
		} catch (NetworkException e1) {
			e1.printStackTrace();
		}
		
	}

	private static void startServer() {

		try {
			beacon = new TestBeacon(8888);
			beacon.start();
		} catch (NetworkException e) {
			e.printStackTrace();
		}
		
	}
}
