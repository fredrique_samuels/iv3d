package iv3d.base.net;

import iv3d.base.net.discover.ServerBeacon;

public class TestBeacon extends ServerBeacon {

	public TestBeacon(int port) throws NetworkException {
		super(port);
	}

	@Override
	protected byte[] getServerResponse(byte[] data) {
		return "DISCOVER_FUIFSERVER_RESPONSE".getBytes();
	}

	@Override
	protected boolean isClientRequest(byte[] data) {
		String message = new String(data).trim();
		return message.equals("DISCOVER_FUIFSERVER_REQUEST");
	}

}
