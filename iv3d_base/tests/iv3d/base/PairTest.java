package iv3d.base;

import junit.framework.TestCase;

public class PairTest extends TestCase {
	public void testConstruction() {
		Pair<String, String> pair = new Pair<String, String>("first", "second");
		
		assertEquals("first", pair.getFirst());
		assertEquals("second", pair.getSecond());
	}
}
