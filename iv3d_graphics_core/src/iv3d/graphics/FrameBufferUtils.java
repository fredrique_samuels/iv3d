/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import java.nio.FloatBuffer;

import org.lwjgl.opengl.EXTFramebufferObject;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL14;

public final class FrameBufferUtils {
	private FrameBufferUtils() {
	}

	public static void bind(int frameBufferId) {
		EXTFramebufferObject.glBindFramebufferEXT(
				EXTFramebufferObject.GL_FRAMEBUFFER_EXT, frameBufferId);
	}

	public static int createFrameBufferColor(int textureId) {
		int fb = EXTFramebufferObject.glGenFramebuffersEXT();
		bind(fb);
		EXTFramebufferObject.glFramebufferTexture2DEXT(
				EXTFramebufferObject.GL_FRAMEBUFFER_EXT,
				EXTFramebufferObject.GL_COLOR_ATTACHMENT0_EXT,
				GL11.GL_TEXTURE_2D, textureId, 0);
		return fb;
	}

	public static int createRenderBuffer(int width, int height) {
		int renderId = EXTFramebufferObject.glGenRenderbuffersEXT();
		EXTFramebufferObject.glBindRenderbufferEXT(
				EXTFramebufferObject.GL_RENDERBUFFER_EXT, renderId);
		EXTFramebufferObject.glRenderbufferStorageEXT(
				EXTFramebufferObject.GL_RENDERBUFFER_EXT,
				GL14.GL_DEPTH_COMPONENT24, width, height);
		EXTFramebufferObject.glFramebufferRenderbufferEXT(
				EXTFramebufferObject.GL_FRAMEBUFFER_EXT,
				EXTFramebufferObject.GL_DEPTH_ATTACHMENT_EXT,
				EXTFramebufferObject.GL_RENDERBUFFER_EXT, renderId);
		return renderId;
	}

	public static void destroyFrameBuffer(int frameBufferId) {
		if (frameBufferId != 0) {
			EXTFramebufferObject.glDeleteFramebuffersEXT(frameBufferId);
		}
	}

	public static void destroyRenderBuffer(int renderBufferId) {
		if (renderBufferId != 0) {
			EXTFramebufferObject.glDeleteRenderbuffersEXT(renderBufferId);
		}
	}

	public static int createColorTexture(int width, int height) {
		int __color_texture_id = GL11.glGenTextures();
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, __color_texture_id);
		GL11.glTexEnvf(GL11.GL_TEXTURE_ENV, GL11.GL_TEXTURE_ENV_MODE,
				GL11.GL_MODULATE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER,
				GL11.GL_LINEAR);
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, width, height,
				0, GL11.GL_RGBA, GL11.GL_FLOAT, (FloatBuffer) null);
		return __color_texture_id;
	}

	public static void unbind() {
		bind(0);
	}

	public static int createDepthTexture(int width, int height) {
		int __color_texture_id = GL11.glGenTextures();
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, __color_texture_id);
		
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL14.GL_DEPTH_COMPONENT24, width, height,
				0, GL11.GL_DEPTH_COMPONENT, GL11.GL_FLOAT, (FloatBuffer) null);
		
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
		EXTFramebufferObject.glFramebufferTexture2DEXT(EXTFramebufferObject.GL_FRAMEBUFFER_EXT, 
				EXTFramebufferObject.GL_DEPTH_ATTACHMENT_EXT, GL11.GL_TEXTURE_2D,
				__color_texture_id, 0);
		return __color_texture_id;
	}

	public static int createFrameBufferDepth(int textureId) {
		int fb = EXTFramebufferObject.glGenFramebuffersEXT();
		bind(fb);
		EXTFramebufferObject.glFramebufferTexture2DEXT(
				EXTFramebufferObject.GL_FRAMEBUFFER_EXT,
				EXTFramebufferObject.GL_DEPTH_ATTACHMENT_EXT,
				GL11.GL_TEXTURE_2D, textureId, 0);
		return fb;
	}

}
