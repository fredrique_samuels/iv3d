/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_LEQUAL;
import static org.lwjgl.opengl.GL11.GL_NORMALIZE;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_RGB;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glCullFace;
import static org.lwjgl.opengl.GL11.glDepthFunc;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import iv3d.base.Color;
import iv3d.base.ImageSource;
import iv3d.base.ResourceLoader;
import iv3d.base.StaticImageSource;
import iv3d.base.exceptions.GraphicsException;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;


public final class GraphicsUtils {

	public static final int TRIANGLES = GL11.GL_TRIANGLES;
	public static final int QUADS = GL11.GL_QUADS;
	public static final int LINES = GL11.GL_LINES;
	public static final int RGBA = GL_RGBA;
	public static final int RGB = GL_RGB;
	public static final int FALSE = GL_FALSE;
	public static final int TRUE = GL_TRUE;
	public static final int FLOAT = GL_FLOAT;
	public static final int INT = GL_UNSIGNED_INT;
	public static final int UNSIGNED_INT = GL_UNSIGNED_INT;
	public static final int UNSIGNED_BYTE = GL_UNSIGNED_BYTE;

	private GraphicsUtils() {
	}

	public static void clearColorAndDepth() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	public static void clearColor() {
		glClear(GL_COLOR_BUFFER_BIT);
	}

	public static void clearDepth() {
		glClear(GL_DEPTH_BUFFER_BIT);
	}

	public static void setClearColor(float r, float g, float b, float a) {
		glClearColor(r, g, b, a);
	}

	public static void setClearColor(Color color) {
		float[] c = color.normalize().toArray();
		glClearColor(c[0], c[1], c[2], c[3]);
	}

	public static void init() {
		glEnable(GL_NORMALIZE);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_TEXTURE_2D);
		glDepthFunc(GL_LEQUAL);
		glEnable(GL_CULL_FACE);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glCullFace(GL11.GL_FRONT);
	}
	
	public static void enableCullFace() {
		glEnable(GL_CULL_FACE);
	}
	
	public static void disableCullFace() {
		glDisable(GL_CULL_FACE);
	}
	
	public static InputStream readFileAsInputStream(final String filename) throws GraphicsException {
		try {
			return new FileInputStream(filename);
		} catch (FileNotFoundException e) {
			throw new GraphicsException(e);
		}
	}

	public static String readFileAsString(final String filename)
			throws GraphicsException {
		StringBuilder source = new StringBuilder();
		IOException exception = null;
		BufferedReader reader;
		FileInputStream in = null;
		try {
			in = new FileInputStream(filename);
			reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			IOException innerExc = null;
			try {
				String line;
				while ((line = reader.readLine()) != null)
					source.append(line).append('\n');
			} catch (IOException exc) {
				exception = exc;
			} finally {
				try {
					reader.close();
				} catch (IOException exc) {
					if (innerExc == null)
						innerExc = exc;
					else
						exc.printStackTrace();
				}
			}
			if (innerExc != null)
				throw innerExc;
		} catch (IOException exc) {
			exception = exc;
		} finally {
			try {
				in.close();
			} catch (IOException exc) {
				if (exception == null)
					exception = exc;
				else
					exc.printStackTrace();
			}
			if (exception != null)
				throw new GraphicsException(exception);
		}
		return source.toString();
	}

	public static ImageSource readImageFromFile(String filename, int filterOptions)
			throws GraphicsException {

		ByteBuffer buf = null;
		int width = 0;
		int height = 0;
		int bpp = 3;
		int format = GraphicsUtils.RGB;
		try {
			// Open the PNG file as an InputStream
			InputStream in = ResourceLoader.read(filename);
			
			if(in==null) {
				throw new GraphicsException("Unable to load file " + filename);
			}
			BufferedImage image = ImageIO.read(in);
			
			
			width = image.getWidth();
			height = image.getHeight();
			
			if (image.getColorModel().hasAlpha()) {
				bpp = 4;
				format = GraphicsUtils.RGBA;
			}
			
			int[] pixels = new int[image.getWidth() * image.getHeight()];
	        image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());
	        
	        buf = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * bpp); //4 for RGBA, 3 for RGB
	        
	        for(int y = 0; y < image.getHeight(); y++){
	            for(int x = 0; x < image.getWidth(); x++){
	                int pixel = pixels[y * image.getWidth() + x];
	                buf.put((byte) ((pixel >> 16) & 0xFF));     // Red component
	                buf.put((byte) ((pixel >> 8) & 0xFF));      // Green component
	                buf.put((byte) (pixel & 0xFF));               // Blue component
	                if(image.getColorModel().hasAlpha()){
	                	buf.put((byte) ((pixel >> 24) & 0xFF));    // Alpha component. Only for RGBA
	                }
	            }
	        }
			buf.flip();

			in.close();
		} catch (IOException e) {
			throw new GraphicsException(e);
		}
		return new StaticImageSource(width, height, bpp, format,
				GraphicsUtils.UNSIGNED_BYTE, buf, filterOptions);
	}

	public static FloatBuffer asFloatBuffer(float[] data) {
//		FloatBuffer buf = BufferUtils.createFloatBuffer(data.length);
		FloatBuffer buf = ByteBuffer.allocateDirect(4 * data.length).order(ByteOrder.nativeOrder()).asFloatBuffer();
		put(data, buf);
		return buf;
	}

	private static void put(float[] data, FloatBuffer buf) {
		buf.put(data);
		buf.flip();
	}

	public static ByteBuffer asByteBuffer(byte[] data) {
		ByteBuffer buf = BufferUtils.createByteBuffer(data.length).put(data);
		buf.flip();
		return buf;
	}

	public static IntBuffer asIntBuffer(int[] data) {
		IntBuffer buf = BufferUtils.createIntBuffer(data.length).put(data);
		buf.flip();
		return buf;
	}

	public static void drawElements(RenderMode renderMode, int size) {
		GL11.glDrawElements(renderMode.getMode(), size,
						GL11.GL_UNSIGNED_INT, 0);
	}

	public static void setViewport(int x, int y, int width, int height) {
		GL11.glViewport(x, y, width, height);
	}

	private static int floatToPixel(float b) {
		return (int) (b * 255);
	}

	public static Color getReadPixelAt(int x, int y) {
		FloatBuffer pixelsBuf = asFloatBuffer(new float[4]);
		GL11.glReadPixels(x, y, 1, 1, RGBA, GL11.GL_FLOAT, pixelsBuf);
		return new Color(floatToPixel(pixelsBuf.get(0)), floatToPixel(pixelsBuf
				.get(1)), floatToPixel(pixelsBuf.get(2)),
				floatToPixel(pixelsBuf.get(3)));
	}

	public static float getDepthAt(int mouseX, int mouseY) {
		FloatBuffer depth = asFloatBuffer(new float[1]);
		GL11.glReadPixels( mouseX, mouseY, 1, 1, GL11.GL_DEPTH_COMPONENT, GL_FLOAT, depth );
		return depth.get(0);
	}

	public static void setLineWidth(float width) {
		GL11.glLineWidth(width);
	}
}
