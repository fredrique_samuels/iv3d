/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import static org.lwjgl.opengl.GL11.GL_FALSE;

import iv3d.base.exceptions.GraphicsException;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;


import org.lwjgl.opengl.ARBFragmentShader;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.ARBVertexShader;
import org.lwjgl.opengl.GL20;


public final class ShaderUtils {
	
	private ShaderUtils(){}

	public static enum ShaderType {
		VERTEX_SHADER(ARBVertexShader.GL_VERTEX_SHADER_ARB), FRAGMENT_SHADER(
				ARBFragmentShader.GL_FRAGMENT_SHADER_ARB);

		private final int type;

		ShaderType(int type) {
			this.type = type;
		}

		final int getType() {
			return type;
		}
	};

	public static int createShader(String filename, ShaderType shaderType)
			throws GraphicsException {
		String readFileAsString = GraphicsUtils.readFileAsString(filename);
		return ShaderUtils.createShaderFromString(readFileAsString, shaderType);
	}

	public static void bind(int program) {
		ARBShaderObjects.glUseProgramObjectARB(program);
	}

	public static String getLogInfo(int obj) {
		return ARBShaderObjects.glGetInfoLogARB(obj, ARBShaderObjects
				.glGetObjectParameteriARB(obj,
						ARBShaderObjects.GL_OBJECT_INFO_LOG_LENGTH_ARB));
	}
	
	public static void deleteProgram(int shader) {
		if (shader==0){
			return;
		}
		ARBShaderObjects.glDeleteObjectARB(shader);
	}

	public static int createShaderProgram(String vertSource, String fragSource)
			throws GraphicsException {
		int program = 0;
		int vertShader = 0, fragShader = 0;
		try {

			vertShader = ShaderUtils.createShaderFromString(vertSource,
					ShaderType.VERTEX_SHADER);

			fragShader = ShaderUtils.createShaderFromString(fragSource,
					ShaderType.FRAGMENT_SHADER);
		} catch (Exception exc) {
			throw new GraphicsException(exc);
		} finally {
		}
		program = ARBShaderObjects.glCreateProgramObjectARB();
		if (program == 0) {
			return program;
		}
		/*
		 * 055. if the vertex and fragment shaders setup sucessfully, 056.
		 * attach them to the shader program, link the sahder program 057. (into
		 * the GL context I suppose), and validate 058.
		 */

		ARBShaderObjects.glAttachObjectARB(program, vertShader);
		ARBShaderObjects.glDeleteObjectARB(vertShader);

		ARBShaderObjects.glAttachObjectARB(program, fragShader);
		ARBShaderObjects.glDeleteObjectARB(fragShader);

		ARBShaderObjects.glLinkProgramARB(program);

		if (ARBShaderObjects.glGetObjectParameteriARB(program,
				ARBShaderObjects.GL_OBJECT_LINK_STATUS_ARB) == GL_FALSE) {
			throw new GraphicsException(ShaderUtils.getLogInfo(program));
		}
		ARBShaderObjects.glValidateProgramARB(program);
		if (ARBShaderObjects.glGetObjectParameteriARB(program,
				ARBShaderObjects.GL_OBJECT_VALIDATE_STATUS_ARB) == GL_FALSE) {
			throw new GraphicsException(ShaderUtils.getLogInfo(program));
		}
		return program;
	}

	public static int createShaderFromString(String readFileAsString,
			ShaderType shaderType) throws GraphicsException {
		int shader = 0;
		try {
			shader = ARBShaderObjects.glCreateShaderObjectARB(shaderType
					.getType());
			if (shader == 0)
				return 0;
			ARBShaderObjects.glShaderSourceARB(shader, readFileAsString);
			ARBShaderObjects.glCompileShaderARB(shader);
			if (ARBShaderObjects.glGetObjectParameteriARB(shader,
					ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL_FALSE)

				throw new GraphicsException("Error creating shader: "
						+ getLogInfo(shader));
			return shader;
		} catch (Exception exc) {
			ARBShaderObjects.glDeleteObjectARB(shader);
			throw new GraphicsException(exc);
		}
	}

	public static void setUniform4f(int program, String name, float v0, float v1, float v2, float v3) {
		int location = GL20.glGetUniformLocation(program, name);
		GL20.glUniform4f(location, v0, v1, v2, v3);
	}

	public static void setUniform3f(int program, String name, float v0, float v1, float v2) {
		int location = GL20.glGetUniformLocation(program, name);
		GL20.glUniform3f(location, v0, v1, v2);
	}

	public static void setUniform2f(int program, String name, float v0, float v1) {
		int location = GL20.glGetUniformLocation(program, name);
		GL20.glUniform2f(location, v0, v1);
	}

	public static void setUniform1f(int program, String name, float v0) {
		int location = GL20.glGetUniformLocation(program, name);
		GL20.glUniform1f(location, v0);
	}

	public static void setUniform4i(int program, String name, int v0, int v1, int v2, int v3) {
		int location = GL20.glGetUniformLocation(program, name);
		GL20.glUniform4i(location, v0, v1, v2, v3);
	}

	public static void setUniform3i(int program, String name, int v0, int v1, int v2) {
		int location = GL20.glGetUniformLocation(program, name);
		GL20.glUniform3i(location, v0, v1, v2);
	}

	public static void setUniform2i(int program, String name, int v0, int v1) {
		int location = GL20.glGetUniformLocation(program, name);
		GL20.glUniform2i(location, v0, v1);
	}

	public static void setUniform1i(int program, String name, int v0) {
		int location = GL20.glGetUniformLocation(program, name);
		GL20.glUniform1i(location, v0);
	}

	public static void setUniform4iv(int program, String name, IntBuffer v) {
		int location = GL20.glGetUniformLocation(program, name);
		GL20.glUniform4(location, v);
	}

	public static void setUniform3iv(int program, String name, IntBuffer v) {
		int location = GL20.glGetUniformLocation(program, name);
		GL20.glUniform3(location, v);
	}

	public static void setUniform2iv(int program, String name, IntBuffer v) {
		int location = GL20.glGetUniformLocation(program, name);
		GL20.glUniform2(location, v);
	}
	
	public static void setUniform1iv(int program, String name, IntBuffer v) {
		int location = GL20.glGetUniformLocation(program, name);
		GL20.glUniform1(location, v);
	}
	
	public static void setUniformMat4(int program, String name, FloatBuffer matrix) {
		int location = GL20.glGetUniformLocation(program, name);
		GL20.glUniformMatrix4(location, false, matrix);
	}
	
	public static void setUniformMat3(int program, String name, FloatBuffer matrix) {
		int location = GL20.glGetUniformLocation(program, name);
		GL20.glUniformMatrix3(location, false, matrix);
	}
	
	public static void setVertexArrayAttrPointer(int program, int vboId, String name, int format, int size) {
		VboUtils.bindArrayBuffer(vboId);
		int loc = GL20.glGetAttribLocation(program, name);	
		GL20.glVertexAttribPointer(loc, size, format, false, 0, 0);
        GL20.glEnableVertexAttribArray(loc);
	}

}
