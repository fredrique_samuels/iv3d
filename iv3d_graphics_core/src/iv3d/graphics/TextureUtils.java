/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import iv3d.base.ImageSource;

import java.nio.ByteBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL30;


public final class TextureUtils {

	public static final int TEXTURE_OPTION_NONE = 0x0000;
	public static final int TEXTURE_OPTION_FILTER = 0x0001;
	public static final int TEXTURE_OPTIONS_ANISOTROPY = 0x0080;
	public static final int TEXTURE_OPTION_CLAMP = 0x0100;
	public static final int TEXTURE_OPTION_MIPMAP = 0x0200;

	private TextureUtils() {
	}

	public static void applyFilterOptions(int options) {
		GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);

		if ((options & TEXTURE_OPTION_CLAMP) != 0) {
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S,
					GL11.GL_CLAMP);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T,
					GL11.GL_CLAMP);
		} else {
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S,
					GL11.GL_REPEAT);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T,
					GL11.GL_REPEAT);
		}

		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER,
				GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER,
				GL11.GL_NEAREST);

		// if ( (options & TEXTURE_ANISOTROPY ) > 0) {
		// glTexParameterf(GL11.GL_TEXTURE_2D, GL31.GL_TEXTURE_MAX_ANISOTROPY,
		// 1.0);
		// }

		if ((options & TEXTURE_OPTION_MIPMAP) > 0) {
			GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
			if ((options & TEXTURE_OPTION_FILTER) > 0) {
				GL11.glTexParameterf(GL11.GL_TEXTURE_2D,
						GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
				GL11.glTexParameterf(GL11.GL_TEXTURE_2D,
						GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);
			}
		} else {
			if ( ( options & TEXTURE_OPTION_FILTER ) > 0) {
				GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
				GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
			}
		}

		GL11.glTexEnvf(GL11.GL_TEXTURE_ENV, GL11.GL_TEXTURE_ENV_MODE, GL11.GL_DECAL);
	}

	public static void bindTexture(int texId) {
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texId);
	}

	public static void bindTexture(int texId, int index) {
		GL13.glActiveTexture(GL13.GL_TEXTURE0 + index);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texId);
	}

	public static int createTexture(ByteBuffer buf, int tWidth, int tHeight,
			int format, int internalFormat, int bytesPerPixel, int options) {
		int texId = GL11.glGenTextures();
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texId);
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, bytesPerPixel, tWidth,
				tHeight, 0, format, internalFormat, buf);
		applyFilterOptions(options);
		return texId;
	}

	public static int createTexture(ImageSource image, int filtecOptions) {
		return createTexture(image.getPixels(), image.getWidth(), image
				.getHeight(), image.getDataFormat(), image.getInternalFormat(),
				image.getBytesPerPixel(), filtecOptions);
	}

	public static void deleteTexture(int t) {
		if (t == 0) {
			return;
		}
		GL11.glDeleteTextures(t);
	}

	public static void updateTexture(int textureId, ImageSource imageSource) {
		bindTexture(textureId);
	    applyFilterOptions(imageSource.getFilterOptions());
	    GL11.glTexSubImage2D(GL11.GL_TEXTURE_2D, 0,0,0, imageSource.getWidth(),
	        imageSource.getHeight(), imageSource.getDataFormat(),
	        imageSource.getInternalFormat(), imageSource.getPixels());
		bindTexture(0);
	}
}
