/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.graphics;

import static org.lwjgl.opengl.ARBBufferObject.GL_DYNAMIC_DRAW_ARB;
import static org.lwjgl.opengl.ARBBufferObject.glBindBufferARB;
import static org.lwjgl.opengl.ARBBufferObject.glBufferDataARB;
import static org.lwjgl.opengl.ARBBufferObject.glDeleteBuffersARB;
import static org.lwjgl.opengl.ARBBufferObject.glGenBuffersARB;
import static org.lwjgl.opengl.ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB;

import org.lwjgl.opengl.GL15;

public final class VboUtils {
	private VboUtils(){}
	
	public static int createStaticVbo(float[] data){
		int vboId = glGenBuffersARB();
		glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboId);
		glBufferDataARB(GL_ARRAY_BUFFER_ARB, GraphicsUtils.asFloatBuffer(data),
				GL_DYNAMIC_DRAW_ARB);
		glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
		return vboId;
	}
	
	public void updateVbo(int vboId, float[] data) { 
		glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboId);
		glBufferDataARB(GL_ARRAY_BUFFER_ARB, GraphicsUtils.asFloatBuffer(data),
				GL_DYNAMIC_DRAW_ARB);
		glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
	}
	
	public static void bindArrayBuffer(int vboId) {
		glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboId);
	}
	
	public static void bindElementBuffer(int vboId) {
		glBindBufferARB(GL15.GL_ELEMENT_ARRAY_BUFFER, vboId);
	}
	
	public static void delete(int vboId) {
		glDeleteBuffersARB(vboId);
	}
	
	public static int createElementVbo(int[] data) {
		int vboId = glGenBuffersARB();
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboId);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, GraphicsUtils.asIntBuffer(data), GL15.GL_STATIC_DRAW);
		glBindBufferARB(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
		return vboId;
	}
}
